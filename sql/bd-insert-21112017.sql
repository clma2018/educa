CREATE TABLE `tabSecao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txtTitulo` varchar(255) NOT NULL,
  `txtResumo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabLicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSecao` int(11) DEFAULT NULL,
  `intOrdem` int(11) DEFAULT NULL,
  `intTipoLicao` int(11) DEFAULT NULL,
  `txtTitulo` varchar(255) NOT NULL,
  `txtResumo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabVideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLicao` int(11) DEFAULT NULL,
  `txtUrl` varchar(255) NOT NULL,
  `txtTempoVideo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabPDF` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLicao` int(11) DEFAULT NULL,
  `txtArquivo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabTextos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLicao` int(11) DEFAULT NULL,
  `txtNome` varchar(255) NOT NULL,
  `txtData` varchar(255) NOT NULL,
  `txtTexto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabStatusLicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLicao` int(11) DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `txtStatus` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tabTempoVideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLicao` int(11) DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `txtTempo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;