<?php
/**
 * template_head_end.php
 *
 * Author: RRaised - JRJ
 *
 */
?>
    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="<?= assets_url('../assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" id="css-main" href="<?= assets_url('../assets/css/oneui.css'); ?>">
    <link rel="stylesheet" id="css-main" href="<?= assets_url('../assets/css/custom-admin-raised.css'); ?>">
    <link rel="stylesheet" href="<?= assets_url('js/plugins/jquery-ui-1.11.4/jquery-ui.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= assets_url('js/plugins/notie/notie.css'); ?>"/>
    <!-- END Stylesheets -->
</head>
<body class="bg-image" style="background-image:url(<?= assets_url(BODY_BG); ?>)">