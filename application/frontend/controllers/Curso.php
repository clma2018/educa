<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Curso extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');
         $this->load->model('user_model');
        $this->load->model('curso_model');

        // Helper
        $this->load->helper('security');


        $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);

        $this->data['qtdSecoes'] = count($this->data['secoes'] = $this->curso_model->get_secoes());
    }

    public function index() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        

        //SECOES
        $this->data['secoes'] = $this->curso_model->get_secoes();

        for ($i=0; $i < count($this->data['secoes']); $i++) { 
            $this->data['status'] =  $this->curso_model->get_status_licoes($this->data['secoes'][$i]->id, $this->session->userdata['user']['id']);
            
            $this->data['secoes'][$i]->porcentagem = $this->data['status'][0]->porcentagem;
            $this->data['secoes'][$i]->idLicao = $this->data['status'][0]->idLicao;
        }


        //LICOES
        $this->data['licoes'] = $this->curso_model->get_licoes();
        
        for ($i=0; $i < count($this->data['licoes']); $i++) { 
            if ($this->data['licoes'][$i]->intTipoLicao == 1) {
                $this->data['tempo'] =  $this->curso_model->get_tempos_video($this->data['licoes'][$i]->id, $this->session->userdata['user']['id']);

                if (isset($this->data['tempo'][0])) {
                    $this->data['licoes'][$i]->porcentagem = $this->data['tempo'][0]->porcentagem;
                }else{
                    $this->data['licoes'][$i]->porcentagem = '';
                }
            }
        }

        $this->template->showSite('list-cursos', $this->data);    

    }

    public function detalhe($idLicao = 0) {

        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['licao'] = $this->curso_model->get_detalhe_licao($this->encrypt->decode($idLicao), $this->session->userdata['user']['id']);

        $this->data['proximo'] = $this->curso_model->get_licao_ordem($this->data['licao'][0]->idSecao, $this->data['licao'][0]->intOrdem+1); 

        $this->data['anterior'] = $this->curso_model->get_licao_ordem($this->data['licao'][0]->idSecao, $this->data['licao'][0]->intOrdem-1);

        $this->data['secoes'] = $this->curso_model->get_secoes();

        // QTD DE LICOES TOTAL E JA REALIZADAS
        for ($i=0; $i < count($this->data['secoes']); $i++) { 
            $this->data['qtdConcluida'] =  $this->curso_model->get_qtd_licoes($this->data['secoes'][$i]->id, $this->session->userdata['user']['id'], 'Concluído');
            $this->data['qtdTotal'] =   $this->curso_model->get_qtd_licoes($this->data['secoes'][$i]->id, $this->session->userdata['user']['id']);
            $this->data['secoes'][$i]->qtdConcluida = $this->data['qtdConcluida'][0]->qtdLicoes; 
            $this->data['secoes'][$i]->qtdTotal = $this->data['qtdTotal'][0]->qtdLicoes; 
        }

        $this->data['secoes'] = array_values($this->data['secoes']);

        $this->template->showSite('curso', $this->data);    

    }

     public function save_time() {
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        $objInsertTime = new stdClass();


        $arrayCondition = array('idUsuario = ' . $this->session->userdata['user']['id'] . ' AND idLicao = '.$objData->idLicao);
        $this->crud_model->delete('tabTempoVideo', $arrayCondition);

        //SALVA AS INFORMACOES DO TEMPO DE VIDEO 
        $objInsertTime->txtTempo = $objData->txtTempo;
        $objInsertTime->idLicao = (int)$objData->idLicao;
        $objInsertTime->idUsuario = (int)$this->session->userdata['user']['id'];
        $this->crud_model->insert('tabTempoVideo', $objInsertTime);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

    public function change_status() {
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        $objUpdateStatus = new stdClass();

        $objUpdateStatus->txtStatus = 'Concluído';
        $arrayCondition = array('idLicao = ' . (int)($objData->id). ' AND idUsuario = '. $this->session->userdata['user']['id']);
      
        $query = $this->crud_model->update($objUpdateStatus, 'tabStatusLicao', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

}
