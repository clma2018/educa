<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Quality extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('normativo_model');
        $this->load->model('base_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $dataInicio = date('Y-m-d', strtotime("-6 days"));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['dataInicio'] = $dataInicio;
        $this->data['dataFim'] = $dataFim;

        $this->data['bases']=  $this->normativo_model->get_bases();
        
        // DataInicial 20170305 DataFinal 20170311
        $this->data['normativos'] = $this->normativo_model->get_normativo_base('', $dataInicio, $dataFim);

        $this->template->showSite('list-normativos-quality', $this->data);    

    }

    public function list_normativos() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        if ($this->session->userdata['user']['txtModelo'] != 2)
            redirect('login', 'refresh');


        $dataInicio = date('Y-m-d', strtotime("-6 days"));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);
        
        $this->data['dataInicio'] = $dataInicio;
        $this->data['dataFim'] = $dataFim;
        
        // DataInicial 20170305 DataFinal 20170311
        $this->data['usuarios'] = $this->user_model->get_users(0, $this->session->userdata['user']['id']);

        $this->data['normativosRecentes'] = $this->normativo_model->get_normativo_base('', $dataInicio, $dataFim);
        $this->data['normativos'] = '';

        $x = 0;
        for($i = 0; $i < count($this->data['normativosRecentes']); $i++){

            if($this->data['normativosRecentes']){
                $qe = 'qe'.$i; 
                $qe = new stdClass();
                $qe = $this->normativo_model->get_normativo_idNorma2($this->data['normativosRecentes'][$i]->idNorma, $this->session->userdata['user']['idInstituicao']);
                if ($qe) {        
                    
                }else{
                    $x++;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosRecentes'][$i]->idNorma;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosRecentes'][$i]->txtOrigem;
                    $this->data['normativos'][$x]['DataPubli'] = $this->data['normativosRecentes'][$i]->DataPubli;
                    $this->data['normativos'][$x]['Titulo'] = $this->data['normativosRecentes'][$i]->Titulo;
                    $this->data['normativos'][$x]['link'] = $this->data['normativosRecentes'][$i]->link;
                    $this->data['normativos'][$x]['Classe'] = $this->data['normativosRecentes'][$i]->Classe;
                    $this->data['normativos'][$x]['Assunto'] = $this->data['normativosRecentes'][$i]->Assunto;
                    $this->data['normativos'][$x]['TipoNorma'] = $this->data['normativosRecentes'][$i]->TipoNorma;
                    $this->data['normativos'][$x]['Numero'] = $this->data['normativosRecentes'][$i]->Numero;
                    $this->data['normativos'][$x]['Revogado'] = $this->data['normativosRecentes'][$i]->Revogado;
                    $this->data['normativos'][$x]['DataEmissao'] = $this->data['normativosRecentes'][$i]->DataEmissao;
                    $this->data['normativos'][$x]['Responsavel'] = $this->data['normativosRecentes'][$i]->Responsavel;
                    $this->data['normativos'][$x]['Grupo'] = $this->data['normativosRecentes'][$i]->Grupo;
                    $this->data['normativos'][$x]['Exclusao'] = $this->data['normativosRecentes'][$i]->Exclusao;

                    $nomeUsuario = '';
                    $idUsuario = '';
                    for ($z=0; $z < count($this->data['usuarios']); $z++) { 
                        $this->data['temas'] = $this->user_model->get_users_tema($this->data['usuarios'][$z]->id);
                        
                        for ($c=0; $c <count($this->data['temas']) ; $c++) { 
                            if ($this->data['normativos'][$x]['Classe'] == $this->data['temas'][$c]->txtTitulo) { 
                                $nomeUsuario .= '/'.$this->data['usuarios'][$z]->txtNome;
                                $idUsuario .= '/'. $this->data['usuarios'][$z]->id;
                                $this->data['normativos'][$x]['txtNomeUser'] = $nomeUsuario;
                                $this->data['normativos'][$x]['txtIdUser'] = $idUsuario;
                            }
                        }
                    }
                }
                unset($qe);
            }
        }   
            
        $this->template->showSite('list-normativos-admin', $this->data);    

    }



     public function save_normativos_quality(){
        $objData = new stdClass();
        $objData = (object)$_POST;

        //Salvar os normativos
        $arr1 = $objData->idNorma; 
        $arr2 = $objData->txtOrigem; 
        $arr3 = $objData->txtTitulo; 
        $arr4 = $objData->txtTipoNorma; 
        $arr5 = $objData->txtNumero; 
        $arr6 = $objData->txtDataPublicacao; 
        $arr7 = $objData->txtRevogado;
        $arr8 = $objData->txtLink;
        $arr9 = $objData->txtDataEmissao;
        $arr10 = $objData->txtAssunto;
        $arr11 = $objData->txtResponsavel;
        $arr12 = $objData->txtGrupo;
        $arr13 = $objData->txtClasse;
        $arr14 = $objData->txtExclusao;
        
        for($i = 0; $i < count($arr1); $i++){
            if($arr1){
                $objUpdateNormativo = new stdClass();
                $objUpdateNormativo->idNorma = $arr1[$i];
                $objUpdateNormativo->txtOrigem = $arr2[$i];
                $objUpdateNormativo->Titulo = $arr3[$i];
                $objUpdateNormativo->TipoNorma = $arr4[$i];
                $objUpdateNormativo->Numero = $arr5[$i];
                $objUpdateNormativo->DataPubli = $arr6[$i];
                $objUpdateNormativo->Revogado = $arr7[$i];
                $objUpdateNormativo->link = $arr8[$i];
                $objUpdateNormativo->DataEmissao = $arr9[$i];
                $objUpdateNormativo->Assunto = $arr10[$i];
                $objUpdateNormativo->Responsavel = $arr11[$i];
                $objUpdateNormativo->Grupo = $arr12[$i];
                $objUpdateNormativo->Classe = $arr13[$i];
                $objUpdateNormativo->Exclusao = $arr14[$i];


                $arrayCondition = array('idNorma = \'' . $objUpdateNormativo->idNorma . '\'');
                $query = $this->crud_model->update($objUpdateNormativo, 'LegalbotN1_cont', $arrayCondition);
                unset($objUpdateNormativo);
            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }


     public function list_normativos_gestao() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $dataInicio = date('Y-m-d', strtotime("-7 days"));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['dataInicio'] = $dataInicio;
        $this->data['dataFim'] = $dataFim;

        $this->data['bases']=  $this->normativo_model->get_bases();

        $this->data['usuarios'] = $this->user_model->get_users(0, 0, $this->session->userdata['user']['idInstituicao']);
        
        // DataInicial 20170305 DataFinal 20170311
        $this->data['normativosRecentes'] = $this->normativo_model->get_normativo_base('', $dataInicio, $dataFim);
        $this->data['normativos'] = '';

        $x = 0;
        for($i = 0; $i < count($this->data['normativosRecentes']); $i++){

            if($this->data['normativosRecentes']){
                $this->data['normativos'][$x]['idNorma'] = $this->data['normativosRecentes'][$i]->idNorma;
                $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosRecentes'][$i]->txtOrigem;
                $this->data['normativos'][$x]['DataPubli'] = $this->data['normativosRecentes'][$i]->DataPubli;
                $this->data['normativos'][$x]['Titulo'] = $this->data['normativosRecentes'][$i]->Titulo;
                $this->data['normativos'][$x]['link'] = $this->data['normativosRecentes'][$i]->link;
                $this->data['normativos'][$x]['Classe'] = $this->data['normativosRecentes'][$i]->Classe;
                $this->data['normativos'][$x]['Assunto'] = $this->data['normativosRecentes'][$i]->Assunto;
                $this->data['normativos'][$x]['TipoNorma'] = $this->data['normativosRecentes'][$i]->TipoNorma;
                $this->data['normativos'][$x]['Numero'] = $this->data['normativosRecentes'][$i]->Numero;
                $this->data['normativos'][$x]['Revogado'] = $this->data['normativosRecentes'][$i]->Revogado;
                $this->data['normativos'][$x]['DataEmissao'] = $this->data['normativosRecentes'][$i]->DataEmissao;
                $this->data['normativos'][$x]['Responsavel'] = $this->data['normativosRecentes'][$i]->Responsavel;
                $this->data['normativos'][$x]['Grupo'] = $this->data['normativosRecentes'][$i]->Grupo;
                $this->data['normativos'][$x]['Exclusao'] = $this->data['normativosRecentes'][$i]->Exclusao;

                $nomeUsuario = '';
                $idUsuario = '';
                for ($z=0; $z < count($this->data['usuarios']); $z++) { 
                    $this->data['normativosUser'] = $this->normativo_model->get_normativo_user($this->data['usuarios'][$z]->id);
                    
                    for ($c=0; $c <count($this->data['normativosUser']) ; $c++) {                 
                        if ($this->data['normativos'][$x]['idNorma'] == $this->data['normativosUser'][$c]->idNormativo) { 
                            $nomeUsuario .= '/'.$this->data['usuarios'][$z]->txtNome;
                            $idUsuario .= '/'. $this->data['usuarios'][$z]->id;
                            $this->data['normativos'][$x]['txtNomeUser'] = $nomeUsuario;
                            $this->data['normativos'][$x]['txtIdUser'] = $idUsuario;
                        }
                    }
                }
                $x++;

            }
        }   

        $this->template->showSite('list-normativos-gestao', $this->data);    

    }

}
