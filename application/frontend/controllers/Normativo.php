<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Normativo extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //Model
        $this->load->model('user_model');
        $this->load->model('base_model');
        $this->load->model('area_model');
        $this->load->model('normativo_model');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {

        $this->template->showSite('dashboard', $this->data);
    }

    public function detalhe($idNormativo =0) {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        $this->load->model('normativo_model');
        $temp = explode('encode_', $idNormativo);
        if (isset($temp[1])) {
            $idNormativo = $this->encrypt->encode($temp[1]);
        }

        
        $this->data['normativo']=  $this->normativo_model->get_normativo($this->encrypt->decode($idNormativo));

        $this->data['avaliacao']=  $this->normativo_model->get_avalicao($this->encrypt->decode($idNormativo));

        $this->data['planosAcao']=  $this->normativo_model->get_planoAcao($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['idInstituicao']);

        $this->data['qtPlanosAcao']=  count($this->normativo_model->get_planoAcao($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['idInstituicao']));

        $this->data['anotacao']=  $this->normativo_model->get_anotacaoNormativo($this->encrypt->decode($idNormativo));

        $this->data['comentario']=  $this->normativo_model->get_normativo_comentario($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['idInstituicao']);

        // $this->data['usuariosEncaminhado']=  $this->normativo_model->get_user_encaminhado($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['id']);

        $this->data['emailsNormativo']=  $this->normativo_model->get_emails_normativo($this->data['normativo'][0]->idNormativo);

        $this->data['comentariosNormativo']=  $this->normativo_model->get_comentariosNormativo($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['idInstituicao']);
        
        $this->data['infoencaminhado']=  $this->normativo_model->get_anotacoes_normativo_encaminhado($this->data['normativo'][0]->idNormativo, $this->session->userdata['user']['id']);

        $idNormas = explode(',', $this->data['normativo'][0]->txtRevogado);

        for ($i=0; $i < count($idNormas); $i++) { 
            $normativosRelacionados[$i] = $this->normativo_model->get_normativo_idNorma($idNormas[$i]);
        }

        for ($i=0; $i < count($normativosRelacionados); $i++) { 
            if(isset($normativosRelacionados[$i][0])){
                $this->data['normativosRelacionados'][$i] = new StdClass;

                $this->data['normativosRelacionados'][$i]->idNorma = $normativosRelacionados[$i][0]->idNorma;
                $this->data['normativosRelacionados'][$i]->txtOrigem = $normativosRelacionados[$i][0]->txtOrigem;
                $this->data['normativosRelacionados'][$i]->DataPubli = $normativosRelacionados[$i][0]->DataPubli;
                $this->data['normativosRelacionados'][$i]->Titulo = $normativosRelacionados[$i][0]->Titulo;
                $this->data['normativosRelacionados'][$i]->link = $normativosRelacionados[$i][0]->link;
                $this->data['normativosRelacionados'][$i]->Classe = $normativosRelacionados[$i][0]->Classe;
                $this->data['normativosRelacionados'][$i]->Assunto = $normativosRelacionados[$i][0]->Assunto;
                $this->data['normativosRelacionados'][$i]->TipoNorma = $normativosRelacionados[$i][0]->TipoNorma;
                $this->data['normativosRelacionados'][$i]->Numero = $normativosRelacionados[$i][0]->Numero;        
                
            }
        }
        
        
        $comentariosNorm = array();
        for ($i=0; $i < count($this->data['comentariosNormativo']); $i++) { 
            $comentariosNorm[$i]['id'] = $this->data['comentariosNormativo'][$i]->id;
            $comentariosNorm[$i]['idUsuario'] = $this->data['comentariosNormativo'][$i]->idUsuario;
            $comentariosNorm[$i]['idComentarioResposta'] = $this->data['comentariosNormativo'][$i]->idComentarioResposta;
            $comentariosNorm[$i]['idNormativo'] = $this->data['comentariosNormativo'][$i]->idNormativo;
            $comentariosNorm[$i]['dateCreate'] = $this->data['comentariosNormativo'][$i]->dateCreate;
            $comentariosNorm[$i]['txtComentario'] = $this->data['comentariosNormativo'][$i]->txtComentario;
            $comentariosNorm[$i]['txtNomeUsuario'] = $this->data['comentariosNormativo'][$i]->txtNomeUsuario;
        }

    
        $references = array();
        $tree = array();
        foreach ($comentariosNorm as $id=> &$node) {

            // Use id as key to make a references to the tree and initialize it with node reference.
            $references[$node['id']] = &$node;

            // Add empty array to hold the children/subcategories
            $node['children'] = array();

            // Get your root node and add this directly to the tree
            if ($node['idComentarioResposta']==0) {
                $tree[$node['id']] = &$node;
            } else {
                // Add the non-root node to its parent's references
                $references[$node['idComentarioResposta']]['children'][$node['id']] = &$node;
            }
        }

        $this->data['estruturaComentarios']=  $tree;
    

        $this->template->showSite('normativo', $this->data);
    }

    public function list_normativos($dataFiltro = '') {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        
        if ($dataFiltro != '') {
            $arr = explode('_', $dataFiltro);
             if ($arr[0] == 'txtRelevancia') {
                $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, $arr[1]);
            }elseif ($arr[0] == 'txtNota') {
                $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],$arr[1]);            
            }
        }else{
            $this->data['normativos']=  $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id']);
        }
        
        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]->txtNota == NULL){
                $this->data['normativos'][$i]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$i]->bitCiente == 1){
                $this->data['normativos'][$i]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$i]->bitCiente == 0) {
                $this->data['normativos'][$i]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$i]->txtRelevancia = 'Não Lido';
            }
        }
        
         
        $this->template->showSite('list-normativos', $this->data);
    }


    public function filtro_normativo(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->txtOrigem) && isset($objData->txtRelevancia) && isset($objData->txtDataInicio)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, $objData->txtRelevancia, 0, $objData->txtOrigem, $objData->txtDataInicio, $objData->txtDataFim);
        }elseif (isset($objData->txtOrigem) && isset($objData->txtRelevancia)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, $objData->txtRelevancia, 0, $objData->txtOrigem);
        }elseif (isset($objData->txtOrigem) && isset($objData->txtDataInicio)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, 3, 0, $objData->txtOrigem, $objData->txtDataInicio, $objData->txtDataFim);
        }elseif (isset($objData->txtRelevancia) && isset($objData->txtDataInicio)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, $objData->txtRelevancia, 0, '', $objData->txtDataInicio, $objData->txtDataFim);
        }elseif (isset($objData->txtRelevancia)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, $objData->txtRelevancia);
        }elseif (isset($objData->txtDataInicio)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, 3, 0, '', $objData->txtDataInicio, $objData->txtDataFim);
        }elseif (isset($objData->txtOrigem)) {
            $this->data['normativos'] = $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id'],90, 3, 0, $objData->txtOrigem);
        }
        
        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]->txtNota == NULL){
                $this->data['normativos'][$i]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$i]->bitCiente == 1){
                $this->data['normativos'][$i]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$i]->bitCiente == 0) {
                $this->data['normativos'][$i]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$i]->txtRelevancia = 'Não Lido';
            }
        }


        echo json_encode(array("normativos" => $this->data['normativos']));
        
    }
   

    public function alter_user_normativo() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateNormativo = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . (int)($objData->id));
        $objUpdateNormativo->bitCiente = $objData->bitCiente;

        $query = $this->crud_model->update($objUpdateNormativo, 'tabnormativousuario', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

    public function alter_user_normativo_prioridade() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertNormativoPrioridade = new stdClass();
        $objData = (object)$_POST;

        //Insercao na tabela de avaliacao
        $objInsertNormativoPrioridade->idUsuario =  $this->session->userdata['user']['id'];
        $objInsertNormativoPrioridade->idNormativoUsuario = $objData->idNormativoUsuario;
        $objInsertNormativoPrioridade->txtNota = $objData->txtNota;
        $query = $this->crud_model->insert('tabavaliacao', $objInsertNormativoPrioridade);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

    public function encaminhar_normativo() {
        if(!$_POST)
            redirect('index', 'refresh');        
        

        $objData = new stdClass();
        $objData = (object)$_POST;
        
        //Array email dos usuarios
        $txtEmailUsuarios[] = '';
        $array = explode(",",$objData->txtEmail);

        for ($i=0; $i < count($array); $i++) { 
            array_push($txtEmailUsuarios,$array[$i]);
        }
        array_shift($txtEmailUsuarios);

        $arrayRegistros = $txtEmailUsuarios;

        $infoNormativo = $this->normativo_model->get_normativo_idNorma($objData->idNormativoUsuario);
        $dataAtual = date('d-m-Y');
        $dataAtual = str_replace('-', '/', $dataAtual);
        
 
        for($i = 0; $i < count($arrayRegistros); $i++){
            if($arrayRegistros){

                //Grava o email inserido 
                $objInsertNormativoEncaminhar = new stdClass();
                $objInsertNormativoEncaminhar->idUsuario =  $this->session->userdata['user']['id'];
                $objInsertNormativoEncaminhar->idNormativoUsuario = $objData->idNormativoUsuario;
                $objInsertNormativoEncaminhar->txtAnotacao = $objData->txtAnotacao;
                $objInsertNormativoEncaminhar->txtEmail = $arrayRegistros[$i];
                $this->crud_model->insert('tabencaminhar',$objInsertNormativoEncaminhar);
    
                // //Disparo do e-mail
                $httpClient = new GuzzleAdapter(new Client());
                $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
                

                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'encaminhar-normativo'],
                    'substitution_data' => 
                            ['txtEmail' => $arrayRegistros[$i],
                            'txtNomeUsuario' => $this->session->userdata['user']['txtNome'],
                            'txtNomeNormativo' => $objData->txtNomeNormativo,
                            'txtLinkNormativo' => $objData->txtLinkNormativo,
                            'txtAnotacao' => $objData->txtAnotacao,
                            'txtEmailUsuario' => $this->session->userdata['user']['txtEmail']],
                    'recipients' => [
                        [
                            'address' => [
                                'email' => $arrayRegistros[$i],
                            ],
                        ],
                    ],
                ]);

                try {
                    $response = $promise->wait();
                } catch (\Exception $e) {
                    
                }   
                unset($objInsertNormativoEncaminhar);
            }
        }

        $objInsertAnotacao = new stdClass();
        $objInsertAnotacao->txtDescricao = 'E-mails: '.$objData->txtEmail;
        $objInsertAnotacao->txtDataVencimento = $dataAtual;
        $objInsertAnotacao->idAdminComentario = '0';
        $objInsertAnotacao->idUsuario =  $this->session->userdata['user']['id'];

        $this->crud_model->insert('tabanotacaousuario',$objInsertAnotacao);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

    public function anotacao_normativo() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertNormativo = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('idNormativo = ' . $objData->idNormativo);
        $query = $this->crud_model->delete('tabanotacaonormativo', $arrayCondition);

        $objInsertNormativo->idNormativo = $objData->idNormativo;
        $objInsertNormativo->txtAnotacao = $objData->txtAnotacao;
        $query2 = $this->crud_model->insert('tabanotacaonormativo', $objInsertNormativo);


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }


    public function new_user_normativo() {

        set_time_limit(0);

        $this->load->model('normativo_model');
    
        $this->data['temasUsuario'] = $this->user_model->get_temas_user($this->session->userdata['user']['id']);    
        $arrayidNormativos = array();
        $arraydateCadastro = array(); 
        $arraytxtTitulos = array(); 
        $arraytxtLinks = array(); 
        $arraytxtClasses = array(); 
        $arraytxtAssuntos = array(); 
        $arraytxtRelevantes = array();
        $arraytxtOrigens = array();
        $arraytxtRevogado = array();
       
        for ($z=0; $z < count($this->data['temasUsuario']); $z++) { 
            $this->data['normativosTemas'] = $this->normativo_model->get_normativos_tema($this->data['temasUsuario'][$z]->txtTitulo);
            for ($x=0; $x < count($this->data['normativosTemas']); $x++) { 
                array_push($arrayidNormativos, $this->data['normativosTemas'][$x]->idNorma);
                array_push($arraydateCadastro, $this->data['normativosTemas'][$x]->DataPubli);
                array_push($arraytxtTitulos, $this->data['normativosTemas'][$x]->Titulo);
                array_push($arraytxtLinks, $this->data['normativosTemas'][$x]->link);
                array_push($arraytxtClasses, $this->data['normativosTemas'][$x]->Classe);
                array_push($arraytxtAssuntos, $this->data['normativosTemas'][$x]->Assunto);
                array_push($arraytxtOrigens, $this->data['normativosTemas'][$x]->txtOrigem);
                array_push($arraytxtRevogado, $this->data['normativosTemas'][$x]->Revogado);
            }
        }

        $valido = 0;    
        for($i = 0; $i < count($arrayidNormativos); $i++){
            $arrData = explode("/", $arraydateCadastro[$i]);
            $newDate = $arrData[2].'-'.$arrData[1].'-'.$arrData[0];
            $today_dt = new DateTime('now');
            $expire_dt = new DateTime($newDate);
            $validade_normativo =  $today_dt->diff($expire_dt, true);
            if($arrayidNormativos){
                if ($validade_normativo->days < 180) {
                    $qe = 'qe'.$i; 
                    $qe = new stdClass();
                    $qe = $this->normativo_model->get_normativo_id($arrayidNormativos[$i], $this->session->userdata['user']['id']);
                    if ($qe) {        
                        $valido = 0;
                    }
                    else{
                        $objInsertNormativo = new stdClass();
                        $objInsertNormativo->idNormativo = $arrayidNormativos[$i];
                        $objInsertNormativo->dateCadastro = $arraydateCadastro[$i];
                        $objInsertNormativo->txtTitulo = $arraytxtTitulos[$i];
                        $objInsertNormativo->txtLink = $arraytxtLinks[$i];
                        $objInsertNormativo->txtClasse = $arraytxtClasses[$i];
                        $objInsertNormativo->txtAssunto = $arraytxtAssuntos[$i];
                        $objInsertNormativo->txtOrigem = $arraytxtOrigens[$i];
                        // $objInsertNormativo->txtRelevante = $arraytxtRelevantes[$i];
                        $objInsertNormativo->txtRelevante = "Sim";
                        $objInsertNormativo->idUsuario = $this->session->userdata['user']['id'];
                        $objInsertNormativo->bitCiente = 2;
                        $objInsertNormativo->txtDataVigencia = $arraydateCadastro[$i];
                        $objInsertNormativo->txtTipoAcao = 1;
                        $objInsertNormativo->txtRevogado = $arraytxtRevogado[$i];

                        $query = $this->crud_model->insert('tabnormativousuario', $objInsertNormativo);
                        unset($objInsertNormativo);
                        $valido = 1;
                    }
                    unset($qe);
                }
            }
        }   
        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'valido'=>$valido));

    }
    

    public function new_area_normativo() {

        $objData = new stdClass();
        $objData = (object)$_POST;

        set_time_limit(0);

        date_default_timezone_set('America/Sao_Paulo');

        $this->load->model('area_model');
        $this->load->model('normativo_model');
           
        $idAreas = $objData->idAreas;
        $arrayidNormativos = array();
        $arraydateCadastro = array(); 
        $arraytxtTitulos = array(); 
        $arraytxtLinks = array(); 
        $arraytxtClasses = array(); 
        $arraytxtAssuntos = array(); 
        $arraytxtRelevantes = array();
        $arraytxtOrigens = array();
        $arraytxtTipoNorma = array();
        $arraytxtNumero = array();
        $arrayidArea = array();
        $arrayidInstituicao = array();

        for ($z=0; $z < count($idAreas); $z++) { 
            $this->data['temasArea'] = $this->area_model->get_temas_area($idAreas[$z]);  
            for ($a=0; $a < count($this->data['temasArea']); $a++) { 
                $this->data['normativosTemas'] = $this->normativo_model->get_normativos_tema($this->data['temasArea'][$a]->txtTitulo);
                for ($x=0; $x < count($this->data['normativosTemas']); $x++) { 
                    array_push($arrayidNormativos, $this->data['normativosTemas'][$x]->idNorma);
                    array_push($arraydateCadastro, $this->data['normativosTemas'][$x]->DataPubli);
                    array_push($arraytxtTitulos, $this->data['normativosTemas'][$x]->Titulo);
                    array_push($arraytxtLinks, $this->data['normativosTemas'][$x]->link);
                    array_push($arraytxtClasses, $this->data['normativosTemas'][$x]->Classe);
                    array_push($arraytxtAssuntos, $this->data['normativosTemas'][$x]->Assunto);
                    array_push($arraytxtOrigens, $this->data['normativosTemas'][$x]->txtOrigem);
                    array_push($arraytxtTipoNorma, $this->data['normativosTemas'][$x]->TipoNorma);
                    array_push($arraytxtNumero, $this->data['normativosTemas'][$x]->Numero);
                    array_push($arrayidArea, $idAreas[$z]);
                    array_push($arrayidInstituicao, $this->data['temasArea'][$a]->idInstituicao);
                }
            }
        }

        for($i = 0; $i < count($arrayidNormativos); $i++){
            $arrData = explode("/", $arraydateCadastro[$i]);
            $newDate = $arrData[2].'-'.$arrData[1].'-'.$arrData[0];
            $today_dt = new DateTime('now');
            $expire_dt = new DateTime($newDate);
            $validade_normativo =  $today_dt->diff($expire_dt, true);

            if($arrayidNormativos){
                if ($validade_normativo->days < 90) {
                    $qe = new stdClass();
                    $qe = $this->normativo_model->get_normativo_idArea($arrayidNormativos[$i], $arrayidArea[$i], array('1','2','4'));
                    if ($qe) {        
                 
                 
                    }
                    else{
                        $objInsertNormativo = new stdClass();
                        $objInsertNormativo->idNormativo = $arrayidNormativos[$i];
                        $objInsertNormativo->dateCadastro = $arraydateCadastro[$i];
                        $objInsertNormativo->txtTitulo = $arraytxtTitulos[$i];
                        $objInsertNormativo->txtLink = $arraytxtLinks[$i];
                        $objInsertNormativo->txtClasse = $arraytxtClasses[$i];
                        $objInsertNormativo->txtAssunto = $arraytxtAssuntos[$i];
                        $objInsertNormativo->txtOrigem = $arraytxtOrigens[$i];
                        $objInsertNormativo->txtRelevante = "Sim";
                        $objInsertNormativo->idArea = $arrayidArea[$i];
                        $objInsertNormativo->bitCiente = 2;
                        $objInsertNormativo->txtTipoNorma = $arraytxtTipoNorma[$i];
                        $objInsertNormativo->txtNumero = $arraytxtNumero[$i];
                        $objInsertNormativo->idInstituicao = $arrayidInstituicao[$i];

                        $query = $this->crud_model->insert('tabnormativoarea', $objInsertNormativo);
                        unset($objInsertNormativo);
                    }
                    unset($qe);
                }
            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

    public function desativar_normativo() {
       
        $objData = new stdClass();
        $objUpdateNormativo = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . (int)($objData->idNormativo));
        $objUpdateNormativo->bitCiente = 3;
        $query = $this->crud_model->update($objUpdateNormativo, 'tabnormativoarea', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }
    
    public function disparar_normativo(){

        $this->load->model('area_model');

        $arraytxtEmails = array();
        $arrayidAreaEmail = array();

        $this->data['emailsArea'] =  $this->area_model->get_emails_area(0, $this->session->userdata['user']['idInstituicao']);
        
        for ($z=0; $z < count($this->data['emailsArea']); $z++) { 
            array_push($arraytxtEmails, $this->data['emailsArea'][$z]->txtEmail);
            array_push($arrayidAreaEmail, $this->data['emailsArea'][$z]->idArea);
        }


        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
        
        for($i = 0; $i < count($arraytxtEmails); $i++){
            $this->data['normativoArea'] = $this->normativo_model->get_normativo_idArea('', $arrayidAreaEmail[$i], array('2'));  
    
            $normativos[$i] = ''; 
            for($n = 0; $n < count($this->data['normativoArea']); $n++){
                $comentarioGestor[$i] = '';
                if (strlen($this->data['normativoArea'][$n]->txtComentario) > 10) {
                    $comentarioGestor[$i] .= '<p style="margin-top:15px;font-family:Ubuntu, sans-serif;font-size:12px;line-height:17px;color: #404040;"><span style="font-weight: bold;font-size:18px;line-height:25px;color: #4383ae;">Comentário Gestor:</span><br>'.$this->data['normativoArea'][$n]->txtComentario.'</p>';
                };

                $normativos[$i] .=
                    '<hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;">'.
                    '<p style="font-family:Ubuntu, sans-serif;font-size:21px;line-height:23px;color: #404040;font-weight: 600;margin:15px 0px 0px!important;">'.$this->data['normativoArea'][$n]->txtTitulo.'</p>'.
                    '<p style="font-family:Ubuntu, sans-serif;font-size:16px;line-height:18x;color: #404040;font-weight: 600;margin-top: 10px;margin-bottom:10px;">'.$this->data['normativoArea'][$n]->txtClasse.'</p>'.
                    '<p style="font-family:Ubuntu, sans-serif;font-size:12px;line-height:17px;color: #404040;">'.$this->data['normativoArea'][$n]->txtAssunto.'</p>'.
                    $comentarioGestor[$i].   
                    '<a href="'.$this->data['normativoArea'][$n]->txtLink.'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;width: 27%;font-family: Helvetica, Arial, sans-serif;font-weight: bold;">Acessar Normativo</a>'
                ;
               
                $objInsertLog = new stdClass();

                $objInsertLog->txtEmail =  $arraytxtEmails[$i];
                $objInsertLog->idNormativo =  $this->data['normativoArea'][$n]->id;
                $objInsertLog->idInstituicao = $this->session->userdata['user']['idInstituicao'];
                $this->crud_model->insert('tablogemailnormativo',$objInsertLog);
            };

            if (isset($this->data['normativoArea'][0]->txtLink)) {
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'enviar-normativo'],
                    'substitution_data' => [
                        'txtEmail' => $arraytxtEmails[$i],
                        'txtNormativos' => $normativos[$i],
                    ],
                    'recipients' => [
                        [
                            'address' => [
                                'email' => $arraytxtEmails[$i],
                            ],
                        ],
                    ],
                ]);

                try {
                    $response = $promise->wait();

                } catch (\Exception $e) {
                    // die(var_dump($e));
                }
            }
   
        }

        $this->data['normativoArea2'] = $this->normativo_model->get_normativo_idArea('', 0, array('2'), $this->session->userdata['user']['idInstituicao']);

        for($n = 0; $n < count($this->data['normativoArea2']); $n++){

            $objUpdateNormativo = new stdClass();
            $arrayCondition = array('id = ' . (int)($this->data['normativoArea2'][$n]->id));
            $objUpdateNormativo->bitCiente = 4;
            $query = $this->crud_model->update($objUpdateNormativo, 'tabnormativoarea', $arrayCondition);
            
        };

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));     
    
    }

    public function sugerir_tema() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertTema = new stdClass();
        $objData = (object)$_POST;

        //Insercao na tabela de sugestao
        $objInsertTema->idNormativo =  $objData->idNormativo;
        $objInsertTema->txtTema = $objData->txtTema;
        $objInsertTema->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $query = $this->crud_model->insert('tabsugestaotema', $objInsertTema);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

    public function encaminhar_normativo_area() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateNormativo = new stdClass();
        $objInsertNormativo = new stdClass();
        $objData = (object)$_POST;


        //Salva o dados do normativo
        if ($objData->idNormativoArea != '') {
            $this->data['normativoArea'] = $this->normativo_model->get_normativo_idArea($objData->idNormativo);              
            $objInsertNormativo->idArea = $objData->idArea;
            $objInsertNormativo->idNormativo = $this->data['normativoArea'][0]->idNormativo;
            $objInsertNormativo->dateCadastro = $this->data['normativoArea'][0]->dateCadastro;
            $objInsertNormativo->txtTitulo = $this->data['normativoArea'][0]->txtTitulo;
            $objInsertNormativo->txtLink = $this->data['normativoArea'][0]->txtLink;
            $objInsertNormativo->txtClasse = $this->data['normativoArea'][0]->txtClasse;
            $objInsertNormativo->txtAssunto = $this->data['normativoArea'][0]->txtAssunto;
            $objInsertNormativo->txtRelevante = $this->data['normativoArea'][0]->txtRelevante;
            $objInsertNormativo->txtOrigem = $this->data['normativoArea'][0]->txtOrigem;
            $objInsertNormativo->txtTipoNorma = $this->data['normativoArea'][0]->txtTipoNorma;
            $objInsertNormativo->txtNumero = $this->data['normativoArea'][0]->txtNumero;
            $objInsertNormativo->bitCiente = 2;
            $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        }else{
            $this->data['normativoArea'] = $this->normativo_model->get_normativo_idNorma($objData->idNormativo);              
            $objInsertNormativo->idArea = $objData->idArea;
            $objInsertNormativo->idNormativo = $this->data['normativoArea'][0]->idNorma;
            $objInsertNormativo->dateCadastro = $this->data['normativoArea'][0]->DataPubli;
            $objInsertNormativo->txtTitulo = $this->data['normativoArea'][0]->Titulo;
            $objInsertNormativo->txtLink = $this->data['normativoArea'][0]->link;
            $objInsertNormativo->txtClasse = $this->data['normativoArea'][0]->Classe;
            $objInsertNormativo->txtAssunto = $this->data['normativoArea'][0]->Assunto;
            $objInsertNormativo->txtRelevante = "Sim";
            $objInsertNormativo->txtOrigem = $this->data['normativoArea'][0]->txtOrigem;
            $objInsertNormativo->txtTipoNorma = $this->data['normativoArea'][0]->TipoNorma;
            $objInsertNormativo->txtNumero = $this->data['normativoArea'][0]->Numero;
            $objInsertNormativo->bitCiente = 2;
            $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        }

        $query = $this->crud_model->insert('tabnormativoarea', $objInsertNormativo);

        if ($objData->txtRemover == 1) {
            $arrayCondition = array('id = ' . (int)($objData->idNormativoArea));
            $objUpdateNormativo->bitCiente = 3;
            $query = $this->crud_model->update($objUpdateNormativo, 'tabnormativoarea', $arrayCondition);
            $removido = 1;
        }else{
            $removido = 0;
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'status'=>$removido));
    }


    public function exportar_excel(){

        $idArea =  $this->uri->segment(3);
        $dataInicio =  $this->uri->segment(4);
        $dataFim =  $this->uri->segment(5);
        $txtBases =  $this->uri->segment(6);
        // $idEnviado =  $this->uri->segment(7);
        if (isset($txtBases)) {
            $txtBases = explode(",", $txtBases);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        // if ($idEnviado == 1) {
            // $normativosArea = $this->normativo_model->get_normativo_export2($txtClasses);  
        // }else{
            $normativosArea = $this->normativo_model->get_normativo_export($idArea, $dataInicio, $dataFim, $txtBases);
        // }

        $dadosFormatado = array();
        foreach ($normativosArea as $key => $normativo) {
            $dados = new stdClass();
            $dados->Origem = $normativo->txtOrigem;
            $dados->Area = $normativo->txtArea;
            $dados->Titulo = $normativo->txtTitulo;
            $dados->DataPublicacao = $normativo->dateCadastro;
            $dados->TipoNorma = $normativo->txtTipoNorma;
            $dados->Numero = $normativo->txtNumero;
            // $dados->Classe = $normativo->txtClasse;
            $dados->Assunto = $normativo->txtAssunto;
            $dados->Link = $normativo->txtLink;
            array_push($dadosFormatado, $dados);
        }

        
        $this->excel->stream('Relatorio-normativos.xls', $dadosFormatado); 

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }


    public function filtro_normativo_base(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['bases'] = $this->normativo_model->get_bases();
        $dataInicio = new DateTime("first day of ".$objData->txtMes."");
        $dataInicio = str_replace('-', '', $dataInicio->format('Y-m-d'));
        $dataFim = new DateTime("last day of ".$objData->txtMes."");
        $dataFim = str_replace('-', '', $dataFim->format('Y-m-d'));
        

        if ($objData->txtArea !== '') {   
            $this->data['normativosBase'] = $this->normativo_model->get_normativo_base_area($objData->txtArea, $dataInicio, $dataFim);

            for ($i=0; $i < count($this->data['normativosBase']); $i++) {
                $this->data['normativosBase'][$i]->qtdNormativos = count($this->normativo_model->get_normativo_base_area($objData->txtArea, $dataInicio, $dataFim, $this->data['normativosBase'][$i]->txtOrigem));
            }
            
        }else{
            $this->data['normativosBase'] = $this->normativo_model->get_normativo_base('',$dataInicio, $dataFim);
            
            for ($i=0; $i < count($this->data['normativosBase']); $i++) { 
               $this->data['normativosBase'][$i]->qtdNormativos = count($this->normativo_model->get_normativo_base($this->data['normativosBase'][$i]->txtOrigem, $dataInicio, $dataFim));
            } 


        }
          
        echo json_encode(array("normativos" => $this->data['normativosBase']));
        
    }

    public function save_normativos(){
        $objData = new stdClass();
        $objData = (object)$_POST;


        //Salvar os normativos
        $arr1 = $objData->idNorma; 
        $arr2 = $objData->txtOrigem; 
        $arr3 = $objData->txtTitulo; 
        $arr4 = $objData->txtTipoNorma; 
        $arr5 = $objData->txtNumero; 
        $arr6 = $objData->txtDataPublicacao; 
        $arr7 = $objData->txtRevogado;
        $arr8 = $objData->txtLink;
        $arr9 = $objData->txtDataEmissao;
        $arr10 = $objData->txtAssunto;
        $arr11 = $objData->txtResponsavel;
        $arr12 = $objData->txtGrupo;
        $arr13 = $objData->txtClasse;
        $arr14 = $objData->txtExclusao;

        
        for($i = 0; $i < count($arr1); $i++){
            if($arr1){
                $qe = 'qe'.$i; 
                $qe = new stdClass();
                $qe = $this->normativo_model->get_normativo_idNorma($arr1[$i]);
                if ($qe) {        
                    
                }else{
                    $objInsertNormativo = new stdClass();
                    $objInsertNormativo->idNorma = $arr1[$i];
                    $objInsertNormativo->txtOrigem = $arr2[$i];
                    $objInsertNormativo->Titulo = $arr3[$i];
                    $objInsertNormativo->TipoNorma = $arr4[$i];
                    $objInsertNormativo->Numero = $arr5[$i];
                    $objInsertNormativo->DataPubli = $arr6[$i];
                    $objInsertNormativo->Revogado = $arr7[$i];
                    $objInsertNormativo->link = $arr8[$i];
                    $objInsertNormativo->DataEmissao = $arr9[$i];
                    $objInsertNormativo->Assunto = $arr10[$i];
                    $objInsertNormativo->Responsavel = $arr11[$i];
                    $objInsertNormativo->Grupo = $arr12[$i];
                    $objInsertNormativo->Classe = $arr13[$i];
                    $objInsertNormativo->Exclusao = $arr14[$i];
                    $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];

                    $query = $this->crud_model->insert('tabnormativos', $objInsertNormativo);
                    unset($objInsertNormativo);
                }
                unset($qe);
            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }


    public function save_comentario(){
         if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertNormativo = new stdClass();
        $objData = (object)$_POST;


        $objInsertNormativo->idNorma = $objData->idNorma;
        $objInsertNormativo->txtComentario = $objData->txtComentario;
        $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $query2 = $this->crud_model->insert('tabcomentarionormativo', $objInsertNormativo);

        if (isset($objData->idAnotacao)) {
            $objInsertAnotacaoEnviada = new stdClass();
            $objInsertAnotacaoEnviada->idAnotacao = $objData->idAnotacao;
            $objInsertAnotacaoEnviada->idUsuario = $this->session->userdata['user']['id'];
            $query2 = $this->crud_model->insert('tabanotacoesenviadasBPO', $objInsertAnotacaoEnviada);
        }


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }


      public function save_normativos_usuario(){
        $objData = new stdClass();
        $objData = (object)$_POST;


        //Salvar os normativos
        $arr1 = $objData->idNorma; 
        $arr2 = $objData->txtOrigem; 
        $arr3 = $objData->txtTitulo; 
        $arr4 = $objData->txtTipoNorma; 
        $arr5 = $objData->txtNumero; 
        $arr6 = $objData->txtDataPublicacao; 
        $arr7 = $objData->txtRevogado;
        $arr8 = $objData->txtLink;
        $arr9 = $objData->txtDataEmissao;
        $arr10 = $objData->txtAssunto;
        $arr11 = $objData->txtResponsavel;
        $arr12 = $objData->txtGrupo;
        $arr13 = $objData->txtClasse;
        $arr14 = $objData->txtExclusao;
        $arr15 = $objData->txtFlag;
        $arr16 = $objData->idUsuarios;
        $arr17 = $objData->txtDataVigencia;
        
        for($i = 0; $i < count($arr1); $i++){
            if($arr1){
                $objInsertNormativo = new stdClass();
                $objInsertNormativo->idNorma = $arr1[$i];
                $objInsertNormativo->txtOrigem = $arr2[$i];
                $objInsertNormativo->Titulo = $arr3[$i];
                $objInsertNormativo->TipoNorma = $arr4[$i];
                $objInsertNormativo->Numero = $arr5[$i];
                $objInsertNormativo->DataPubli = $arr6[$i];
                $objInsertNormativo->Revogado = $arr7[$i];
                $objInsertNormativo->link = $arr8[$i];
                $objInsertNormativo->DataEmissao = $arr9[$i];
                $objInsertNormativo->Assunto = $arr10[$i];
                $objInsertNormativo->Responsavel = $arr11[$i];
                $objInsertNormativo->Grupo = $arr12[$i];
                $objInsertNormativo->Classe = $arr13[$i];
                $objInsertNormativo->Exclusao = $arr14[$i];
                $objInsertNormativo->txtDataVigencia = $arr17[$i];
                $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];

                $query = $this->crud_model->insert('tabnormativos', $objInsertNormativo);
                unset($objInsertNormativo);
            }
            $arrayIdUsuario[$i] = explode("/",$arr16[$i]);
            $arrayFlags[$i] = explode("/",$arr15[$i]);

            for($x = 0; $x < count($arrayIdUsuario[$i]); $x++){

                $acaoNorm = 0;
                if (isset($arrayFlags[$i][$x])) {
                    $idUser =   explode(":",$arrayFlags[$i][$x]);
                    if ($idUser[0] = $arrayIdUsuario[$i][$x]) {
                        if ($idUser[1] == 1) {
                            $acaoNorm = 1;
                        }
                    }
                }else{
                    $acaoNorm = 1;
                }

                if ($arrayIdUsuario[$i][$x]) {
                    $objInsertNormativoUser = new stdClass();
                    $objInsertNormativoUser->idUsuario = $arrayIdUsuario[$i][$x];
                    $objInsertNormativoUser->idNormativo = $arr1[$i];
                    $objInsertNormativoUser->txtOrigem = $arr2[$i];
                    $objInsertNormativoUser->txtTitulo = $arr3[$i];
                    $objInsertNormativoUser->dateCadastro = $arr6[$i];
                    $objInsertNormativoUser->txtLink = $arr8[$i];
                    $objInsertNormativoUser->txtAssunto = $arr10[$i];
                    $objInsertNormativoUser->txtRelevante = "Sim";
                    $objInsertNormativoUser->bitCiente = 2;
                    $objInsertNormativoUser->txtClasse = $arr13[$i];
                    $objInsertNormativoUser->txtDataVigencia = $arr17[$i];
                    $objInsertNormativoUser->txtTipoAcao = $acaoNorm;
                    $objInsertNormativoUser->txtRevogado = $arr7[$i];
                    
                    $query2 = $this->crud_model->insert('tabnormativousuario', $objInsertNormativoUser);
                }
                
                unset($objInsertNormativoUser);

            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

     public function alter_user_normativo_planoAcao() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertNormativoPlanoAcao = new stdClass();
        $objData = (object)$_POST;

        //Insercao na tabela de plano de acao
        $objInsertNormativoPlanoAcao->idUsuario =  $this->session->userdata['user']['id'];
        $objInsertNormativoPlanoAcao->idNormativo = $objData->idNormativo;
        $objInsertNormativoPlanoAcao->txtDataVencimento = $objData->txtDataVencimento;
        $objInsertNormativoPlanoAcao->txtDescricao = $objData->txtDescricao;
        $objInsertNormativoPlanoAcao->txtStatus = 'Ativo';
        $objInsertNormativoPlanoAcao->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        
        $this->crud_model->insert('tabplanoacao', $objInsertNormativoPlanoAcao);


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

    public function mudar_status() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateNormativoPlanoAcao = new stdClass();
        $objData = (object)$_POST;


        $arrayCondition = array('id = ' . (int)($objData->id));
        $objUpdateNormativoPlanoAcao->txtStatus = $objData->txtStatus;

        $query = $this->crud_model->update($objUpdateNormativoPlanoAcao, 'tabplanoacao', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }


     public function notificacao_planoAcao(){

        $day = date('w');
        $dataInicio = date('Y-m-d', strtotime('-'.($day).' days'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('+'.(2+$day).' days'));
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativosPlanoAcao'] = $this->normativo_model->get_planoAcao_atrasado($dataInicio, $dataFim);        
    
        $idAdministrador = [];
        $txtEmailAdmin = [];
        $txtNomeAdmin = [];
        $txtNomeUsuario = [];
        $txtTituloNormativo = [];
        $dataVencimentoPA = [];
        $txtLink = [];

        for($i = 0; $i < count($this->data['normativosPlanoAcao']); $i++){
            $this->data['usuarioAdmin'] = $this->user_model->get_users($this->data['normativosPlanoAcao'][$i]->idAdministrador);

            array_push($idAdministrador, $this->data['usuarioAdmin'][0]->id);
            array_push($txtNomeUsuario, $this->data['normativosPlanoAcao'][$i]->txtNome);
            array_push($txtTituloNormativo, $this->data['normativosPlanoAcao'][$i]->Titulo);
            array_push($dataVencimentoPA, $this->data['normativosPlanoAcao'][$i]->txtDataVencimento);
            array_push($txtLink, $this->config->base_url().'normativo/visualizar_normativo/'.$this->encrypt->encode($this->data['normativosPlanoAcao'][$i]->idNorma));
            array_push($txtEmailAdmin, $this->data['usuarioAdmin'][0]->txtEmail);
            array_push($txtNomeAdmin, $this->data['usuarioAdmin'][0]->txtNome);
        };
 
        
        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);

        if (count($idAdministrador) > 0) {
            $normativos = '';
            
            for ($i=0; $i < count($idAdministrador); $i++) { 
                $normativos .= '<table align="left" style="width:100%;margin-bottom:20px;"><tr><td colspan="2"><hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;"></td></tr><tr><td align="left" valign="top" colspan="2" style="padding: 10px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:22px;line-height:23px;color: #404040;font-weight: 600;margin:0px 0px 15px!important;">'.$txtTituloNormativo[$i].'</p></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;margin: 0px"> Responsável: '.$txtNomeUsuario[$i].'</p></td><td align="center"><a href="'.$txtLink[$i].'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;font-family: Helvetica, Arial, sans-serif;font-weight: bold;display: block;width: 150px;margin:auto;position:relative;top:10px;">Acessar Normativo</a></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px" colspan="2"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;font-weight: 600;margin-top: 0px">Data de Vencimento: '.$dataVencimentoPA[$i].'</p></td></tr></table>';
            }            
                
            $arrEmails = array_unique($txtEmailAdmin);
            $arrNomes = array_unique($txtNomeAdmin);

            for ($i=0; $i <count($arrEmails) ; $i++) { 
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'plano-acao-vencimento1dia'],
                    'substitution_data' => [
                        'txtNomeAdmin' => $arrNomes[$i],
                        'txtNormativos' => $normativos
                    ],
                    'recipients' => [
                        [
                           'address' => [
                                'email' => $arrEmails[$i],
                            ],
                        ],
                    ],
                ]);
            }


            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                // echo $e->getCode()."\n";
                // echo $e->getMessage()."\n";
            }

        }
   

        echo json_encode(array("msg" => 'success', 'validate'=>true));     
         
    }

    public function visualizar_normativo($idNormativo= '') {

        $this->data['normativo']=  $this->normativo_model->get_normativo_id($this->encrypt->decode($idNormativo)); 

        $this->data['planosAcao']=  $this->normativo_model->get_planoAcao_id($this->data['normativo'][0]->idNormativo);

        $this->data['qtPlanosAcao']=  count($this->normativo_model->get_planoAcao_id($this->data['normativo'][0]->idNormativo));

        if ($this->encrypt->decode($idNormativo)) {    
            $this->template->showSite('normativo-visualizacao', $this->data);
        }else{
            redirect('login', 'refresh');
        }

    }

    public function text_analytics() {

        $this->data['bases']=  $this->normativo_model->get_bases();
     
        $this->template->showSite('text_analytics', $this->data);
    }


    public function list_planos_user($id = '') {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->load->model('user_model');
        $this->load->model('normativo_model');
        
        
        $this->data['planosAcao']=  $this->normativo_model->get_planoAcao_user($this->session->userdata['user']['id']);

         
        $this->template->showSite('list-planos-user', $this->data);
    }
    

    public function alterar_status_planoAcao(){

        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativosPlanoAcao'] = $this->normativo_model->get_planoAcao_atrasado($dataFim, $dataFim);        

        for($i = 0; $i < count($this->data['normativosPlanoAcao']); $i++){
            $objUpdatePA = new stdClass();
            $arrayCondition = array('id = ' . (int)($this->data['normativosPlanoAcao'][$i]->id));
            $objUpdatePA->txtStatus = 'Atrasado Data de Vencimento';

            $query = $this->crud_model->update($objUpdatePA, 'tabplanoacao', $arrayCondition);
        };
 
   
        echo json_encode(array("msg" => 'success', 'validate'=>true));     
         
    }

    public function salvar_comentario(){
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertComentario = new stdClass();
        $objData = (object)$_POST;

        $objInsertComentario->idUsuario =  $this->session->userdata['user']['id'];
        $objInsertComentario->idNormativo = $objData->idNormativo;
        $objInsertComentario->idComentarioResposta = $objData->idComentarioResposta;
        $objInsertComentario->txtComentario = $objData->txtComentario;
        $objInsertComentario->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $query = $this->crud_model->insert('tabcomentarios', $objInsertComentario);

        $query->dateCreate = date('Y-m-d H:i:s');
    
        echo json_encode(array("msg" => 'success', "comentario" => $query, 'validate'=>true));     
         
    }


    public function filtro_normativo_analytics(){
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['normativosBase'] = $this->normativo_model->get_normativo_base($objData->txtBase, $objData->dataInicio, $objData->dataFim);

        echo json_encode(array("normativos" => $this->data['normativosBase']));
        
    }


     public function filtro_normativos_dataQuality(){

        set_time_limit(0);

        $objData = new stdClass();
        $objData = (object)$_POST;

        if ($objData->idNorma != '') {
            $this->data['normativos'] = $this->normativo_model->get_normativos_N1($objData->idNorma);
        }elseif ($objData->txtClasse != '') {
            $this->data['normativos'] = $this->normativo_model->get_normativos_N1('',$objData->txtOrigem, $objData->txtClasse, $objData->txtDataInicio, $objData->txtDataFim);
        }elseif (($objData->txtDataFim != '') && ($objData->txtDataInicio != '')) {
            if (($objData->txtOrigem != '') && ($objData->txtClasse != '')) {
                $this->data['normativos'] = $this->normativo_model->get_normativos_N1('',$objData->txtOrigem,$objData->txtClasse, $objData->txtDataInicio, $objData->txtDataFim);
            }elseif ($objData->txtOrigem != '') {
                $this->data['normativos'] = $this->normativo_model->get_normativos_N1('',$objData->txtOrigem,'', $objData->txtDataInicio, $objData->txtDataFim);
            }else{
                $this->data['normativos'] = $this->normativo_model->get_normativos_N1('', '', '', $objData->txtDataInicio, $objData->txtDataFim);
            }
        }


        $this->data['usuarios'] = $this->user_model->get_users(0, $this->session->userdata['user']['id']);

        $this->data['normativos2'] = '';
        $x = 0;
        for($i = 0; $i < count($this->data['normativos']); $i++){

            if($this->data['normativos']){
                $x++;
                $this->data['normativos2'][$x]['idNorma'] = $this->data['normativos'][$i]->idNorma;
                $this->data['normativos2'][$x]['txtOrigem'] = $this->data['normativos'][$i]->txtOrigem;
                $this->data['normativos2'][$x]['DataPubli'] = $this->data['normativos'][$i]->DataPubli;
                $this->data['normativos2'][$x]['Titulo'] = $this->data['normativos'][$i]->Titulo;
                $this->data['normativos2'][$x]['link'] = $this->data['normativos'][$i]->link;
                $this->data['normativos2'][$x]['Classe'] = $this->data['normativos'][$i]->Classe;
                $this->data['normativos2'][$x]['Assunto'] = $this->data['normativos'][$i]->Assunto;
                $this->data['normativos2'][$x]['TipoNorma'] = $this->data['normativos'][$i]->TipoNorma;
                $this->data['normativos2'][$x]['Numero'] = $this->data['normativos'][$i]->Numero;
                $this->data['normativos2'][$x]['Revogado'] = $this->data['normativos'][$i]->Revogado;
                $this->data['normativos2'][$x]['DataEmissao'] = $this->data['normativos'][$i]->DataEmissao;
                $this->data['normativos2'][$x]['Responsavel'] = $this->data['normativos'][$i]->Responsavel;
                $this->data['normativos2'][$x]['Grupo'] = $this->data['normativos'][$i]->Grupo;
                $this->data['normativos2'][$x]['Exclusao'] = $this->data['normativos'][$i]->Exclusao;

                $idUsuario = '';
                $list = '';
                $this->data['normativos2'][$x]['listUser'] = '';
                for ($z=0; $z < count($this->data['usuarios']); $z++) { 
                    $this->data['normativosUser'] = $this->normativo_model->get_normativo_user($this->data['usuarios'][$z]->id);
                    
                    for ($c=0; $c <count($this->data['normativosUser']) ; $c++) {                 
                        if ($this->data['normativos2'][$x]['idNorma'] == $this->data['normativosUser'][$c]->idNormativo) { 
                            $list .= '<li class="remove-user"><a href="#" data-idUser="/'.$this->data['usuarios'][$z]->id.'">'.$this->data['usuarios'][$z]->txtNome.'</a></li>';
                            $idUsuario .= '/'. $this->data['usuarios'][$z]->id;
                            $this->data['normativos2'][$x]['txtIdUser'] = $idUsuario;
                            $this->data['normativos2'][$x]['listUser'] = $list;
                        }
                    }
                    
                }

            }
        }   


        echo json_encode(array("normativos" => $this->data['normativos'], "normativos2" => $this->data['normativos2'], "qtdNormativos" => count($this->data['normativos2'])));
        
    }

    public function filtrar_temas_base(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['temas'] = $this->normativo_model->get_bases_temas($objData->txtBase);

        echo json_encode(array("temas" => $this->data['temas']));
        
    }

    public  function notificacao_normativo(){

        $day = date('w');
        $dataInicio = date('Y-m-d', strtotime('-'.(7-$day).' days'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('+'.($day).' days'));
        $dataFim = str_replace('-', '', $dataFim);


        $this->data['normativos'] = $this->normativo_model->get_normativos_semana($dataInicio, $dataFim);        

        $txtNomeUsuario = [];
        $txtOrigem = [];
        $txtEmailUsuario = [];
        $txtTituloNormativo = [];
        $dataPubli = [];
        $txtLink = [];

        for($i = 0; $i < count($this->data['normativos']); $i++){
            array_push($txtNomeUsuario, $this->data['normativos'][$i]->txtNome);
            array_push($txtEmailUsuario, $this->data['normativos'][$i]->txtEmail);
            array_push($txtTituloNormativo, $this->data['normativos'][$i]->txtTitulo);
            array_push($dataPubli, $this->data['normativos'][$i]->dateCadastro);
            array_push($txtOrigem, $this->data['normativos'][$i]->txtOrigem);
            array_push($txtLink, $this->data['normativos'][$i]->txtLink);
        };

        
        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);

        if (count($txtNomeUsuario) > 0) {
            $normativos = '';
            
            for ($i=0; $i < count($txtNomeUsuario); $i++) { 
                $normativos .= '<table align="left" style="width:100%;margin-bottom:0px;"><tr><td colspan="2"><hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;"></td></tr><tr><td align="left" valign="top" colspan="2" style="padding: 10px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:22px;line-height:23px;color: #404040;font-weight: 600;margin:0px 0px 15px!important;">'.$txtTituloNormativo[$i].'</p></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;margin: 0px;font-weight: 600;"> Origem: '.$txtOrigem[$i].'</p></td><td align="center"><a href="'.$txtLink[$i].'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;font-family: Helvetica, Arial, sans-serif;font-weight: bold;display: block;width: 150px;margin:auto;position:relative;top:10px;">Acessar Normativo</a></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px" colspan="2"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;font-weight: 600;margin-top: 0px">Data de Publicação: '.$dataPubli[$i].'</p></td></tr></table>';
            }            
                
            $arrEmails = array_values(array_unique($txtEmailUsuario));
            $arrNomes = array_values(array_unique($txtNomeUsuario));

            
            for ($i=0; $i <count($arrEmails) ; $i++) { 
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'normativos-semana'],
                    'substitution_data' => [
                        'txtNomeAdmin' => $arrNomes[$i],
                        'txtNormativos' => $normativos
                    ],
                    'recipients' => [
                        [
                           'address' => [
                                'email' => $arrEmails[$i],
                            ],
                        ],
                    ],
                ]);
            }


            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                // echo $e->getCode()."\n";
                // echo $e->getMessage()."\n";
            }

        }
   

        echo json_encode(array("msg" => 'success', 'validate'=>true));    


    }

    public function visualizar_normativo_user($idNormativo= '') {

        $this->data['normativo']=  $this->normativo_model->get_normativo_id($this->encrypt->decode($idNormativo));
            
        $this->data['planosAcao']=  $this->normativo_model->get_planoAcao($this->encrypt->decode($idNormativo), $this->session->userdata['user']['idInstituicao']);

        if ($this->encrypt->decode($idNormativo)) {    
            $this->template->showSite('normativo-visualizacao-user', $this->data);
        }else{
            redirect('login', 'refresh');
        }

    }

    public function filtrar_classificao_base(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['classeBase'] = $this->normativo_model->get_bases_temas($objData->txtBase);

        $this->data['grupoBase'] = $this->normativo_model->get_bases_grupos($objData->txtBase);

        echo json_encode(array("grupoBase" => $this->data['grupoBase'], "classeBase" => $this->data['classeBase']));
        
    }

    public function filtro_normativos_BPO(){

        $this->load->model('anotacao_model');

        $objData = new stdClass();
        $objData = (object)$_POST;
        if (($objData->txtDataFim != '') && ($objData->txtDataInicio != '')) {
            if ($objData->txtOrigem != '') {
                $this->data['normativosFiltro'] = $this->normativo_model->get_normativos_N1('',$objData->txtOrigem,'', $objData->txtDataInicio, $objData->txtDataFim);
            }else{
                $this->data['normativosFiltro'] = $this->normativo_model->get_normativos_N1('', '', '', $objData->txtDataInicio, $objData->txtDataFim);
            }
        }

        $x = 0;
        for($i = 0; $i < count($this->data['normativosFiltro']); $i++){

            if($this->data['normativosFiltro']){
                $qe = 'qe'.$i; 
                $qe = new stdClass();
                $qe = $this->anotacao_model->get_anotacoes_bpo($this->data['normativosFiltro'][$i]->idNorma, $this->session->userdata['user']['idInstituicao']);
                if ($qe) {        
                    
                }else{
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosFiltro'][$i]->idNorma;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosFiltro'][$i]->txtOrigem;
                    $this->data['normativos'][$x]['DataPubli'] = $this->data['normativosFiltro'][$i]->DataPubli;
                    $this->data['normativos'][$x]['Titulo'] = $this->data['normativosFiltro'][$i]->Titulo;
                    $this->data['normativos'][$x]['link'] = $this->data['normativosFiltro'][$i]->link;
                    $this->data['normativos'][$x]['Classe'] = $this->data['normativosFiltro'][$i]->Classe;
                    $this->data['normativos'][$x]['Assunto'] = $this->data['normativosFiltro'][$i]->Assunto;
                    $this->data['normativos'][$x]['TipoNorma'] = $this->data['normativosFiltro'][$i]->TipoNorma;
                    $this->data['normativos'][$x]['Numero'] = $this->data['normativosFiltro'][$i]->Numero;
                    $this->data['normativos'][$x]['Revogado'] = $this->data['normativosFiltro'][$i]->Revogado;
                    $this->data['normativos'][$x]['DataEmissao'] = $this->data['normativosFiltro'][$i]->DataEmissao;
                    $this->data['normativos'][$x]['Responsavel'] = $this->data['normativosFiltro'][$i]->Responsavel;
                    $this->data['normativos'][$x]['Grupo'] = $this->data['normativosFiltro'][$i]->Grupo;
                    $this->data['normativos'][$x]['Exclusao'] = $this->data['normativosFiltro'][$i]->Exclusao;
                    $x++;

                }
                unset($qe);
            }
        }   
    
        echo json_encode(array("normativos" => $this->data['normativos'], "qtdNormativos" => count($this->data['normativos'])));    
    }


    public function add_normativo_user(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->idNormativoInstituicao)) {
            $this->data['normativos'] = $this->normativo_model->get_normativos_instituicao_idNorma($objData->idNormaAtual);
        }else{
            $this->data['normativos'] = $this->normativo_model->get_normativo_idNorma($objData->idNormaAtual);
        }
        
        $array = explode('/', $objData->idUsuarios);

        $dataHoje = date('Y-m-d');        
        $dataHoje = explode('-', $dataHoje);
        $dataHoje = $dataHoje[2].'/'.$dataHoje[1].'/'.$dataHoje[0];

        $this->data['usuarios'] = '';
        $z= 0;

        for ($i=1; $i < count($array); $i++) { 
            $objInsertNormativo = new stdClass();
            $objInsertLogNormativo = new stdClass();
            if (isset($objData->idNormativoInstituicao)) {
                $objInsertNormativo->idNormativo = $this->data['normativos'][0]->idNorma;
                $objInsertNormativo->dateCadastro = $dataHoje;
                $objInsertNormativo->txtTitulo = $this->data['normativos'][0]->txtTitulo;
                $objInsertNormativo->txtLink = $this->data['normativos'][0]->txtLink;
                $objInsertNormativo->txtClasse = $this->data['normativos'][0]->txtClasse;
                $objInsertNormativo->txtAssunto = $this->data['normativos'][0]->txtAssunto;
                $objInsertNormativo->txtOrigem = $this->data['normativos'][0]->txtOrigem;
                $objInsertNormativo->txtRelevante = "Sim";
                $objInsertNormativo->idUsuario = $array[$i];
                $objInsertNormativo->bitCiente = 2;
                $objInsertNormativo->txtDataVigencia = $this->data['normativos'][0]->txtDataPubli;
                $objInsertNormativo->txtTipoAcao = 1;
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->bitNormativoHistorico = 1;
            }else{
                $objInsertNormativo->idNormativo = $this->data['normativos'][0]->idNorma;
                $objInsertNormativo->dateCadastro = $dataHoje;
                $objInsertNormativo->txtTitulo = $this->data['normativos'][0]->Titulo;
                $objInsertNormativo->txtLink = $this->data['normativos'][0]->link;
                $objInsertNormativo->txtClasse = $this->data['normativos'][0]->Classe;
                $objInsertNormativo->txtAssunto = $this->data['normativos'][0]->Assunto;
                $objInsertNormativo->txtOrigem = $this->data['normativos'][0]->txtOrigem;
                $objInsertNormativo->txtRelevante = "Sim";
                $objInsertNormativo->idUsuario = $array[$i];
                $objInsertNormativo->bitCiente = 2;
                $objInsertNormativo->txtDataVigencia = $this->data['normativos'][0]->DataPubli;
                $objInsertNormativo->txtTipoAcao = 1;
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->bitNormativoHistorico = 1;
            }
            
            $query = $this->crud_model->insert('tabnormativousuario', $objInsertNormativo);

            $objInsertLogNormativo->idNormativo = $query->id;
            $objInsertLogNormativo->idUsuario = $array[$i];

            $query2 = $this->crud_model->insert('tabnormativohistorico', $objInsertLogNormativo);

            $this->data['usuario'] = $this->user_model->get_users($array[$i]);

            for ($ai=0; $ai < count($this->data['usuario']); $ai++) { 
                $this->data['usuarios'][$z]['id'] = $this->data['usuario'][0]->id;
                $this->data['usuarios'][$z]['txtNome'] = $this->data['usuario'][0]->txtNome;
                $z++;
            }

            unset($objInsertNormativo);
            unset($objInsertLogNormativo);
        }
       
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'usuarios' => $this->data['usuarios']));
        
    }


    public function list_normativos_instituicao() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['areaTemas'] = $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao']);

        $this->data['bases']=  $this->normativo_model->get_bases();

        $this->data['usuarios'] = $this->user_model->get_users(0, $this->session->userdata['user']['id']);
        
        $this->data['normativosRecentes'] = $this->normativo_model->get_normativos_instituicao($this->session->userdata['user']['idInstituicao']);

        $this->data['normativos'] = '';

        $x = 0;
        for($i = 0; $i < count($this->data['normativosRecentes']); $i++){

            if($this->data['normativosRecentes']){
                $this->data['normativos'][$x]['idNorma'] = $this->data['normativosRecentes'][$i]->idNorma;
                $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosRecentes'][$i]->txtOrigem;
                $this->data['normativos'][$x]['DataPubli'] = $this->data['normativosRecentes'][$i]->txtDataPubli;
                $this->data['normativos'][$x]['Titulo'] = $this->data['normativosRecentes'][$i]->txtTitulo;
                $this->data['normativos'][$x]['link'] = $this->data['normativosRecentes'][$i]->txtLink;
                $this->data['normativos'][$x]['Classe'] = $this->data['normativosRecentes'][$i]->txtClasse;
                $this->data['normativos'][$x]['Assunto'] = $this->data['normativosRecentes'][$i]->txtAssunto;
                $this->data['normativos'][$x]['TipoNorma'] = $this->data['normativosRecentes'][$i]->txtTipoNorma;
                $this->data['normativos'][$x]['Numero'] = $this->data['normativosRecentes'][$i]->txtNumero;
                $this->data['normativos'][$x]['Revogado'] = $this->data['normativosRecentes'][$i]->txtRevogado;
                $this->data['normativos'][$x]['DataEmissao'] = $this->data['normativosRecentes'][$i]->txtDataEmissao;
                $this->data['normativos'][$x]['Responsavel'] = $this->data['normativosRecentes'][$i]->txtResponsavel;
                $this->data['normativos'][$x]['Grupo'] = $this->data['normativosRecentes'][$i]->txtGrupo;
                $this->data['normativos'][$x]['Exclusao'] = $this->data['normativosRecentes'][$i]->txtExclusao;

                $nomeUsuario = '';
                $idUsuario = '';
                for ($z=0; $z < count($this->data['usuarios']); $z++) { 
                    $this->data['normativosUser'] = $this->normativo_model->get_normativo_user($this->data['usuarios'][$z]->id);
                    
                    for ($c=0; $c <count($this->data['normativosUser']) ; $c++) {                 
                        if ($this->data['normativos'][$x]['idNorma'] == $this->data['normativosUser'][$c]->idNormativo) { 
                            $nomeUsuario .= '/'.$this->data['usuarios'][$z]->txtNome;
                            $idUsuario .= '/'. $this->data['usuarios'][$z]->id;
                            $this->data['normativos'][$x]['txtNomeUser'] = $nomeUsuario;
                            $this->data['normativos'][$x]['txtIdUser'] = $idUsuario;
                        }
                    }
                }
                $x++;

            }
        }   


        $this->template->showSite('list-normativos-instituicao', $this->data);
    }


    public function salvar_normativo_instituicao(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $objInsertNormativo = new stdClass();

        //REGRA
        $origem = substr($objData->txtOrigem, 0, 3);
        $tipoNorma = substr($objData->txtTipoNorma, 0, 3);
        $data = explode('/', $objData->txtDataPubli);

        //3 letras da ORIGEM + 3 letras do TipoNorma + WW + ANO da DataPubli (2000) + 0 + Numero
        $objInsertNormativo->idNorma = strtoupper($origem).strtoupper($tipoNorma).'WW'.$data[2].'0'.$objData->txtNumero;

        $objInsertNormativo->idUsuario = $this->session->userdata['user']['id'];
        $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $objInsertNormativo->txtOrigem = $objData->txtOrigem;
        $objInsertNormativo->txtTitulo = $objData->txtTitulo;
        $objInsertNormativo->txtTipoNorma = $objData->txtTipoNorma;
        $objInsertNormativo->txtNumero = $objData->txtNumero;
        $objInsertNormativo->txtDataPubli = $objData->txtDataPubli;
        $objInsertNormativo->txtRevogado = '|*|';
        $objInsertNormativo->txtLink = $objData->txtLink;
        $objInsertNormativo->txtDataEmissao = $objData->txtDataPubli;
        $objInsertNormativo->txtAssunto = $objData->txtAssunto;
        $objInsertNormativo->txtResponsavel = $objData->txtResponsavel;
        $objInsertNormativo->txtGrupo = $objData->txtGrupo;
        $objInsertNormativo->txtClasse = $objData->txtClasse;
        $objInsertNormativo->txtExclusao = '|*|';

        $query = $this->crud_model->insert('tabnormativosinstituicao', $objInsertNormativo);
        unset($objInsertNormativo);


       echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }


    public function normativos_area() {
        if (!isset($this->session->userdata['user']))
                redirect('login', 'refresh');
            if ($this->session->userdata['user']['idAdministrador'] != 0 )
                redirect('dashboard', 'refresh');

        $this->data['areasTemas']=  $this->area_model->get_temas_area(0,$this->session->userdata['user']['idInstituicao']);

        $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao']);
        $this->data['normativos'] = '';
        $this->data['normativos2'] = '';

        $txtTemasArea = array();
        for ($i=0; $i < count($this->data['areasTemas']); $i++) { 
            $txtTemasArea += [ $this->data['areasTemas'][$i]->txtTitulo => 1 ];
        }

        $idNormativos = array();
        $posicao = array();

        $x = 0;
        $x2 = 0;
        for ($b=0; $b < count($this->data['normativosInstituicao']); $b++) {
            if (array_key_exists($this->data['normativosInstituicao'][$b]->txtClasse, $txtTemasArea)) {
                //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
                if (!array_key_exists($this->data['normativosInstituicao'][$b]->idNormativo, $idNormativos)) {
                    $this->data['normativos'][$x]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                    $this->data['normativos'][$x]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                    $this->data['normativos'][$x]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                    $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                    $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['normativosInstituicao'][$b]->qtdPlanosAcao;
                    $this->data['normativos'][$x]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;
                    $this->data['normativos'][$x]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;

                    $R[$x] = 0;
                    $NR[$x] = 0;
                    $NL[$x] = 0;
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$x]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$x]++;
                    }else{
                        $NL[$x]++;
                    }

                    $this->data['normativos'][$x]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$x]."</li><li>Não Relevante: ".$NR[$x]." </li><li>Não Lido: ".$NL[$x]."</li></ul>";
                    
                    $idNormativos += [ $this->data['normativosInstituicao'][$b]->idNormativo => $x ];
                    $posicao += [ $x => $this->data['normativosInstituicao'][$b]->idNormativo ];
                    $x++;
                }else{
                    $key = array_search($this->data['normativosInstituicao'][$b]->idNormativo, $posicao);
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$key]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$key]++;
                    }else{
                        $NL[$key]++;
                    }
                    $this->data['normativos'][$key]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$key]."</li><li>Não Relevante: ".$NR[$key]." </li><li>Não Lido: ".$NL[$key]."</li></ul>";
                    
                }

                //VARIAVEL AUXILIAR
                $this->data['normativos2'][$x2]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                $this->data['normativos2'][$x2]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                $this->data['normativos2'][$x2]['bitCiente'] = $this->data['normativosInstituicao'][$b]->bitCiente;
                $this->data['normativos2'][$x2]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                $this->data['normativos2'][$x2]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                $this->data['normativos2'][$x2]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                $this->data['normativos2'][$x2]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                $this->data['normativos2'][$x2]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                $this->data['normativos2'][$x2]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                $this->data['normativos2'][$x2]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                $this->data['normativos2'][$x2]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                $this->data['normativos2'][$x2]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;
                $this->data['normativos2'][$x2]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;

                $x2++;
            }
        }


        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        $indiceOrigem = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceOrigem += [ $i => $this->data['normativos'][$i]['txtOrigem']];
        }

        $indiceOrigem = array_values(array_unique($indiceOrigem));

        $this->data['nomesBase'] = $indiceOrigem;
        
        for ($i=0; $i < count($indiceOrigem); $i++) { 
            $this->data['origem'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtOrigem'] == $indiceOrigem[$i]) {
                    $this->data['origem'.$i.'']++;
                }
            }       
            for ($a=0; $a < count($this->data['bases']); $a++) { 
                if ($this->data['bases'][$a]->txtNome == $indiceOrigem[$i]) {
                    $this->data['cor'.$i.''] = $this->data['bases'][$i]->txtCorPrimaria;
                }
            }   
        }


        //AREAS
        $indiceArea = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceArea += [ $i => $this->data['normativos'][$i]['txtArea']];
        }

        $indiceArea = array_values(array_unique($indiceArea));

        $this->data['nomesAreas'] = $indiceArea;
        
        for ($i=0; $i < count($indiceArea); $i++) { 
            $this->data['area'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtArea'] == $indiceArea[$i]) {
                    $this->data['area'.$i.'']++;
                }
            }   
        }

        //STATUS DO NORMATIVO
        $this->data['relevante0'] = 0;
        $this->data['relevante1'] = 0;
        $this->data['relevante2'] = 0;

        for ($x=0; $x < count($this->data['normativos2']); $x++) {
            if($this->data['normativos2'][$x]['bitCiente'] == 1){
                $this->data['relevante1']++;
            }elseif ($this->data['normativos2'][$x]['bitCiente'] == 0) {
                $this->data['relevante2']++;
            }else{
                $this->data['relevante0']++;
            }
        }   

        //PLANOS DE ACAO (STATUS)
        $this->data['status1'] = 0;
        $this->data['status2'] = 0;
        $this->data['status3'] = 0;
        $this->data['status4'] = 0;
        $this->data['status5'] = 0;
        $this->data['status6'] = 0;

        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]['txtStatus'] == 'Concluído'){
                $this->data['status1']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Cancelado') {
                $this->data['status2']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Ativo') {
                $this->data['status3']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vigência') {
                $this->data['status4']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vencimento') {
                $this->data['status5']++;
            }else{
                $this->data['status6']++;
            }
        } 
         
        $this->template->showSite('normativos-area', $this->data);
    }

    public function filtro_normativos_grafico_origem(){

        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['areasTemas']=  $this->area_model->get_temas_area(0,$this->session->userdata['user']['idInstituicao']);
        
        $objData->txtOrigem = rtrim($objData->txtOrigem, ",");
        $txtBases = explode(',', $objData->txtOrigem);

        $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao'], $txtBases);
        

        $this->data['normativos'] = '';
        $this->data['normativos2'] = '';

        $txtTemasArea = array();
        for ($i=0; $i < count($this->data['areasTemas']); $i++) { 
            $txtTemasArea += [ $this->data['areasTemas'][$i]->txtTitulo => 1 ];
        }

        $idNormativos = array();
        $posicao = array();

        $x = 0;
        $x2 = 0;
        for ($b=0; $b < count($this->data['normativosInstituicao']); $b++) {
            if (array_key_exists($this->data['normativosInstituicao'][$b]->txtClasse, $txtTemasArea)) {
                //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
                if (!array_key_exists($this->data['normativosInstituicao'][$b]->idNormativo, $idNormativos)) {
                    $this->data['normativos'][$x]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                    $this->data['normativos'][$x]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                    $this->data['normativos'][$x]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                    $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                    $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['normativosInstituicao'][$b]->qtdPlanosAcao;
                    $this->data['normativos'][$x]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;
                    $this->data['normativos'][$x]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;

                    $R[$x] = 0;
                    $NR[$x] = 0;
                    $NL[$x] = 0;
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$x]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$x]++;
                    }else{
                        $NL[$x]++;
                    }

                    $this->data['normativos'][$x]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$x]."</li><li>Não Relevante: ".$NR[$x]." </li><li>Não Lido: ".$NL[$x]."</li></ul>";
                    
                    $idNormativos += [ $this->data['normativosInstituicao'][$b]->idNormativo => $x ];
                    $posicao += [ $x => $this->data['normativosInstituicao'][$b]->idNormativo ];
                    $x++;
                }else{
                    $key = array_search($this->data['normativosInstituicao'][$b]->idNormativo, $posicao);
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$key]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$key]++;
                    }else{
                        $NL[$key]++;
                    }
                    $this->data['normativos'][$key]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$key]."</li><li>Não Relevante: ".$NR[$key]." </li><li>Não Lido: ".$NL[$key]."</li></ul>";
                    
                }

                //VARIAVEL AUXILIAR
                $this->data['normativos2'][$x2]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                $this->data['normativos2'][$x2]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                $this->data['normativos2'][$x2]['bitCiente'] = $this->data['normativosInstituicao'][$b]->bitCiente;
                $this->data['normativos2'][$x2]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                $this->data['normativos2'][$x2]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                $this->data['normativos2'][$x2]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                $this->data['normativos2'][$x2]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                $this->data['normativos2'][$x2]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                $this->data['normativos2'][$x2]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                $this->data['normativos2'][$x2]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                $this->data['normativos2'][$x2]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                $this->data['normativos2'][$x2]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;
                $this->data['normativos2'][$x2]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;

                $x2++;
            }
        }
        

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        $indiceOrigem = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceOrigem += [ $i => $this->data['normativos'][$i]['txtOrigem']];
        }

        $indiceOrigem = array_values(array_unique($indiceOrigem));

        $this->data['nomesBase'] = array();
        $this->data['corBases'] = array();
        $this->data['infoBases'] = array();

        for ($i=0; $i < count($indiceOrigem); $i++) { 
            $this->data['origem'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtOrigem'] == $indiceOrigem[$i]) {
                    $this->data['origem'.$i.'']++;
                }
            }       
            for ($a=0; $a < count($this->data['bases']); $a++) { 
                if ($this->data['bases'][$a]->txtNome == $indiceOrigem[$i]) {
                    array_push($this->data['nomesBase'], $this->data['bases'][$a]->txtNome);
                    array_push($this->data['corBases'], $this->data['bases'][$a]->txtCorPrimaria);
                }
            }   
        }
        for ($i=0; $i < count($this->data['nomesBase']); $i++) { 
            array_push($this->data['infoBases'], $this->data['origem'.$i.'']);
        }

        //AREAS
        $indiceArea = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceArea += [ $i => $this->data['normativos'][$i]['txtArea']];
        }

        $indiceArea = array_values(array_unique($indiceArea));

        $this->data['nomesAreas'] = $indiceArea;
        
        for ($i=0; $i < count($indiceArea); $i++) { 
            $this->data['area'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtArea'] == $indiceArea[$i]) {
                    $this->data['area'.$i.'']++;
                }
            }   
        }

        //QTD POR AREA
        $this->data['dataInfo'] = array();
        for ($i=0; $i < count($this->data['nomesAreas']); $i++) { 
            array_push($this->data['dataInfo'], $this->data['area'.$i.'']);
        }

        //NOME AREAS
        $this->data['labels'] = array();
        for ($x=0; $x < count($this->data['nomesAreas']); $x++) {    
            array_push($this->data['labels'], $this->data['nomesAreas'][$x]);
        };


        //STATUS DO NORMATIVO
        $this->data['relevante0'] = 0;
        $this->data['relevante1'] = 0;
        $this->data['relevante2'] = 0;

        for ($x=0; $x < count($this->data['normativos2']); $x++) {
            if($this->data['normativos2'][$x]['bitCiente'] == 1){
                $this->data['relevante1']++;
            }elseif ($this->data['normativos2'][$x]['bitCiente'] == 0) {
                $this->data['relevante2']++;
            }else{
                $this->data['relevante0']++;
            }
        }

        $this->data['statusNormativo'] = array();        
        for ($x=0; $x < 3; $x++) {    
            array_push($this->data['statusNormativo'], $this->data['relevante'.$x.'']);
        };


        //PLANOS DE ACAO (STATUS)
        $this->data['status1'] = 0;
        $this->data['status2'] = 0;
        $this->data['status3'] = 0;
        $this->data['status4'] = 0;
        $this->data['status5'] = 0;
        $this->data['status6'] = 0;

        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]['txtStatus'] == 'Concluído'){
                $this->data['status1']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Cancelado') {
                $this->data['status2']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Ativo') {
                $this->data['status3']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vigência') {
                $this->data['status4']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vencimento') {
                $this->data['status5']++;
            }else{
                $this->data['status6']++;
            }
        } 

        $this->data['statusPA'] = array();
        for ($x=1; $x < 7; $x++) {    
            array_push($this->data['statusPA'], $this->data['status'.$x.'']);
        };

        echo json_encode(array("normativos" => $this->data['normativos'], "dataInfo" => $this->data['dataInfo'], "labels" => $this->data['labels'], "statusNormativo" => $this->data['statusNormativo'], "statusPA" => $this->data['statusPA'], "nomesBase" => $this->data['nomesBase'], "corBases" => $this->data['corBases'], "infoBases" => $this->data['infoBases'] ));    
    }

    public function filtro_normativos_grafico_area(){

        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['areasTemas']=  $this->area_model->get_temas_area(0,$this->session->userdata['user']['idInstituicao']);
        
        //ORIGEM
        $objData->txtOrigem = rtrim($objData->txtOrigem, ",");
        $txtBases = explode(',', $objData->txtOrigem);

        //AREA
        $objData->txtArea = rtrim($objData->txtArea, ",");
        $txtArea = explode(',', $objData->txtArea);

        $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao'], $txtBases, $txtArea);
        

        $this->data['normativos'] = '';
        $this->data['normativos2'] = '';

        $txtTemasArea = array();
        for ($i=0; $i < count($this->data['areasTemas']); $i++) { 
            $txtTemasArea += [ $this->data['areasTemas'][$i]->txtTitulo => 1 ];
        }

        $idNormativos = array();
        $posicao = array();

        $x = 0;
        $x2 = 0;
        for ($b=0; $b < count($this->data['normativosInstituicao']); $b++) {
            if (array_key_exists($this->data['normativosInstituicao'][$b]->txtClasse, $txtTemasArea)) {
                //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
                if (!array_key_exists($this->data['normativosInstituicao'][$b]->idNormativo, $idNormativos)) {
                    $this->data['normativos'][$x]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                    $this->data['normativos'][$x]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                    $this->data['normativos'][$x]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                    $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                    $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['normativosInstituicao'][$b]->qtdPlanosAcao;
                    $this->data['normativos'][$x]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;
                    $this->data['normativos'][$x]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;

                    $R[$x] = 0;
                    $NR[$x] = 0;
                    $NL[$x] = 0;
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$x]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$x]++;
                    }else{
                        $NL[$x]++;
                    }

                    $this->data['normativos'][$x]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$x]."</li><li>Não Relevante: ".$NR[$x]." </li><li>Não Lido: ".$NL[$x]."</li></ul>";
                    
                    $idNormativos += [ $this->data['normativosInstituicao'][$b]->idNormativo => $x ];
                    $posicao += [ $x => $this->data['normativosInstituicao'][$b]->idNormativo ];
                    $x++;
                }else{
                    $key = array_search($this->data['normativosInstituicao'][$b]->idNormativo, $posicao);
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$key]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$key]++;
                    }else{
                        $NL[$key]++;
                    }
                    $this->data['normativos'][$key]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$key]."</li><li>Não Relevante: ".$NR[$key]." </li><li>Não Lido: ".$NL[$key]."</li></ul>";
                    
                }

                //VARIAVEL AUXILIAR
                $this->data['normativos2'][$x2]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                $this->data['normativos2'][$x2]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                $this->data['normativos2'][$x2]['bitCiente'] = $this->data['normativosInstituicao'][$b]->bitCiente;
                $this->data['normativos2'][$x2]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                $this->data['normativos2'][$x2]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                $this->data['normativos2'][$x2]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                $this->data['normativos2'][$x2]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                $this->data['normativos2'][$x2]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                $this->data['normativos2'][$x2]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                $this->data['normativos2'][$x2]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                $this->data['normativos2'][$x2]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                $this->data['normativos2'][$x2]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;
                $this->data['normativos2'][$x2]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;

                $x2++;
            }
        }
 

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        $indiceOrigem = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceOrigem += [ $i => $this->data['normativos'][$i]['txtOrigem']];
        }

        $indiceOrigem = array_values(array_unique($indiceOrigem));

        $this->data['nomesBase'] = array();
        $this->data['corBases'] = array();
        $this->data['infoBases'] = array();

        
        for ($i=0; $i < count($indiceOrigem); $i++) { 
            $this->data['origem'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtOrigem'] == $indiceOrigem[$i]) {
                    $this->data['origem'.$i.'']++;
                }
            }       
            for ($a=0; $a < count($this->data['bases']); $a++) { 
                if ($this->data['bases'][$a]->txtNome == $indiceOrigem[$i]) {
                    array_push($this->data['nomesBase'], $this->data['bases'][$a]->txtNome);
                    array_push($this->data['corBases'], $this->data['bases'][$a]->txtCorPrimaria);
                }
            }   
        }
        for ($i=0; $i < count($this->data['nomesBase']); $i++) { 
            array_push($this->data['infoBases'], $this->data['origem'.$i.'']);
        }

        //AREAS
        $indiceArea = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceArea += [ $i => $this->data['normativos'][$i]['txtArea']];
        }

        $indiceArea = array_values(array_unique($indiceArea));

        $this->data['nomesAreas'] = $indiceArea;
        
        for ($i=0; $i < count($indiceArea); $i++) { 
            $this->data['area'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtArea'] == $indiceArea[$i]) {
                    $this->data['area'.$i.'']++;
                }
            }   
        }

        //QTD POR AREA
        $this->data['dataInfo'] = array();
        for ($i=0; $i < count($this->data['nomesAreas']); $i++) { 
            array_push($this->data['dataInfo'], $this->data['area'.$i.'']);
        }

        //NOME AREAS
        $this->data['labels'] = array();
        for ($x=0; $x < count($this->data['nomesAreas']); $x++) {    
            array_push($this->data['labels'], $this->data['nomesAreas'][$x]);
        };


        //STATUS DO NORMATIVO
        $this->data['relevante0'] = 0;
        $this->data['relevante1'] = 0;
        $this->data['relevante2'] = 0;

        for ($x=0; $x < count($this->data['normativos2']); $x++) {
            if($this->data['normativos2'][$x]['bitCiente'] == 1){
                $this->data['relevante1']++;
            }elseif ($this->data['normativos2'][$x]['bitCiente'] == 0) {
                $this->data['relevante2']++;
            }else{
                $this->data['relevante0']++;
            }
        }

        $this->data['statusNormativo'] = array();        
        for ($x=0; $x < 3; $x++) {    
            array_push($this->data['statusNormativo'], $this->data['relevante'.$x.'']);
        };


        //PLANOS DE ACAO (STATUS)
        $this->data['status1'] = 0;
        $this->data['status2'] = 0;
        $this->data['status3'] = 0;
        $this->data['status4'] = 0;
        $this->data['status5'] = 0;
        $this->data['status6'] = 0;

        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]['txtStatus'] == 'Concluído'){
                $this->data['status1']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Cancelado') {
                $this->data['status2']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Ativo') {
                $this->data['status3']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vigência') {
                $this->data['status4']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vencimento') {
                $this->data['status5']++;
            }else{
                $this->data['status6']++;
            }
        } 

        $this->data['statusPA'] = array();
        for ($x=1; $x < 7; $x++) {    
            array_push($this->data['statusPA'], $this->data['status'.$x.'']);
        };

        echo json_encode(array("normativos" => $this->data['normativos'], "dataInfo" => $this->data['dataInfo'], "labels" => $this->data['labels'], "statusNormativo" => $this->data['statusNormativo'], "statusPA" => $this->data['statusPA'], "nomesBase" => $this->data['nomesBase'], "corBases" => $this->data['corBases'], "infoBases" => $this->data['infoBases'] ));        
    }

    public function filtro_normativos_grafico_status(){

        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['areasTemas']=  $this->area_model->get_temas_area(0,$this->session->userdata['user']['idInstituicao']);
        
        //ORIGEM
        $objData->txtOrigem = rtrim($objData->txtOrigem, ",");
        $txtBases = explode(',', $objData->txtOrigem);

        //AREA
        $objData->txtArea = rtrim($objData->txtArea, ",");
        $txtArea = explode(',', $objData->txtArea);

        //STATUS
        $arrayStatus = explode(',', $objData->txtStatus);
        $txtStatus = array();
        for ($z=0; $z < count($arrayStatus); $z++) {
            if($arrayStatus[$z] == 'Não lidos'){
                array_push($txtStatus, 2);
            }
            elseif($arrayStatus[$z] == 'Relevantes'){
                array_push($txtStatus, 1);
            }elseif ($arrayStatus[$z] == 'Não Relevantes') {
                array_push($txtStatus, 0);
            }
        }
        if (count($txtStatus) > 0) {
            $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao'], $txtBases, $txtArea, $txtStatus);
        }else{
            $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao'], $txtBases, $txtArea, '');
        }


        $this->data['normativos'] = '';
        $this->data['normativos2'] = '';

        $txtTemasArea = array();
        for ($i=0; $i < count($this->data['areasTemas']); $i++) { 
            $txtTemasArea += [ $this->data['areasTemas'][$i]->txtTitulo => 1 ];
        }

        $idNormativos = array();
        $posicao = array();

        $x = 0;
        $x2 = 0;
        for ($b=0; $b < count($this->data['normativosInstituicao']); $b++) {
            if (array_key_exists($this->data['normativosInstituicao'][$b]->txtClasse, $txtTemasArea)) {
                //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
                if (!array_key_exists($this->data['normativosInstituicao'][$b]->idNormativo, $idNormativos)) {
                    $this->data['normativos'][$x]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                    $this->data['normativos'][$x]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                    $this->data['normativos'][$x]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                    $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                    $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['normativosInstituicao'][$b]->qtdPlanosAcao;
                    $this->data['normativos'][$x]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;
                    $this->data['normativos'][$x]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;

                    $R[$x] = 0;
                    $NR[$x] = 0;
                    $NL[$x] = 0;
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$x]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$x]++;
                    }else{
                        $NL[$x]++;
                    }

                    $this->data['normativos'][$x]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$x]."</li><li>Não Relevante: ".$NR[$x]." </li><li>Não Lido: ".$NL[$x]."</li></ul>";
                    
                    $idNormativos += [ $this->data['normativosInstituicao'][$b]->idNormativo => $x ];
                    $posicao += [ $x => $this->data['normativosInstituicao'][$b]->idNormativo ];
                    $x++;
                }else{
                    $key = array_search($this->data['normativosInstituicao'][$b]->idNormativo, $posicao);
                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $R[$key]++;
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $NR[$key]++;
                    }else{
                        $NL[$key]++;
                    }
                    $this->data['normativos'][$key]['statusNormativo'] = "<ul class='list-statusNormativo'><li>Relevante: ".$R[$key]."</li><li>Não Relevante: ".$NR[$key]." </li><li>Não Lido: ".$NL[$key]."</li></ul>";
                    
                }

                //VARIAVEL AUXILIAR
                $this->data['normativos2'][$x2]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                $this->data['normativos2'][$x2]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                $this->data['normativos2'][$x2]['bitCiente'] = $this->data['normativosInstituicao'][$b]->bitCiente;
                $this->data['normativos2'][$x2]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                $this->data['normativos2'][$x2]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                $this->data['normativos2'][$x2]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                $this->data['normativos2'][$x2]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                $this->data['normativos2'][$x2]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                $this->data['normativos2'][$x2]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                $this->data['normativos2'][$x2]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                $this->data['normativos2'][$x2]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                $this->data['normativos2'][$x2]['txtStatus'] = $this->data['normativosInstituicao'][$b]->txtStatus;
                $this->data['normativos2'][$x2]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;

                $x2++;
            }
        }
 

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        $indiceOrigem = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceOrigem += [ $i => $this->data['normativos'][$i]['txtOrigem']];
        }

        $indiceOrigem = array_values(array_unique($indiceOrigem));

        $this->data['nomesBase'] = array();
        $this->data['corBases'] = array();
        $this->data['infoBases'] = array();

        
        for ($i=0; $i < count($indiceOrigem); $i++) { 
            $this->data['origem'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtOrigem'] == $indiceOrigem[$i]) {
                    $this->data['origem'.$i.'']++;
                }
            }       
            for ($a=0; $a < count($this->data['bases']); $a++) { 
                if ($this->data['bases'][$a]->txtNome == $indiceOrigem[$i]) {
                    array_push($this->data['nomesBase'], $this->data['bases'][$a]->txtNome);
                    array_push($this->data['corBases'], $this->data['bases'][$a]->txtCorPrimaria);
                }
            }   
        }
        for ($i=0; $i < count($this->data['nomesBase']); $i++) { 
            array_push($this->data['infoBases'], $this->data['origem'.$i.'']);
        }

        //AREAS
        $indiceArea = array();
        for ($i=0; $i < count($this->data['normativos']); $i++) { 
            $indiceArea += [ $i => $this->data['normativos'][$i]['txtArea']];
        }

        $indiceArea = array_values(array_unique($indiceArea));

        $this->data['nomesAreas'] = $indiceArea;
        
        for ($i=0; $i < count($indiceArea); $i++) { 
            $this->data['area'.$i.''] = 0;
            for ($x=0; $x < count($this->data['normativos']); $x++) {
                if ($this->data['normativos'][$x]['txtArea'] == $indiceArea[$i]) {
                    $this->data['area'.$i.'']++;
                }
            }   
        }

        //QTD POR AREA
        $this->data['dataInfo'] = array();
        for ($i=0; $i < count($this->data['nomesAreas']); $i++) { 
            array_push($this->data['dataInfo'], $this->data['area'.$i.'']);
        }

        //NOME AREAS
        $this->data['labels'] = array();
        for ($x=0; $x < count($this->data['nomesAreas']); $x++) {    
            array_push($this->data['labels'], $this->data['nomesAreas'][$x]);
        };


        //STATUS DO NORMATIVO
        $this->data['relevante0'] = 0;
        $this->data['relevante1'] = 0;
        $this->data['relevante2'] = 0;

        for ($x=0; $x < count($this->data['normativos2']); $x++) {
            if($this->data['normativos2'][$x]['bitCiente'] == 1){
                $this->data['relevante1']++;
            }elseif ($this->data['normativos2'][$x]['bitCiente'] == 0) {
                $this->data['relevante2']++;
            }else{
                $this->data['relevante0']++;
            }
        }

        $this->data['statusNormativo'] = array();        
        for ($x=0; $x < 3; $x++) {    
            array_push($this->data['statusNormativo'], $this->data['relevante'.$x.'']);
        };


        //PLANOS DE ACAO (STATUS)
        $this->data['status1'] = 0;
        $this->data['status2'] = 0;
        $this->data['status3'] = 0;
        $this->data['status4'] = 0;
        $this->data['status5'] = 0;
        $this->data['status6'] = 0;

        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]['txtStatus'] == 'Concluído'){
                $this->data['status1']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Cancelado') {
                $this->data['status2']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Ativo') {
                $this->data['status3']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vigência') {
                $this->data['status4']++;
            }elseif ($this->data['normativos'][$i]['txtStatus'] == 'Atrasado Data Vencimento') {
                $this->data['status5']++;
            }else{
                $this->data['status6']++;
            }
        } 

        $this->data['statusPA'] = array();
        for ($x=1; $x < 7; $x++) {    
            array_push($this->data['statusPA'], $this->data['status'.$x.'']);
        };

        echo json_encode(array("normativos" => $this->data['normativos'], "dataInfo" => $this->data['dataInfo'], "labels" => $this->data['labels'], "statusNormativo" => $this->data['statusNormativo'], "statusPA" => $this->data['statusPA'], "nomesBase" => $this->data['nomesBase'], "corBases" => $this->data['corBases'], "infoBases" => $this->data['infoBases'] ));        
    }

    public function filtrar_comentario_feeder(){

        $this->load->model('anotacao_model');
        
        $objData = new stdClass();
        $objData = (object)$_POST;
                
        $this->data['anotacoes'] = $this->anotacao_model->get_anotacoes_bpo($objData->idNorma, $this->session->userdata['user']['idInstituicao']);           
        
        if (isset($this->data['anotacoes'][0]->txtOrigem)) {
            $this->data['txtTipoComentario'] = 'Comentário inserido pelo BPO:';
            $this->data['txtComentario'] = $this->data['anotacoes'][0]->txtComentario;
        }else{
            $this->data['txtTipoComentario'] = 'Inserir um novo comentário:';
            $this->data['txtComentario'] = '';
        }

        echo json_encode(array("comentario" => $this->data['txtComentario'], "tipoComentario" => $this->data['txtTipoComentario']));
        
    }


    public function save_comentario_feeder(){
         if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertNormativo = new stdClass();
        $objData = (object)$_POST;

        $objInsertNormativo->idNormativo = $objData->id;
        $objInsertNormativo->txtComentario = $objData->txtComentario;
        $objInsertNormativo->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $query2 = $this->crud_model->insert('tabcomentarionormativofeeder', $objInsertNormativo);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }


      public function add_normativo_area(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['normativos'] = $this->normativo_model->get_normativos_instituicao_idNorma($objData->idNormaAtual);

        $array = explode('/', $objData->idAreas);

        for ($i=1; $i < count($array); $i++) { 

            $objInsertNormativo = new stdClass();
            $objInsertNormativo->idNormativo = $this->data['normativos'][0]->idNorma;
            $objInsertNormativo->dateCadastro = $this->data['normativos'][0]->txtDataEmissao;
            $objInsertNormativo->txtTitulo = $this->data['normativos'][0]->txtTitulo;
            $objInsertNormativo->txtLink = $this->data['normativos'][0]->txtLink;
            $objInsertNormativo->txtClasse = $this->data['normativos'][0]->txtClasse;
            $objInsertNormativo->txtAssunto = $this->data['normativos'][0]->txtAssunto;
            $objInsertNormativo->txtOrigem = $this->data['normativos'][0]->txtOrigem;
            $objInsertNormativo->txtRelevante = "Sim";
            $objInsertNormativo->idArea = $array[$i];
            $objInsertNormativo->bitCiente = 2;
            $objInsertNormativo->txtTipoNorma = $this->data['normativos'][0]->txtTipoNorma;
            $objInsertNormativo->txtNumero = $this->data['normativos'][0]->txtNumero;
            $objInsertNormativo->idInstituicao =  $this->session->userdata['user']['idInstituicao'];

            $query = $this->crud_model->insert('tabnormativoarea', $objInsertNormativo);

            unset($objInsertNormativo);
        }
       
        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }
}
