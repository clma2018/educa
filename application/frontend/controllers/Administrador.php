<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Administrador extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

    }

    public function index() {
        
        $this->template->showSite('login-admin');
    }

    public function validar_login(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $objData->txtSenha = do_hash(do_hash($objData->txtSenha, 'md5'));
        $query = $this->user_model->validate_user_admin($objData->txtEmail, $objData->txtSenha);

        if ($query){
            // Salva os dados na sessão do usuário
            $this->data['user'] = array(
                'id'                        => $query[0]->id,
                'txtNome'                   => $query[0]->txtNome,
                'txtEmail'                  => $query[0]->txtEmail,
                'bitAdministrador'          => 1,
                'logged'                    => true
            );

            $this->session->set_userdata($this->data);

            echo json_encode(array("valid" => true));
        }else{
            echo json_encode(array("valid" => false, 'mensagem'=>'E-mail ou senha incorretos '));
        }
    }

}
