<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();

        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //Model
        $this->load->model('base_model');
        $this->load->model('normativo_model');
        $this->load->model('user_model');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }

        $this->load->model('curso_model');
        
        $this->data['qtdSecoes'] = count($this->data['secoes'] = $this->curso_model->get_secoes());
    }
    
    public function index() {

        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        $this->load->model('user_model');
        $this->load->model('normativo_model');
        
        $this->data['datanormativos']=  $this->normativo_model->get_normativo_user_data($this->session->userdata['user']['id']);

        $this->data['normativos']=  $this->normativo_model->get_normativo_user($this->session->userdata['user']['id']);        

        $dataNorm = array();
        for ($a=0; $a < count($this->data['normativos']); $a++) {
            if ($this->data['normativos'][$a]->bitCiente == 2) {
                if (array_key_exists($this->data['normativos'][$a]->dateCadastro, $dataNorm)) {
                    $dataNorm[$this->data['normativos'][$a]->dateCadastro]++;
                }else{
                    $dataNorm += [ $this->data['normativos'][$a]->dateCadastro => 1 ];
                }
            }
        } 

        $this->data['dataNormativo'] = $dataNorm;        

        //RELEVANCIA DO NORMATIVO
        for ($z=0; $z < count($this->data['normativos']); $z++) {
            if($this->data['normativos'][$z]->txtNota == NULL){
                $this->data['normativos'][$z]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$z]->bitCiente == 1){
                $this->data['normativos'][$z]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$z]->bitCiente == 0) {
                $this->data['normativos'][$z]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$z]->txtRelevancia = 'Não Lido';
            }
        }

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        for ($b=0; $b < count($this->data['normativos']); $b++) {
            for ($i=0; $i < count($this->data['bases']); $i++) { 
                if ($this->data['normativos'][$b]->txtOrigem == $this->data['bases'][$i]->txtNome) {
                    $this->data['normativos'][$b]->txtCorPrimaria = $this->data['bases'][$i]->txtCorPrimaria;
                    $this->data['normativos'][$b]->txtCorSecundaria = $this->data['bases'][$i]->txtCorSecundaria;
                }
            }   

            if ($this->data['normativos'][$b]->bitNormativoHistorico == 1) {
                $this->data['normativos'][$b]->txtCorPrimaria = '#fdfdfd;border:2px solid #990000';
                $this->data['normativos'][$b]->txtCorSecundaria = '';
            }
            
        }
         
        //STATUS DO NORMATIVO
        $this->data['normativosGrafico']=  $this->normativo_model->get_normativo_avaliacao($this->session->userdata['user']['id']);

        $this->data['relevante0'] = 0;
        $this->data['relevante1'] = 0;
        $this->data['relevante2'] = 0;

        for ($x=0; $x < count($this->data['normativosGrafico']); $x++) {
            if($this->data['normativosGrafico'][$x]->bitCiente == 1){
                $this->data['relevante1']++;
            }elseif ($this->data['normativosGrafico'][$x]->bitCiente == 0) {
                $this->data['relevante2']++;
            }else{
                $this->data['relevante0']++;
            }
        }   

        //PLANOS DE ACAO (STATUS)
        $this->data['PAGrafico']=  $this->normativo_model->get_planoAcao_user($this->session->userdata['user']['id']);
        
        $this->data['status1'] = 0;
        $this->data['status2'] = 0;
        $this->data['status3'] = 0;
        $this->data['status4'] = 0;
        $this->data['status5'] = 0;

        for ($i=0; $i < count($this->data['PAGrafico']); $i++) {
            if($this->data['PAGrafico'][$i]->txtStatus == 'Concluído'){
                $this->data['status1']++;
            }elseif ($this->data['PAGrafico'][$i]->txtStatus == 'Cancelado') {
                $this->data['status2']++;
            }elseif ($this->data['PAGrafico'][$i]->txtStatus == 'Ativo') {
                $this->data['status3']++;
            }elseif ($this->data['PAGrafico'][$i]->txtStatus == 'Atrasado Data Vigência') {
                $this->data['status4']++;
            }elseif ($this->data['PAGrafico'][$i]->txtStatus == 'Atrasado Data Vencimento') {
                $this->data['status5']++;
            }
        } 
        

        $this->template->showSite('dashboard', $this->data);
    }

    public function historico() {

        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
                 
        $this->template->showSite('dashboard-historico', $this->data);
    }


    public function dashboards() {

        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
                 
        $this->template->showSite('list-powerbi', $this->data);
    }

    public function filtrar_semana() {

        $this->data['normativos']=  $this->normativo_model->get_normativo_user_semana($this->session->userdata['user']['id']);        

        //RELEVANCIA DO NORMATIVO
        for ($z=0; $z < count($this->data['normativos']); $z++) {
            if($this->data['normativos'][$z]->txtNota == NULL){
                $this->data['normativos'][$z]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$z]->bitCiente == 1){
                $this->data['normativos'][$z]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$z]->bitCiente == 0) {
                $this->data['normativos'][$z]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$z]->txtRelevancia = 'Não Lido';
            }
        }

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        for ($b=0; $b < count($this->data['normativos']); $b++) {
            for ($i=0; $i < count($this->data['bases']); $i++) { 
                if ($this->data['normativos'][$b]->txtOrigem == $this->data['bases'][$i]->txtNome) {
                    $this->data['normativos'][$b]->txtCorPrimaria = $this->data['bases'][$i]->txtCorPrimaria;
                    $this->data['normativos'][$b]->txtCorSecundaria = $this->data['bases'][$i]->txtCorSecundaria;
                }
            }

            if ($this->data['normativos'][$b]->bitNormativoHistorico == 1) {
                $this->data['normativos'][$b]->txtCorPrimaria = '#fdfdfd;border:2px solid #990000';
                $this->data['normativos'][$b]->txtCorSecundaria = '';
            }
        } 

        //HTML
        $html = '';

        //DATA INICIAL E FINAL
        $primeiro = end($this->data['normativos']);
        $ultimo = reset($this->data['normativos']);

        $start_date = date('Y-m-d', strtotime($primeiro->data2));
        $end_date = date('Y-m-d', strtotime($ultimo->data2));
        
        $i=1;

        for($date = $start_date; $date <= $end_date; $date = date('Y-m-d',       strtotime($date. ' + 7 days'))) {
            $week =  date('W', strtotime($date));
            $year =  date('Y', strtotime($date));
            $from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); 
            if($from < $start_date) $from = $start_date;
            $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   
            if($to > $end_date) $to = $end_date;

            $fim = explode('-', $to);
            $fim = $fim[2].'/'.$fim[1].'/'.$fim[0];

            $inicio = explode('-', $from);
            $inicio = $inicio[2].'/'.$inicio[1].'/'.$inicio[0];

            //FORMATANDO DATA DE INICIO DA SEMANA E DATA FIM
            $dataFormataInicio = explode('-', $from);
            $dataFormataInicio = $dataFormataInicio[2].'/'.$dataFormataInicio[1].'/'.$dataFormataInicio[0];
            $dataFormataFim =  explode('-', $to);
            $dataFormataFim = $dataFormataFim[2].'/'.$dataFormataFim[1].'/'.$dataFormataFim[0];

            $html.= '<div class="panel panel-default">  
                    <div class="panel-heading" role="tab">
                        <a role="button" data-toggle="collapse" data-parent="#listagemNormativos" href="#semanal'.$i.'">
                            Semana '.$i.'
                            <span class="pull-right">'.$inicio.' - '.$fim.'</span>
                        </a>
                    </div>
                    <div id="semanal'.$i.'" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="list-group">';

            //LOCAL AONDE OS NORMATIVOS SERAO INSERIDOS
            for ($z=0; $z < count($this->data['normativos']); $z++) {
                $dataFormatada =  explode('-', $this->data['normativos'][$z]->data2);
                $dataFormatada = $dataFormatada[2].'/'.$dataFormatada[1].'/'.$dataFormatada[0];
                $dataNormativo = DateTime::createFromFormat('d/m/Y', $dataFormatada);
                $dataIni = DateTime::createFromFormat('d/m/Y', $dataFormataInicio);
                $dataFim = DateTime::createFromFormat('d/m/Y',  $dataFormataFim);

                $dataPubli = '';
                $normHist='';
                if ($this->data['normativos'][$z]->bitNormativoHistorico == 1){
                    $dataPubli = '<li>Data de Publicação: '.$this->data['normativos'][$z]->txtDataVigencia.'</li>';
                    $normHist = '<h4 style="font-family: "GothamBook";color:#990000;">Normativo Histórico <i class="fa fa-history"></i></h4>';
                }
                
                $var ='';
                if ($this->data['normativos'][$z]->txtNota > 0) {
                    for ($ai=0; $ai < $this->data['normativos'][$z]->txtNota; $ai++){
                        $var .= '<i class="fa fa-fw fa-star push-5-r"></i>';
                    }
                }else{
                    $var = $this->data['normativos'][$z]->txtNota;
                }

                if ($dataNormativo >= $dataIni && $dataNormativo <= $dataFim){
                    $html .= '<a class="list-group-item push-15" href="'.base_url("normativo/detalhe/".encode($this->data['normativos'][$z]->id)).'" style="background:'.$this->data['normativos'][$z]->txtCorPrimaria.'">
                                <h5 style="color:'.$this->data['normativos'][$z]->txtCorSecundaria.'">'.$this->data['normativos'][$z]->txtOrigem.'</h5>
                                '.$this->data['normativos'][$z]->txtTitulo.'
                                <h4>'.$this->data['normativos'][$z]->txtClasse.'</h4>
                                '.$normHist.'
                                <ul class="footerNormativo push-10">
                                    <li>Relevância: <strong>'.$this->data['normativos'][$z]->txtRelevancia.'</strong></li>
                                    <li>
                                        Avaliação: <strong>'.$var.'</strong>
                                    </li>
                                    '.$dataPubli.'
                                </ul>
                            </a>';                    
                }
            }
            $html .= '</div>
                   </div>
                </div>
            </div>';

            $i++;
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'html'=>$html));
        
    }

     public function filtrar_mes() {
        
        $this->data['normativos']=  $this->normativo_model->get_normativo_user_mes($this->session->userdata['user']['id']);        

        //RELEVANCIA DO NORMATIVO
        for ($z=0; $z < count($this->data['normativos']); $z++) {
            if($this->data['normativos'][$z]->txtNota == NULL){
                $this->data['normativos'][$z]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$z]->bitCiente == 1){
                $this->data['normativos'][$z]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$z]->bitCiente == 0) {
                $this->data['normativos'][$z]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$z]->txtRelevancia = 'Não Lido';
            }
        }

        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        for ($b=0; $b < count($this->data['normativos']); $b++) {
            for ($i=0; $i < count($this->data['bases']); $i++) { 
                if ($this->data['normativos'][$b]->txtOrigem == $this->data['bases'][$i]->txtNome) {
                    $this->data['normativos'][$b]->txtCorPrimaria = $this->data['bases'][$i]->txtCorPrimaria;
                    $this->data['normativos'][$b]->txtCorSecundaria = $this->data['bases'][$i]->txtCorSecundaria;
                }
            }
            
            if ($this->data['normativos'][$b]->bitNormativoHistorico == 1) {
                $this->data['normativos'][$b]->txtCorPrimaria = '#fdfdfd;border:2px solid #990000';
                $this->data['normativos'][$b]->txtCorSecundaria = '';
            }
        } 

        //HTML
        $html = '';

        //DATA INICIAL E FINAL
        $primeiro = end($this->data['normativos']);
        $ultimo = reset($this->data['normativos']);

        $start_date = date('Y-m-t', strtotime($primeiro->data2));
        $end_date = date('Y-m-t', strtotime($ultimo->data2));


        $i=1;

        for($date = $start_date; $date <= $end_date; $date = date('Y-m-d', strtotime($date. ' last day next month'))) {
    
            $from = date('Y-m-01', strtotime($date));
            if($from < $start_date) $from = $start_date;

            $to = date('Y-m-t', strtotime($date));  
            if($to > $end_date) $to = $end_date;

            //FORMATANDO DATA DE INICIO DA SEMANA E DATA FIM
            $dataFormataInicio = explode('-', $from);
            $dataFormataInicio = $dataFormataInicio[2].'/'.$dataFormataInicio[1].'/'.$dataFormataInicio[0];
            $dataFormataFim =  explode('-', $to);
            $mesEscrito = MesPorExtenso($dataFormataFim[1]);
            $dataFormataFim = $dataFormataFim[2].'/'.$dataFormataFim[1].'/'.$dataFormataFim[0];

            $html.= '<div class="panel panel-default">  
                    <div class="panel-heading" role="tab">
                        <a role="button" data-toggle="collapse" data-parent="#listagemNormativos" href="#mes'.$i.'">
                            '.$mesEscrito.'
                        </a>
                    </div>
                    <div id="mes'.$i.'" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="list-group">';

            //LOCAL AONDE OS NORMATIVOS SERAO INSERIDOS
            for ($z=0; $z < count($this->data['normativos']); $z++) {
                $dataFormatada =  explode('-', $this->data['normativos'][$z]->data2);
                $dataFormatada = $dataFormatada[2].'/'.$dataFormatada[1].'/'.$dataFormatada[0];
                $dataNormativo = DateTime::createFromFormat('d/m/Y', $dataFormatada);
                $dataIni = DateTime::createFromFormat('d/m/Y', $dataFormataInicio);
                $dataFim = DateTime::createFromFormat('d/m/Y',  $dataFormataFim);

                $dataPubli = '';
                $normHist='';
                if ($this->data['normativos'][$z]->bitNormativoHistorico == 1){
                    $dataPubli = '<li>Data de Publicação: '.$this->data['normativos'][$z]->txtDataVigencia.'</li>';
                    $normHist = '<h4 style="font-family: "GothamBook";color:#990000;">Normativo Histórico <i class="fa fa-history"></i></h4>';
                }


                $var ='';
                if ($this->data['normativos'][$z]->txtNota > 0) {
                    for ($ai=0; $ai < $this->data['normativos'][$z]->txtNota; $ai++){
                        $var .= '<i class="fa fa-fw fa-star push-5-r"></i>';
                    }
                }else{
                    $var = $this->data['normativos'][$z]->txtNota;
                }

                if ($dataNormativo >= $dataIni && $dataNormativo <= $dataFim){
                    $html .= '<a class="list-group-item push-15" href="'.base_url("normativo/detalhe/".encode($this->data['normativos'][$z]->id)).'" style="background:'.$this->data['normativos'][$z]->txtCorPrimaria.'">
                                <h5 style="color:'.$this->data['normativos'][$z]->txtCorSecundaria.'">'.$this->data['normativos'][$z]->txtOrigem.'</h5>
                                '.$this->data['normativos'][$z]->txtTitulo.'
                                <h4>'.$this->data['normativos'][$z]->txtClasse.'</h4>
                                '.$normHist.'
                                <ul class="footerNormativo push-10">
                                    <li>Relevância: <strong>'.$this->data['normativos'][$z]->txtRelevancia.'</strong></li>
                                    <li>
                                        Avaliação: <strong>'.$var.'</strong>
                                    </li>
                                    '.$dataPubli.'
                                </ul>
                            </a>';                    
                }
            }
            $html .= '</div>
                   </div>
                </div>
            </div>';

            $i++;

        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'html'=>$html ));
        
    }


    public function acoes_usuario() {
        if (!isset($this->session->userdata['user']))
                redirect('login', 'refresh');
            if ($this->session->userdata['user']['idAdministrador'] != 0 )
                redirect('dashboard', 'refresh');
        
        //DATAS ABAS
        $this->data['datanormativos']=  $this->normativo_model->get_normativo_user_data_instituicao($this->session->userdata['user']['idInstituicao']);


        //TRATAMENTO DOS DADOS
        $this->data['normativos2']=  $this->normativo_model->get_normativo_user_instituicao($this->session->userdata['user']['idInstituicao']);        
        $this->data['normativos'] = '';
        $idNormativos = array();
        $posicao = array();

        $x = 0;
        for ($b=0; $b < count($this->data['normativos2']); $b++) {
            
            //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
            if (!array_key_exists($this->data['normativos2'][$b]->idNormativo, $idNormativos)) {
                $this->data['normativos'][$x]['id'] = $this->data['normativos2'][$b]->id;
                $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativos2'][$b]->txtOrigem;
                $this->data['normativos'][$x]['dateCadastro'] = $this->data['normativos2'][$b]->dateCadastro;
                $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativos2'][$b]->txtDataVigencia;
                $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativos2'][$b]->txtTitulo;
                $this->data['normativos'][$x]['txtClasse'] = $this->data['normativos2'][$b]->txtClasse;
                $this->data['normativos'][$x]['txtArea'] = $this->data['normativos2'][$b]->txtArea;
                $this->data['normativos'][$x]['bitNormativoHistorico'] = $this->data['normativos2'][$b]->bitNormativoHistorico;
                $this->data['normativos'][$x]['qtdComentarios'] = $this->data['normativos2'][$b]->qtdComentarios;

                $this->data['qtdPa'] =  count($this->normativo_model->get_planoAcao($this->data['normativos2'][$b]->idNormativo,$this->session->userdata['user']['idInstituicao']));        
                
                $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['qtdPa'];
                
                if($this->data['normativos2'][$b]->txtStatus == 'Concluído'){
                    $estilo[$b] = "#0F60E2"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Cancelado') {
                    $estilo[$b] = "#FFD131"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Ativo') {
                    $estilo[$b] = "#72B01D"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Atrasado Data Vigência') {
                    $estilo[$b] = "#E81E2E"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Atrasado Data Vencimento') {
                    $estilo[$b] = "#FF9B71"; 
                }else{
                    $estilo[$b] = "#333"; 
                }
                
                $this->data['normativos'][$x]['usuarios'] = "<li style='background:".$estilo[$b]."'>".$this->data['normativos2'][$b]->txtNome."</li>";
                
                $idNormativos += [ $this->data['normativos2'][$b]->idNormativo => $x ];
                $posicao += [ $x => $this->data['normativos2'][$b]->idNormativo ];
                $x++;
            }else{
                $key = array_search($this->data['normativos2'][$b]->idNormativo, $posicao);
                
                if($this->data['normativos2'][$b]->txtStatus == 'Concluído'){
                    $estilo[$b] = "#0F60E2"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Cancelado') {
                    $estilo[$b] = "#FFD131"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Ativo') {
                    $estilo[$b] = "#72B01D"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Atrasado Data Vigência') {
                    $estilo[$b] = "#E81E2E"; 
                }elseif ($this->data['normativos2'][$b]->txtStatus == 'Atrasado Data Vencimento') {
                    $estilo[$b] = "#FF9B71"; 
                }else{
                    $estilo[$b] = "#333"; 
                }

                $this->data['normativos'][$key]['usuarios'] .= "<li style='background:".$estilo[$b]."'>".$this->data['normativos2'][$b]->txtNome."</li>";
                $this->data['normativos'][$key]['txtArea'] .= ", ".$this->data['normativos2'][$b]->txtArea;
            }
        }

        //QUANTIDADE DE NORMATIVO PARA O DIA
        $dataNorm = array();
        for ($a=0; $a < count($this->data['normativos']); $a++) {
            if (array_key_exists($this->data['normativos'][$a]['dateCadastro'], $dataNorm)) {
                $dataNorm[$this->data['normativos'][$a]['dateCadastro']]++;
            }else{
                $dataNorm += [ $this->data['normativos'][$a]['dateCadastro'] => 1 ];
            }
        } 

        $this->data['dataNormativo'] = $dataNorm;    
            
        //CORES DA BASE
        $this->data['bases']=  $this->base_model->get_bases();

        for ($b=0; $b < count($this->data['normativos']); $b++) {
            for ($i=0; $i < count($this->data['bases']); $i++) { 
                if ($this->data['normativos'][$b]['txtOrigem'] == $this->data['bases'][$i]->txtNome) {
                    $this->data['normativos'][$b]['txtCorPrimaria'] = $this->data['bases'][$i]->txtCorPrimaria;
                    $this->data['normativos'][$b]['txtCorSecundaria'] = $this->data['bases'][$i]->txtCorSecundaria;
                }
            }   

            if ($this->data['normativos'][$b]['bitNormativoHistorico'] == 1) {
                $this->data['normativos'][$b]['txtCorPrimaria'] = '#fdfdfd;border:2px solid #990000';
                $this->data['normativos'][$b]['txtCorSecundaria'] = '';
            }
            
        }

        $this->template->showSite('acoes-usuario', $this->data);
    }


    public function graficos() {

        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
                 
        $this->template->showSite('graficos', $this->data);
    }

    public function save_json() {
        $objData = new stdClass();
        $objData = (object)$_POST;

        $myFile = "./assets/js/novo.json";
        $fh = fopen($myFile, 'w') or die("can't open file");

        fwrite($fh, $objData->novaInfo);
        fclose($fh);
                
    }

    public function zerar_json() {

        //CAPTURA A INFORMACAO
        $str = file_get_contents('./assets/js/flare4.json');
        
        //SALVA NO JSON NOVO
        $myFile = "./assets/js/novo.json";
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $str);
        fclose($fh);
    
        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success'));
                
    }

    public function graficos_dc() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

         $this->load->model('area_model');

        $this->data['areasTemas']=  $this->area_model->get_temas_area(0,$this->session->userdata['user']['idInstituicao']);

        $this->data['normativosInstituicao'] = $this->normativo_model->get_normas_instituicao($this->session->userdata['user']['idInstituicao']);
        $this->data['normativos'] = '';
        
        $txtTemasArea = array();
        for ($i=0; $i < count($this->data['areasTemas']); $i++) { 
            $txtTemasArea += [ $this->data['areasTemas'][$i]->txtTitulo => 1 ];
        }

        $idNormativos = array();
        $posicao = array();

        $x = 0;
        for ($b=0; $b < count($this->data['normativosInstituicao']); $b++) {
            if (array_key_exists($this->data['normativosInstituicao'][$b]->txtClasse, $txtTemasArea)) {
                //NORMATIVOS QUE SERAO EXIBIDOS PARA O USUARIO
                if (!array_key_exists($this->data['normativosInstituicao'][$b]->idNormativo, $idNormativos)) {
                    $this->data['normativos'][$x]['id'] = $this->data['normativosInstituicao'][$b]->id;
                    $this->data['normativos'][$x]['txtNomeUsuario'] = $this->data['normativosInstituicao'][$b]->txtNome;
                    $this->data['normativos'][$x]['txtEmail'] = $this->data['normativosInstituicao'][$b]->txtEmail;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosInstituicao'][$b]->idNormativo;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosInstituicao'][$b]->txtOrigem;
                    $this->data['normativos'][$x]['txtDataPubli'] = $this->data['normativosInstituicao'][$b]->dateCadastro;
                    $this->data['normativos'][$x]['txtDataVigencia'] = $this->data['normativosInstituicao'][$b]->txtDataVigencia;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['normativosInstituicao'][$b]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['normativosInstituicao'][$b]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['normativosInstituicao'][$b]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['normativosInstituicao'][$b]->txtAssunto;
                    $this->data['normativos'][$x]['qtdPlanosAcao'] = $this->data['normativosInstituicao'][$b]->qtdPlanosAcao;
                    $this->data['normativos'][$x]['txtArea'] = $this->data['normativosInstituicao'][$b]->txtArea;

                    if($this->data['normativosInstituicao'][$b]->bitCiente == 1){
                        $this->data['normativos'][$x]['txtStatus'] = "Relevante";
                    }elseif ($this->data['normativosInstituicao'][$b]->bitCiente == 0) {
                        $this->data['normativos'][$x]['txtStatus'] = "Não Relevante";
                    }else{
                        $this->data['normativos'][$x]['txtStatus'] = "Não Lido";
                    }

                    $x++;
                }
              
            }
        }

        $this->template->showSite('graficos-dc', $this->data);
    }
}

