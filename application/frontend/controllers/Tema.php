<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Tema extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');;
        $this->load->model('tema_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');


        $this->data['temas'] =  $this->tema_model->get_all_temas();

        $this->data['subtemas'] =  $this->tema_model->get_all_subtemas();

        $this->template->showSite('list-temas', $this->data);    

    }

    public function edit_tema($idTema = 0){
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        if ($idTema != '') {
        
            $this->data['tema'] =  $this->tema_model->get_all_temas($this->encrypt->decode($idTema));             
            $this->data['subtemas'] =  $this->tema_model->get_all_subtemas($this->encrypt->decode($idTema));         
        }else{
             $this->data['tema'] = '';
        }

        $this->template->showSite('edit-tema', $this->data);
    }

    public function config_tema() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->id)) {

            $objUpdateTema = new stdClass();
            
            
            $arrayCondition = array('id = ' . $this->encrypt->decode($objData->id));
            $objUpdateTema->txtTitulo = $objData->txtTitulo;

            $query = $this->crud_model->update($objUpdateTema, 'tabtema', $arrayCondition);
        
            
            $this->data['subtemas'] =  $this->tema_model->get_all_subtemas($this->encrypt->decode($objData->id)); 

            $txtSubTemas[] = '';
            for ($i=0; $i < count($objData->txtSubTemas); $i++) { 
                array_push($txtSubTemas, $objData->txtSubTemas[$i]['txtSubTemas']);
            }
            array_shift($txtSubTemas);

            $arraytituloTemas = array();
            for ($i=0; $i < count($this->data['subtemas']); $i++) { 
                array_push($arraytituloTemas, $this->data['subtemas'][$i]->txtTitulo);
            }   

            
            for($i = 0; $i < count($arraytituloTemas); $i++){
                if(!in_array($arraytituloTemas[$i], $txtSubTemas)){
                    $arrayCondition2 = array('txtTitulo = \'' . $arraytituloTemas[$i] . '\'');
                    $query2 = $this->crud_model->delete('tabsubtema', $arrayCondition2);
                }
            }
          
       
            for($i = 0; $i < count($txtSubTemas); $i++){
                if($txtSubTemas[$i]){
                    $qe = new stdClass();
                    $qe = $this->tema_model->get_all_subtemas_titulo($txtSubTemas[$i]);
                    if (count($qe) > 0) {        
                        $valido = 0;
                    }else{
                        $objSubTemas = new stdClass();
                        $objSubTemas->idTema = $this->encrypt->decode($objData->id);
                        $objSubTemas->txtTitulo = $txtSubTemas[$i];
                         $this->crud_model->insert('tabsubtema',$objSubTemas);
                        unset($objSubTemas);
                    }
                    unset($qe);
                }
            }


            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Tema editado com sucesso. ', 'insert'=> false));
        }else{
            $objInsertTema = new stdClass();
       
            $objInsertTema->txtTitulo = $objData->txtTitulo;
            $tema = $this->crud_model->insert('tabtema',$objInsertTema);

            $txtSubTemas[] = '';
            for ($i=0; $i < count($objData->txtSubTemas); $i++) { 
                array_push($txtSubTemas, $objData->txtSubTemas[$i]['txtSubTemas']);
            }
            array_shift($txtSubTemas);

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = $txtSubTemas; 
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros){
                    $objSubTemas = new stdClass();
                    $objSubTemas->idTema = $tema->id;
                    $objSubTemas->txtTitulo = $arrayRegistros[$i];
                    $this->crud_model->insert('tabsubtema',$objSubTemas);
                    unset($objSubTemas);
                }
            }

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Tema inserido com sucesso. ', 'insert'=> true));
        }

    }

    public function filtrar_temas() {
       
        $objData = new stdClass();
        $objData = (object)$_POST;
        $this->data['temas'] =  $this->tema_model->get_all_subtemas($objData->idTema);         

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "temas" => $this->data['temas']));
    }
}
