<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;


class Cadastro extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');
         $this->load->model('user_model');
        $this->load->model('curso_model');

        // Helper
        $this->load->helper('security');

        $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);

        $this->data['qtdSecoes'] = count($this->data['secoes'] = $this->curso_model->get_secoes());
    }

    public function index() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');


        $this->template->showSite('cadastro', $this->data);  
        
    }

    public function new_user() {
    
        $objData = new stdClass();
        $objData = (object)$_POST;

         //Funcao para gerar senha de forma aleatoria
        function generatePassword($length = 8) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);

            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }

            return $result;
        }

        $senhaRand = generatePassword(6);
        $objUsuarios = new stdClass();
        $objUsuarios->idAdministrador = 011;
        $objUsuarios->bitCadastro = 1;
        $objUsuarios->idInstituicao = 999;
        $objUsuarios->txtNome = $objData->txtNome;
        $objUsuarios->txtEmail = $objData->txtEmail;
        $objUsuarios->txtSenha = do_hash(do_hash($senhaRand, 'md5'));

        $usuario = $this->crud_model->insert('tabusuario',$objUsuarios);

        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
        
        //DISPARO EMAIL
        $promise = $sparky->transmissions->post([
            'content' => ['template_id' => 'legal-bot-ead'],
            'substitution_data' => 
                    ['txtNome' => $objData->txtNome,
                    'txtEmail' => $objData->txtEmail,
                    'txtSenha' => $senhaRand],
            'recipients' => [
                [
                    'address' => [
                        'name' => $objData->txtNome,
                        'email' => $objData->txtEmail,
                    ],
                ],
            ],
        ]);

        try {
            $response = $promise->wait();
            // echo $response->getStatusCode()."\n";
            // print_r($response->getBody())."\n";
        } catch (\Exception $e) {
            // echo $e->getCode()."\n";
            // echo $e->getMessage()."\n";
        }

        unset($objUsuarios);


        //SALVA STATUS TEMPORARIO
        $this->data['licoes'] = $this->curso_model->get_licoes_simple();
        
        for ($i=0; $i < count($this->data['licoes']); $i++) { 
            $obdjInsertStatus = new stdClass();
            $obdjInsertStatus->idUsuario = $usuario->id;
            $obdjInsertStatus->idLicao =  $this->data['licoes'][$i]->id;
            $obdjInsertStatus->txtStatus =  'Pendente';
            
            $this->crud_model->insert('tabStatusLicao',$obdjInsertStatus);
            unset($obdjInsertStatus);


            $obdjInsertVideo = new stdClass();
            $obdjInsertVideo->idUsuario = $usuario->id;
            $obdjInsertVideo->idLicao =  $this->data['licoes'][$i]->id;
            $obdjInsertVideo->txtTempo =  '0';
            
            $this->crud_model->insert('tabTempoVideo',$obdjInsertVideo);
            unset($obdjInsertVideo);
        }
     
        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

}
