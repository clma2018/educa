<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Logs extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('logs_model');
        $this->load->model('instituicao_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('login', 'refresh');

        $this->data['logs']=  $this->logs_model->get_logins();

        $this->template->showSite('list-login', $this->data);    

    }

    public function list_disparo_email() {
       if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('login', 'refresh');

        $this->data['logs']=  $this->logs_model->get_disparos_email();

        $this->template->showSite('list-disparo-email', $this->data);    

    }

    public function list_norm_list() {
       if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('login', 'refresh');

        $this->data['logs'] =  $this->logs_model->get_acessos_normativos();

        $this->template->showSite('list-normativos-acesso', $this->data);    

    }


    public function filtrar_logs(){
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['logs'] = $this->logs_model->get_logs_filtro($objData->txtDataInicio, $objData->txtDataFim);

        for ($i=0; $i < count($this->data['logs']); $i++) { 
            $this->data['logs'][$i]->txtDataAcesso = date('d/m/Y H:i:s', strtotime($this->data['logs'][$i]->dateCreate)  - 60 * 30 * 6);
        }

        echo json_encode(array("logs" => $this->data['logs']));    
    }

}
