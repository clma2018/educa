<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Agenda extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('normativo_model');
        $this->load->model('agenda_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['planosAcao'] = $this->normativo_model->get_planoAcao_user($this->session->userdata['user']['id']);

        $this->data['normativos']=  $this->normativo_model->get_normativos_dataVigencia($this->session->userdata['user']['id']); 

        $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->session->userdata['user']['id']);

        $this->data['normativoPrazo']= $this->agenda_model->get_normativos_prazo();

        $this->data['dadosAgenda']=  ''; 

        $a = 0;
        for ($i=0; $i < count($this->data['planosAcao']); $i++) {
            $data = explode('/', $this->data['planosAcao'][$i]->txtDataVencimento);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$i.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['planosAcao'][$i]->id.', text:"'.$this->data['planosAcao'][$i]->txtDescricao.'", subject:"PLA" }'; 
            $a++;
        }

        for ($x=0; $x < count($this->data['normativos']); $x++) {  
            $data = explode('/', $this->data['normativos'][$x]->dateCadastro);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$x.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['normativos'][$x]->id.', text:"'.$this->data['normativos'][$x]->txtAssunto.'", subject:"NP" }';
            $a++;
        }

        for ($z=0; $z < count($this->data['anotacoes']); $z++) { 
            if ($this->data['anotacoes'][$z]->idAdminComentario == '0') {
                $sub = 'ANOTUSER';
            }else{
                $sub = 'ANOTADMIN';
            }

            if (strpos($this->data['anotacoes'][$z]->txtDescricao, 'E-mails') !== false) {
                $sub = 'NORMENCAM';
            }
            
            $data = explode('/', $this->data['anotacoes'][$z]->txtDataVencimento);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$z.':00';
                
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['anotacoes'][$z]->id.', text:"'.$this->data['anotacoes'][$z]->txtDescricao.'", subject:"'.$sub.'"}';
            $a++;
        }
        
        for ($x=0; $x < count($this->data['normativoPrazo']); $x++) {  
            $data = explode('/', $this->data['normativoPrazo'][$x]->txtDataInicio);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$x.':00';
            $data2 = explode('/', $this->data['normativoPrazo'][$x]->txtDataFim);
            $data2 = $data2[2].'-'.$data2[1].'-'.$data2[0].' 1'.$x.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data2.'", idAnotacao:'.$this->data['normativoPrazo'][$x]->id.', text:"'.$this->data['normativoPrazo'][$x]->txtDescricao.'", txtObservacao:"'.$this->data['normativoPrazo'][$x]->txtObservacao.'", subject:"PRAZONORM" }';
            $a++;
        }

        $this->template->showSite('agenda', $this->data);    

    }

    public function alter_anotacao() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;

        if ($objData->idAnotacao !== '') {
            $objUpdateAnotacao = new stdClass();

            $objUpdateAnotacao->txtDescricao = $objData->txtDescricao;
            $objUpdateAnotacao->txtDataVencimento = $objData->txtDataVencimento;
            $arrayCondition = array('id = ' . (int)$objData->idAnotacao);

            $query = $this->crud_model->update($objUpdateAnotacao, 'tabanotacaousuario', $arrayCondition);

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Anotação editada com sucesso. ', 'insert'=> false));
        }else{
            $objInsertAnotacao = new stdClass();
            $objInsertAnotacao->txtDescricao = $objData->txtDescricao;
            $objInsertAnotacao->txtDataVencimento = $objData->txtDataVencimento;

            if ($this->session->userdata['user']['idAdministrador'] == 0) {
                $objInsertAnotacao->idUsuario = $objData->idUsuario;
                if ($this->session->userdata['user']['id'] == $objData->idUsuario) {
                    $objInsertAnotacao->idAdminComentario = '0';
                }else{
                    $objInsertAnotacao->idAdminComentario = $this->session->userdata['user']['id'];
                }
            }else{
                $objInsertAnotacao->idUsuario = $this->session->userdata['user']['id'];
                $objInsertAnotacao->idAdminComentario = '0';
            }
            
            $this->crud_model->insert('tabanotacaousuario',$objInsertAnotacao);

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Anotação inserida com sucesso. ', 'insert'=> true));
        }

    }

    public function remover_anotacao() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;    
        
        $arrayCondition2 = array('id = ' . (int)$objData->idAnotacao);
        $query2 = $this->crud_model->delete('tabanotacaousuario', $arrayCondition2);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'mensagem'=>'Anotação excluída com sucesso. ', 'insert'=> false));        
    }

    public function agenda_user($idUsuario =0) {

       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        //USUARIO
        $this->data['usuario'] = $this->user_model->get_users($this->encrypt->decode($idUsuario));

        $this->data['planosAcao'] = $this->normativo_model->get_planoAcao_user($this->encrypt->decode($idUsuario));

        $this->data['normativos']=  $this->normativo_model->get_normativos_dataVigencia($this->encrypt->decode($idUsuario)); 

        $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->encrypt->decode($idUsuario));

         $this->data['normativoPrazo']= $this->agenda_model->get_normativos_prazo();

        $this->data['dadosAgenda']=  ''; 

        $a = 0;
        for ($i=0; $i < count($this->data['planosAcao']); $i++) {
            $data = explode('/', $this->data['planosAcao'][$i]->txtDataVencimento);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$i.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['planosAcao'][$i]->id.', text:"'.$this->data['planosAcao'][$i]->txtDescricao.'", subject:"PLA" }'; 
            $a++;
        }

        for ($x=0; $x < count($this->data['normativos']); $x++) {  
            $data = explode('/', $this->data['normativos'][$x]->dateCadastro);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$x.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['normativos'][$x]->id.', text:"'.$this->data['normativos'][$x]->txtAssunto.'", subject:"NP" }';
            $a++;
        }

        for ($z=0; $z < count($this->data['anotacoes']); $z++) { 
            if ($this->data['anotacoes'][$z]->idAdminComentario == '0') {
                $sub = 'ANOTUSER';
            }else{
                $sub = 'ANOTADMIN';
            }

            if (strpos($this->data['anotacoes'][$z]->txtDescricao, 'E-mails') !== false) {
                $sub = 'NORMENCAM';
            }
            
            $data = explode('/', $this->data['anotacoes'][$z]->txtDataVencimento);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$z.':00';

            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['anotacoes'][$z]->id.', text:"'.$this->data['anotacoes'][$z]->txtDescricao.'", subject:"'.$sub.'"}';
            $a++;
        }

        for ($x=0; $x < count($this->data['normativoPrazo']); $x++) {  
            $data = explode('/', $this->data['normativoPrazo'][$x]->txtDataInicio);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 1'.$x.':00';
            $data2 = explode('/', $this->data['normativoPrazo'][$x]->txtDataFim);
            $data2 = $data2[2].'-'.$data2[1].'-'.$data2[0].' 1'.$x.':00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data2.'", idAnotacao:'.$this->data['normativoPrazo'][$x]->id.', text:"'.$this->data['normativoPrazo'][$x]->txtDescricao.'", txtObservacao:"'.$this->data['normativoPrazo'][$x]->txtObservacao.'", subject:"PRAZONORM" }';
            $a++;
        }

        $this->template->showSite('agenda-user', $this->data);    

    }


    public function agenda_admin() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        if ($this->session->userdata['user']['idAdministrador'] != 0)
            redirect('login', 'refresh');

        $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->session->userdata['user']['id']);

        $this->data['dadosAgenda']=  ''; 

        $a = 0;

        for ($z=0; $z < count($this->data['anotacoes']); $z++) { 
            if ($this->data['anotacoes'][$z]->idAdminComentario == '0') {
                $sub = 'ANOTUSER';
            }else{
                $sub = 'ANOTADMIN';
            }
            $data = explode('/', $this->data['anotacoes'][$z]->txtDataVencimento);
            $data = $data[2].'-'.$data[1].'-'.$data[0].' 10:00';
            $this->data['dadosAgenda'][$a] = '{ start_date: "'.$data.'", end_date: "'.$data.'", idAnotacao:'.$this->data['anotacoes'][$z]->id.', text:"'.$this->data['anotacoes'][$z]->txtDescricao.'", subject:"'.$sub.'"}';
            $a++;
        }
        

        $this->template->showSite('agenda', $this->data);    

    }

    public function filtrar_eventos(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['eventos'] = '';

        $a = 0;

        if ($objData->valorCampo == 1) {
            $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->session->userdata['user']['id']);
            
            for ($z=0; $z < count($this->data['anotacoes']); $z++) {    

                if ($this->data['anotacoes'][$z]->idAdminComentario == '0') {  
                    $this->data['eventos'][$a] = new stdClass;         
                    $this->data['eventos'][$a]->txtCategoria = 'Anotação do Usuário';
                    $this->data['eventos'][$a]->txtNome = $this->data['anotacoes'][$z]->txtDescricao;
                    $this->data['eventos'][$a]->txtDataVencimento = $this->data['anotacoes'][$z]->txtDataVencimento;
                    $a++;
                }
            }
            
        }elseif ($objData->valorCampo == 2) {
            $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->session->userdata['user']['id']);
            
            for ($z=0; $z < count($this->data['anotacoes']); $z++) { 

                if ($this->data['anotacoes'][$z]->idAdminComentario !== '0') {
                    $this->data['eventos'][$a] = new stdClass;
                    $this->data['eventos'][$a]->txtCategoria = 'Anotação do Administrador';
                    $this->data['eventos'][$a]->txtNome = $this->data['anotacoes'][$z]->txtDescricao;
                    $this->data['eventos'][$a]->txtDataVencimento = $this->data['anotacoes'][$z]->txtDataVencimento;
                    $a++;
                }
            }
        }elseif ($objData->valorCampo == 3) {
            $this->data['normativos']=  $this->normativo_model->get_normativos_dataVigencia($this->session->userdata['user']['id']); 
            
            for ($x=0; $x < count($this->data['normativos']); $x++) {
            $this->data['eventos'][$a] = new stdClass;  
                $this->data['eventos'][$a]->txtCategoria = 'Normativos Publicados';            
                $this->data['eventos'][$a]->txtNome = $this->data['normativo_model'][$x]->txtAssunto;
                $this->data['eventos'][$a]->txtDataVencimento = $this->data['normativo_model'][$x]->txtDataVigencia;
                $a++;
            }
            
        }elseif ($objData->valorCampo == 4) {
            $this->data['planosAcao'] = $this->normativo_model->get_planoAcao_user($this->session->userdata['user']['id']);
            
            for ($i=0; $i < count($this->data['planosAcao']); $i++) {
                $this->data['eventos'][$a] = new stdClass;
                $this->data['eventos'][$a]->txtCategoria = 'Plano de Ação do Usuário';
                $this->data['eventos'][$a]->txtNome = $this->data['planosAcao'][$i]->txtDescricao;
                $this->data['eventos'][$a]->txtDataVencimento = $this->data['planosAcao'][$i]->txtDataVencimento;
                $a++;
            }
            
        }elseif ($objData->valorCampo == 6) {
            $this->data['normativoPrazo']= $this->agenda_model->get_normativos_prazo();
            
            for ($i=0; $i < count($this->data['normativoPrazo']); $i++) {
                $this->data['eventos'][$a] = new stdClass;
                $this->data['eventos'][$a]->txtCategoria = 'Normativos com Prazo';
                $this->data['eventos'][$a]->txtNome = $this->data['normativoPrazo'][$i]->txtDescricao;
                $this->data['eventos'][$a]->txtObservacao = $this->data['normativoPrazo'][$i]->txtObservacao;
                $this->data['eventos'][$a]->txtDataVencimento = $this->data['normativoPrazo'][$i]->txtDataFim;
                $a++;
            }
            
        }else {
            $this->data['anotacoes']=  $this->agenda_model->get_anotacoes_user($this->session->userdata['user']['id']);

            for ($z=0; $z < count($this->data['anotacoes']); $z++) { 
                if (strpos($this->data['anotacoes'][$z]->txtDescricao, 'E-mails') !== false) {
                    $this->data['eventos'][$a] = new stdClass;
                    $this->data['eventos'][$a]->txtCategoria = 'Normativo Encaminhado';   
                    $this->data['eventos'][$a]->txtNome = $this->data['anotacoes'][$z]->txtDescricao;
                    $this->data['eventos'][$a]->txtDataVencimento = $this->data['anotacoes'][$z]->txtDataVencimento;
                    $a++;
                }
                    
            }

        }

        echo json_encode(array("eventos" => $this->data['eventos']));
        
    }

}
