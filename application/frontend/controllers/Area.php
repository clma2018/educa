<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Area extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('area_model');
        $this->load->model('anotacao_model');
        $this->load->model('user_model');
        $this->load->model('tema_model');
        $this->load->model('base_model');
        $this->load->model('instituicao_model');
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }


    public function index($idInstituicao = '') {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        if (!isset($this->session->userdata['user']['bitAdministrador'])){
            $this->data['areaTemas'] = $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao']);
        }else{
            $this->data['areaTemas'] = $this->area_model->get_area(0, $idInstituicao);
        }

        $temas =  array();
        $base =  array();

        for ($i=0; $i < count($this->data['areaTemas']); $i++) { 
            if (!isset($this->session->userdata['user']['bitAdministrador'])){
                $this->data['temasArea'][$i] = $this->area_model->get_temas_area($this->data['areaTemas'][$i]->id, $this->session->userdata['user']['idInstituicao']);   
            }else{
                $this->data['temasArea'][$i] = $this->area_model->get_temas_area($this->data['areaTemas'][$i]->id);
            }
            for ($z=0; $z < count($this->data['temasArea'][$i]); $z++) {    
                array_push($temas,$this->data['temasArea'][$i][$z]);
            }

            $b[$i] = $this->base_model->get_bases_area($this->data['areaTemas'][$i]->id);
            for ($xi=0; $xi < count($b[$i]); $xi++) { 
                array_push($base,$b[$i][$xi] );   
            }
        }

        $this->data['bases'] = $base;

        $this->data['temas'] = $temas;

        $this->data['idInstituicao'] = $idInstituicao;

        $this->template->showSite('list-areas', $this->data);    

    }

   

    public function edit_area($idArea = ''){
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        //TEMAS
        $this->data['subtemas']=  $this->tema_model->get_all_subtemas();
        $this->data['bases']=  $this->base_model->get_bases();
        
        if (isset($this->session->userdata['user']['txtBPO'])){
            if ($idArea != '') {
                $this->data['area'] =  $this->area_model->get_area($this->encrypt->decode($idArea));
                $this->data['temasArea'] =  $this->area_model->get_temas_area($this->encrypt->decode($idArea));
                    
                for($a = 0; $a < count($this->data['temasArea']); $a++){
                    if($this->data['temasArea']){
                        $base = $this->area_model->get_base_tema($this->data['temasArea'][$a]->idTema);
                        $this->data['temasArea'][$a]->txtBase =  $base[0]->txtNome;
                        $this->data['temasArea'][$a]->txtCor =   $base[0]->txtCorSecundaria;
                    }
                }

                $this->data['emailsArea'] =  $this->area_model->get_emails_area($this->encrypt->decode($idArea));
                $this->data['valorTemas'] = '';
                for($x = 0; $x < count($this->data['temasArea']); $x++){
                    if($this->data['temasArea']){
                        // if ($x < 1) {
                            // $this->data['valorTemas'] .= $this->data['temasArea'][$x]->idTema;
                        // }else{
                            $this->data['valorTemas'] .= '/'. $this->data['temasArea'][$x]->idTema;
                        // }
                    }
                }  
            }
        }

        $this->data['idArea'] = $idArea;

        $this->template->showSite('edit-area', $this->data);
    }

    public function config_area() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->id)) {

            $objUpdateArea = new stdClass();

            
            //Atualização dos dados da area no banco de dados
            $arrayCondition = array('id = ' . $this->encrypt->decode($objData->id));
            $objUpdateArea->txtArea = $objData->txtArea;
            if (isset($objData->txtDescricaoArea)) {
                $objUpdateArea->txtDescricaoArea = $objData->txtDescricaoArea;
            }
            
            if (isset($this->session->userdata['user']['idInstituicao'])) {
                $objUpdateArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
            }else{
                $objUpdateArea->idInstituicao = $objData->idInstituicao;
            }

            $query = $this->crud_model->update($objUpdateArea, 'tabarea', $arrayCondition);

            //Atualização dos dados area temas no banco de dados
            $arrayCondition2 = array('idArea = ' . $this->encrypt->decode($objData->id));
            $query2 = $this->crud_model->delete('tabareatemas', $arrayCondition2);

            //Atualização dos e-mails relacionados a àrea no banco de dados
            $arrayCondition3 = array('idArea = ' . $this->encrypt->decode($objData->id));
            $query3 = $this->crud_model->delete('tabareaemails', $arrayCondition3);

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = explode("/", $objData->txtSubTemas);
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros[$i]){
                    $objTemasArea = new stdClass();
                    $objTemasArea->idArea = $this->encrypt->decode($objData->id);
                    $objTemasArea->idTema = $arrayRegistros[$i];
                    if (isset($this->session->userdata['user']['idInstituicao'])) {
                        $objTemasArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
                    }else{
                        $objTemasArea->idInstituicao = $objData->idInstituicao;
                    }
                    $this->crud_model->insert('tabareatemas',$objTemasArea);
                    unset($objTemasArea);
                }
            }

            if (isset($objData->txtEmails)) {
                //Salvar os e-mails cadastados pelo usuário
                $txtEmailUsuarios[] = '';
                $array = explode(",",$objData->txtEmails);

                for ($ar=0; $ar < count($array); $ar++) { 
                    array_push($txtEmailUsuarios,$array[$ar]);
                }
                array_shift($txtEmailUsuarios);

                $arrayRegistros2 = $txtEmailUsuarios;
                
                for($z = 0; $z < count($arrayRegistros2); $z++){
                    if($arrayRegistros2[$z]){
                        $objEmailsArea = new stdClass();
                        $objEmailsArea->idArea = $this->encrypt->decode($objData->id);
                        $objEmailsArea->txtEmail = $arrayRegistros2[$z];
                        $objEmailsArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
                        $this->crud_model->insert('tabareaemails',$objEmailsArea);
                        unset($objEmailsArea);
                    }
                }  
            }  

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Área editada com sucesso. ', 'insert'=> false));
        }else{
            $objInsertArea = new stdClass();
            
            //Inserção dos dados da área no banco de dados
            $objInsertArea->txtArea = $objData->txtArea;
             if (isset($objData->txtDescricaoArea)) {
                $objInsertArea->txtDescricaoArea = $objData->txtDescricaoArea;
            }

            if (isset($this->session->userdata['user']['idInstituicao'])) {
                $objInsertArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
            }else{
                $objInsertArea->idInstituicao = $objData->idInstituicao;
            }
            

            $area = $this->crud_model->insert('tabarea',$objInsertArea);

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = explode("/", $objData->txtSubTemas);
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros){
                    $objTemasArea = new stdClass();
                    $objTemasArea->idArea = $area->id;
                    $objTemasArea->idTema = $arrayRegistros[$i];
                    if (isset($this->session->userdata['user']['idInstituicao'])) {
                        $objTemasArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
                    }else{
                        $objTemasArea->idInstituicao = $objData->idInstituicao;
                    }
                    $this->crud_model->insert('tabareatemas',$objTemasArea);
                    unset($objTemasArea);
                }
            }


            if (isset($objData->txtEmails)) {
                //Salvar os e-mails cadastados pelo usuário
                $txtEmailUsuarios[] = '';
                $array = explode(",",$objData->txtEmails);

                for ($ar=0; $ar < count($array); $ar++) { 
                    array_push($txtEmailUsuarios,$array[$ar]);
                }
                array_shift($txtEmailUsuarios);

                $arrayRegistros2 = $txtEmailUsuarios;
                
                for($z = 0; $z < count($arrayRegistros2); $z++){
                    if($arrayRegistros2[$z]){
                        $objEmailsArea = new stdClass();
                        $objEmailsArea->idArea = $area->id;
                        $objEmailsArea->txtEmail = $arrayRegistros2[$z];
                        $objEmailsArea->idInstituicao = $this->session->userdata['user']['idInstituicao'];
                        $this->crud_model->insert('tabareaemails',$objEmailsArea);
                        unset($objEmailsArea);
                    }
                }  
            }


            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Área inserida com sucesso. ', 'insert'=> true));
        }

    }

    public function list_areas_normativos() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        //TEMAS
        $this->data['subtemas']=  $this->tema_model->get_all_subtemas();

        $this->data['bases']=  $this->base_model->get_bases();

        $this->data['areaTemas'] = $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao']);
        $this->data['temasArea'] = $this->area_model->get_temas_area(0, $this->session->userdata['user']['idInstituicao']);
        $this->data['normativosArea'] = $this->normativo_model->get_normativo_idArea('', 0, array('1','2'), $this->session->userdata['user']['idInstituicao']);

        for ($i=0; $i < count($this->data['areaTemas']); $i++) { 
           $this->data['areaTemas'][$i]->qtdNormativos = count($this->normativo_model->get_normativo_idArea('',$this->data['areaTemas'][$i]->id));
        }   

        for ($i=0; $i < count($this->data['normativosArea']); $i++) { 
            $qtd = count($this->anotacao_model->get_normativo_comentario_feeder($this->data['normativosArea'][$i]->id));
            $comentario = $this->anotacao_model->get_normativo_comentario_feeder($this->data['normativosArea'][$i]->id);
            if ($qtd > 0) {
                $this->data['normativosArea'][$i]->txtBG = 'background-color:#b9e8b9';
                $this->data['normativosArea'][$i]->txtComentario = $comentario[0]->txtComentario;
            }else{
                $this->data['normativosArea'][$i]->txtBG = '';
            }
        }   

        $objInsertLog = new stdClass();

        $objInsertLog->idUsuario = $this->session->userdata['user']['id']; 
        $objInsertLog->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $this->crud_model->insert('tablogacessolistnormativo',$objInsertLog);

        $this->template->showSite('list-area-normativo', $this->data); 
    }


    public function admin_acoes() {
        if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('dashboard', 'refresh');

        //TEMAS
        $this->data['subtemas']=  $this->tema_model->get_all_subtemas();

        $this->data['bases']=  $this->base_model->get_bases();

        $this->data['areaTemas'] = $this->area_model->get_area();
        $this->data['temasArea'] = $this->area_model->get_temas_area(0);
        $this->data['normativosArea'] = $this->normativo_model->get_normativo_idArea();

        $this->data['idAreas'] = '';

        for ($i=0; $i < count($this->data['areaTemas']); $i++) { 

            $this->data['txtModelo'] = $this->instituicao_model->get_instituicao($this->data['areaTemas'][$i]->idInstituicao);
            if ($this->data['txtModelo'][0]->txtModelo == 3) {
                $this->data['idAreas'][$i] = $this->data['areaTemas'][$i]->id;
            }

           $this->data['areaTemas'][$i]->qtdNormativos = count($this->normativo_model->get_normativo_idArea('',$this->data['areaTemas'][$i]->id));
        }   

        $this->data['idAreas'] = array_values($this->data['idAreas']);

        $this->template->showSite('admin-acoes', $this->data); 
    }

    public function filtrar_temas() {
       
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['temasBase'] = $this->base_model->get_temas_base($objData->txtBase);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "temas" => $this->data['temasBase']));
    }

    public function list_base_normativos() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        //TEMAS
        $this->data['areaTemas'] = $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao']);
        
        $this->data['bases'] = $this->normativo_model->get_bases();

        $this->data['basesSistema'] = $this->base_model->get_bases();

        $mes = date('Y-m');
        $dataInicio = new DateTime("first day of ".$mes."");
        $dataInicio = str_replace('-', '', $dataInicio->format('Y-m-d'));
        $dataFim = new DateTime("last day of ".$mes."");
        $dataFim = str_replace('-', '', $dataFim->format('Y-m-d'));

        // DataInicial 20170305 DataFinal 20170311
        for ($i=0; $i < count($this->data['bases']); $i++) { 
           $this->data['bases'][$i]->qtdTemas = count($this->normativo_model->get_bases_temas($this->data['bases'][$i]->txtOrigem));
           $this->data['bases'][$i]->qtdNormativos = count($this->normativo_model->get_normativo_base($this->data['bases'][$i]->txtOrigem, $dataInicio, $dataFim));
           if (isset($this->data['basesSistema'][$i])) {
               $this->data['bases'][$i]->txtCor = $this->data['basesSistema'][$i]->txtCorSecundaria;
           }

        }   

        $this->data['normativosBase'] = $this->normativo_model->get_normativo_base('',$dataInicio, $dataFim);

        $this->template->showSite('list-base-normativo', $this->data); 
    }

    public function change_status() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateArea = new stdClass();
        $objData = (object)$_POST;

        $novoStatus = '';
        if ($objData->bitStatus == 0) {
            $novoStatus = 1;
        }else{
            $novoStatus = 0;
        }

        $arrayCondition = array('id = ' . (int)($objData->id));
        $objUpdateArea->bitStatus = $novoStatus;

        $query = $this->crud_model->update($objUpdateArea, 'tabarea', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }


    public function list_normativos_area() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['bases']=  $this->base_model->get_bases();

        $this->data['temasArea'] = $this->area_model->get_temas_area(0, $this->session->userdata['user']['idInstituicao']);

        $this->data['normativosArea'] = $this->normativo_model->get_normativo_idArea('', 0, array('1','2','3','4'), $this->session->userdata['user']['idInstituicao']);

        $this->template->showSite('list-normativos-area', $this->data); 
    }

    public function list_respostas_normativos() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['areas'] = $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao']);

        $this->data['respostas'] = $this->area_model->get_respostas_area(0, $this->session->userdata['user']['idInstituicao']);

        $this->template->showSite('list-respostas-normativos', $this->data); 
    }

    public function filtrar_usuarios_area(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['usuarios'] = $this->area_model->get_emails_area($objData->txtArea);

        $txtEmailUsuarios[] = '';


        for ($ar=0; $ar < count($this->data['usuarios']); $ar++) { 
            array_push($txtEmailUsuarios,$this->data['usuarios'][$ar]->txtEmail);
        }

        array_shift($txtEmailUsuarios);

        $txtEmailUsuarios = $txtEmailUsuarios;
        
        echo json_encode(array("usuarios" => $txtEmailUsuarios));
    }

    public function filtrar_normativo_area(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['normativos'] = $this->area_model->get_normativos_email(trim($objData->txtUsuario)); 

        echo json_encode(array("normativos" => $this->data['normativos']));
        
    }

    public function salvar_resposta(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;
        $objInsertResposta = new stdClass();


        $objInsertResposta->txtArea = $objData->txtArea; 
        $objInsertResposta->txtUsuario = $objData->txtUsuario; 
        $objInsertResposta->txtNormativo = $objData->txtNormativo;
        $objInsertResposta->txtResposta = $objData->txtResposta;
        
        $this->crud_model->insert('tabrespostanormativoarea',$objInsertResposta);

        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }

}
