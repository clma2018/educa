<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Crons extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('agenda_model');
        $this->load->model('area_model');
        $this->load->model('base_model');
        $this->load->model('logs_model');
        $this->load->model('user_model');
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }


    //ATUALIZA A BASE DO USUÁRIO CONFORME AS CONFIGURAÇÕES DOS TEMAS 
    public function new_user_normativo() {

        set_time_limit(0);

        $this->load->model('normativo_model');
    
        // $this->data['temasUsuario'] = $this->user_model->get_temas_user();    

        // $arrayidUsuarios = array();
        // $arrayidNormativos = array();
        // $arraydateCadastro = array(); 
        // $arraytxtTitulos = array(); 
        // $arraytxtLinks = array(); 
        // $arraytxtClasses = array(); 
        // $arraytxtAssuntos = array(); 
        // $arraytxtRelevantes = array();
        // $arraytxtOrigens = array();
        // $arraytxtRevogado = array();
       
        // for ($z=0; $z < count($this->data['temasUsuario']); $z++) { 
        //     $this->data['normativosTemas'] = $this->normativo_model->get_normativos_tema($this->data['temasUsuario'][$z]->txtTitulo);
        //     for ($x=0; $x < count($this->data['normativosTemas']); $x++) { 
        //         array_push($arrayidUsuarios, $this->data['temasUsuario'][$z]->idUsuario);
        //         array_push($arrayidNormativos, $this->data['normativosTemas'][$x]->idNorma);
        //         array_push($arraydateCadastro, $this->data['normativosTemas'][$x]->DataPubli);
        //         array_push($arraytxtTitulos, $this->data['normativosTemas'][$x]->Titulo);
        //         array_push($arraytxtLinks, $this->data['normativosTemas'][$x]->link);
        //         array_push($arraytxtClasses, $this->data['normativosTemas'][$x]->Classe);
        //         array_push($arraytxtAssuntos, $this->data['normativosTemas'][$x]->Assunto);
        //         array_push($arraytxtOrigens, $this->data['normativosTemas'][$x]->txtOrigem);
        //         array_push($arraytxtRevogado, $this->data['normativosTemas'][$x]->Revogado);
        //     }
        // }


        // $valido = 0;    
        // for($i = 0; $i < count($arrayidNormativos); $i++){
        //     $arrData = explode("/", $arraydateCadastro[$i]);
        //     $newDate = $arrData[2].'-'.$arrData[1].'-'.$arrData[0];
        //     $today_dt = new DateTime('now');
        //     $expire_dt = new DateTime($newDate);
        //     $validade_normativo =  $today_dt->diff($expire_dt, true);
        //     if($arrayidNormativos){
        //         if ($validade_normativo->days < 180) {
        //             $qe = 'qe'.$i; 
        //             $qe = new stdClass();
        //             $qe = $this->normativo_model->get_normativo_id($arrayidNormativos[$i], $arrayidUsuarios[$i]);
        //             if ($qe) {        
        //                 $valido = 0;
        //             }
        //             else{
        //                 $objInsertNormativo = new stdClass();
        //                 $objInsertNormativo->idNormativo = $arrayidNormativos[$i];
        //                 $objInsertNormativo->dateCadastro = $arraydateCadastro[$i];
        //                 $objInsertNormativo->txtTitulo = $arraytxtTitulos[$i];
        //                 $objInsertNormativo->txtLink = $arraytxtLinks[$i];
        //                 $objInsertNormativo->txtClasse = $arraytxtClasses[$i];
        //                 $objInsertNormativo->txtAssunto = $arraytxtAssuntos[$i];
        //                 $objInsertNormativo->txtOrigem = $arraytxtOrigens[$i];
        //                 // $objInsertNormativo->txtRelevante = $arraytxtRelevantes[$i];
        //                 $objInsertNormativo->txtRelevante = "Sim";
        //                 $objInsertNormativo->idUsuario = $arrayidUsuarios[$i];
        //                 $objInsertNormativo->bitCiente = 2;
        //                 $objInsertNormativo->txtDataVigencia = $arraydateCadastro[$i];
        //                 $objInsertNormativo->txtTipoAcao = 1;
        //                 $objInsertNormativo->txtRevogado = $arraytxtRevogado[$i];

        //                 $query = $this->crud_model->insert('tabnormativousuario', $objInsertNormativo);
        //                 unset($objInsertNormativo);
        //                 $valido = 1;
        //             }
        //             unset($qe);
        //         }
        //     }
        // }   


        $day = date('w');
        // $dataInicio = date('Y-m-d', strtotime('-'.$day.' days'));
        $dataInicio = date('Y-m-d', strtotime('-'.(30-$day).' days'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('+'.(6-$day).' days'));
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativos'] = $this->normativo_model->get_normativo_base('',$dataInicio, $dataFim);    

        $this->data['temasUsuario'] = $this->user_model->get_temas_user();   

        $this->data['usuarios'] = $this->user_model->get_users();

        $arrayidNormativos = array();
        $arraydateCadastro = array(); 
        $arraytxtTitulos = array(); 
        $arraytxtLinks = array(); 
        $arraytxtClasses = array(); 
        $arraytxtAssuntos = array(); 
        $arraytxtRelevantes = array();
        $arraytxtOrigens = array();
        $arraytxtRevogado = array();
       
        for ($z=0; $z < count($this->data['normativos']); $z++) { 
            for ($i=0; $i < count($this->data['usuarios']); $i++) { 
                if ($this->data['usuarios'][$i]->txtModelo == 1) {
                    
                    $this->data['temasUsuario'] = $this->user_model->get_users_tema($this->data['usuarios'][$i]->id); 

                    for ($ai=0; $ai < count($this->data['temasUsuario']); $ai++) { 
                        
                        if ($this->data['temasUsuario'][$ai]->txtTitulo == $this->data['normativos'][$z]->Classe) { 

                            $qe = 'qe'.$i; 
                            $qe = new stdClass();
                            $qe = $this->normativo_model->get_normativo_id($this->data['normativos'][$z]->idNorma, $this->data['usuarios'][$i]->id);

                            if ($qe) {        
                                $valido = 0;
                            }
                            else{
                                $objInsertNormativo = new stdClass();
                                $objInsertNormativo->idNormativo = $this->data['normativos'][$z]->idNorma;
                                $objInsertNormativo->dateCadastro = $this->data['normativos'][$z]->DataPubli;
                                $objInsertNormativo->txtTitulo = $this->data['normativos'][$z]->Titulo;
                                $objInsertNormativo->txtLink = $this->data['normativos'][$z]->link;
                                $objInsertNormativo->txtClasse = $this->data['normativos'][$z]->Classe;
                                $objInsertNormativo->txtAssunto = $this->data['normativos'][$z]->Assunto;
                                $objInsertNormativo->txtOrigem = $this->data['normativos'][$z]->txtOrigem;
                                $objInsertNormativo->txtRelevante = "Sim";
                                $objInsertNormativo->idUsuario = $this->data['usuarios'][$i]->id;
                                $objInsertNormativo->bitCiente = 2;
                                $objInsertNormativo->txtDataVigencia = $this->data['normativos'][$z]->DataPubli;
                                $objInsertNormativo->txtTipoAcao = 1;
                                $objInsertNormativo->txtRevogado = $this->data['normativos'][$z]->Revogado;


                                $query = $this->crud_model->insert('tabnormativousuario', $objInsertNormativo);
                                unset($objInsertNormativo);
                                $valido = 1;
                            }
                            unset($qe);
                        }
                    }
                }
            }   

        }

        // for($i = 0; $i < count($arrayidNormativos); $i++){
        //     $qe = 'qe'.$i; 
        //     $qe = new stdClass();
        //     $qe = $this->normativo_model->get_normativo_id($arrayidNormativos[$i], $arrayidUsuarios[$i]);
        //     if ($qe) {        
        //         $valido = 0;
        //     }
        //     else{
        //         $objInsertNormativo = new stdClass();
        //         $objInsertNormativo->idNormativo = $arrayidNormativos[$i];
        //         $objInsertNormativo->dateCadastro = $arraydateCadastro[$i];
        //         $objInsertNormativo->txtTitulo = $arraytxtTitulos[$i];
        //         $objInsertNormativo->txtLink = $arraytxtLinks[$i];
        //         $objInsertNormativo->txtClasse = $arraytxtClasses[$i];
        //         $objInsertNormativo->txtAssunto = $arraytxtAssuntos[$i];
        //         $objInsertNormativo->txtOrigem = $arraytxtOrigens[$i];
        //         $objInsertNormativo->txtRelevante = "Sim";
        //         $objInsertNormativo->idUsuario = $arrayidUsuarios[$i];
        //         $objInsertNormativo->bitCiente = 2;
        //         $objInsertNormativo->txtDataVigencia = $arraydateCadastro[$i];
        //         $objInsertNormativo->txtTipoAcao = 1;
        //         $objInsertNormativo->txtRevogado = $arraytxtRevogado[$i];

        //         $query = $this->crud_model->insert('tabnormativousuario', $objInsertNormativo);
        //         unset($objInsertNormativo);
        //         $valido = 1;
        //     }
        //     unset($qe);
        // }


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, 'valido'=>$valido));
    }

    //NOTIFIÇÃO DOS PLANOS DE ACAO COM VENCIMENTO PARA ESTÁ SEMANA (ADMIN)
    public function notificacao_planoAcao(){

        $day = date('w');
        $dataInicio = date('Y-m-d', strtotime('-'.($day).' days'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('+'.(1+$day).' days'));
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativosPlanoAcao'] = $this->normativo_model->get_planoAcao_atrasado($dataInicio, $dataFim);        
    
        $idAdministrador = [];
        $txtEmailAdmin = [];
        $txtNomeAdmin = [];
        $txtNomeUsuario = [];
        $txtTituloNormativo = [];
        $dataVencimentoPA = [];
        $txtLink = [];


        for($i = 0; $i < count($this->data['normativosPlanoAcao']); $i++){
            $this->data['usuarioAdmin'] = $this->user_model->get_users($this->data['normativosPlanoAcao'][$i]->idAdministrador);

            array_push($idAdministrador, $this->data['usuarioAdmin'][0]->id);
            array_push($txtNomeUsuario, $this->data['normativosPlanoAcao'][$i]->txtNome);
            array_push($txtTituloNormativo, $this->data['normativosPlanoAcao'][$i]->txtTitulo);
            array_push($dataVencimentoPA, $this->data['normativosPlanoAcao'][$i]->txtDataVencimento);
            array_push($txtLink, $this->config->base_url().'normativo/visualizar_normativo/'.$this->encrypt->encode($this->data['normativosPlanoAcao'][$i]->idNormativo));
            array_push($txtEmailAdmin, $this->data['usuarioAdmin'][0]->txtEmail);
            array_push($txtNomeAdmin, $this->data['usuarioAdmin'][0]->txtNome);
        };
 
        
        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);


        if (count($idAdministrador) > 0) {
            $normativos = '';
            
            for ($i=0; $i < count($idAdministrador); $i++) { 
                $normativos .= '<table align="left" style="width:100%;margin-bottom:20px;"><tr><td colspan="2"><hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;"></td></tr><tr><td align="left" valign="top" colspan="2" style="padding: 10px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:22px;line-height:23px;color: #404040;font-weight: 600;margin:0px 0px 15px!important;">'.$txtTituloNormativo[$i].'</p></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;margin: 0px"> Responsável: '.$txtNomeUsuario[$i].'</p></td><td align="center"><a href="'.$txtLink[$i].'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;font-family: Helvetica, Arial, sans-serif;font-weight: bold;display: block;width: 150px;margin:auto;position:relative;top:10px;">Acessar Normativo</a></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px" colspan="2"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;font-weight: 600;margin-top: 0px">Data de Vencimento: '.$dataVencimentoPA[$i].'</p></td></tr></table>';
            }            
                
            $arrEmails = array_unique($txtEmailAdmin);
            $arrNomes = array_unique($txtNomeAdmin);

            for ($i=0; $i <count($arrEmails) ; $i++) { 
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'plano-acao-vencimento1dia'],
                    'substitution_data' => [
                        'txtNomeAdmin' => $arrNomes[$i],
                        'txtNormativos' => $normativos
                    ],
                    'recipients' => [
                        [
                           'address' => [
                                'email' => $arrEmails[$i],
                            ],
                        ],
                    ],
                ]);
            }


            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                // echo $e->getCode()."\n";
                // echo $e->getMessage()."\n";
            }

        }
   

        echo json_encode(array("msg" => 'success', 'validate'=>true));     
         
    }

    //NOTIFIÇÃO DOS NORMATIVOS DA SEMANA QUE AINDA NÃO FORAM LIDOS (USUÁRIO)
    public  function notificacao_normativo(){

        $day = date('w');
        $dataInicio = date('Y-m-d', strtotime('monday last week'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('sunday last week'));
        $dataFim = str_replace('-', '', $dataFim);


        $this->data['normativos'] = $this->normativo_model->get_normativos_semana($dataInicio, $dataFim);        

        $txtNomeUsuario = [];
        $txtOrigem = [];
        $txtEmailUsuario = [];
        $txtTituloNormativo = [];
        $dataPubli = [];
        $txtLink = [];

        for($i = 0; $i < count($this->data['normativos']); $i++){
            array_push($txtNomeUsuario, $this->data['normativos'][$i]->txtNome);
            array_push($txtEmailUsuario, $this->data['normativos'][$i]->txtEmail);
            array_push($txtTituloNormativo, $this->data['normativos'][$i]->txtTitulo);
            array_push($dataPubli, $this->data['normativos'][$i]->dateCadastro);
            array_push($txtOrigem, $this->data['normativos'][$i]->txtOrigem);
            array_push($txtLink, $this->data['normativos'][$i]->txtLink);
        };

        
        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);

        if (count($txtNomeUsuario) > 0) {
            $normativos = '';
            
            for ($i=0; $i < count($txtNomeUsuario); $i++) { 
                $normativos .= '<table align="left" style="width:100%;margin-bottom:0px;"><tr><td colspan="2"><hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;"></td></tr><tr><td align="left" valign="top" colspan="2" style="padding: 10px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:22px;line-height:23px;color: #404040;font-weight: 600;margin:0px 0px 15px!important;">'.$txtTituloNormativo[$i].'</p></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;margin: 0px;font-weight: 600;"> Origem: '.$txtOrigem[$i].'</p></td><td align="center"><a href="'.$txtLink[$i].'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;font-family: Helvetica, Arial, sans-serif;font-weight: bold;display: block;width: 150px;margin:auto;position:relative;top:10px;">Acessar Normativo</a></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px" colspan="2"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;font-weight: 600;margin-top: 0px">Data de Publicação: '.$dataPubli[$i].'</p></td></tr></table>';
            }            
                
            $arrEmails = array_values(array_unique($txtEmailUsuario));
            $arrNomes = array_values(array_unique($txtNomeUsuario));

            
            for ($i=0; $i <count($arrEmails) ; $i++) { 
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'normativos-semana'],
                    'substitution_data' => [
                        'txtNomeAdmin' => $arrNomes[$i],
                        'txtNormativos' => $normativos
                    ],
                    'recipients' => [
                        [
                           'address' => [
                                'email' => $arrEmails[$i],
                            ],
                        ],
                    ],
                ]);
            }


            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                // echo $e->getCode()."\n";
                // echo $e->getMessage()."\n";
            }

        }
   

        echo json_encode(array("msg" => 'success', 'validate'=>true));    


    }

    //ALTERAÇÃO DO STATUS DO PLANO DE AÇÃO (USUÁRIO)
    public function alterar_status_planoAcao(){

        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativosPlanoAcao'] = $this->normativo_model->get_planoAcao_atrasado($dataFim, $dataFim);        

        for($i = 0; $i < count($this->data['normativosPlanoAcao']); $i++){
            if ($this->data['normativosPlanoAcao'][$i]->txtStatus == 'Ativo') {
                $objUpdatePA = new stdClass();
                $arrayCondition = array('id = ' . (int)($this->data['normativosPlanoAcao'][$i]->id));
                $objUpdatePA->txtStatus = 'Atrasado Data de Vencimento';

                $query = $this->crud_model->update($objUpdatePA, 'tabplanoacao', $arrayCondition);
            }
        };
 
   
        echo json_encode(array("msg" => 'success', 'validate'=>true));     
         
    }

    //ATUALIZA A BASE POR AREA CONFORME AS CONFIGURAÇÕES DOS TEMAS 
    public function atualiza_normativo_area() {

        $objData = new stdClass();
        $objData = (object)$_POST;

        set_time_limit(0);

        date_default_timezone_set('America/Sao_Paulo');

        $this->load->model('area_model');
        $this->load->model('normativo_model');
        

        $this->data['area'] = $this->area_model->get_area(0, 0);

        $idAreas = [];
        
        for ($a=0; $a < count($this->data['area']); $a++) { 
            array_push($idAreas, $this->data['area'][$a]->id);    
        }
            
        // $idAreas = $idAreas;
        $arrayidNormativos = array();
        $arraydateCadastro = array(); 
        $arraytxtTitulos = array(); 
        $arraytxtLinks = array(); 
        $arraytxtClasses = array(); 
        $arraytxtAssuntos = array(); 
        $arraytxtRelevantes = array();
        $arraytxtOrigens = array();
        $arraytxtTipoNorma = array();
        $arraytxtNumero = array();
        $arrayidArea = array();
        $arrayidInstituicao = array();

        for ($z=0; $z < count($idAreas); $z++) {
 
            $this->data['temasArea'] = $this->area_model->get_temas_area($idAreas[$z]);  
            for ($a=0; $a < count($this->data['temasArea']); $a++) { 
                $this->data['normativosTemas'] = $this->normativo_model->get_normativos_tema($this->data['temasArea'][$a]->txtTitulo);
                for ($x=0; $x < count($this->data['normativosTemas']); $x++) { 
                    array_push($arrayidNormativos, $this->data['normativosTemas'][$x]->idNorma);
                    array_push($arraydateCadastro, $this->data['normativosTemas'][$x]->DataPubli);
                    array_push($arraytxtTitulos, $this->data['normativosTemas'][$x]->Titulo);
                    array_push($arraytxtLinks, $this->data['normativosTemas'][$x]->link);
                    array_push($arraytxtClasses, $this->data['normativosTemas'][$x]->Classe);
                    array_push($arraytxtAssuntos, $this->data['normativosTemas'][$x]->Assunto);
                    array_push($arraytxtOrigens, $this->data['normativosTemas'][$x]->txtOrigem);
                    array_push($arraytxtTipoNorma, $this->data['normativosTemas'][$x]->TipoNorma);
                    array_push($arraytxtNumero, $this->data['normativosTemas'][$x]->Numero);
                    array_push($arrayidArea, $idAreas[$z]);
                    array_push($arrayidInstituicao, $this->data['temasArea'][$a]->idInstituicao);
                }
            }
        }

        for($i = 0; $i < count($arrayidNormativos); $i++){
            $arrData = explode("/", $arraydateCadastro[$i]);
            $newDate = $arrData[2].'-'.$arrData[1].'-'.$arrData[0];
            $today_dt = new DateTime('now');
            $expire_dt = new DateTime($newDate);
            $validade_normativo =  $today_dt->diff($expire_dt, true);

            if($arrayidNormativos){
                if ($validade_normativo->days < 90) {
                    $qe = new stdClass();
                    $qe = $this->normativo_model->get_normativo_idArea($arrayidNormativos[$i], $arrayidArea[$i], array('1','2','4'));
                    if ($qe) {        
                 
                 
                    }
                    else{
                        $objInsertNormativo = new stdClass();
                        $objInsertNormativo->idNormativo = $arrayidNormativos[$i];
                        $objInsertNormativo->dateCadastro = $arraydateCadastro[$i];
                        $objInsertNormativo->txtTitulo = $arraytxtTitulos[$i];
                        $objInsertNormativo->txtLink = $arraytxtLinks[$i];
                        $objInsertNormativo->txtClasse = $arraytxtClasses[$i];
                        $objInsertNormativo->txtAssunto = $arraytxtAssuntos[$i];
                        $objInsertNormativo->txtOrigem = $arraytxtOrigens[$i];
                        $objInsertNormativo->txtRelevante = "Sim";
                        $objInsertNormativo->idArea = $arrayidArea[$i];
                        $objInsertNormativo->bitCiente = 2;
                        $objInsertNormativo->txtTipoNorma = $arraytxtTipoNorma[$i];
                        $objInsertNormativo->txtNumero = $arraytxtNumero[$i];
                        $objInsertNormativo->idInstituicao = $arrayidInstituicao[$i];

                        $query = $this->crud_model->insert('tabnormativoarea', $objInsertNormativo);
                        unset($objInsertNormativo);
                    }
                    unset($qe);
                }
            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }




    //NOTIFIÇÃO DOS PLANOS DE ACAO COM VENCIMENTO PARA ESTÁ SEMANA (USER)
    public function notificacao_planoAcao_user(){

        $day = date('w');
        $dataInicio = date('Y-m-d', strtotime('-'.($day).' days'));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d', strtotime('+'.(2+$day).' days'));
        $dataFim = str_replace('-', '', $dataFim);

        $this->data['normativosPlanoAcao'] = $this->normativo_model->get_planoAcao_atrasado($dataInicio, $dataFim);        
    
        
        $txtEmailUsuario = [];
        $txtNomeUsuario = [];
        $txtTituloNormativo = [];
        $dataVencimentoPA = [];
        $txtLink = [];

        for($i = 0; $i < count($this->data['normativosPlanoAcao']); $i++){

            array_push($txtEmailUsuario, $this->data['normativosPlanoAcao'][$i]->txtEmail);
            array_push($txtNomeUsuario, $this->data['normativosPlanoAcao'][$i]->txtNome);
            array_push($txtTituloNormativo, $this->data['normativosPlanoAcao'][$i]->txtTitulo);
            array_push($dataVencimentoPA, $this->data['normativosPlanoAcao'][$i]->txtDataVencimento);
            array_push($txtLink, $this->config->base_url().'normativo/visualizar_normativo/'.$this->encrypt->encode($this->data['normativosPlanoAcao'][$i]->idNormativo));
            
        };

        
        // //Disparo do e-mail
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);


        if (count($txtEmailUsuario) > 0) {
            $normativos = '';
            
            for ($i=0; $i < count($txtEmailUsuario); $i++) { 
                $normativos .= '<table align="left" style="width:100%;margin-bottom:20px;"><tr><td colspan="2"><hr style="width:100%;border:0;border-top:1px solid #CCCCCC;margin-top: 25px;margin-bottom: 25px;"></td></tr><tr><td align="left" valign="top" colspan="2" style="padding: 10px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:22px;line-height:23px;color: #404040;font-weight: 600;margin:0px 0px 15px!important;">'.$txtTituloNormativo[$i].'</p></td></tr><tr><td align="left" valign="top" style="padding: 0px 15px;"><p style="font-family:Ubuntu, sans-serif;font-size:14px;line-height:16px;color: #404040;font-weight: 600;margin-top: 10px">Data de Vencimento: '.$dataVencimentoPA[$i].'</p></td><td align="center"><a href="'.$txtLink[$i].'" target="_blank" style="text-transform: uppercase;padding: 10px 5px;border: 1px solid #4383ae; border-radius: 5px;color: #4383ae;text-decoration: none;text-align: center;display: block;font-size:10px;margin: 15px 0 30px;font-family: Helvetica, Arial, sans-serif;font-weight: bold;display: block;width: 150px;margin:auto;position:relative;top:10px;">Acessar Normativo</a></td></tr></table>';
            }            
                
            $arrEmails = array_unique($txtEmailUsuario);
            $arrNomes = array_unique($txtNomeUsuario);

            for ($i=0; $i <count($arrEmails) ; $i++) { 
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'plano-acao-vencimento1dia'],
                    'substitution_data' => [
                        'txtNomeAdmin' => $arrNomes[$i],
                        'txtNormativos' => $normativos
                    ],
                    'recipients' => [
                        [
                           'address' => [
                                'email' => $arrEmails[$i],
                            ],
                        ],
                    ],
                ]);
            }


            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                echo $e->getCode()."\n";
                echo $e->getMessage()."\n";
            }

        }
   

        echo json_encode(array("msg" => 'success', 'validate'=>true));     
         
    }

}
