<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Base extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('base_model');
        $this->load->model('tema_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');


        $this->data['bases']=  $this->base_model->get_bases();

        for ($i=0; $i < count($this->data['bases']); $i++) { 
            $this->data['bases'][$i]->qtdTemas =  count($this->base_model->get_temas_base($this->data['bases'][$i]->id)); 
        }


        $this->template->showSite('list-bases', $this->data);    

    }

    public function edit_base($idBase = ''){
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        $this->data['temas'] =  $this->tema_model->get_all_subtemas(); 
        
        if ($idBase != '') {
            //TEMAS
            $this->data['base']=  $this->base_model->get_bases($this->encrypt->decode($idBase));
        
            $this->data['temasBase'] =  $this->base_model->get_temas_base($this->encrypt->decode($idBase)); 

            $this->data['valorTemas'] = '';
            for($x = 0; $x < count($this->data['temasBase']); $x++){
                if($this->data['temasBase']){
                    // if ($x < 1) {
                        // $this->data['valorTemas'] .= $this->data['temasBase'][$x]->idTema;
                    // }else{
                        $this->data['valorTemas'] .= '/'. $this->data['temasBase'][$x]->idTema;
                    // }
                }
            }
        }

        $this->template->showSite('edit-base', $this->data);
    }

    public function config_base() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;


        if (isset($objData->id)) {

            $objUpdateBase = new stdClass();

            $objData->txtSubTemas = ltrim($objData->txtSubTemas, '/');
                     
            //Atualização dos dados da area no banco de dados
            $arrayCondition = array('id = ' . $this->encrypt->decode($objData->id));
            $objUpdateBase->txtNome = $objData->txtNome;
            $objUpdateBase->txtCorPrimaria = $objData->txtCorPrimaria;
            $objUpdateBase->txtCorSecundaria = $objData->txtCorSecundaria;

            $query = $this->crud_model->update($objUpdateBase, 'tabbase', $arrayCondition);

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = explode("/", $objData->txtSubTemas);
            
            $this->data['temasBase'] =  $this->base_model->get_temas_base($this->encrypt->decode($objData->id)); 

            $arrayidTemas = array();
            for ($i=0; $i < count($this->data['temasBase']); $i++) { 
                array_push($arrayidTemas, $this->data['temasBase'][$i]->idTema);
            }   
            
            for($i = 0; $i < count($arrayidTemas); $i++){
                if(!in_array($arrayidTemas[$i], $arrayRegistros)){
                    $arrayCondition2 = array('idTema = ' . $arrayidTemas[$i]);
                    $query2 = $this->crud_model->delete('tabtemasbase', $arrayCondition2);
                }
            }
       
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros[$i]){
                    $qe = new stdClass();
                    $qe = $this->base_model->get_subtema_base($arrayRegistros[$i]);
                    if (count($qe) > 0) {        
                        $valido = 0;
                    }else{
                        $objTemasBase = new stdClass();
                        $objTemasBase->idBase = $this->encrypt->decode($objData->id);
                        $objTemasBase->idTema = $arrayRegistros[$i];
                        $this->crud_model->insert('tabtemasbase',$objTemasBase);
                        unset($objTemasBase);
                    }
                    unset($qe);
                }
            }

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Base editada com sucesso. ', 'insert'=> false));
        }else{
            $objInsertBase = new stdClass();
            
            //Inserção dos dados da área no banco de dados
            $objInsertBase->txtNome = $objData->txtNome;
            $objInsertBase->txtCorPrimaria = $objData->txtCorPrimaria;
            $objInsertBase->txtCorSecundaria = $objData->txtCorSecundaria;

            $base = $this->crud_model->insert('tabbase',$objInsertBase);

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = explode("/", $objData->txtSubTemas);
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros[$i]){
                    $objTemasBase = new stdClass();
                    $objTemasBase->idBase = $base->id;
                    $objTemasBase->idTema = $arrayRegistros[$i];
                    $this->crud_model->insert('tabtemasbase',$objTemasBase);
                    unset($objTemasBase);
                }
            }

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Base inserida com sucesso. ', 'insert'=> true));
        }

    }


}
