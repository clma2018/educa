<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');

        // Library
        $this->load->library('encryption');

        // Helper
        $this->load->helper('security');
    }

    public function index() {
        
        $this->template->showSite('index');
    }

    public function validar_login(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $objData->txtSenha = do_hash(do_hash($objData->txtSenha, 'md5'));


        if (strpos($objData->txtEmail, '@') !== false) {
            $query = $this->user_model->validate_user($objData->txtEmail, $objData->txtSenha);
        }else{
            $query = $this->user_model->validate_user('', $objData->txtSenha, $objData->txtEmail);
        }

        if ($query){
            // Salva os dados na sessão do usuário
            $this->data['user'] = array(
                'id'                        => $query[0]->id,
                'txtNome'                   => $query[0]->txtNome,
                'txtEmail'                  => $query[0]->txtEmail,
                'txtAreaAtuacao'            => $query[0]->txtAreaAtuacao,
                'txtFuncao'                 => $query[0]->txtFuncao,
                'idAdministrador'           => $query[0]->idAdministrador,
                'bitCadastro'               => $query[0]->bitCadastro,
                'idInstituicao'             => $query[0]->idInstituicao,
                'txtModelo'                 => $query[0]->txtModelo,
                'txtBPO'                    => $query[0]->txtBPO,
                'logged'                    => true
            );

            $this->session->set_userdata($this->data);

            echo json_encode(array("valid" => true, "bitCadastro" => $query[0]->bitCadastro, "txtModelo" => $query[0]->txtModelo, "bitAdministrador" => $query[0]->idAdministrador));

            $objInsertLog = new stdClass();

            $objInsertLog->idUsuario = $query[0]->id; 
            $this->crud_model->insert('tabloglogin',$objInsertLog);

        }else{
            echo json_encode(array("valid" => false, 'mensagem'=>'E-mail ou senha incorretos '));
        }

    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }

    public function esqueci_senha(){

        $objData = new stdClass();
        $objInsertToken = new stdClass();
        $objData = (object)$_POST;

        $query = $this->user_model->valida_email($objData->txtEmail);
        
        if ($query){

            function crypto_rand_secure($min, $max){
                $range = $max - $min;
                if ($range < 1) return $min; // not so random...
                    $log = ceil(log($range, 2));
                    $bytes = (int) ($log / 8) + 1; // length in bytes
                    $bits = (int) $log + 1; // length in bits
                    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
                do {
                    $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                    $rnd = $rnd & $filter; // discard irrelevant bits
                } while ($rnd > $range);
                return $min + $rnd;
            }

            function getToken($length){
                $token = "";
                $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
                $codeAlphabet.= "0123456789";
                $max = strlen($codeAlphabet); // edited

                for ($i=0; $i < $length; $i++) {
                    $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
                }

                return $token;
            }

            // 1 - Gravar dados do usuário em uma tabela de Log(idUsuario, url do token, data do cadastro do token, bitStatus)
            $valorToken = getToken(70);

            $objInsertToken->idUsuario = $query[0]->id;
            $objInsertToken->txtToken = $valorToken;

            $this->crud_model->insert('tabtoken',$objInsertToken);

            // 2 - Disparar e-mail para o usuário com o link para a pagina de redefinir senha
            $httpClient = new GuzzleAdapter(new Client());
            $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
            
            $promise = $sparky->transmissions->post([
                'content' => ['template_id' => 'esqueci-senha'],
                'substitution_data' => 
                        ['txtNome' => $query[0]->txtNome,
                        'txtEmail' => $query[0]->txtEmail,
                        'txtToken' => $valorToken],
                'recipients' => [
                    [
                        'address' => [
                            'name' => $query[0]->txtNome,
                            'email' => $query[0]->txtEmail,
                        ],
                    ],
                ],
            ]);

            try {
                $response = $promise->wait();
                // echo $response->getStatusCode()."\n";
                // print_r($response->getBody())."\n";
            } catch (\Exception $e) {
                // echo $e->getCode()."\n";
                // echo $e->getMessage()."\n";
            }

            echo json_encode(array("valid" => true, "mensagem" => 'Um mensagem foi enviada para o seu e-mail', 'styleMensagem'=>'success'));
        }else{
            
            echo json_encode(array("valid" => false, 'mensagem'=>'E-mail não encontrado', 'styleMensagem'=>'danger'));
        }
        
    }

    public function nova_senha($token =  ''){

        if($token == '')
            redirect('index', 'refresh');

        date_default_timezone_set('America/Sao_Paulo');
        
        $query = $this->user_model->validate_token($token);
        
        if ($query){          

            $today_dt = new DateTime('now');
            $expire_dt = new DateTime($query[0]->datCreate);

            $validade_token =  $today_dt->diff($expire_dt, true);

            if ($validade_token->d < 1) { 
                if ($query[0]->bitStatus == 0) {
                    if ($validade_token->h < 2) {
                        $this->data['mensagemToken']=  "Token Ok";
                        $this->data['idUser']= $query[0]->idUsuario;
                        $this->data['idToken']= $query[0]->id;
                    }else{
                        $this->data['mensagemToken']=  "Token Expirado";
                    }
                }else{
                    $this->data['mensagemToken']=  "Token já utilizado";    
                }
            }else{
                $this->data['mensagemToken']=  "Token Expirado";
            }
        }else{
            $this->data['mensagemToken']=  "Token Inválido";
        }

        $this->template->showSite('nova-senha', $this->data);
    }

    public function salvar_senha(){
        $objData = new stdClass();
        $objUpdateSenha = new stdClass();
        $objUpdateToken = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . $objData->idUsuario);
        $arrayCondition2 = array('id = ' . $objData->idToken);

        $objUpdateSenha->txtSenha = do_hash(do_hash($objData->txtNovaSenha, 'md5'));
        $objUpdateToken->bitStatus = 1;

        $this->crud_model->update($objUpdateToken, 'tabtoken', $arrayCondition2);
        $query = $this->crud_model->update($objUpdateSenha, 'tabusuario', $arrayCondition);
        

        echo json_encode(array("valid" => true, 'mensagem' => 'Nova senha salva com sucesso'));

    }

}
