<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instituicao extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('instituicao_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
        if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('dashboard', 'refresh');

        $this->data['instituicoes'] = $this->instituicao_model->get_instituicao();

        $this->template->showSite('list-instituicao', $this->data);    

    }

    public function edit_instituicao($idInstituicao =''){

        if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('dashboard', 'refresh');

        if ($idInstituicao != '') {
            $this->data['instituicao'] =  $this->instituicao_model->get_instituicao($this->encrypt->decode($idInstituicao));
            $this->template->showSite('edit-instituicao', $this->data);
        }else{
            $this->template->showSite('edit-instituicao',  array());
        }

    }

    public function config_instituicao() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->id)) {

            $objUpdateInstituicao = new stdClass();
            
            //Atualização dos dados da area no banco de dados
            $arrayCondition = array('id = ' . $this->encrypt->decode($objData->id));
            $objUpdateInstituicao->txtNomeInstituicao = $objData->txtNomeInstituicao;
            $objUpdateInstituicao->txtNumeroFuncionarios = $objData->txtNumeroFuncionarios;

            $this->crud_model->update($objUpdateInstituicao, 'tabinstituicao', $arrayCondition);

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Instituição editada com sucesso. '));
        }else{

            $objInsertInstituicao = new stdClass();
            
            //Inserção dos dados da área no banco de dados
            $objInsertInstituicao->txtNomeInstituicao = $objData->txtNomeInstituicao;
            $objInsertInstituicao->txtNumeroFuncionarios = $objData->txtNumeroFuncionarios;
            $objInsertInstituicao->txtModelo = $objData->txtModelo;

            $this->crud_model->insert('tabinstituicao',$objInsertInstituicao);

            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'mensagem'=>'Instituição inserida com sucesso. ', 'insert'=> true));
        }

    }

}
