<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Investigador extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
    
        // DataInicial 20170305 DataFinal 20170311
        // $this->data['normativos'] = $this->normativo_model->get_normativo_base('', '20161105', '20170101');

        // $this->data['normativos'] = $this->normativo_model->get_normativo_base('', $dataInicio, $dataFim);

        $this->template->showSite('upload', array());    

    }

    public function resposta() {


        $this->data['bitResultado'] = rand(0, 1) ? '1' : '0';

        $this->template->showSite('upload-resposta', $this->data);    

    }

}
