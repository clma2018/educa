<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class BPO extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('anotacao_model');
        $this->load->model('user_model');
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {
       if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        set_time_limit(0);
    
        $dataInicio = date('Y-m-d', strtotime("-16 days"));
        $dataInicio = str_replace('-', '', $dataInicio);
        $dataFim = date('Y-m-d');
        $dataFim = str_replace('-', '', $dataFim);
        
        $this->data['dataInicio'] = $dataInicio;
        $this->data['dataFim'] = $dataFim;
        
        // DataInicial 20170305 DataFinal 20170311
        $this->data['normativosRecentes'] = $this->normativo_model->get_normativo_base('', $dataInicio, $dataFim);
        $this->data['bases']=  $this->normativo_model->get_bases();
        $this->data['normativos'] = '';

        $x = 0;
        for($i = 0; $i < count($this->data['normativosRecentes']); $i++){

            if($this->data['normativosRecentes']){
                $qe = 'qe'.$i; 
                $qe = new stdClass();
                $qe = $this->anotacao_model->get_anotacoes_bpo($this->data['normativosRecentes'][$i]->idNorma, $this->session->userdata['user']['idInstituicao']);
                if ($qe) {        
                    
                }else{
                    $x++;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['normativosRecentes'][$i]->idNorma;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['normativosRecentes'][$i]->txtOrigem;
                    $this->data['normativos'][$x]['DataPubli'] = $this->data['normativosRecentes'][$i]->DataPubli;
                    $this->data['normativos'][$x]['Titulo'] = $this->data['normativosRecentes'][$i]->Titulo;
                    $this->data['normativos'][$x]['link'] = $this->data['normativosRecentes'][$i]->link;
                    $this->data['normativos'][$x]['Classe'] = $this->data['normativosRecentes'][$i]->Classe;
                    $this->data['normativos'][$x]['Assunto'] = $this->data['normativosRecentes'][$i]->Assunto;
                    $this->data['normativos'][$x]['TipoNorma'] = $this->data['normativosRecentes'][$i]->TipoNorma;
                    $this->data['normativos'][$x]['Numero'] = $this->data['normativosRecentes'][$i]->Numero;
                    $this->data['normativos'][$x]['Revogado'] = $this->data['normativosRecentes'][$i]->Revogado;
                    $this->data['normativos'][$x]['DataEmissao'] = $this->data['normativosRecentes'][$i]->DataEmissao;
                    $this->data['normativos'][$x]['Responsavel'] = $this->data['normativosRecentes'][$i]->Responsavel;
                    $this->data['normativos'][$x]['Grupo'] = $this->data['normativosRecentes'][$i]->Grupo;
                    $this->data['normativos'][$x]['Exclusao'] = $this->data['normativosRecentes'][$i]->Exclusao;

                }
                unset($qe);
            }
        }   

        $this->template->showSite('list-normativos-bpo', $this->data);

    }


    public function salvar_anotacao(){
         if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertAnotacao = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->txtNovaClasse)) {
            if ($objData->txtNovaClasse != '') {
                $objInsertAnotacao->txtClasse = $objData->txtNovaClasse;
            }else{
                $objInsertAnotacao->txtClasse = $objData->txtClasse;
            }
        }else{
            $objInsertAnotacao->txtClasse = $objData->txtClasse;
        }

        if (isset($objData->txtNovoGrupo)) {
            if ($objData->txtNovoGrupo != '') {
                $objInsertAnotacao->txtGrupo = $objData->txtNovoGrupo;
            }else{
                $objInsertAnotacao->txtGrupo = $objData->txtGrupo;
            }
        }else{
                $objInsertAnotacao->txtGrupo = $objData->txtGrupo;
        }

        $objInsertAnotacao->idNorma = $objData->idNormaSelecionada;
        $objInsertAnotacao->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $objInsertAnotacao->idUsuario = $this->session->userdata['user']['id'];
        $objInsertAnotacao->txtArea = $objData->txtArea;
        $objInsertAnotacao->txtProcesso = $objData->txtProcesso;
        $objInsertAnotacao->txtSistema = $objData->txtSistema;
        $objInsertAnotacao->txtComentario = $objData->txtComentario;
       
                        
        $query2 = $this->crud_model->insert('tabAnotacoesBPO', $objInsertAnotacao);

        
        $txtDataInicio[] = '';
        $txtDataFim[] = '';
        for ($i=0; $i < count($objData->txtDataVigencia); $i++) { 
            array_push($txtDataInicio, $objData->txtDataVigencia[$i]['txtDataInicioVigencia']);
            array_push($txtDataFim,$objData->txtDataVigencia[$i]['txtDataFimVigencia']);
        }

        array_shift($txtDataInicio);
        array_shift($txtDataFim);

        $arrayRegistros = $txtDataInicio; 
        for($i = 0; $i < count($arrayRegistros); $i++){
            if($arrayRegistros){

                $objInsertDataVigencia = new stdClass();
                $objInsertDataVigencia->idAnotacao = $query2->id;
                $objInsertDataVigencia->txtDataInicio = $arrayRegistros[$i];
                $objInsertDataVigencia->txtDataFim = $txtDataFim[$i];
                $query = $this->crud_model->insert('tabdataVigenciaBPO', $objInsertDataVigencia);

                unset($objInsertDataVigencia);
            }
        }


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }


    public function list_anotacoes_bpo() {
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');
        
        $this->data['anotacoes'] = $this->anotacao_model->get_anotacoes_bpo('',$this->session->userdata['user']['idInstituicao']);            

        $this->data['anotacoesEnviadas'] = $this->anotacao_model->get_anotacoes_bpo_enviadas($this->session->userdata['user']['idInstituicao']);

        $idAnotacoesEnviadas = array();
        for ($i=0; $i < count($this->data['anotacoesEnviadas']); $i++) { 
            $idAnotacoesEnviadas += [ $this->data['anotacoesEnviadas'][$i]->idAnotacao => 1 ];
        }

        
        for ($b=0; $b < count($this->data['anotacoes']); $b++) {
            if (array_key_exists($this->data['anotacoes'][$b]->id, $idAnotacoesEnviadas)) {
                $this->data['anotacoes'][$b]->txtBG = 'background-color:#b9e8b9';
            }else{
                $this->data['anotacoes'][$b]->txtBG = '';
            }
        }

        $this->template->showSite('list-anotacoes-bpo', $this->data);
    }

     public function filtrar_datasvigencias() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objData = (object)$_POST;
        
        $this->data['dataVigencia'] = $this->anotacao_model->get_data_anotacao($objData->idAnotacao);

        header('Content-Type: application/json');
        echo json_encode(array("dataVigencia" => $this->data['dataVigencia'], 'validate'=>true));

    }


     public function alter_anotacao(){
         if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objAlterAnotacao = new stdClass();
        $objData = (object)$_POST;

        if ($this->session->userdata['user']['txtBPO'] == 0) {
            $objInsertLogAnotacao = new stdClass();
            $objInsertLogAnotacao->txtCamposAlterados = '';
            
            $this->data['anotacao'] = $this->anotacao_model->get_anotacao($objData->idAnotacao);
            
            for ($i=0; $i < count($this->data['anotacao']); $i++) { 

                if ($this->data['anotacao'][$i]->txtClasse !== $objData->txtClasse) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Classe: '.$this->data['anotacao'][$i]->txtClasse.' | ';
                }
                if ($this->data['anotacao'][$i]->txtGrupo !== $objData->txtGrupo) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Grupo: '.$this->data['anotacao'][$i]->txtGrupo.' | ';
                }
                if ($this->data['anotacao'][$i]->txtArea !== $objData->txtArea) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Área: '.$this->data['anotacao'][$i]->txtArea.' | ';
                }
                if ($this->data['anotacao'][$i]->txtProcesso !== $objData->txtProcesso) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Processos: '.$this->data['anotacao'][$i]->txtProcesso.' | ';
                }
                if ($this->data['anotacao'][$i]->txtSistema !== $objData->txtSistema) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Sistemas: '.$this->data['anotacao'][$i]->txtSistema.' | ';
                }
                if ($this->data['anotacao'][$i]->txtComentario !== $objData->txtComentario) {
                    $objInsertLogAnotacao->txtCamposAlterados .= 'Comentários: '.$this->data['anotacao'][$i]->txtComentario.' | ';
                }
            }


            $objInsertLogAnotacao->idAnotacao = (int)($objData->idAnotacao);
            $objInsertLogAnotacao->idUsuario = $this->session->userdata['user']['id'];  

            $this->crud_model->insert('tabalteracaoBPO', $objInsertLogAnotacao);
        }

        $objAlterAnotacao->txtClasse = $objData->txtClasse;
        $objAlterAnotacao->txtGrupo = $objData->txtGrupo;
        $objAlterAnotacao->txtArea = $objData->txtArea;
        $objAlterAnotacao->txtProcesso = $objData->txtProcesso;
        $objAlterAnotacao->txtSistema = $objData->txtSistema;
        $objAlterAnotacao->txtComentario = $objData->txtComentario;
        
        $arrayCondition = array('id = ' . (int)($objData->idAnotacao));

        $query = $this->crud_model->update($objAlterAnotacao, 'tabAnotacoesBPO', $arrayCondition);


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
        
    }               

}
