<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Usuario extends CI_Controller {


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //Model
        $this->load->model('tema_model');
        $this->load->model('area_model');
        $this->load->model('user_model');
        $this->load->model('base_model');
        $this->load->model('normativo_model');
        $this->load->model('instituicao_model');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
        $this->load->model('curso_model');
        
        $this->data['qtdSecoes'] = count($this->data['secoes'] = $this->curso_model->get_secoes());
    }
    public function index() {

        if (!isset($this->session->userdata['user'])){
            redirect('login', 'refresh');
        }else{
            if ($this->session->userdata['user']['bitCadastro'] == 1)    
                redirect('dashboard', 'refresh');    
            
            //AREA ATUACAO
            $this->data['areaAtuacao']=  $this->area_model->get_area(0, $this->session->userdata['user']['idInstituicao'], 1);

            //TEMAS
            $this->data['subtemas']=  $this->tema_model->get_all_subtemas();
            $this->data['bases']=  $this->base_model->get_bases();

            //TEMAS EXCLUSAO
            $this->data['catexclusao']=  $this->tema_model->get_all_cat_exclusao();
            $this->data['temasexclusao']=  $this->tema_model->get_all_temas_exclusao();

            //TEMAS SELECIONADOS PARA O USUARIO
            $this->data['temasUsuario'] =  $this->user_model->get_users_tema($this->session->userdata['user']['id']);

            for($a = 0; $a < count($this->data['temasUsuario']); $a++){
                if($this->data['temasUsuario']){
                    $base = $this->area_model->get_base_tema($this->data['temasUsuario'][$a]->idTema);
                    $this->data['temasUsuario'][$a]->txtBase =  $base[0]->txtNome;
                    $this->data['temasUsuario'][$a]->txtCor =   $base[0]->txtCorSecundaria;
                }
            }

            $this->data['valorTemas'] = '';
            for($x = 0; $x < count($this->data['temasUsuario']); $x++){
                if($this->data['temasUsuario']){
                    $this->data['valorTemas'] .= '/'. $this->data['temasUsuario'][$x]->idTema;
                }
            }
            
            $this->template->showSite('cadastro', $this->data);
        }
    }


    public function cadastro_admin($token =  '') {
        $query = $this->user_model->validate_token($token);

        if ($query){
            if ($query[0]->bitStatus == 0) {

                $this->data['usuario'] = $this->user_model->get_users($query[0]->idUsuario);


                $this->data['idToken']  = $query[0]->id;
                
                //AREA ATUACAO
                $this->data['areaAtuacao']=  $this->area_model->get_area(0, $this->data['usuario'][0]->idInstituicao, 1);

                //TEMAS
                $this->data['subtemas']=  $this->tema_model->get_all_subtemas();
                $this->data['bases']=  $this->base_model->get_bases();

                //TEMAS EXCLUSAO
                $this->data['catexclusao']=  $this->tema_model->get_all_cat_exclusao();
                $this->data['temasexclusao']=  $this->tema_model->get_all_temas_exclusao();

                //TEMAS SELECIONADOS PARA O USUARIO
                $this->data['temasUsuario'] =  $this->user_model->get_users_tema($query[0]->idUsuario);

                for($a = 0; $a < count($this->data['temasUsuario']); $a++){
                    if($this->data['temasUsuario']){
                        $base = $this->area_model->get_base_tema($this->data['temasUsuario'][$a]->idTema);
                        $this->data['temasUsuario'][$a]->txtBase =  $base[0]->txtNome;
                        $this->data['temasUsuario'][$a]->txtCor =   $base[0]->txtCorSecundaria;
                    }
                }

                $this->data['valorTemas'] = '';
                for($x = 0; $x < count($this->data['temasUsuario']); $x++){
                    if($this->data['temasUsuario']){
                        $this->data['valorTemas'] .= '/'. $this->data['temasUsuario'][$x]->idTema;
                    }
                }

                $this->template->showSite('cadastro-administrador', $this->data);

            }else{
                redirect('login', 'refresh');        
            }
        }else{
            redirect('login', 'refresh');
        }
    }

    public function new_user_admin() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateUser = new stdClass();
        $objData = (object)$_POST;

        //Inserção dos dados do usuário no banco de dados
        $condition = array('id = ' . $objData->idUsuario);
        $objUpdateUser->txtNome = $objData->txtNome;
        $objUpdateUser->txtEmail = $objData->txtEmail;
        $objUpdateUser->txtAreaAtuacao = $objData->txtAreaAtuacao;
        $objUpdateUser->txtFuncao = $objData->txtFuncao;
        $objUpdateUser->txtNickname = $objData->txtNickname;
        $objUpdateUser->idAdministrador = 0;
        $objUpdateUser->bitCadastro = 1;
        $objUpdateUser->bitExclusaoTemas = 1;
        $objUpdateUser->txtSenha = do_hash(do_hash($objData->txtSenha, 'md5'));

        $query = $this->crud_model->update($objUpdateUser, 'tabusuario', $condition);


        $objUpdateToken = new stdClass();
        $objUpdateToken->bitStatus = 1;
        $arrayCondition2 = array('id = ' . $objData->idToken);
        $this->crud_model->update($objUpdateToken, 'tabtoken', $arrayCondition2);

        $this->data['usuario'] = $this->user_model->get_user_id($objData->idUsuario);


        // Salva os dados na sessão do usuário
        $this->data['user'] = array(
            'id'                        => $objData->idUsuario,
            'txtNome'                   => $query->txtNome,
            'txtEmail'                  => $query->txtEmail,
            'idAdministrador'           => $query->idAdministrador,
            'bitCadastro'               => $query->bitCadastro,
            'idInstituicao'             => $objData->idInstituicao,
            'txtModelo'                 => $this->data['usuario'][0]->txtModelo,
            'txtBPO'                    => $this->data['usuario'][0]->txtBPO,
            'logged'                    => true
        );
        $this->session->set_userdata($this->data);

        //Array com nome e email dos usuarios
        if (isset($objData->txtUsuarios)) {
            
            $txtNomeUsuarios[] = '';
            $txtEmailUsuarios[] = '';
            for ($i=0; $i < count($objData->txtUsuarios); $i++) { 
                array_push($txtNomeUsuarios, $objData->txtUsuarios[$i]['txtNomeUsuario']);
                array_push($txtEmailUsuarios,$objData->txtUsuarios[$i]['txtEmailUsuario']);
            }
            array_shift($txtNomeUsuarios);
            array_shift($txtEmailUsuarios);

            //Funcao para gerar senha de forma aleatoria
            function generatePassword($length = 8) {
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $count = mb_strlen($chars);

                for ($i = 0, $result = ''; $i < $length; $i++) {
                    $index = rand(0, $count - 1);
                    $result .= mb_substr($chars, $index, 1);
                }

                return $result;
            }

            //Salvar os temas selecionados pelo usuário
            $arrayRegistros = $txtNomeUsuarios; 
            for($i = 0; $i < count($arrayRegistros); $i++){
                if($arrayRegistros){
                    $senhaRand = generatePassword(6);
                    $objUsuarios = new stdClass();
                    $objUsuarios->idAdministrador = $objData->idUsuario;
                    $objUsuarios->bitCadastro = 0;
                    $objUsuarios->idInstituicao = $objData->idInstituicao;
                    $objUsuarios->bitExclusaoTemas = $objData->txtOpcaoTemasExclusao;
                    $objUsuarios->txtNome = $arrayRegistros[$i];
                    $objUsuarios->txtEmail = $txtEmailUsuarios[$i];
                    $objUsuarios->txtSenha = do_hash(do_hash($senhaRand, 'md5'));

                    $usuario = $this->crud_model->insert('tabusuario',$objUsuarios);
     
                    $arrayRegistros2 = $objData->txtTemasExclusao;  
                    for($bs = 0; $bs < count($arrayRegistros2); $bs++){
                        if($arrayRegistros2){
                            $objTemasExclusaoUser = new stdClass();
                            $objTemasExclusaoUser->idUsuario = $usuario->id;
                            $objTemasExclusaoUser->idTemaExclusao = $arrayRegistros2[$bs];
                            $this->crud_model->insert('tabtemaexclusaousuario',$objTemasExclusaoUser);
                            unset($objTemasExclusaoUser);
                        }
                    }

                    $httpClient = new GuzzleAdapter(new Client());
                    $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
                    
                    //DISPARO EMAIL
                    $promise = $sparky->transmissions->post([
                        'content' => ['template_id' => 'legal-bot'],
                        'substitution_data' => 
                                ['txtNome' => $arrayRegistros[$i],
                                'txtEmail' => $txtEmailUsuarios[$i],
                                'txtSenha' => $senhaRand,
                                'txtNomeAdministrador' => $query->txtNome,
                                'txtEmailAdministrador' => $query->txtEmail],
                        'recipients' => [
                            [
                                'address' => [
                                    'name' => $arrayRegistros[$i],
                                    'email' => $txtEmailUsuarios[$i],
                                ],
                            ],
                        ],
                    ]);

                    try {
                        $response = $promise->wait();
                        echo $response->getStatusCode()."\n";
                        print_r($response->getBody())."\n";
                    } catch (\Exception $e) {
                        echo $e->getCode()."\n";
                        echo $e->getMessage()."\n";
                    }

                    unset($objUsuarios);
                    unset($senhaRand);
                }
            }
        }

        //Salvar os temas de exclusão selecionados pelo usuário
        if (isset($objData->txtTemasExclusao)) {
            $arrayRegistros2 = $objData->txtTemasExclusao;  
            for($as = 0; $as < count($arrayRegistros2); $as++){
                if($arrayRegistros2){
                    $objTemasExclusao = new stdClass();
                    $objTemasExclusao->idUsuario = $objData->idUsuario;
                    $objTemasExclusao->idTemaExclusao = $arrayRegistros2[$as];
                    $this->crud_model->insert('tabtemaexclusaousuario',$objTemasExclusao);
                    unset($objTemasExclusao);
                }
            }
        }


        //Salvar os temas selecionados pelo usuário
        if (isset($objData->txtSubTemas)) {
            $arrayRegistros = explode("/", $objData->txtSubTemas);
            for($i = 1; $i < count($arrayRegistros); $i++){
                if($arrayRegistros){
                    $objTemas = new stdClass();
                    $objTemas->idUsuario = $objData->idUsuario;
                    $objTemas->idTema = $arrayRegistros[$i];
                    $this->crud_model->insert('tabtemausuario',$objTemas);
                    unset($objTemas);
                }
            } 
        }    

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }


    public function new_user() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objUpdateUser = new stdClass();
        $objData = (object)$_POST;


        //Inserção dos dados do usuário no banco de dados
        $arrayCondition = array('id = ' . $this->session->userdata['user']['id']);
        $objUpdateUser->txtNome = $objData->txtNome;
        $objUpdateUser->txtEmail = $objData->txtEmail;
        $objUpdateUser->txtAreaAtuacao = $objData->txtAreaAtuacao;
        $objUpdateUser->txtFuncao = $objData->txtFuncao;
        $objUpdateUser->txtNickname = $objData->txtNickname;
        $objUpdateUser->bitCadastro = 1;
        $objUpdateUser->txtSenha = do_hash(do_hash($objData->txtSenha, 'md5'));

        $query = $this->crud_model->update($objUpdateUser, 'tabusuario', $arrayCondition);

        // Salva os dados na sessão do usuário
        $this->data['user'] = array(
            'id'                        => $this->session->userdata['user']['id'],
            'txtNome'                   => $query->txtNome,
            'txtEmail'                  => $query->txtEmail,
            'txtAreaAtuacao'            => $query->txtAreaAtuacao,
            'txtFuncao'                 => $query->txtFuncao,
            'idInstituicao'            => $this->session->userdata['user']['idInstituicao'],
            'bitCadastro'               => $query->bitCadastro,
            'txtModelo'                 => $this->session->userdata['user']['txtModelo'],
            'idAdministrador'           => $this->session->userdata['user']['idAdministrador'],
            'logged'                    => true
        );
        $this->session->set_userdata($this->data);

        
        $arrayCondition2 = array('idUsuario = ' . $this->session->userdata['user']['id']);
        $query2 = $this->crud_model->delete('tabtemausuario', $arrayCondition2);

        //Salvar os temas selecionados pelo usuário
        $arrayRegistros = explode("/", $objData->txtSubTemas);
        for($i = 1; $i < count($arrayRegistros); $i++){
            if($arrayRegistros){
                $objTemas = new stdClass();
                $objTemas->idUsuario = $this->session->userdata['user']['id'];
                $objTemas->idTema = $arrayRegistros[$i];
                $this->crud_model->insert('tabtemausuario',$objTemas);
                unset($objTemas);
            }
        }    

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

    public function edit_user($idUsuario = 0) {

        //TEMAS
        $this->data['subtemas']=  $this->tema_model->get_all_subtemas();
        $this->data['bases']=  $this->base_model->get_bases();

        //TEMAS EXCLUSAO
        $this->data['catexclusao']=  $this->tema_model->get_all_cat_exclusao();
        $this->data['temasexclusao']=  $this->tema_model->get_all_temas_exclusao();

        //USUARIO
        $this->data['usuario'] = $this->user_model->get_users($this->encrypt->decode($idUsuario));

        //TEMAS USUARIO
        $this->data['temasUsuario'] = $this->user_model->get_users_tema($this->encrypt->decode($idUsuario)); 

        // TEMAS AREA
        $this->data['temasArea'] = $this->area_model->get_temas_area($this->data['usuario'][0]->txtAreaAtuacao); 
    
        $idTemasArea = array();
        for ($i=0; $i < count($this->data['temasArea']); $i++) { 
            $idTemasArea += [ $this->data['temasArea'][$i]->idTema => 1 ];
        }

        $this->data['idTemasArea'] =  $idTemasArea;

        for($a = 0; $a < count($this->data['temasUsuario']); $a++){
            if($this->data['temasUsuario']){
                $base = $this->area_model->get_base_tema($this->data['temasUsuario'][$a]->idTema);
                $this->data['temasUsuario'][$a]->txtBase =  $base[0]->txtNome;
                $this->data['temasUsuario'][$a]->txtCor =   $base[0]->txtCorSecundaria;
            }
        }

        $this->data['valorTemas'] = '';
        for($x = 0; $x < count($this->data['temasUsuario']); $x++){
            if($this->data['temasUsuario']){
                $this->data['valorTemas'] .= '/'. $this->data['temasUsuario'][$x]->idTema;
            }
        }

        //TEMAS EXCLUSAO USUARIO
        $this->data['temaexclusaousuario'] = $this->user_model->get_users_temaexclusao($this->encrypt->decode($idUsuario));  

        for ($i=0; $i < count($this->data['temasexclusao']); $i++) {
            $this->data['temasexclusao'][$i]->txtCheckBox = '';
            for ($x=0; $x < count($this->data['temaexclusaousuario']); $x++) {
                if($this->data['temasexclusao'][$i]->id == $this->data['temaexclusaousuario'][$x]->idTemaExclusao){
                    $this->data['temasexclusao'][$i]->txtCheckBox = 'checked';
                }
            }
        }

        $this->template->showSite('edit-user', $this->data);    
    }

    public function edit_info(){

        $objData = new stdClass();
        $objUpdateUser = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . (int)$this->encrypt->decode($objData->id));
        $objUpdateUser->txtNome = $objData->txtNome;
        $objUpdateUser->txtEmail = $objData->txtEmail;
        $objUpdateUser->txtAreaAtuacao = $objData->txtAreaAtuacao;
        $objUpdateUser->txtFuncao = $objData->txtFuncao;
        $objUpdateUser->txtNickname = $objData->txtNickname;

        $query = $this->crud_model->update($objUpdateUser, 'tabusuario', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, "mensagem" => 'Informações alteradas com sucesso'));

    }


    public function edit_tema(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('idUsuario = ' . (int)$this->encrypt->decode($objData->id));
        $query = $this->crud_model->delete('tabtemausuario', $arrayCondition);

        //Salvar os temas selecionados pelo usuário
        $arrayRegistros = explode("/", $objData->txtSubTemas);
        for($i = 0; $i < count($arrayRegistros); $i++){
            if($arrayRegistros[$i]){
                $objTemas = new stdClass();
                $objTemas->idUsuario = (int)$this->encrypt->decode($objData->id);
                $objTemas->idTema = $arrayRegistros[$i];
                $this->crud_model->insert('tabtemausuario',$objTemas);
                unset($objTemas);
            }
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, "mensagem" => 'Temas alterados com sucesso'));

    }

    public function edit_tema_exclusao(){

        $objData = new stdClass();
        $objTemasExclusao = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('idUsuario = ' . (int)$this->encrypt->decode($objData->id));
        $query = $this->crud_model->delete('tabtemaexclusaousuario', $arrayCondition);

        //Salvar os temas selecionados pelo usuário
        $arrayRegistros = $objData->txtTemasExclusao; 
        for($i = 0; $i < count($arrayRegistros); $i++){
            if($arrayRegistros){
                $objTemasExclusao = new stdClass();
                $objTemasExclusao->idUsuario = (int)$this->encrypt->decode($objData->id);
                $objTemasExclusao->idTemaExclusao = $arrayRegistros[$i];
                $this->crud_model->insert('tabtemaexclusaousuario',$objTemasExclusao);
                unset($objTemasExclusao);
            }
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, "mensagem" => 'Temas de Exclusão alterados com sucesso'));

    }

    public function cadastro_administradores(){
        if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('', 'refresh');

        $this->data['instituicoes'] = $this->instituicao_model->get_instituicao();
        
        $this->template->showSite('cadastro-user-admin', $this->data);    
    }

    public function save_admin(){
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        //Array com nome e email dos usuarios
        $txtNomeUsuarios[] = '';
        $txtEmailUsuarios[] = '';
        $txtOpcaoBPO[] = '';
        for ($i=0; $i < count($objData->txtUsuarios); $i++) { 
            array_push($txtNomeUsuarios, $objData->txtUsuarios[$i]['txtNomeUsuario']);
            array_push($txtEmailUsuarios,$objData->txtUsuarios[$i]['txtEmailUsuario']);
            array_push($txtOpcaoBPO,$objData->txtUsuarios[$i]['txtBPO']);
        }
        array_shift($txtNomeUsuarios);
        array_shift($txtEmailUsuarios);
        array_shift($txtOpcaoBPO);

        function crypto_rand_secure($min, $max){
            $range = $max - $min;
            if ($range < 1) return $min; // not so random...
                $log = ceil(log($range, 2));
                $bytes = (int) ($log / 8) + 1; // length in bytes
                $bits = (int) $log + 1; // length in bits
                $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ($rnd > $range);
            return $min + $rnd;
        }

        function getToken($length){
            $token = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet.= "0123456789";
            $max = strlen($codeAlphabet); // edited

            for ($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
            }

            return $token;
        }

        //Salvar os temas selecionados pelo usuário
        $arrayRegistros = $txtNomeUsuarios; 
        for($i = 0; $i < count($arrayRegistros); $i++){
            if($arrayRegistros){

                //Grava os dados do usuário
                $objUsuarios = new stdClass();
                $objUsuarios->bitCadastro = 0;
                $objUsuarios->idAdministrador = 0;
                $objUsuarios->idInstituicao = $objData->idInstituicao;
                $objUsuarios->txtNome = $arrayRegistros[$i];
                $objUsuarios->txtEmail = $txtEmailUsuarios[$i];
                if ($txtOpcaoBPO[$i] == 'UserBPO') {
                    $objUsuarios->txtBPO = 1;
                }else{
                    $objUsuarios->txtBPO = 0;
                }
                $usuario = $this->crud_model->insert('tabusuario',$objUsuarios);

                //Gera um token e grava na tabtoken
                $valorToken = getToken(70);
                $objInsertToken = new stdClass();
                $objInsertToken->idUsuario = $usuario->id;
                $objInsertToken->txtToken = $valorToken;
                $this->crud_model->insert('tabtoken',$objInsertToken);
    
                //Disparo do e-mail
                $httpClient = new GuzzleAdapter(new Client());
                $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
            
                $promise = $sparky->transmissions->post([
                    'content' => ['template_id' => 'cadastro-admin'],
                    'substitution_data' => 
                            ['txtNome' => $arrayRegistros[$i],
                            'txtEmail' => $txtEmailUsuarios[$i],
                            'txtToken' => $valorToken],
                    'recipients' => [
                        [
                            'address' => [
                                'name' => $arrayRegistros[$i],
                                'email' => $txtEmailUsuarios[$i],
                            ],
                        ],
                    ],
                ]);

                try {
                    $response = $promise->wait();
                } catch (\Exception $e) {
                    
                }

                unset($objUsuarios);
                unset($objInsertToken);
            }
        }
    
        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }

    public function check_email($txtEmail= ''){
        $objData = new stdClass();
        $objData = (object)$_POST;

        $validaDuplicidadeEmail = $this->user_model->valida_duplicidade_email($objData->txtEmail);

        if($validaDuplicidadeEmail){
            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'danger', 'validate'=>false));
        }else{
            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'validate'=>true));
        }
    }


    public function new_user_duplicate() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertUser = new stdClass();
        $objData = (object)$_POST;
        

        $arrayRegistros =  $this->user_model->get_users_tema($this->encrypt->decode($objData->idUsuario));
        $arrayRegistros2 =  $this->user_model->get_users_temaexclusao($this->encrypt->decode($objData->idUsuario));


        //Funcao para gerar senha de forma aleatoria
        function generatePassword($length = 8) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);

            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }

            return $result;
        }

        //Salvar os temas selecionados pelo usuário 
        $senhaRand = generatePassword(6);
        $objInsertUser->idAdministrador = $this->session->userdata['user']['id'];
        $objInsertUser->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $objInsertUser->bitCadastro = 0;
        $objInsertUser->bitExclusaoTemas = 1;
        $objInsertUser->txtNome = $objData->txtNomeUsuario; 
        $objInsertUser->txtEmail = $objData->txtEmailUsuario;
        $objInsertUser->txtSenha = do_hash(do_hash($senhaRand, 'md5'));
        $usuario = $this->crud_model->insert('tabusuario',$objInsertUser);

        for($tm = 0; $tm < count($arrayRegistros); $tm++){
            if($arrayRegistros){
                $objTemasUser = new stdClass();
                $objTemasUser->idUsuario = $usuario->id;
                $objTemasUser->idTema = $arrayRegistros[$tm]->idTema;
                $this->crud_model->insert('tabtemausuario',$objTemasUser);
                unset($objTemasUser);
            }
        }

        for($bs = 0; $bs < count($arrayRegistros2); $bs++){
            if($arrayRegistros2){
                $objTemasExclusaoUser = new stdClass();
                $objTemasExclusaoUser->idUsuario = $usuario->id;
                $objTemasExclusaoUser->idTemaExclusao = $arrayRegistros2[$bs]->idTemaExclusao;
                $this->crud_model->insert('tabtemaexclusaousuario',$objTemasExclusaoUser);
                unset($objTemasExclusaoUser);
            }
        }

        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
        
        //DISPARO EMAIL
        $promise = $sparky->transmissions->post([
            'content' => ['template_id' => 'legal-bot'],
            'substitution_data' => 
                    ['txtNome' => $objData->txtNomeUsuario,
                    'txtEmail' => $objData->txtEmailUsuario,
                    'txtSenha' => $senhaRand,
                    'txtNomeAdministrador' => $this->session->userdata['user']['txtNome'],
                    'txtEmailAdministrador' => $this->session->userdata['user']['txtEmail']],
            'recipients' => [
                [
                    'address' => [
                        'name' => $objData->txtNomeUsuario,
                        'email' => $objData->txtEmailUsuario,
                    ],
                ],
            ],
        ]);

        try {
            $response = $promise->wait();
            // echo $response->getStatusCode()."\n";
            // print_r($response->getBody())."\n";
        } catch (\Exception $e) {
            // echo $e->getCode()."\n";
            // echo $e->getMessage()."\n";
        }

        unset($senhaRand);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }


    public function perfil_user($idUsuario = 0) {

        //USUARIO
        $this->data['usuario'] = $this->user_model->get_users($this->encrypt->decode($idUsuario));

        //TEMAS USUARIO
        $this->data['temausuario'] = $this->user_model->get_users_tema($this->encrypt->decode($idUsuario)); 
        
        //TEMAS EXCLUSAO USUARIO
        $this->data['temaexclusaousuario'] = $this->user_model->get_users_temaexclusao($this->encrypt->decode($idUsuario));

        //PLANOS DE ACAO
        $this->data['planosAcao'] = $this->normativo_model->get_planoAcao_user($this->encrypt->decode($idUsuario)); 

        //PLANOS DE ACAO
        $this->data['emailsEncaminhado'] = $this->normativo_model->get_user_encaminhado(0, $this->encrypt->decode($idUsuario)); 
        
        //NORMATIVOS
        $this->data['normativos']=  $this->normativo_model->get_normativo_avaliacao( $this->encrypt->decode($idUsuario),90, 3, 10);

        for ($i=0; $i < count($this->data['normativos']); $i++) {
            if($this->data['normativos'][$i]->txtNota == NULL){
                $this->data['normativos'][$i]->txtNota = 'Não avaliado';
            }
            if($this->data['normativos'][$i]->bitCiente == 1){
                $this->data['normativos'][$i]->txtRelevancia = 'Relevante';
            }elseif ($this->data['normativos'][$i]->bitCiente == 0) {
                $this->data['normativos'][$i]->txtRelevancia = 'Não Relevante';
            }else{
                $this->data['normativos'][$i]->txtRelevancia = 'Não Lido';
            }
        }

        $this->template->showSite('perfil-user', $this->data);    
    }

    public function list_user(){ 

        if (isset($this->session->userdata['user']['bitAdministrador'])){
            $this->data['usuarios']=  $this->user_model->get_users();
        }else{
            if (!isset($this->session->userdata['user']))
                redirect('login', 'refresh');
            if ($this->session->userdata['user']['idAdministrador'] != 0 )
                redirect('dashboard', 'refresh');
            
            $this->data['usuarios']=  $this->user_model->get_users(0, 0,$this->session->userdata['user']['idInstituicao']);
        }

        $this->template->showSite('list-user', $this->data);
    }

    public function insert_user(){ 
        if (!isset($this->session->userdata['user']))
            redirect('login', 'refresh');

        if ( $this->session->userdata['user']['idAdministrador'] == 0){
            //TEMAS EXCLUSAO
            $this->data['catexclusao']=  $this->tema_model->get_all_cat_exclusao();
            $this->data['temasexclusao']=  $this->tema_model->get_all_temas_exclusao();

            $this->template->showSite('insert-user', $this->data);
        }else{
            redirect('dashboard', 'refresh');
        }
    }


    public function new_user_dashboard() {
        if(!$_POST)
            redirect('index', 'refresh');        
    
        $objData = new stdClass();
        $objInsertUser = new stdClass();
        $objData = (object)$_POST;

        //Funcao para gerar senha de forma aleatoria
        function generatePassword($length = 8) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);

            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }

            return $result;
        }

        //Salvar os temas selecionados pelo usuário
        $senhaRand = generatePassword(6);
        $objInsertUser->idAdministrador = $this->session->userdata['user']['id'];
        $objInsertUser->idInstituicao = $this->session->userdata['user']['idInstituicao'];
        $objInsertUser->bitCadastro = 0;
        $objInsertUser->bitExclusaoTemas = $objData->txtOpcaoTemasExclusao;
        $objInsertUser->txtNome = $objData->txtNome;
        $objInsertUser->txtEmail = $objData->txtEmail;
        $objInsertUser->txtSenha = do_hash(do_hash($senhaRand, 'md5'));

        $usuario = $this->crud_model->insert('tabusuario',$objInsertUser);

        if (isset($objData->txtTemasExclusao)) {
            $arrayRegistros2 = $objData->txtTemasExclusao;  
            for($bs = 0; $bs < count($arrayRegistros2); $bs++){
                if($arrayRegistros2){
                    $objTemasExclusaoUser = new stdClass();
                    $objTemasExclusaoUser->idUsuario = $usuario->id;
                    $objTemasExclusaoUser->idTemaExclusao = $arrayRegistros2[$bs];
                    $this->crud_model->insert('tabtemaexclusaousuario',$objTemasExclusaoUser);
                    unset($objTemasExclusaoUser);
                }
            }
        }

        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key'=>'394bb118c21ece7c81844576ae3d0d70eed74634']);
        
        //DISPARO EMAIL
        $promise = $sparky->transmissions->post([
            'content' => ['template_id' => 'legal-bot'],
            'substitution_data' => 
                    ['txtNome' => $objData->txtNome,
                    'txtEmail' => $objData->txtEmail,
                    'txtSenha' => $senhaRand,
                    'txtNomeAdministrador' => $this->session->userdata['user']['txtNome'],
                    'txtEmailAdministrador' => $this->session->userdata['user']['txtEmail']],
            'recipients' => [
                [
                    'address' => [
                        'name' => $objData->txtNome,
                        'email' => $objData->txtEmail,
                    ],
                ],
            ],
        ]);

        try {
            $response = $promise->wait();
            // echo $response->getStatusCode()."\n";
            // print_r($response->getBody())."\n";
        } catch (\Exception $e) {
            // echo $e->getCode()."\n";
            // echo $e->getMessage()."\n";
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

    public function temas_area() {
        if(!$_POST)
            redirect('index', 'refresh');        
        
        $objData = new stdClass();
        $objData = (object)$_POST;

        if (isset($objData->idInstituicao)) {
            $this->data['temasArea'] = $this->area_model->get_temas_area($objData->txtAreaEscolhida, $objData->idInstituicao);
        }else{
            $this->data['temasArea'] = $this->area_model->get_temas_area($objData->txtAreaEscolhida, $this->session->userdata['user']['idInstituicao']);
        }
        

        $this->data['valorTemas'] = '';
        for($x = 0; $x < count($this->data['temasArea']); $x++){
            if($this->data['temasArea']){
                if ($x < 1) {
                    $this->data['valorTemas'] .= $this->data['temasArea'][$x]->idTema;
                }else{
                    $this->data['valorTemas'] .= '/'. $this->data['temasArea'][$x]->idTema;
                }
            }
        }

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "temas" => $this->data['temasArea'], "idTemas"=> $this->data['valorTemas']));
    }

    public function limpar_base() {

        $objData = new stdClass();
        $objData = (object)$_POST;
    
        $arrayCondition = array('idUsuario = ' . (int)$this->encrypt->decode($objData->idUsuario));
        $this->crud_model->delete('tabnormativousuario', $arrayCoindtion);
        $this->crud_model->delete('tabencaminhar', $arrayCondition);
        $this->crud_model->delete('tabplanoacao', $arrayCondition);
        $this->crud_model->delete('tabanotacaousuario', $arrayCondition);
        $this->crud_model->delete('tabavaliacao', $arrayCondition);
        $this->crud_model->delete('tabcomentarios', $arrayCondition);
        $this->crud_model->delete('tablogacessolistnormativo', $arrayCondition);
        $this->crud_model->delete('tabloglogin', $arrayCondition);
        $this->crud_model->delete('tabtemausuario', $arrayCondition);
        $this->crud_model->delete('tabtemaexclusaousuario', $arrayCondition);
        $this->crud_model->delete('tabtoken', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "mensagem"=> 'Alteração realizada com sucesso.'));
    }

    public function filtrar_grupos() {
       
        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->data['temasBase'] = $this->base_model->get_grupo_base($objData->txtBase);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "temas" => $this->data['temasBase']));
    }


    public function check_nickname($txtNickname= ''){
        $objData = new stdClass();
        $objData = (object)$_POST;

        $validaDuplicidadeNickname = $this->user_model->valida_duplicidade_nickname($objData->txtNickname);

        if($validaDuplicidadeNickname){
            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'danger', 'validate'=>false));
        }else{
            header('Content-Type: application/json');
            echo json_encode(array("msg" => 'success', 'validate'=>true));
        }
    }

    public function excluir_usuario() {
        $objData = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('idUsuario = ' . (int)$this->encrypt->decode($objData->idUsuario));
        $arrayCondition2 = array('id = ' . (int)$this->encrypt->decode($objData->idUsuario));

        $this->crud_model->delete('tabnormativousuario', $arrayCondition);
        $this->crud_model->delete('tabencaminhar', $arrayCondition);
        $this->crud_model->delete('tabplanoacao', $arrayCondition);
        $this->crud_model->delete('tabanotacaousuario', $arrayCondition);
        $this->crud_model->delete('tabavaliacao', $arrayCondition);
        $this->crud_model->delete('tabcomentarios', $arrayCondition);
        $this->crud_model->delete('tablogacessolistnormativo', $arrayCondition);
        $this->crud_model->delete('tabloglogin', $arrayCondition);
        $this->crud_model->delete('tabtemausuario', $arrayCondition);
        $this->crud_model->delete('tabtemaexclusaousuario', $arrayCondition);
        $this->crud_model->delete('tabtoken', $arrayCondition);

        $this->crud_model->delete('tabusuario', $arrayCondition2);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', "mensagem"=> 'Usuário excluído com sucesso.'));
    }


    public function perfil_usuario($idUsuario = 0) {
   
        $this->data['usuario'] = $this->user_model->get_users($this->encrypt->decode($idUsuario));

        $this->template->showSite('perfil-user', $this->data);    
    }


    public function save_edit_user(){

        $objData = new stdClass();
        $objUpdateUser = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . (int)$this->session->userdata['user']['id']);
        $objUpdateUser->txtNome = $objData->txtNome;
        $objUpdateUser->txtEmail = $objData->txtEmail;

        $query = $this->crud_model->update($objUpdateUser, 'tabusuario', $arrayCondition);

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true, "mensagem" => 'Informações alteradas com sucesso'));

    }

}
