<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportar extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');;
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');

        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    }
    
    public function exportar_excel_historico(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
       
        $dadosFormatado = array();

        $array = explode(',', $objData->idNormativosExportar);

        for ($i=0; $i < count($array); $i++) {
            $dados = new stdClass();
            $this->data['normativos'] = $this->normativo_model->get_normativo_idNorma($array[$i]);              

            $dados->Origem = $this->data['normativos'][0]->txtOrigem;
            $dados->Título = $this->data['normativos'][0]->Titulo;
            $dados->DataPublicação = $this->data['normativos'][0]->DataPubli;
            $dados->TipoNorma = $this->data['normativos'][0]->TipoNorma;
            $dados->Número = $this->data['normativos'][0]->Numero;
            $dados->Classe = $this->data['normativos'][0]->Classe;
            $dados->Assunto = $this->data['normativos'][0]->Assunto;
            $dados->Link = $this->data['normativos'][0]->link;
            array_push($dadosFormatado, $dados);
        }
        

        $this->excel->stream('Historico-normativos.xls', $dadosFormatado); 

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    

    }


    public function exportar_excel_area(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
       
        $dadosFormatado = array();

        $array = explode(',', $objData->idNormativosExportar);

        for ($i=0; $i < count($array); $i++) {
            $dados = new stdClass();
            $this->data['normativos'] = $this->normativo_model->get_normativo($array[$i]); 


            $dados->Origem = $this->data['normativos'][0]->txtOrigem;
            $dados->Título = $this->data['normativos'][0]->txtTitulo;
            $dados->DataPublicação = $this->data['normativos'][0]->dateCadastro;
            $dados->Usuário = $this->data['normativos'][0]->txtNomeUsuario;
            if($this->data['normativos'][0]->bitCiente == 1){
                $dados->Status = "Relevante";
            }elseif ($this->data['normativos'][0]->bitCiente == 0) {
               $dados->Status = "Não Relevante";
            }else{
                $dados->Status = "Não Lido";
            }
            $dados->Classe = $this->data['normativos'][0]->txtClasse;
            $dados->Assunto = $this->data['normativos'][0]->txtAssunto;
            $dados->Link = $this->data['normativos'][0]->txtLink;
            
            array_push($dadosFormatado, $dados);
        }
        

        $this->excel->stream('Normativos-Area.xls', $dadosFormatado); 

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));
    

    }

    
}
