<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use WebScraper\ApiClient\Client;
use WebScraper\ApiClient\WebScraperApiException;
use League\Csv\Reader;

class Scrap extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();

        // Model
        $this->load->model('user_model');
        $this->load->model('normativo_model');

        // Library
        $this->load->library('encryption');
        $this->load->library('user_agent');

        // Helper
        $this->load->helper('security');


        //USUARIO
        if (isset($this->session->userdata['user']['bitAdministrador'])){
        }else{
            if (isset($this->session->userdata['user']['id'])) {
                $this->data['userLogged'] = $this->user_model->get_users($this->session->userdata['user']['id']);
            }
        }
    }

    public function index() {

        $apiClient = new Client([
            'token' => 'B7voIQkKS9yYI78eQvMKrRulamfSqWupXcLn0iX4a7YDnk6tTL5DnkdG4JvC',
        ]);

    
        set_time_limit(0);


        // $scrapingJobId =  85633;
        // $status = 'finished';
        // $sitemapId = 11356;
        // $sitemapName = 'anbima-atividadesconveniadas2107';    

        // $scrapingJobId =  85635;
        // $status = 'finished';
        // $sitemapId = 11357;
        // $sitemapName = 'anbima-certificacao2107';    

        // $scrapingJobId =  85647;
        // $status = 'finished';
        // $sitemapId = 11367;
        // $sitemapName = 'anbima-codigoetica2107';    

        // $scrapingJobId =  85649;
        // $status = 'finished';
        // $sitemapId = 11368;
        // $sitemapName = 'anbima-destaques2107';       

        // $scrapingJobId =  85643;
        // $status = 'finished';
        // $sitemapId = 11364;
        // $sitemapName = 'anbima-fip-fiee2107';        

        // $scrapingJobId =  85636;
        // $status = 'finished';
        // $sitemapId = 11358;
        // $sitemapName = 'anbima-gestaodepatrimonio2107';

        // $scrapingJobId =  85638;
        // $status = 'finished';
        // $sitemapId = 11359;
        // $sitemapName = 'anbima-negociacaoinstrfinanceiros2107';

        // $scrapingJobId =  85640;
        // $status = 'finished';
        // $sitemapId = 11361;
        // $sitemapName = 'anbima-ofertaspublicas2107';

        // $scrapingJobId =  85644;
        // $status = 'finished';
        // $sitemapId = 11365;
        // $sitemapName = 'anbima-privatebanking2107';

        // $scrapingJobId =  85641;
        // $status = 'finished';
        // $sitemapId = 11362;
        // $sitemapName = 'anbima-processos2107';        

        // $scrapingJobId =  85639;
        // $status = 'finished';
        // $sitemapId = 11360;
        // $sitemapName = 'anbima-rendafixa2107';

        // $scrapingJobId =  85642;
        // $status = 'finished';
        // $sitemapId = 11363;
        // $sitemapName = 'anbima-servicosqualificados2107';

        // $scrapingJobId =  85646;
        // $status = 'finished';
        // $sitemapId = 11366;
        // $sitemapName = 'anbima-varejo2107';

        // $scrapingJobId =  88260;
        // $status = 'finished';
        // $sitemapId = 11374;
        // $sitemapName = 'ans-audienciapublica2107';

        // $scrapingJobId =  88258;
        // $status = 'finished';
        // $sitemapId = 11373;
        // $sitemapName = 'ans-consultapublica2107';
    
        // $scrapingJobId =  85229;
        // $status = 'finished';
        // $sitemapId = 11324;
        // $sitemapName = 'rfb-consulpublidisponiveis-1907';

        // $scrapingJobId =  85226;
        // $status = 'finished';
        // $sitemapId = 11322;
        // $sitemapName = 'rfb-parecer-1907';

        //  $scrapingJobId =  85218;
        // $status = 'finished';
        // $sitemapId = 11316;
        // $sitemapName = 'rfb-instnormativa-1907';

        // $scrapingJobId =  85219;
        // $status = 'finished';
        // $sitemapId = 11317;
        // $sitemapName = 'rfb-atodeclaratoriointerpretativo-1407';
        
        // $scrapingJobId =  84697;
        // $status = 'finished';
        // $sitemapId = 11256;
        // $sitemapName = 'rfb-solucconsultadivergencia-1407';

        // $scrapingJobId =  84698;
        // $status = 'finished';
        // $sitemapId = 11257;
        // $sitemapName = 'rfb-portarias-1407';

        // $scrapingJobId = 85241;
        // $status = 'finished';
        // $sitemapId = 11193;
        // $sitemapName = 'cvm-junho-datapubli1307';      

        // $scrapingJobId = 84069;
        // $status = 'finished';
        // $sitemapId = 10848;
        // $sitemapName = 'planalto-decretos-link2106';      

        // $scrapingJobId = 84071;
        // $status = 'finished';
        // $sitemapId = 10841;
        // $sitemapName = 'planalto-decretos-naonumerados-link2106';      

        // $scrapingJobId = 84074;
        // $status = 'finished';
        // $sitemapId = 10866;
        // $sitemapName = 'planalto-leis-delegadas-link2106';      

        // $scrapingJobId = 84076;
        // $status = 'finished';
        // $sitemapId = 10868;
        // $sitemapName = 'planalto-leis-ordinarias-link2106';      

        // $scrapingJobId = 84080;
        // $status = 'finished';
        // $sitemapId = 10870;
        // $sitemapName = 'planalto-medidasprovisorias-link2106';     

        // $scrapingJobId = 84037;
        // $status = 'finished';
        // $sitemapId = 10878;
        // $sitemapName = 'planalto-msgveto-link2106';    

        // $scrapingJobId = 84043;
        // $status = 'finished';
        // $sitemapId = 10847;
        // $sitemapName = 'planalto-decretos1706';

        // $scrapingJobId = 84027;
        // $status = 'finished';
        // $sitemapId = 10840;
        // $sitemapName = 'planalto-decretos-naonumerados-1706';        

        // $scrapingJobId = 84062;
        // $status = 'finished';
        // $sitemapId = 10867;
        // $sitemapName = 'planalto-leis-ordinarias-1706';        

        // $scrapingJobId = 84036;
        // $status = 'finished';
        // $sitemapId = 10883;
        // $sitemapName = 'planalto-msgveto-2106';        

        // $scrapingJobId = 84053;
        // $status = 'finished';
        // $sitemapId = 10869;
        // $sitemapName = 'planalto-medidasprovisorias2015-2018-1706';        
  
        // $scrapingJobId = 84046;
        // $status = 'finished';
        // $sitemapId = 10865;
        // $sitemapName = 'planalto-leis-delegadas-completa1706';         
        
        // $scrapingJobId =  83887;
        // $status = 'finished';
        // $sitemapId = 11179;
        // $sitemapName = 'crsfn-conselhinho-bcb-1207';

        // $scrapingJobId =  83879;
        // $status = 'finished';
        // $sitemapId = 10823;
        // $sitemapName = 'denatran-deliberacoes-completa1906';
      
        // $scrapingJobId =  83880;
        // $status = 'finished';
        // $sitemapId = 10831;
        // $sitemapName = 'denatran-portarias2017-1906';

        // $scrapingJobId =  83881;
        // $status = 'finished';
        // $sitemapId = 10828;
        // $sitemapName = 'denatran-resolucoes-completa1906';

        // $scrapingJobId =  83884;
        // $status = 'finished';
        // $sitemapId = 10829;
        // $sitemapName = 'denatran-resolucoes-link1906';

        // $scrapingJobId =  83883;
        // $status = 'finished';
        // $sitemapId = 10827;
        // $sitemapName = 'denatran-portarias-link1906';

        // $scrapingJobId =  83882;
        // $status = 'finished';
        // $sitemapId = 10830;
        // $sitemapName = 'denatran-deliberacoes-link1906';

        // $scrapingJobId =  83597;
        // $status = 'finished';
        // $sitemapId = 11144;
        // $sitemapName = 'anbima-fundosinvestimento-1007';

        // $scrapingJobId =  104685;
        // $status = 'finished';
        // $sitemapId = 11156;
        // $sitemapName = 'susep-ultimaspubli1107';

        // $scrapingJobId =  88669;
        // $status = 'finished';
        // $sitemapId = 10606;
        // $sitemapName = 'bmfbovespa-0906';

        // $scrapingJobId =  79649;
        // $status = 'finished';
        // $sitemapId = 10798;
        // $sitemapName = 'bndes-regula-fgi-link1906';

        // $scrapingJobId =  79648;
        // $status = 'finished';
        // $sitemapId = 10797;
        // $sitemapName = 'bndes-indiretas-naoautomaticas-link1906';

        // $scrapingJobId =  79647;
        // $status = 'finished';
        // $sitemapId = 10796;
        // $sitemapName = 'bndes-indiretas-exportacao-link1906';

        // $scrapingJobId =  78968;
        // $status = 'finished';
        // $sitemapId = 10693;
        // $sitemapName = 'bndes-indiretas-automaticas-link1406';

        // $scrapingJobId =  83739;
        // $status = 'finished';
        // $sitemapId = 10692;
        // $sitemapName = 'bndes-indiretas-agropecuarias-link1406';

        // $scrapingJobId =  79308;
        // $status = 'finished';
        // $sitemapId = 10744;
        // $sitemapName = 'ans2017-link1706';
        
        // $scrapingJobId =  85046;
        // $status = 'finished';
        // $sitemapId = 10591;
        // $sitemapName = 'ans2017-0706';

        // $scrapingJobId =  79441;
        // $status = 'finished';
        // $sitemapId = 10758;
        // $sitemapName = 'cvm2017-mensalmente';        

        // $scrapingJobId =  83093;
        // $status = 'finished';
        // $sitemapId = 10757;
        // $sitemapName = 'cvm-audienciaspublicas-abertamanifestacao1706';        

        // $scrapingJobId =  89906;
        // $status = 'finished';
        // $sitemapId = 12858;
        // $sitemapName = 'cvm-ultimos2017';

        // $scrapingJobId =  84088;
        // $status = 'finished';
        // $sitemapId = 10822;
        // $sitemapName = 'cpc-revisoes-link1906';

        // $scrapingJobId =  84090;
        // $status = 'finished';
        // $sitemapId = 10750;
        // $sitemapName = 'cpc-pronunciamentos-link1706';

        // $scrapingJobId =  84089;
        // $status = 'finished';
        // $sitemapId = 10749;
        // $sitemapName = 'cpc-pronunciamentos-total1706';

        // $scrapingJobId =  84087;
        // $status = 'finished';
        // $sitemapId = 10751;
        // $sitemapName = 'cpc-revisoes-total1706';

        // $scrapingJobId =  84091;
        // $status = 'finished';
        // $sitemapId = 10748;
        // $sitemapName = 'cpc-iasb-total1706';

        // $scrapingJobId =  84130;
        // $status = 'finished';
        // $sitemapId = 10679;
        // $sitemapName = 'cip-c3-comunioperacionais-1206';

        // $scrapingJobId =  78899;
        // $status = 'finished';
        // $sitemapId = 10680;
        // $sitemapName = 'cip-c3-infooperacionais-1206';

        // $scrapingJobId =  78900;
        // $status = 'finished';
        // $sitemapId = 10681;
        // $sitemapName = 'cip-ctc-comunoperacionais-1206';

        // $scrapingJobId = 78901;
        // $status = 'finished';
        // $sitemapId = 10677;
        // $sitemapName = 'cip-ctc-infooperacionais-1206';

        // $scrapingJobId =  78895;
        // $status = 'finished';
        // $sitemapId = 10678;
        // $sitemapName = 'cip-dda-infooperacionais-1206';

        // $scrapingJobId =  84061;
        // $status = 'finished';
        // $sitemapId = 10676;
        // $sitemapName = 'cip-siloc-infooperacionais-1206';

        // $scrapingJobId =  78894;
        // $status = 'finished';
        // $sitemapId = 10677;
        // $sitemapName = 'cip-dda-comunoperacionais-1206';

        // $scrapingJobId =  78892;
        // $status = 'finished';
        // $sitemapId = 10675;
        // $sitemapName = 'cip-siloc-comunoperacionais-1206';

        // $scrapingJobId =  83899;
        // $status = 'finished';
        // $sitemapId = 10690;
        // $sitemapName = 'febraban-sarb-1406';

        // $scrapingJobId =  84082;
        // $status = 'finished';
        // $sitemapId = 10881;
        // $sitemapName = 'previc-instrucoes-previc-2106';

        // $scrapingJobId =  78867;
        // $status = 'finished';
        // $sitemapId = 10649;
        // $sitemapName = 'previc-decretos2015-2017-1206';

        // $scrapingJobId =  104682;
        // $status = 'finished';
        // $sitemapId = 10882;
        // $sitemapName = 'previc-portarias-2106';

        // $scrapingJobId =  83897;
        // $status = 'finished';
        // $sitemapId = 10655;
        // $sitemapName = 'ibgc2017-1206';

        // $scrapingJobId =  83721;
        // $status = 'finished';
        // $sitemapId = 10912;
        // $sitemapName = 'cetip-1206';
        
        // $scrapingJobId =  96769;
        // $status = 'finished';
        // $sitemapId = 10602;
        // $sitemapName = 'ministeriofazenda2017-0906';
       
        // $scrapingJobId =  83748;
        // $status = 'finished';
        // $sitemapId = 10610;
        // $sitemapName = 'coaf-comunicados-0906';

        // $scrapingJobId =  83772;
        // $status = 'finished';
        // $sitemapId = 10578;
        // $sitemapName = 'abbc-comunicados-0706';

        // $scrapingJobId =  83774;
        // $status = 'finished';
        // $sitemapId = 10580;
        // $sitemapName = 'abecs-0706';

        // $scrapingJobId =  79311;
        // $status = 'finished';
        // $sitemapId = 10590;
        // $sitemapName = 'bacen-2806';

        // $scrapingJobId =  85047;
        // $status = 'finished';
        // $sitemapId = 10593;
        // $sitemapName = 'anvisa-0706';

        // $scrapingJobId =  89673;
        // $status = 'finished';
        // $sitemapId = 10920;
        // $sitemapName = 'alesp-0706';

        // $scrapingJobId =  83778;
        // $status = 'finished';
        // $sitemapId = 10577;
        // $sitemapName = 'anbima-comunicados-0706';

        // $scrapingJobId =  83777;
        // $status = 'finished';
        // $sitemapId = 10575;
        // $sitemapName = 'anbima-circulares-0706';

        // $scrapingJobId =  83779;
        // $status = 'finished';
        // $sitemapId = 10576;
        // $sitemapName = 'anbima-oficios-0706';

        $scrapingJobId = (int) $_POST['scrapingjob_id'];
        $status = $_POST['status'];
        $sitemapId = (int) $_POST['sitemap_id'];
        $sitemapName = $_POST['sitemap_name'];

        ob_start();

        header('Connection: close');
        header('Content-Length: '.ob_get_length());
        ob_end_flush();
        ob_flush();
        flush();


        $outputFile =  "./assets/tmp/scrapingjob-data".$scrapingJobId.".csv";        
        $apiClient->downloadScrapingJobCSV($scrapingJobId, $outputFile);
        $records = Reader::createFromPath($outputFile)->fetchAssoc();

        foreach($records as $record) {
            $objInsertNormativo = new stdClass();

            if ($sitemapName == 'anbima-oficios-0706') {
                if ($record['DataEmissao'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['DataEmissao'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $data = explode(' ', $record['DataPubli']);
                    $objInsertNormativo->DataPubli = $data[0];
                    $objInsertNormativo->DataEmissao = $data[0];
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['DataEmissao-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['DataEmissao-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtTipoNorma = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif ($sitemapName == 'anbima-circulares-0706') {
                if ($record['Titulo'] != 'null') {
                    $data = explode('-', $record['Titulo']);
                    $numero = explode('nº', $data[0]);
                    $numero = preg_replace('/\s+/', '', $numero[1]);
                    $dataNorma = preg_replace('/\s+/', '', $data[1]);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->DataPubli = $dataNorma;
                    $objInsertNormativo->DataEmissao = $dataNorma;
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtTipoNorma = 'Circular';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif ($sitemapName == 'anbima-comunicados-0706') {
                if ($record['Titulo'] != 'null') {
                    $data = explode('-', $record['Titulo']);
                    $numero = explode('nº', $data[0]);
                    $numero = preg_replace('/\s+/', '', $numero[1]);
                    $dataNorma = preg_replace('/\s+/', '', $data[1]);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->DataPubli = $dataNorma;
                    $objInsertNormativo->DataEmissao = $dataNorma;
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtTipoNorma = 'Comunicado de Supervisão';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif ($sitemapName == 'alesp-0706') {
                if ($record['Titulo'] != 'null') {
                    $data = explode(',', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $numero = explode('nº', $data[0]);
                    $numero = preg_replace('/\s+/', '', $numero[1]);
                    $dataNorma =  explode('de', $data[1]);
                    $dataNorma = preg_replace('/\s+/', '', $dataNorma[1]);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->DataEmissao = trim($dataNorma);
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                // if ($record['Responsavel'] != 'null') {
                //     $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                // }else{
                // }

                if ($record['DataPubli'] != 'null') {
                    $data = explode('(', $record['DataPubli']);
                    $data2 =  explode(',', $data[1]);
                    $dataNorma =  explode(' ', $data2[0]);
                    $dataNorma = preg_replace('/\s+/', '', $dataNorma[1]);

                    $objInsertNormativo->DataPubli = $dataNorma;
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Status'] != 'null') {
                    $objInsertNormativo->txtRevogado = $record['Status'];
                }else{
                    $objInsertNormativo->txtRevogado = '|*|';
                }

                $objInsertNormativo->txtResponsavel = 'ALESP';
                $objInsertNormativo->txtOrigem = 'ALESP';
            }elseif ($sitemapName == 'anvisa-0706') {
                if ($record['Titulo'] != 'null') {
                    $data = explode('nº', $record['Titulo']);
                    $numero = explode('de', $data[1]);
                    $numero = trim($numero[0]);
                    $tipoNorma = explode('-', $record['Titulo']);
                    $tipoN = explode(' ', $tipoNorma[0]);
                    $dataEmissao = explode(' ', $data[1] );

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->txtTipoNorma = trim($tipoN[0]);
                    $objInsertNormativo->DataEmissao = end($dataEmissao);
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $assunto = explode('Ementa:', $record['Assunto']);
                    $assunto = ltrim($assunto[1]);
                    $objInsertNormativo->txtAssunto = $assunto;
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                // if ($record['Responsavel'] != 'null') {
                //     $responsavel = explode('Origem:', $record['Responsavel']);
                //     $responsavel = ltrim($responsavel[1]);
                //     $objInsertNormativo->txtResponsavel = $responsavel;
                // }else{
                // }

                if ($record['DataPubli'] != 'null') {
                    $data = explode('Data:', $record['DataPubli']);
                    $data = ltrim($data[1]);

                    $objInsertNormativo->DataPubli = $data;
                   
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                $objInsertNormativo->txtResponsavel = 'ANVISA';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANVISA';
            }elseif ($sitemapName == 'bacen-julho2017') {
                if ($record['Titulo'] != 'null') {
                    $data = explode('nº', $record['Titulo']);
                    $numero = explode(',', $data[1]);
                    $dataPubli = trim($numero[1]);
                    $numero = trim($numero[0]);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->DataPubli = $dataPubli;
                    $objInsertNormativo->DataEmissao = $dataPubli;
                    $objInsertNormativo->txtTipoNorma = trim($data[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['normativo'] != 'null') {
                    $responsavel = explode('Responsável:', $record['normativo']);
                    $responsavel = ltrim($responsavel[1]);
                    $objInsertNormativo->txtResponsavel = $responsavel;
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BACEN';
            }elseif ($sitemapName == 'abecs-0706') {
                if ($record['Titulo'] != 'null') {
                    $num = explode(' ', $record['Titulo']);
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $num[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Normativo';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'ABECS';
                $objInsertNormativo->txtOrigem = 'ABECS';
            }elseif ($sitemapName == 'abbc-comunicados-0706-mensamente') {
                // if ($record['Titulo'] != 'null') {
                //     $objInsertNormativo->txtTitulo = $record['Titulo'];
                // }else{
                // }
                
                if ($record['Assunto-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Assunto-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtTitulo = 'Comunicado ABBC';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'ABBC';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'ABBC';
            }elseif (($sitemapName == 'coaf-comunicados-0906')) {
                if ($record['Titulo'] != 'null') {
                    $numero = explode(' ', $record['Titulo']);
                    $titulo = explode('-', $record['Titulo']);
                    $objInsertNormativo->txtTitulo = trim($titulo[1]);
                    $objInsertNormativo->txtNumero = $numero[4];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $data = explode(' ', $record['DataPubli']);
                    $data = trim($data[5]);
                    $objInsertNormativo->DataPubli = $data;
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'SISCOAF';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'COAF';
            }elseif ($sitemapName == 'ministeriofazenda2017-0906') {
                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero = explode('nº', $record['Titulo']);
                    $numero2 = explode(',', $numero[1]);
                    $numero2 = trim($numero2[0]);
                    $objInsertNormativo->txtNumero = $numero2;
                    $tipoNorm = trim($numero[0]);

                    $dataEmissao = explode('de', $record['Titulo']);
                    $objInsertNormativo->DataEmissao = $dataEmissao[1].'de'.$dataEmissao[2].'de'.$dataEmissao[3];

                    
                    if ($tipoNorm == 'Portaria') {
                        $objInsertNormativo->txtTipoNorma = 'Portaria Ministerial';
                    }else{
                        $objInsertNormativo->txtTipoNorma = 'Portaria Interministerial';
                    }

                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->DataEmissao = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $data = explode(' ', $record['DataPubli']);
                    $data = trim($data[5]);
                    $objInsertNormativo->DataPubli = $data;
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'MFazenda';
                $objInsertNormativo->txtOrigem = 'MFazenda';
            }elseif ($sitemapName == 'cetip-1206') {
                if ($record['normativo'] != 'null') {
                    $data = explode('-', $record['normativo']);
                    $numero = explode(' ', $data[0]);
                    $numero = trim($numero[2]);
                    $titulo = array_shift($data);
                    $objInsertNormativo->txtNumero = $numero;
                    $objInsertNormativo->txtTitulo = trim($titulo);
                    $objInsertNormativo->txtAssunto = trim($data[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtAssunto = '';
                }
                
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtResponsavel = 'Cetip';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'Cetip';
            }elseif ($sitemapName == 'ibgc2017-1206') {
                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Titulo'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                    $objInsertNormativo->DataEmissao = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->DataPubli = '';
                }

                $objInsertNormativo->txtTitulo = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtResponsavel = 'IBGC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Notícia';
                $objInsertNormativo->txtOrigem = 'IBGC';
            }elseif ($sitemapName == 'previc-portarias-2106') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']); 
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $link = explode('/view', $record['Titulo-href']); 
                    $objInsertNormativo->txtLink = $link[0];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'PREVIC';
                $objInsertNormativo->txtTipoNorma = 'Portaria';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'PREVIC';
            }elseif ($sitemapName == 'previc-decretos-2106') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $link = explode('/view', $record['Titulo-href']); 
                    $objInsertNormativo->txtLink = $link[0];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Decreto';
                $objInsertNormativo->txtResponsavel = 'PREVIC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'PREVIC';
            }elseif ($sitemapName == 'previc-instrucoes-previc-2106') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $link = explode('/view', $record['Titulo-href']); 
                    $objInsertNormativo->txtLink = $link[0];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'PREVIC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Instrução';
                $objInsertNormativo->txtOrigem = 'PREVIC';
            }elseif ($sitemapName == 'febraban-sarb-1406') {
                if ($record['Titulo'] != 'null') {
                    $num = explode('nº', $record['Titulo']);
                    $num1 = explode('-', $num[1]);
                    
                    $var = explode('-', $record['Titulo'], 2);

                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtNumero = trim($num1[0]);
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'Febraban';
                $objInsertNormativo->txtTipoNorma = 'Autorregulação Bancária';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'FEBRABAN';
            }elseif ($sitemapName == 'cip-c3-comunioperacionais-1206') {
                if ($record['normativo'] != 'null') {        
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP C3';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'CIP';

            }elseif ($sitemapName == 'cip-c3-infooperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP C3';
                $objInsertNormativo->txtTipoNorma = 'Informativo';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-ctc-comunoperacionais-1206') {
               if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP CTC';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-ctc-infooperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP CTC';
                $objInsertNormativo->txtTipoNorma = 'Informativo';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-dda-infooperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP DDA';
                $objInsertNormativo->txtTipoNorma = 'Informativo';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-siloc-infooperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP SILOC';
                $objInsertNormativo->txtTipoNorma = 'Informativo';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-dda-comunoperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP DDA';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cip-siloc-comunoperacionais-1206') {
                if ($record['normativo'] != 'null') {
                    $var = explode(' - ', $record['normativo'], 2);
            
                    $objInsertNormativo->txtTitulo = $var[0];
                    $objInsertNormativo->txtAssunto = $var[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CIP SILOC';
                $objInsertNormativo->txtTipoNorma = 'Comunicado';
                $objInsertNormativo->txtOrigem = 'CIP';
            }elseif ($sitemapName == 'cpc-iasb-total1706') {
                if ($record['normativo'] != 'null') {
                    $data = explode('-', $record['normativo']);
                    $objInsertNormativo->txtTitulo = trim($data[1]);
                    $objInsertNormativo->txtAssunto =  trim(end($data));
                    $objInsertNormativo->DataPubli = trim($data[0]);
                    $objInsertNormativo->DataEmissao = trim($data[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->txtAssunto = '';
                }
            
                if ($record['normativo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['normativo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtTipoNorma = 'Contribuição para IASB';
                $objInsertNormativo->txtResponsavel = 'CPC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CPC';
            }elseif ($sitemapName == 'cpc-revisoes-total1706') {
                if ($record['Documento']) {
                    $num = explode(' ', $record['Documento']);
                    $objInsertNormativo->txtTitulo = $record['Documento'];
                    $objInsertNormativo->txtNumero = $num[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['DataEmissao'] != 'null') {
                    $objInsertNormativo->DataEmissao = $record['DataEmissao'];
                }else{
                    
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Titulo'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtResponsavel = 'CPC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Revisão';
                $objInsertNormativo->txtOrigem = 'CPC';
            }elseif ($sitemapName == 'cpc-pronunciamentos-total1706') {
                if ($record['Numero'] != 'null') {
                    $num = explode(' ', $record['Numero']);
                    $objInsertNormativo->txtTitulo = $record['Numero'];
                    $objInsertNormativo->txtNumero = $num[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['DataEmissao'] != 'null') {
                    $objInsertNormativo->DataEmissao = $record['DataEmissao'];
                }else{
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Titulo'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtTipoNorma = 'Pronunciamento';
                $objInsertNormativo->txtResponsavel = 'CPC';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CPC';
            }elseif ($sitemapName == 'cpc-pronunciamentos-link1706') {               
                $objUpdateNormativo = new stdClass();

                $objUpdateNormativo->txtLink = $record['Numero-href'];
                $arrayCondition = array('txtTitulo = ' .$record['Numero']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'cpc-revisoes-link1906') {               
                $objUpdateNormativo = new stdClass();

                $objUpdateNormativo->txtLink = $record['Documento-href'];
                $arrayCondition = array('txtTitulo = ' .$record['Documento']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'cvm-ultimos2017') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode(' ', $record['Titulo']);
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = end($numero);
                    $objInsertNormativo->txtTipoNorma = $numero[0];
                    $objInsertNormativo->txtResponsavel = $numero[1]; 
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['DataEmissao'] != 'null') {
                    $objInsertNormativo->DataEmissao = $record['DataEmissao'];
                }else{
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                if ($record['DataRetificacao']  != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataRetificacao'];
                }elseif ($record['DataPubli']  != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }elseif ($record['DataEmissao']  != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataEmissao'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CVM';

            }elseif ($sitemapName == 'cvm-audienciaspublicas-abertamanifestacao1706') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode(' ', $record['Titulo']);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = end($numero);
                    $objInsertNormativo->txtTipoNorma = trim($numero[1]);

                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }

                // if ($record['Prazo para Manifestação'] != 'null') {
                //     $objInsertNormativo->DataPubli = $record['Prazo para Manifestação'];
                // }else{
                // }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CVM';
                $objInsertNormativo->txtTipoNorma = 'Audiência Pública';
                $objInsertNormativo->txtOrigem = 'CVM';

            }elseif ($sitemapName == 'cvm2017-mensalmente') {
                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }

                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['DataPublic'] != 'null') {
                    $data = explode('Data:', $record['DataPublic']);
                    $objInsertNormativo->DataPubli = trim($data[1]);
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                if ($record['TipoNorma'] != 'null') {
                    $tipoNorma = explode('Tipo:', $record['TipoNorma']);
                    $objInsertNormativo->txtTipoNorma = trim($tipoNorma[1]);
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }    

                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtResponsavel = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CVM';
            }elseif ($sitemapName == 'ans2017-0706') {
                if ($record['Titulo'] != 'null') {
                    $var = explode('/', $record['Titulo']);
                    $numero = explode('nº', $var[0]);
                
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtResponsavel = trim($var[1]);
                    $objInsertNormativo->txtNumero = trim($numero[1]);
                    $objInsertNormativo->txtTipoNorma = trim($numero[0]);
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }    

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANS';
            }elseif ($sitemapName == 'ans2017-link1706') {               
                $objUpdateNormativo = new stdClass();

                $objUpdateNormativo->txtLink = $record['Link-href'];

                if ($record['Assunto'] ==  null) {
                    $arrayCondition = array('txtAssunto = ' .$record['Assunto2']);
                }else{
                    $arrayCondition = array('txtAssunto = ' .$record['Assunto']);
                }

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif (($sitemapName == 'bndes-indiretas-agropecuarias-link1406')) {
            
                if ($record['Titulo']  != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $var = explode('nº', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtLink =  $record['Titulo-href'];
                }elseif ($record['Titulo2']  != 'null') {
                    $var = explode('nº', $record['Titulo2']);
                    $tipoNorma = explode(' ', $record['Titulo2']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtTitulo = $record['Titulo2'];
                    $objInsertNormativo->txtLink =  $record['Titulo2-href'];
                }elseif ($record['Titulo3']  != 'null') {
                    $var = explode('nº', $record['Titulo3']);
                    $tipoNorma = explode(' ', $record['Titulo3']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtTitulo = $record['Titulo3'];
                    $objInsertNormativo->txtLink =  $record['Titulo3-href'];
                }elseif ($record['Titulo4']  != 'null') {
                    $var = explode('nº', $record['Titulo4']);
                    $tipoNorma = explode(' ', $record['Titulo4']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                     $objInsertNormativo->txtTitulo = $record['Titulo4'];
                    $objInsertNormativo->txtLink =  $record['Titulo4-href'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtLink = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BNDES';
            }elseif ($sitemapName == 'bndes-indiretas-automaticas-link1406') {
            if ($record['Titulo']  != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $var = explode('nº', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtLink =  $record['Titulo-href'];
                }elseif ($record['Titulo2']  != 'null') {
                    $var = explode('nº', $record['Titulo2']);
                    $tipoNorma = explode(' ', $record['Titulo2']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtTitulo = $record['Titulo2'];
                    $objInsertNormativo->txtLink =  $record['Titulo2-href'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtLink = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BNDES';
            }elseif ($sitemapName == 'bndes-indiretas-exportacao-link1906') {
            
               if ($record['Titulo']  != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $var = explode('nº', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtLink =  $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtLink = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BNDES';
            }elseif ($sitemapName == 'bndes-indiretas-naoautomaticas-link1906') {
            
               if ($record['Titulo']  != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $var = explode('nº', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtLink =  $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtLink = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BNDES';
            }elseif ($sitemapName == 'bndes-regula-fgi-link1906') {
            if ($record['Titulo']  != 'null') {
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $var = explode('nº', $record['Titulo']);
                    $tipoNorma = explode(' ', $record['Titulo']);
                    $responsavel = explode(' ', $var[0]);

                    $objInsertNormativo->txtResponsavel = $responsavel[1];
                    $objInsertNormativo->txtTipoNorma = $tipoNorma[0];
                    $objInsertNormativo->txtNumero = trim($var[1]);
                    $objInsertNormativo->txtLink =  $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtLink = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                }

                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'BNDES';
            }elseif ($sitemapName == 'bmfbovespa-0906') {

                if ($record['Titulo'] != 'null') {
                    $tipoNorma = explode('-', $record['Titulo']);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $tipoNorma[0];
                    $objInsertNormativo->txtTipoNorma = end($tipoNorma);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    // $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'BM&FBovespa';
                $objInsertNormativo->txtOrigem = 'BM&FBovespa';
            }elseif ($sitemapName == 'susep-ultimaspubli1107') {

                if ($record['Titulo'] != 'null') {
                    $data = explode('-', $record['Titulo']);
                    $numero = explode(' ', $data[1]);
                    $tipoNorma = explode(' ', $data[0]);
                    $val = explode(' ', str_replace("/"," ",$tipoNorma[1]));
                    $dataEmissao = explode('de', $record['Titulo']);

                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = trim($numero[1]);
                    $objInsertNormativo->txtTipoNorma = trim($tipoNorma[0]);
                    $objInsertNormativo->DataEmissao = $dataEmissao[1].'de'.$dataEmissao[2].'de'.$dataEmissao[3];
                    if (isset($val[1])) {
                        $objInsertNormativo->txtResponsavel = $val[1];
                    }else{
                        $objInsertNormativo->txtResponsavel = 'SUSEP';
                    }
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                    $objInsertNormativo->txtResponsavel = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $dataPubli = explode('Data de Publicação:', $record['DataPubli']);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $assunto = explode('Ementa:', $record['Assunto']);
                    $objInsertNormativo->txtAssunto = trim($assunto[1]);
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'SUSEP';
            }elseif ($sitemapName == 'anbima-fundosinvestimento-1007') {

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                    $objInsertNormativo->DataEmissao = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['fundos-de-investimento-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['fundos-de-investimento-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                $objInsertNormativo->txtTitulo = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Fundo de Investimento';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif ($sitemapName == 'denatran-resolucoes-completa1906') {

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = str_replace(".","/",$record['DataPubli']);
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['DataEmissao'] != 'null') {
                    $objInsertNormativo->DataEmissao = str_replace(".","/",$record['DataEmissao']);
                }else{
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtTitulo = 'Resolução '.$record['Numero'];
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTitulo = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Resolução';
                $objInsertNormativo->txtResponsavel = 'DENATRAN';
                $objInsertNormativo->txtOrigem = 'DENATRAN';
            }elseif ($sitemapName == 'denatran-portarias2017-1906') {

                if ($record['Titulo'] != 'null') {
                    $num = explode(' ', $record['Titulo']);
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $num[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = str_replace(".","/",$record['DataPubli']);
                    $objInsertNormativo->DataEmissao = str_replace(".","/",$record['DataPubli']);
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Portaria';
                $objInsertNormativo->txtResponsavel = 'DENATRAN';
                $objInsertNormativo->txtOrigem = 'DENATRAN';
            }elseif ($sitemapName == 'denatran-deliberacoes-completa1906') {

                if ($record['Titulo'] != 'null') {
                    $num = explode(' ', $record['Titulo']);
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $objInsertNormativo->txtNumero = $num[1];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = str_replace(".","/",$record['DataPubli']);
                    $objInsertNormativo->DataEmissao = str_replace(".","/",$record['DataPubli']);
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Deliberação';
                $objInsertNormativo->txtResponsavel = 'DENATRAN';
                $objInsertNormativo->txtOrigem = 'DENATRAN';
            }elseif ($sitemapName == 'denatran-resolucoes-link1906') {               
                $objUpdateNormativo = new stdClass();

                if ($record['Numero']  != 'null') {
                    $arrayCondition = array('txtTitulo = Resolução '.$record['Numero']);
                    $objUpdateNormativo->txtLink =  $record['Numero2-href'];
                }elseif ($record['Numero2']  != 'null') {
                    $arrayCondition = array('txtTitulo = Resolução '.$record['Numero2']);
                    $objUpdateNormativo->txtLink =  $record['Numero2-href'];
                }elseif ($record['Numero3']  != 'null') {
                    $arrayCondition = array('txtTitulo = Resolução '.$record['Numero3']);
                    $objUpdateNormativo->txtLink =  $record['Numero3-href'];
                }elseif ($record['Numero4']  != 'null') {
                    $arrayCondition = array('txtTitulo = Resolução '.$record['Numero4']);
                    $objUpdateNormativo->txtLink =  $record['Numero4-href'];
                }


                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);

            }elseif ($sitemapName == 'denatran-portarias-link1906') {               
                $objUpdateNormativo = new stdClass();

                if ($record['Titulo']  != 'null') {
                    $arrayCondition = array('txtTitulo = ' .$record['Titulo']);
                    $objUpdateNormativo->txtLink =  $record['Titulo2-href'];
                }elseif ($record['Titulo2']  != 'null') {
                    $arrayCondition = array('txtTitulo = ' .$record['Titulo2']);
                    $objUpdateNormativo->txtLink =  $record['Titulo2-href'];
                }

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);

            }elseif ($sitemapName == 'denatran-deliberacoes-link1906') {               
                $objUpdateNormativo = new stdClass();

                if ($record['Titulo']  != 'null') {
                    $arrayCondition = array('txtTitulo = ' .$record['Titulo']);
                    $objUpdateNormativo->txtLink =  $record['Titulo2-href'];
                }elseif ($record['Titulo2']  != 'null') {
                    $arrayCondition = array('txtTitulo = ' .$record['Titulo2']);
                    $objUpdateNormativo->txtLink =  $record['Titulo2-href'];
                }elseif ($record['Titulo3']  != 'null') {
                    $arrayCondition = array('txtTitulo = ' .$record['Titulo3']);
                    $objUpdateNormativo->txtLink =  $record['Titulo3-href'];
                }

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'crsfn-conselhinho-bcb-1207') {

                if ($record['Titulo'] != 'null') {
                    $objInsertNormativo->txtTitulo =  substr($record['Titulo'], 0, strrpos($record['Titulo'], '.'));
                    if (preg_match('/\s/',$objInsertNormativo->txtTitulo) > 0) {
                        $objInsertNormativo->txtTipoNorma = 'Recurso';
                    }else{
                        $objInsertNormativo->txtTipoNorma = 'Acórdão';    
                    }

                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }
                

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtResponsavel = 'CRSFN';
                $objInsertNormativo->txtOrigem = 'CRSFN';
            }elseif ($sitemapName == 'planalto-leis-delegadas-completa1706') {

                if ($record['Lei Delegada'] != 'null') {
                    $numero = explode(',', $record['Lei Delegada']);
                    $bloco = explode('Publicada', $numero[1]);
                    $data = explode('de', $bloco[0]);
                    $dataPubli = explode('de', $bloco[1]);

                    $objInsertNormativo->txtTitulo =  trim('Lei Delegada nº '.$numero[0].$bloco[0]);
                    $objInsertNormativo->txtNumero = $numero[0];
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Lei Delegada';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-medidasprovisorias2015-2018-1706') {

                if ($record['Medida Provisoria'] != 'null') {
                    $numero = explode(',', $record['Medida Provisoria']);
                    $bloco = explode('Publicada', $numero[1]);
                    $data = explode('de', $bloco[0]);
                    $data2 = explode('de', $bloco[1]);
                    $dataPubli = explode(' ', $data2[1]);

                    $objInsertNormativo->txtTitulo =  trim('Medida Provisória nº '.$numero[0].$bloco[0]);
                    $objInsertNormativo->txtNumero = $numero[0];
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Medida Provisória';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-msgveto-2106') {

                if ($record['numero-veto'] != 'null') {
                    $bloco = explode('Publicado', $record['numero-veto']);
                    $data = explode('de', $bloco[0]);
                    $dataPubli = explode('de', $bloco[1]);

                    $objInsertNormativo->txtTitulo =  trim('Mensagem de Veto Total nº '.$bloco[0]);
                    $objInsertNormativo->txtNumero = $data[0];
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Mensagem de Veto';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-leis-ordinarias-1706') {

                if ($record['Lei Ordinaria'] != 'null') {
                    $numero = explode(',', $record['Lei Ordinaria']);
                    $bloco = explode('Publicada', $record['Lei Ordinaria']);
                    $data = explode('de', $bloco[0]);
                    $dataPubli = explode('de', $bloco[1]);

                    $objInsertNormativo->txtTitulo =  trim('Lei nº '.$bloco[0]);
                    $objInsertNormativo->txtNumero = $numero[0];
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Lei Ordinária';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-decretos-naonumerados-1706') {

                if ($record['Normativo'] != 'null') {
                    $bloco = explode('Publicado', $record['Normativo']);
                    $data = explode('de', $bloco[0]);
                    $dataPubli = explode('de', $bloco[1]);

                    $objInsertNormativo->txtTitulo =  trim($bloco[0]);
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Decreto';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-decretos1706') {

                if ($record['Decreto Numero'] != 'null') {
                    $numero = explode(',', $record['Decreto Numero']);
                    $bloco = explode('Publicado', $record['Decreto Numero']);
                    $data = explode('de', $bloco[0]);
                    $dataPubli = explode('de', $bloco[1]);

                    $objInsertNormativo->txtTitulo =   'Decreto nº '.$bloco[0];
                    $objInsertNormativo->txtNumero = $numero[0];
                    $objInsertNormativo->DataEmissao = trim($data[1]);
                    $objInsertNormativo->DataPubli = trim($dataPubli[1]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Decreto';    
                $objInsertNormativo->txtResponsavel = 'Planalto';
                $objInsertNormativo->txtOrigem = 'Planalto';
            }elseif ($sitemapName == 'planalto-msgveto-link2106') {               
                $objUpdateNormativo = new stdClass();

                $objUpdateNormativo->txtLink = $record['numero-veto-href'];
                $arrayCondition = array('txtTitulo = Mensagem de Veto Total nº ' .$record['numero-veto']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'planalto-medidasprovisorias-link2106') {               
                $objUpdateNormativo = new stdClass();

                $objUpdateNormativo->txtLink = $record['numero-medida-href'];
                $arrayCondition = array('txtTitulo = Medida Provisória nº ' .$record['numero-medida']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'planalto-leis-ordinarias-link2106') {               
                $objUpdateNormativo = new stdClass();
                $objUpdateNormativo->txtLink = $record['numero-lei-href'];
                $arrayCondition = array('txtTitulo = Lei nº ' .$record['numero-lei']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'planalto-leis-delegadas-link2106') {               
                $objUpdateNormativo = new stdClass();
                $objUpdateNormativo->txtLink = $record['numero-lei-href'];
                $arrayCondition = array('txtTitulo = Lei Delegada nº ' .$record['numero-lei']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'planalto-decretos-naonumerados-link2106') {               
                $objUpdateNormativo = new stdClass();
                $objUpdateNormativo->txtLink = $record['Titulo-href'];
                $arrayCondition = array('txtTitulo = ' .$record['Titulo']);

                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'planalto-decretos-link2106') {               
                $objUpdateNormativo = new stdClass();
                $objUpdateNormativo->txtLink = $record['Decreto-href'];
                $arrayCondition = array('txtTitulo = Decreto nº ' .$record['Decreto']);


                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);
            }elseif ($sitemapName == 'cvm-junho-datapubli1307') {               
                $objUpdateNormativo = new stdClass();

                unset($objInsertNormativo);

                if ($record['datapubli'] != 'null') {
                    $objUpdateNormativo->DataPubli = $record['datapubli'];
                }else{
                    $objUpdateNormativo->DataPubli = $record['dataemissao'];
                }

                $arrayCondition = array('txtLink = \'' . $record['Titulo-href'] . '\'');
                
                $query = $this->crud_model->update($objUpdateNormativo, 'tabnovosnormativos', $arrayCondition);

            }elseif ($sitemapName == 'rfb-portarias-1407') {

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTitulo =  $record['TipoNorma'].' '. $record['Responsavel'].' nº '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Responsavel'] != 'null') {
                    $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTipoNorma = $record['TipoNorma'];
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }

                
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'Receita Federal';
            }elseif (trim($sitemapName) == 'rfb-solucconsultadivergencia-1407') {

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTitulo =  $record['TipoNorma'].' '. $record['Responsavel'].' nº '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Ementa'] != 'null') {
                    $assunto = explode('EMENTA:', $record['Ementa']);
                    $assunto2 = explode('ASSUNTO:', $assunto[0]);
                    $objInsertNormativo->txtAssunto = trim($assunto2[1]);
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Responsavel'] != 'null') {
                    $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTipoNorma = $record['TipoNorma'];
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }

                
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'Receita Federal';
            }elseif (trim($sitemapName) == 'rfb-atodeclaratoriointerpretativo-1407') {

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTitulo =  $record['TipoNorma'].' '. $record['Responsavel'].' nº '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Responsavel'] != 'null') {
                    $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTipoNorma = $record['TipoNorma'];
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }

                
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'Receita Federal';
            }elseif (trim($sitemapName) == 'rfb-instnormativa-1907') {

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTitulo =  $record['TipoNorma'].' '. $record['Responsavel'].' nº '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Responsavel'] != 'null') {
                    $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTipoNorma = $record['TipoNorma'];
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }

                
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'Receita Federal';
            }elseif (trim($sitemapName) == 'rfb-parecer-1907') {

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTitulo =  $record['TipoNorma'].' '. $record['Responsavel'].' nº '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                }

                if ($record['Responsavel'] != 'null') {
                    $objInsertNormativo->txtResponsavel = $record['Responsavel'];
                }else{
                    $objInsertNormativo->txtResponsavel = '';
                }

                if ($record['TipoNorma'] != 'null') {
                    $objInsertNormativo->txtTipoNorma = $record['TipoNorma'];
                }else{
                    $objInsertNormativo->txtTipoNorma = '';
                }

                
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'Receita Federal';
            }elseif (trim($sitemapName) == 'rfb-consulpublidisponiveis-1907') {

                if (strlen($record['consul-public']) > 2) {
                    
                    $data = explode(':', $record['consul-public']);
                    $tipoN = explode('nº', $data[0]);
                    $numero = explode('/', $tipoN[1]);
                    $objInsertNormativo->txtTitulo = trim($data[0]);
                    $objInsertNormativo->txtTipoNorma = trim($tipoN[0]);
                    $objInsertNormativo->txtNumero = trim($numero[0]);
                    $objInsertNormativo->txtAssunto = trim($data[1]);
                   

                    if ($record['consul-public-href'] != 'null') {
                        $objInsertNormativo->txtLink = $record['consul-public-href'];
                    }else{
                        $objInsertNormativo->txtLink = '';
                    }

                    $objInsertNormativo->txtResponsavel = 'RFB';
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                    $objInsertNormativo->txtRevogado = '|*|';
                    $objInsertNormativo->txtOrigem = 'Receita Federal';
                }else{
                    unset($objInsertNormativo);
                }
                
            }elseif (trim($sitemapName) == 'ans-consultapublica2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $data = explode(' ', $record['DataPubli']);
                    $objInsertNormativo->DataPubli = trim($data[0]);
                    $objInsertNormativo->DataEmissao = trim($data[0]);
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                    $objInsertNormativo->txtTitulo = 'Consulta Pública '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTitulo = '';
                }
                            
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtTipoNorma = 'Consulta Pública';
                $objInsertNormativo->txtResponsavel = 'ANS';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANS';
            }elseif (trim($sitemapName) == 'ans-audienciapublica2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $data = explode(' ', $record['DataPubli']);
                    $objInsertNormativo->DataPubli = $data[0];
                    $objInsertNormativo->DataEmissao = $data[0];
                }else{
                    $objInsertNormativo->DataPubli = '';
                    $objInsertNormativo->DataEmissao = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Numero'] != 'null') {
                    $objInsertNormativo->txtNumero = $record['Numero'];
                    $objInsertNormativo->txtTitulo = 'Audiência Pública '.$record['Numero'];
                }else{
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTitulo = '';
                }
                            
                $objInsertNormativo->txtLink = '';
                $objInsertNormativo->txtTipoNorma = 'Audiência Pública';
                $objInsertNormativo->txtResponsavel = 'ANS';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANS';
            }elseif (trim($sitemapName) == 'anbima-varejo2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['varejo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['varejo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['varejo'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['varejo'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Varejo';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-servicosqualificados2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['servico-qualificado-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['servico-qualificado-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['servico-qualificado'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['servico-qualificado'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Serviços Qualificados';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-rendafixa2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['novomercado-rendafixa-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['novomercado-rendafixa-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['novomercado-rendafixa'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['novomercado-rendafixa'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Renda Fixa';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-processos2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['processo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['processo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['processo'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['processo'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Processos';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-privatebanking2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['private-banking-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['private-banking-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['private-banking'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['private-banking'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Private Banking';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-ofertaspublicas2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['oferta-publica-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['oferta-publica-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['oferta-publica'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['oferta-publica'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Ofertas Públicas';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-negociacaoinstrfinanceiros2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['instrumento-financeiro-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['instrumento-financeiro-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['instrumento-financeiro'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['instrumento-financeiro'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Instrumento Financeiro';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-gestaodepatrimonio2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['gestao-patrimonio-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['gestao-patrimonio-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['gestao-patrimonio'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['gestao-patrimonio'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Gestão de Patrimônio';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-fip-fiee2107') {
                
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['fip-fiee-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['fip-fiee-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['fip-fiee'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['fip-fiee'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                            
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Fip Fiee';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-destaques2107') {
                
                if ($record['DatPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DatPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['Assunto-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Assunto-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }
            
                $objInsertNormativo->txtTitulo = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Destaques';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-codigoetica2107') {
                

                if ($record['codigo-de-etica-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['codigo-de-etica-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['codigo-de-etica'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['codigo-de-etica'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                            
                $objInsertNormativo->txtAssunto = '';
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Código de Ética';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-certificacao2107') {
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }


                if ($record['certificacao-conveniada-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['certificacao-conveniada-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['certificacao-conveniada'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['certificacao-conveniada'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }

                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Cerficação';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif (trim($sitemapName) == 'anbima-atividadesconveniadas2107') {
                if ($record['DataPubli'] != 'null') {
                    $objInsertNormativo->DataPubli = $record['DataPubli'];
                }else{
                    $objInsertNormativo->DataPubli = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }

                if ($record['atividade-conveniada-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['atividade-conveniada-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['atividade-conveniada'] != 'null') {
                    $objInsertNormativo->txtTitulo = $record['atividade-conveniada'];
                }else{
                    $objInsertNormativo->txtTitulo = '';
                }
                
                $objInsertNormativo->txtNumero = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtTipoNorma = 'Cerficação';
                $objInsertNormativo->txtResponsavel = 'ANBIMA';                
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'ANBIMA';
            }elseif ($sitemapName == 'cade-resolucoes-new') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'CADE';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtTipoNorma = 'Resolução';
                $objInsertNormativo->txtOrigem = 'CADE';
            }elseif ($sitemapName == 'cade-portarias-new') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                    $objInsertNormativo->txtTipoNorma = trim($numero[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'CADE';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CADE';
            }elseif ($sitemapName == 'cade-leis-new') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                    $objInsertNormativo->txtTipoNorma = trim($numero[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'CADE';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CADE';
            }elseif ($sitemapName == 'cade-decretos-new') {
                if ($record['Titulo'] != 'null') {
                    $numero = explode('nº', $record['Titulo']) ;
                    $objInsertNormativo->txtTitulo = $record['Titulo'];
                    $numero2 = explode(',', $numero[1]);
                    $objInsertNormativo->txtNumero = trim($numero2[0]);
                    $objInsertNormativo->txtTipoNorma = trim($numero[0]);
                }else{
                    $objInsertNormativo->txtTitulo = '';
                    $objInsertNormativo->txtNumero = '';
                    $objInsertNormativo->txtTipoNorma = '';
                }
                
                if ($record['Titulo-href'] != 'null') {
                    $objInsertNormativo->txtLink = $record['Titulo-href'];
                }else{
                    $objInsertNormativo->txtLink = '';
                }

                if ($record['Assunto'] != 'null') {
                    $objInsertNormativo->txtAssunto = $record['Assunto'];
                }else{
                    $objInsertNormativo->txtAssunto = '';
                }
                
                $objInsertNormativo->DataPubli = '';
                $objInsertNormativo->DataEmissao = '';
                $objInsertNormativo->txtResponsavel = 'CADE';
                $objInsertNormativo->txtRevogado = '|*|';
                $objInsertNormativo->txtOrigem = 'CADE';
            }

            if (isset($objInsertNormativo)) {
                $qe = $this->normativo_model->get_normativo_link($objInsertNormativo->txtLink);
                $x = '';

                if (count($qe) == 1) {
                    $x = 1;
                }else{
                    $x = 0;
                }

                if ($x == 0) {

                    $ye = $this->normativo_model->get_normativo_titulo($objInsertNormativo->txtTitulo);
                    $y = '';

                    if (count($ye) == 1) {
                        $y = 1;
                    }else{
                        $y = 0;
                    }

                    if ($y == 0) {
                        $dataNormativo = $objInsertNormativo->DataPubli;
                        if (strlen($objInsertNormativo->DataPubli) == 10) {
                            $arrData = explode("/", $dataNormativo);
                            $newDate = $arrData[2].'-'.$arrData[1].'-'.$arrData[0];
                            $today_dt = new DateTime('now');
                            $expire_dt = new DateTime($newDate);
                            $validade_normativo =  $today_dt->diff($expire_dt, true);
                            if ($validade_normativo->days < 5) {
                                $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                            }
                        }else{
                            $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                        }
                    }
                }else if ($objInsertNormativo->txtOrigem == 'MFazenda') {
                    $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                }else if ($objInsertNormativo->txtOrigem == 'COAF') {
                    $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                }else if ($objInsertNormativo->txtOrigem == 'ABBC') {
                    $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                }else if ($objInsertNormativo->txtOrigem == 'IBGC') {
                    $query = $this->crud_model->insert('tabnovosnormativos', $objInsertNormativo);
                }

                unset($objInsertNormativo);
                unset($x);
                unset($y);
            }
        }

        // die();
       
        // $this->template->showSite('scrap', $this->data);    

    }

   
    public function data_quality_scrap() {
        if (!isset($this->session->userdata['user']['bitAdministrador']))
            redirect('login', 'refresh');

        $this->data['novosNormativos'] = $this->normativo_model->get_novos_normativos();

        $this->data['normativos'] = '';
        
        $x = 0;
        for($i = 0; $i < count($this->data['novosNormativos']); $i++){

            if(isset($this->data['novosNormativos'][$i])){
                $qe = new stdClass();
                $v = 0; 
                if ($this->data['novosNormativos'][$i]->idNorma != NULL) {
                    $qe = $this->normativo_model->get_normativo_idNorma($this->data['novosNormativos'][$i]->idNorma);
                    if (count($qe) < 1) {
                        $v = 1;
                    }

                }else{
                    $v = 1;
                }

                if ($v == 1) {
                    $this->data['normativos'][$x]['id'] = $this->data['novosNormativos'][$i]->id;
                    $this->data['normativos'][$x]['idNorma'] = $this->data['novosNormativos'][$i]->idNorma;
                    $this->data['normativos'][$x]['txtOrigem'] = $this->data['novosNormativos'][$i]->txtOrigem;
                    $this->data['normativos'][$x]['DataPubli'] = $this->data['novosNormativos'][$i]->DataPubli;
                    $this->data['normativos'][$x]['txtTitulo'] = $this->data['novosNormativos'][$i]->txtTitulo;
                    $this->data['normativos'][$x]['txtLink'] = $this->data['novosNormativos'][$i]->txtLink;
                    $this->data['normativos'][$x]['txtClasse'] = $this->data['novosNormativos'][$i]->txtClasse;
                    $this->data['normativos'][$x]['txtAssunto'] = $this->data['novosNormativos'][$i]->txtAssunto;
                    $this->data['normativos'][$x]['txtTipoNorma'] = $this->data['novosNormativos'][$i]->txtTipoNorma;
                    $this->data['normativos'][$x]['txtNumero'] = $this->data['novosNormativos'][$i]->txtNumero;
                    $this->data['normativos'][$x]['txtRevogado'] = $this->data['novosNormativos'][$i]->txtRevogado;
                    $this->data['normativos'][$x]['DataEmissao'] = $this->data['novosNormativos'][$i]->DataEmissao;
                    $this->data['normativos'][$x]['txtResponsavel'] = $this->data['novosNormativos'][$i]->txtResponsavel;
                    $this->data['normativos'][$x]['txtGrupo'] = $this->data['novosNormativos'][$i]->txtGrupo;
                    $this->data['normativos'][$x]['txtExclusao'] = $this->data['novosNormativos'][$i]->txtExclusao;
                    $x++;
                    
                }
                unset($qe);
                unset($v);
            }
        }   

        $this->template->showSite('list-normativos-scrap', $this->data);    

    }

    public function save_normativos_scrap(){
        $objData = new stdClass();
        $objData = (object)$_POST;
  
        //Salvar os normativos
        $arr0 = $objData->id; 

        if (isset($objData->idNorma)) {
            $arr1 = $objData->idNorma; 
        }else{
            $arr1 = ''; 
        }
        if (isset($objData->txtOrigem)) { 
            $arr2 = $objData->txtOrigem; 
        }else{
            $arr2 = ''; 
        }
        if (isset($objData->txtTitulo)) {
            $arr3 = $objData->txtTitulo;  
        }else{
            $arr3 = ''; 
        }
        if (isset($objData->txtTipoNorma)) {
            $arr4 = $objData->txtTipoNorma;  
        }else{
            $arr4 = ''; 
        }
        if (isset($objData->txtNumero)) {
            $arr5 = $objData->txtNumero;  
        }else{
            $arr5 = ''; 
        }
        if (isset($objData->txtDataPublicacao)) {
            $arr6 = $objData->txtDataPublicacao;  
        }else{
            $arr6 = ''; 
        }
        if (isset($objData->txtRevogado)) {
            $arr7 = $objData->txtRevogado; 
        }else{
            $arr7 = ''; 
        }
        if (isset($objData->txtLink)) {
            $arr8 = $objData->txtLink; 
        }else{
            $arr8 = ''; 
        }
        if (isset($objData->txtDataEmissao)) { 
            $arr9 = $objData->txtDataEmissao;
        }else{
            $arr9 = ''; 
        }
        if (isset($objData->txtAssunto)) { 
            $arr10 = $objData->txtAssunto;
        }else{
            $arr10 = ''; 
        }
        if (isset($objData->txtResponsavel)) { 
            $arr11 = $objData->txtResponsavel;
        }else{
            $arr11 = ''; 
        }
        if (isset($objData->txtGrupo)) { 
            $arr12 = $objData->txtGrupo;
        }else{
            $arr12 = ''; 
        }
        if (isset($objData->txtClasse)) { 
            $arr13 = $objData->txtClasse;
        }else{
            $arr13 = ''; 
        }
        if (isset($objData->txtExclusao)) { 
            $arr14 = $objData->txtExclusao;
        }else{
            $arr14 = ''; 
        }
        
        for($i = 0; $i < count($arr0); $i++){
            if(isset($arr0[$i])){
                $qe = $this->normativo_model->get_normativo_idNorma($arr1[$i]);
                if ($qe) {        

                }else{
                    $objInsertNormativo = new stdClass();
                    if (isset($arr1[$i])) {
                        $objInsertNormativo->idNorma = $arr1[$i];
                    }else{
                        $objInsertNormativo->idNorma = '';
                    }
                    if (isset($arr2[$i])) {
                        $objInsertNormativo->txtOrigem = $arr2[$i];
                    }else{
                        $objInsertNormativo->txtOrigem = '';
                    }
                    if (isset($arr3[$i])) {
                        $objInsertNormativo->txtTitulo = $arr3[$i];
                    }else{
                        $objInsertNormativo->txtTitulo = '';
                    }
                    if (isset($arr4[$i])) {
                        $objInsertNormativo->txtTipoNorma = $arr4[$i];
                    }else{
                        $objInsertNormativo->txtTipoNorma = '';
                    }
                    if (isset($arr5[$i])) {
                        $objInsertNormativo->txtNumero = $arr5[$i];
                    }else{
                        $objInsertNormativo->txtNumero = '';
                    }
                    if (isset($arr6[$i])) {
                        $objInsertNormativo->DataPubli = $arr6[$i];
                    }else{
                        $objInsertNormativo->DataPubli = '';
                    }
                    if (isset($arr7[$i])) {
                        $objInsertNormativo->txtRevogado = $arr7[$i];
                    }else{
                        $objInsertNormativo->txtRevogado = '';
                    }
                    if (isset($arr8[$i])) {
                        $objInsertNormativo->txtLink = $arr8[$i];
                    }else{
                        $objInsertNormativo->txtLink = '';
                    }
                    if (isset($arr9[$i])) {
                        $objInsertNormativo->DataEmissao = $arr9[$i];
                    }else{
                        $objInsertNormativo->DataEmissao = '';
                    }
                    if (isset($arr10[$i])) {
                        $objInsertNormativo->txtAssunto = $arr10[$i];
                    }else{
                        $objInsertNormativo->txtAssunto = '';
                    }
                    if (isset($arr11[$i])) {
                        $objInsertNormativo->txtResponsavel = $arr11[$i];
                    }else{
                        $objInsertNormativo->txtResponsavel = '';
                    }
                    if (isset($arr12[$i])) {
                        $objInsertNormativo->txtGrupo = $arr12[$i];    
                    }else{
                        $objInsertNormativo->txtGrupo = '';
                    }
                    if (isset($arr13[$i])) {
                        $objInsertNormativo->txtClasse = $arr13[$i];
                    }else{
                        $objInsertNormativo->txtClasse = '';
                    }
                    if (isset($arr14[$i])) {
                        $objInsertNormativo->txtExclusao = $arr14[$i];
                    }else{
                        $objInsertNormativo->txtExclusao = '';
                    }

                    $arrayCondition2 = array('id = ' . $arr0[$i]);
                    $this->crud_model->update($objInsertNormativo, 'tabnovosnormativos', $arrayCondition2);

                    unset($objInsertNormativo);
                }
            }
        }   

        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

     public function remover_normativo(){

        $objData = new stdClass();
        $objData = (object)$_POST;

        $arrayCondition = array('id = ' . (int)$objData->id);
        $query = $this->crud_model->delete('tabnovosnormativos', $arrayCondition);


        header('Content-Type: application/json');
        echo json_encode(array("msg" => 'success', 'validate'=>true));

    }

}
