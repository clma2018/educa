<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dados_invalidos'] 	= '	<h3>Dados Inválidos</h3>
								<p>Os dados inseridos no formulário são inválidos.</p>
								<p>Por favor, verifique os dados digitados e tente novamente.</p>';

$lang['usuario_nao_encontrado'] = '	<h3>Usuário não encontrado</h3>
									<p>Não encontramos nenhum usuário com os dados informados.</p>
									<p>Por favor, verifique os dados digitados e tente novamente.</p>';

$lang['insert_task_success'] = '	<h3>A tarefa foi inserida com sucesso</h3>
									<p>Sua tarefa foi inserida com sucesso no sistema.</p>';

$lang['insert_project_success'] = '	<h3>O projeto foi inserido com sucesso</h3>
									<p>Seu projeto foi inserido com sucesso no sistema.</p>';

$lang['conclusion_task_success'] = '<h3>Tarefa concluída</h3>
									<p>A terefa foi concluída com sucesso no sistema.</p>';

$lang['reactivate_task_success'] = '<h3>Tarefa reativada</h3>
									<p>A tarefa foi reativada com sucesso.</p>';


$lang['cancel_task_success'] = '<h3>Tarefa cancelada</h3>
									<p>A tarefa foi cancelada com sucesso no sistema.</p>';

$lang['insert_user_task_success'] = '<h3>Usuário atribuído</h3>
									<p>O usuário foi vinculado a tarefa com sucesso.</p>';

$lang['delete_user_task_success'] = '<h3>Usuário removido</h3>
									<p>O usuário foi removido da tarefa com sucesso.</p>';

$lang['alter_task_success'] = '<h3>Tarefa alterada</h3>
									<p>A tarefa foi alterada com sucesso.</p>';

$lang['alter_client'] = '<h3>Cliente alterado</h3>
						<p>Os dados do cliente foram alterados com sucesso.</p>';

$lang['create_client'] = '<h3>Cliente cadastrado</h3>
						<p>O cliente foi cadastrado com sucesso.</p>';
