<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/styles2.css'); ?>" />
<body class="bg-main detNorm">  
    <div class="sidebar-l sidebar-mini sidebar-o side-scroll">    
        <?php $this->template->showTemplate('template/menu'); ?>
        <main class="dashboard">
            <div class="container-fluid">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= base_url('dashboard'); ?>">Caixa de Entrada</a></li>
                        <li class="active"><?=$normativo[0]->txtTitulo; ?></li>
                    </ol>
                </div>
                <div class="col-xs-12 col-sm-8">
                    
                    <!-- DETALHE NORMATIVO -->
                    <div class="detalhe-normativo">
                        <div class="block block-personalizado">
                            <h1><?=$normativo[0]->txtTitulo; ?></h1>
                            <h3><?=$normativo[0]->txtClasse; ?></h3>
                            <?php if ($normativo[0]->bitNormativoHistorico == 1): ?>
                             <h4 style='color:#ff0000'>Normativo Histórico <i class="fa fa-history"></i></h4>
                            <?php endif; ?>
                            <h4>Data de Vigência: <?=$normativo[0]->txtDataVigencia; ?></h4>
                            <p><?=$normativo[0]->txtAssunto; ?></p>
                            <a href="<?=$normativo[0]->txtLink; ?>" target="_blank" class="btnPadrao btn">Acessar link</a>

                            <?php if (isset($normativosRelacionados)): ?> 
                                <a href="#" class="btn btnPadrao pull-right btnNormativosRelacionados" data-toggle="modal" data-target="#modalNormativosRelacionados">Ver Normativos Relacionados</a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--END DETALHE NORMATIVO -->

                    <!-- COMENTARIOS --> 
                   <!--  <div class="box">
                        <div class="box-title">
                            <h5>Comentários</h5> <span class="label label-primary"><?= count($comentariosNormativo); ?></span>
                            <div class="box-tools">
                                <a href="#" id="tour-focus" class="btn-focus">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="box-content" style="display: none;">
                            <div class="comments-container">
                                <form class="form-comentario" id="formComentario">
                                    <div class="block-content">
                                        <input type="hidden" name="txtComentarioResposta" value='0'>
                                        <textarea placeholder="Insira seu comentário" name="txtComentario" required=""></textarea>
                                        <div class="push-15-t push-40">
                                            <button class="btn btnPadrao pull-right btnSalvarComentario" type='button'>Enviar</button>
                                        </div>
                                    </div>
                                </form>
                                <ul id="comments-list" class="comments-list">
                                    <?php estrutura_comentarios($estruturaComentarios); ?>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                    <!-- END COMENTARIOS --> 

                    <?php if (isset($comentario[0]->id)): ?>
                    <!-- COMENTARIO GESTOR -->
                    <!-- <div class="box">
                        <div class="box-title">
                            <h5>Comentário Gestor</h5> <span class="label label-primary"></span>
                            <div class="box-tools">
                                <a href="#" id="tour-focus" class="btn-focus">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="box-content" style="display: none;">
                            <h3>Comentário inserido pelo Gestor:</h3>
                            <p><?=$comentario[0]->txtComentario ?></p>
                        </div>
                    </div> -->
                    <!-- END COMENTARIO GESTOR -->
                    <?php endif; ?>
                       
                    <?php if (count($infoencaminhado) > 0): ?>
                    <!-- NORMATIVOS ENCAMINHADOS -->
                    <!-- <div class="box">
                        <div class="box-title">
                            <h5>E-mails Cadastrados</h5> <span class="label label-primary"><?= count($infoencaminhado); ?></span>
                            <div class="box-tools">
                                <a href="#" id="tour-focus" class="btn-focus">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="box-content" style="display: none;">
                            <ul class='listEmailsEncaminhados'>
                            <?php 
                                if (count($infoencaminhado) > 0):
                                    foreach ($infoencaminhado as $key => $infoEnc):
                            ?>   
                                <li>
                                    E-mail: <span class='color-azul push-10-l'><?= $infoEnc->txtEmail; ?></span>
                                    <?php if ($infoEnc->txtAnotacao !==''): ?>
                                    <a href="#" class="btn btn-xs btn-default push-10-l btnVisualizarAnotacaoNorm" type="button" data-toggle="tooltip" title="Visualizar Anotação" data-txtAnotacao="<?= $infoEnc->txtAnotacao; ?>" >
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <?php endif; ?>
                                </li>
                            <?php 
                                    endforeach;
                                endif;
                            ?>
                            </ul>         
                        </div>
                    </div> -->
                    <!-- END NORMATIVOS ENCAMINHADOS -->
                    <?php endif; ?>

                </div>
    
                <!-- <div class="col-sm-4 col-xs-12"> -->
                    <?php if ($normativo[0]->idUsuario == $this->session->userdata['user']['id']) :?>
                    <!-- ACOES -->
                    <!-- <div class="acoes-normativo">
                        <div class="block block-personalizado">
                            <h5>Ações</h5>
                            <div class="list-group position-relative">
                                <?php
                                    $class = '';
                                    if ($normativo[0]->bitCiente == 1){
                                        $class = 'active-list';
                                    }
                                ?>
                                <a class="list-group-item item-ciente <?= $class; ?>" href="#" data-toggle="tooltip" alt="Estou ciente" title="" data-original-title="Estou ciente">
                                    <i class="fa fa-fw fa-check push-5-r"></i> Ciente
                                </a>
                                <?php if (isset($avaliacao[0]->txtNota)): ?>
                                <a class="list-group-item item-prioridade item-block" href="#" title="">
                                    <i class="fa fa-fw fa-star push-5-r"></i> Prioridade <span><?= $avaliacao[0]->txtNota; ?><i class="fa fa-fw fa-star push-5-l"></i></span>
                                </a>
                                <?php else: ?>
                                <a class="list-group-item item-prioridade" data-toggle="modal" data-target="#modalAvaliacao" href="#" data-toggle="tooltip" alt="Definir a prioridade deste normativo" title="" data-original-title="Definir a prioridade deste normativo">
                                    <i class="fa fa-fw fa-star push-5-r"></i> Prioridade
                                </a>
                                <?php endif; ?>
                                <?php if ($normativo[0]->txtTipoAcao == 1): ?>
                                 <a class="list-group-item" data-toggle="modal" data-target="#modalPlanoAcao" href="#" data-toggle="tooltip" alt="Inserir plano de ação para este normativo" title="" data-original-title="Inserir plano de ação para este normativo" data-toggle="modal" data-target="#modalEncaminhar">
                                    <i class="fa fa-fw fa-clock-o push-5-r"></i> Inserir Plano de Ação
                                    </a>
                                <?php endif; ?>
                                <a class="list-group-item" data-toggle="modal" data-target="#modalAnotacao" href="#"  title="">
                                    <i class="fa fa-fw fa-book push-5-r"></i> 
                                    <?php if (isset($anotacao[0]->id)): ?>
                                    <span class="changeTitle">Editar Anotação</span>
                                    <?php else: ?>
                                    <span class="changeTitle">Inserir Anotação</span>
                                    <?php endif; ?>
                                </a>
                                <a class="list-group-item" data-toggle="modal" data-target="#modalEncaminhar" href="#" data-toggle="tooltip" alt="Encaminhar este normativo" title="" data-original-title="Encaminhar este normativo" data-toggle="modal" data-target="#modalEncaminhar">
                                    <i class="fa fa-fw fa-share push-5-r"></i> Encaminhar
                                </a>
                                <?php if ($normativo[0]->bitCiente == 2): ?>
                                <div class="bloco-ciente">
                                    <i class="fa fa-fw fa-lock"></i>
                                    <a href="#" class="btn btnPadrao" data-toggle="modal" data-target="#modalAceito">Ver opções</a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> -->
                    <!-- END ACOES -->
                    <?php endif; ?>

                    <!-- PLANOS DE ACAO -->
                    <?php  if (isset($planosAcao[0]->id)): ?>
                     <!-- <div class="acoes-normativo">
                        <div class="block block-personalizado">
                            <h5>Planos de Ação</h5>
                            <div class="list-group position-relative">
                                <a class="list-group-item" data-toggle="modal" data-target="#modalListPlano" href="#" data-toggle="tooltip">
                                    <i class="fa fa-fw fa-list-ul push-5-r"></i> Planos de Ação <span class="badge" style="top: 3px;position: relative;"><?= $qtPlanosAcao; ?></span>
                                </a>
                            </div>
                        </div>
                    </div> -->
                    <?php endif; ?>
                    <!-- PLANOS DE ACAO -->
                <!-- </div>   -->
            </div>
        </main>
    </div>

    <!-- MODAL PERGUNTA -->
    <div class="modal fade modal-personalizado" tabindex="-1" role="dialog" id="modalAceito" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                 <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Normativo Encaminhado:</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Este normativo deveria ser encaminhado para você?
                        <small>Esta mensagem auxiliará na aprendizagem das suas preferências</small>
                    </p>
                    <ul class='push-20-t'>
                        <li>
                            <a href="#" class="btnPadrao btnPadraoPreto btnOpcoes btn" id="opcaoNao">Não</a>
                        </li>
                        <li>
                            <a href="#" class="btnPadrao btnOpcoes btn" id="opcaoSim">Sim</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL PERGUNTA -->

    <!-- MODAL ENCAMINHAR -->
    <div class="modal fade modal-personalizado" tabindex="-1" role="dialog" id="modalEncaminhar" >
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Encaminhar Normativo</h4>
                </div>
                <div class="modal-body">
                    <form class="js-validation-encaminhar remove-margin-b" action="" method="post" id="formEncaminhar" novalidate="">
                        <div class="block-content">
                            <div class="form-group col-xs-12">
                                <p style="font-size: 14px;margin:0px 0px 15px;text-align:left">
                                    Inserir os e-mails que irão receber este normativo:
                                </p>
                                <input class="js-tags-input form-control" type="email" name="txtEmails" value="" required="required">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group push-20-t col-xs-12">
                                <p style="font-size: 14px;margin: 0px 0px 15px;text-align:left">
                                    Inserir uma anotação para ser enviada para os e-mails inseridos:
                                </p>
                                <textarea name="txtAnotacaoNormativoEncaminhar" class="form-control js-validate" placeholder="Insira uma anotação" value="" style="min-height:100px;box-shadow: none;border: 1px solid #cac8c8;"></textarea>    
                            </div>
                            <button class="btn btnPadrao push-20-t" type="submit">
                                Enviar
                            </button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL ENCAMINHAR -->

    <!-- MODAL AVALIACAO PRIORIDADE -->
    <div class="modal fade modal-personalizado modal-avaliacao" tabindex="-1" role="dialog" id="modalAvaliacao">
        <div class="modal-dialog modal-dialog-popin" role="document" style="margin-top:5%">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Avaliação do Normativo</h4>
                </div>
                <div class="modal-body">
                    <form class="js-validation-prioridade remove-margin-b" action="" method="post" id="formAvalicao">
                        <div class="block-content">
                            <input type="hidden" name="txtNota" id="txtNota">
                            <div class="form-group">
                                <p>Avalie o quanto este normativo foi relevante para você.</p>
                                <p class='color-azul'>Avalição Atual: <span class="js-rating-hint-text"></span></p>
                                <div class="js-rating" data-cancel="true" ></div>
                            </div>
                            <button class="btn btnPadrao push-20-t" type="submit">Enviar</button> 
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL AVALIACAO PRIORIDADE -->

    <!-- MODAL ANOTACAO -->
    <div class="modal fade modal-personalizado modal-avaliacao" tabindex="-1" role="dialog" id="modalAnotacao">
        <div class="modal-dialog modal-dialog-popin" role="document" style="margin-top:5%;">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <?php if (isset($anotacao[0]->id)): ?>
                    <h4 class="modal-title">Editar anotação:</h4>
                    <?php else: ?>
                    <h4 class="modal-title">Inserir anotação:</h4>
                    <?php endif ?>
                </div>
                <div class="modal-body">
                    <form class="remove-margin-b" action="" method="post" id="formAnotacao">
                        <div class="block-content">
                            <div class="boxAnotacaoNormativo">     
                                <div class="form-group form-material">
                                    <textarea name="txtAnotacao" class="form-control js-validate" id="js-ckeditor" placeholder="Insira uma anotação" value=""></textarea>    
                                </div>
                            </div>
                            <button class="btn btnPadrao" type="submit">Enviar</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL ANOTACAO -->

    <!-- MODAL LISTAGEM PLANO DE ACAO -->
    <div class="modal fade modal-personalizado modal-avaliacao" tabindex="-1" role="dialog" id="modalListPlano">
        <div class="modal-dialog modal-dialog-popin" role="document" style="margin-top:5%;">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Planos de Ação </h4>
                </div>
                <div class="modal-body">
                    <form class="block-content-planoAcao remove-margin-b">
                        <div class="block-content">
                            <p class='text-left'>Listagem dos planos de ação criados</p>
                            <div class="listagemPlanoAcao scrollbar">
                                <?php  
                                    if (isset($planosAcao[0]->id)):
                                        foreach ($planosAcao as $key => $plano):
                                            $bg = '';
                                            if ($plano->txtStatus == 'Concluído') {
                                               $bg = 'bg-plano-1';           
                                            }elseif ($plano->txtStatus == 'Cancelado') {
                                                $bg = 'bg-plano-2';
                                            }elseif ($plano->txtStatus == 'Ativo') {
                                                $bg = 'bg-plano-3';
                                            }elseif ($plano->txtStatus == 'Atrasado Data de Vigência') {
                                                $bg = 'bg-plano-4';
                                            }else{
                                                $bg = 'bg-plano-5';
                                            }; 
                                ?>
                                <div class="col-xs-12 detalhePlanoAcao <?= $bg; ?>" style="border:0px">
                                    <div class="col-xs-12 col-sm-4 bloco">
                                        <h2>Nome do Usuário:</h2>
                                        <h3><?= $plano->txtNomeUsuario; ?></h3>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 bloco">
                                        <h2>Data de Vencimento:</h2>
                                        <h3><?= $plano->txtDataVencimento; ?></h3>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 bloco">
                                        <h2>Status:</h2>
                                        <?php if ($plano->idUsuario == $this->session->userdata['user']['id']): ?>
                                            <select class="form-control selectStatus" name="txtStatus" size="1" required="" data-idPlano="<?= $plano->id; ?>">
                                                <option class='optionselected' disabled selected=""><?= $plano->txtStatus; ?></option>   
                                                <option value="Ativo">Ativo</option>
                                                <option value="Concluído">Concluído</option>
                                                <option value="Cancelado">Cancelado</option>
                                            </select>
                                        <?php else: ?>
                                            <h3><?= $plano->txtStatus; ?></h3>
                                        <?php endif ?>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2>Descrição:</h2>
                                        <p><?= $plano->txtDescricao; ?></p>
                                    </div>
                                </div>
                                <?php 
                                        endforeach;
                                    else:
                                ?>
                                <h3>Nenhum plano de ação cadastrado</h3>
                                <?php
                                    endif;
                                ?>
                            </div>
                            <button class="btn btnPadrao push-10-t" type="button" data-dismiss="modal">Fechar</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL LISTAGEM PLANO DE ACAO -->

    <!-- MODAL PLANO ACAO -->
    <div class="modal fade modal-personalizado modal-avaliacao" tabindex="-1" role="dialog" id="modalPlanoAcao">
        <div class="modal-dialog modal-dialog-popin" role="document" style="margin-top:5%">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Inserir Plano de Ação </h4>
                </div>
                <div class="modal-body">
                    <form class="js-validation-prioridade remove-margin-b" action="" method="post" id="formPlanoAcao">
                        <div class="block-content">
                            <div class="boxPlanoAcao text-left">
                                <div class="infoNormativo">    
                                    <h5 class='push-20'>Informações sobre o normativo:</h5>   
                                    <h3><b>Título:</b> <?=$normativo[0]->txtTitulo; ?></h3>
                                    <h3><b>Data de Publicação:</b> <?=$normativo[0]->dateCadastro; ?></h3>
                                    <h3><b>Classe:</b> <?=$normativo[0]->txtClasse; ?></h3>
                                    <h3><b>Data de Vigência:</b> <?=$normativo[0]->txtDataVigencia; ?></h3>
                                </div>
                                <h5>Informações do plano de ação:</h5>     
                                <div class="form-group form-material">
                                    <input type="text" name="txtDataVencimento" id="txtDataVencimento" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" required="required">
                                    <label for="txtDataVencimento">Escolha uma data</label>
                                </div>
                                <div class="form-group form-material">
                                    <textarea name="txtDescricao" class="form-control" id="txtDescricao" required="required"></textarea> 
                                    <label for="txtDescricao">Insira uma descrição</label>
                                </div>
                            </div>
                            <button class="btn btnPadrao push-10-t" type="submit">Enviar</button> 
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL PLANO ACAO -->

    <!-- MODAL VISUALIZAR ANOTACAO -->
    <div class="modal fade modal-personalizado" tabindex="-1" role="dialog" id="modalVisualizarAnotacaoNormativoEncaminhado">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
               <div class="modal-header">
                    <i class="si si-close" data-dismiss="modal"></i>
                    <h4 class="modal-title">Visualizar Anotação</h4>
                </div>
                <div class="modal-body">
                    <div class="block-content">
                        <h4 id='txtAnotacaoBlocoEmail'></h4>
                        <div class="text-center push-30-t">
                            <button class="btn btnPadrao" type="button" data-dismiss="modal">Fechar</button> 
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL VISUALIZAR ANOTACAO -->

    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery-tags-input-validation/jquery.tagsinput.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery-raty/jquery.raty.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/ckeditor/ckeditor.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/base_page_validation.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/legalbot.js'); ?>"></script>
    <script type="text/javascript">

        // OPCOES DO MODAL
        $('#opcaoSim').on('click', function (e) {
            $('#modalAceito').modal('hide');
            $('.bloco-ciente').remove();
            $('.list-opcoes').removeClass('disabled');
            $('.item-ciente').addClass('active-list');
            $.ajax({
                dataType : "json",
                data: { 
                        id : <?= $normativo[0]->id; ?>,
                        bitCiente : 1,
                    }, 
                type : 'post',
                url : '/normativo/alter_user_normativo',
                success : function(json) {
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Status alterado com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 9999,
                        delay: 1000,
                        timer: 500,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                },
                error : function(e) {
                    
                }
            });
        });

         // OPCOES DO MODAL
        $('#opcaoNao').on('click', function (e) {
            $.ajax({
                dataType : "json",
                data: { 
                        id : <?= $normativo[0]->id; ?>,
                        bitCiente : 0,
                    }, 
                type : 'post',
                url : '/normativo/alter_user_normativo',
                success : function(json) {
                    window.location.assign('/dashboard');
                },
                error : function(e) {
                    
                }
            });
        });

        $('#formAvalicao').submit(function() {
            var $form = $(this);
            if(! $form.valid()) return false;

            $('#formAvalicao').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: { 
                        idNormativoUsuario : <?= $normativo[0]->id; ?>,
                        txtNota : $("input[name=txtNota]").val()
                    },
                type : 'post',
                url : '/normativo/alter_user_normativo_prioridade',
                success : function(json) {
                    $('#formAvalicao').removeClass('block-opt-refresh');
                    $('#modalAvaliacao').modal('hide');
                    $('.item-prioridade').click(false);
                    $('.item-prioridade').append('<span>'+$("input[name=txtNota]").val()+' <i class="fa fa-fw fa-star push-5-l"></i></span>');
                    $("input[name=txtNota]").val('');
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Normativo avaliado com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                },
                error : function(e) {
                 
                }
            });
            return false;
        });

        $('#formEncaminhar').submit(function() {
            if ($("input[name=txtEmails]").val() === ''){
                $(".tagsinput").addClass('has-error-text'); 
                return false;
            }
            $('#formEncaminhar').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: { 
                        idNormativoUsuario : "<?= $normativo[0]->idNormativo; ?>",
                        txtNomeNormativo : "<?=$normativo[0]->txtTitulo; ?>",
                        txtLinkNormativo : "<?=$normativo[0]->txtLink; ?>",
                        txtEmail : $("input[name=txtEmails]").val(),
                        txtAnotacao : $("textarea[name=txtAnotacaoNormativoEncaminhar]").val(),
                    },
                type : 'post',
                url : '/normativo/encaminhar_normativo',
                success : function(json) {
                   
                    $('#formEncaminhar').removeClass('block-opt-refresh');
                    $("textarea[name=txtAnotacaoNormativoEncaminhar]").val('');
                    $('.tagsinput .tag').remove();
                    $('.js-tags-input').val('');
                    $(".tagsinput").removeClass('has-error-text'); 
                    $('#modalEncaminhar').modal('hide');

                    setTimeout(function () {
                        location.reload();
                    }, 3500);

                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Normativo encaminhado com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                   // $('.item-prioridade').append('<span>'+$("input[name=txtNota]").val()+' <i class="fa fa-fw fa-star push-5-l"></i></span>');
                },
                error : function(e) {
                 
                }
            });
            return false;
        });


        // TOGGLE DE MENSAGENS
        $('[data-toggle="tooltip"], .js-tooltip').tooltip({
            container: 'body',
            animation: false
        });
        
        // TAGS INPUT
        var emailsEncaminhados = [
            <?php foreach ($emailsNormativo as $key => $userNormEn): ?>
            "<?= $userNormEn->txtEmail; ?>",
            <?php endforeach; ?>
        ];

        var yourRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;// Email address
        $('.js-tags-input').tagsInput({
            width: '100%',
            defaultText: 'Adicionar e-mail',
            removeWithBackspace: true,
            pattern: yourRegex,
            onAddTag: function() {

                var array = $(this)[0].value.split(',');
                var arrayLast = array.pop();
                var elementExist;

                $(emailsEncaminhados).each(function(index) {
                    if (emailsEncaminhados[index] == arrayLast) {
                        elementExist = 1;
                    }
                });

                if (elementExist == 1) {
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'O e-mail "'+arrayLast+'" já recebeu este normativo.',
                    },
                    {
                        element: 'body',
                        type: 'danger',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 99999,
                        delay: 5000,
                        timer: 1200,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                    $('.js-tags-input').removeTag(arrayLast); 
                }else{
                    $(".tagsinput").removeClass('has-error-text'); 
                   
                }
            }
        });


        // RATING
        $('.js-rating').raty({
            starType: 'i',
            hints: ['Irrelevante', 'Pouco Relevante', 'Relevante', 'Muito Relevante', 'Fundamental'],
            number: 5,
            cancel: false,
            target: '.js-rating-hint-text',
            targetScore: true,
            // precision  : false,
            cancelOff: 'fa fa-fw fa-times text-danger',
            cancelOn: 'fa fa-fw fa-times',
            starHalf: 'fa fa-fw fa-star-half-o text-warning',
            starOff: 'fa fa-fw fa-star text-gray',
            starOn: 'fa fa-fw fa-star text-warning',
            click: function(score, evt) {
                 $("input[name=txtNota]").val(score);
            }
        });

        $('.js-datepicker').datepicker({
            startDate: '+1d',
            autoclose: true,
            todayHighlight: true,
            language: 'pt-BR'
        });
        
        
        // Init full text editor
        CKEDITOR.replace( 'js-ckeditor');

        <?php if (isset($anotacao[0]->id)): ?>
            CKEDITOR.instances['js-ckeditor'].setData("<?= $anotacao[0]->txtAnotacao; ?>");
        <?php endif; ?>

        $('#formAnotacao').submit(function() {            
            $('#formAnotacao').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: { 
                        idNormativo : <?= $normativo[0]->id; ?>,
                        txtAnotacao : CKEDITOR.instances['js-ckeditor'].getData()
                    },
                type : 'post',
                url : '/normativo/anotacao_normativo',
                success : function(json) {
                    $(".changeTitle").text("Editar Anotação");
                    $("#formAnotacao p").text("Editar anotação:");
                    $('#formAnotacao').removeClass('block-opt-refresh');
                    $('#modalAnotacao').modal('hide'); 
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Anotação inserida com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                },
                error : function(e) {
                 
                }
            });
            return false;
        });


        $('#formPlanoAcao').submit(function() {
            var $form = $(this);
            if(! $form.valid()) return false;

            $('#formPlanoAcao').addClass('block-opt-refresh');

            $.ajax({
                dataType : "json",
                data: { 
                        idNormativo : '<?= $normativo[0]->idNormativo; ?>',
                        txtDataVencimento : $("input[name=txtDataVencimento]").val(),
                        txtDescricao : $("textarea[name=txtDescricao]").val(),
                    },
                type : 'post',
                url : '/normativo/alter_user_normativo_planoAcao',
                success : function(json) {
                    $('#formPlanoAcao').removeClass('block-opt-refresh');
                    $('#modalPlanoAcao').modal('hide');
                    $("input[name=txtDataVencimento]").val('');
                    $("textarea[name=txtDescricao]").val('');
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Plano de ação criado com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                },
                error : function(e) {
                 
                }
            });
            return false;
        });
        
         $('select[name=txtStatus]').bind('change', function(){
            var txtStatus = $(this).find('option:selected').val();
            var idPlanoAcao = $(this).attr('data-idPlano');
            
            $('.block-content-planoAcao').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: { 
                    txtStatus : txtStatus,
                    id : idPlanoAcao
                },
                type : 'post',
                url : '/normativo/mudar_status',
                success : function(json) {
                    $('.optionselected').remove();
                    $('.block-content-planoAcao').removeClass('block-opt-refresh');
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Status alterado com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 99999,
                        delay: 3000,
                        timer: 700,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
               
                        
                },
                error : function(e) {
                    
                }
            });
            
        });

        $('body').on('click', '.btnSalvarComentario', function(e) {
            var $form = $('.form-comentario');
            // if(! $form.valid()) return false;
            if ($('#formComentarioResposta').length) {
                var txtComentario = $("#formComentarioResposta textarea[name=txtComentario]").val();
                var idComentarioResposta = $("#formComentarioResposta input[name=txtComentarioResposta]").val()
                $('#formComentarioResposta').addClass('block-opt-refresh');
            }else{
                var txtComentario = $("#formComentario textarea[name=txtComentario]").val();
                var idComentarioResposta = $("#formComentario input[name=txtComentarioResposta]").val()
                $('#formComentario').addClass('block-opt-refresh');
            }
            $.ajax({
                dataType : "json",
                data: { 
                        idNormativo : '<?= $normativo[0]->idNormativo; ?>',
                        txtComentario : txtComentario,
                        idComentarioResposta: idComentarioResposta
                    },
                type : 'post',
                url : '/normativo/salvar_comentario',
                success : function(json) {
                    var grupos = json.comentario.dateCreate.split(" ");
                    var data = grupos[0].split("-");
                    var dataCriado = data[2]+'/'+data[1]+'/'+data[0]+' '+grupos[1];
                    if ($('#formComentarioResposta').length) {
                        $('#formComentarioResposta').removeClass('block-opt-refresh');
                          $("li[data-idComentario=" + idComentarioResposta + "]").append('<ul class="comments-list reply-list"><li data-idComentario='+json.comentario.id+'><div class="comment-main-level"><div class="comment-box"><div class="comment-head"><h6 class="comment-name"><?= $this->session->userdata['user']['txtNome']; ?></h6><span>'+dataCriado+'</span><i class="fa fa-reply" data-idComentario='+json.comentario.id+'></i></div><div class="comment-content">'+json.comentario.txtComentario+'</div></div></div></li></ul>');
                    }else{
                        $('#formComentario').removeClass('block-opt-refresh');
                        $('#comments-list').append('<li data-idComentario='+json.comentario.id+'><div class="comment-main-level"><div class="comment-box"><div class="comment-head"><h6 class="comment-name"><?= $this->session->userdata['user']['txtNome']; ?></h6><span>'+dataCriado+'</span><i class="fa fa-reply" data-idComentario='+json.comentario.id+'></i></div><div class="comment-content">'+json.comentario.txtComentario+'</div></div></div></li>'); 
                    }
                    $('html, body').animate({
                        scrollTop: $("li[data-idComentario=" + json.comentario.id + "]").offset().top
                    }, 2000);

                    $('.form-comentario')[0].reset();
                    $('#formComentarioResposta').remove();
                    $('.list-resposta').remove();        
                   
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Comentário inserido com sucesso.',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                },
                error : function(e) {
                 
                }
            });
            return false;
        });



        $('body').on('click', '.fa-reply', function(e) {
            if ($('#formComentarioResposta').length) {

            }else{
                var idComentarioResposta = $(this).parents('li').attr("data-idComentario");
            
                $("li[data-idComentario=" + idComentarioResposta + "]").append('<ul class="comments-list reply-list list-resposta"><li data-idComentario=""><form class="form-comentario" id="formComentarioResposta"><div class="block-content"><input type="hidden" name="txtComentarioResposta" value="'+idComentarioResposta+'"><textarea placeholder="Insira sua resposta" name="txtComentario" required=""></textarea><div class="push-20-t push-40"><button class="btn btnPadrao btn-danger pull-left btnCancelarComentario" type="button">Cancelar</button><button class="btn btnPadrao pull-right btnSalvarComentario" type="button">Enviar</button></div></div></form></li></ul>');                 
            }
        });

        $('body').on('click', '.btnCancelarComentario', function(e) {
            $('#formComentarioResposta')[0].reset();
            $('#formComentarioResposta').remove();
        });
        

        $('body').on('click', '.btnVisualizarAnotacaoNorm', function(e) {
            e.preventDefault();         
            var txtAnotacao = $(this).attr("data-txtAnotacao");

            $('#modalVisualizarAnotacaoNormativoEncaminhado').modal('show');
            $('#txtAnotacaoBlocoEmail').text(txtAnotacao); 

        });

    </script>  
</body>