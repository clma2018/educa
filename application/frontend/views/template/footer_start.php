

<script type="text/javascript" src="<?php echo base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.stellar.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/pages/base_pages_images.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/slick/slick.js'); ?>"></script>
<script type="text/javascript">

    //PARALLAX
     (function(){
        var parallax = document.querySelectorAll(".parallax"),
        speed = 2.0;
        altura = $(".parallax").height()/2;

        window.onscroll = function(){
            [].slice.call(parallax).forEach(function(el,i){
                var windowYOffset = window.pageYOffset,
                novaAltura = windowYOffset+altura
                elBackgrounPos = "center " + (novaAltura * speed) + "px";   

                el.style.backgroundPosition = elBackgrounPos;

            });
        };

    })();


    // //ADICIONA A CLASSE DE SCROLL AO MENU
    // $(document).scroll(function() {
    //     var scrollPage = $(window).scrollTop();
    //     if (scrollPage > 100) {
    //         $('header').addClass('header-fixo');                    
            
    //     }else{
    //         $('header').removeClass('header-fixo');
    //     };
    // });

    // $(window).load(function(){
    //     var scrollPage = $(window).scrollTop();
    //     if (scrollPage > 100) {
    //         $('header').addClass('header-fixo');                    
            
    //     }else{
    //         $('header').removeClass('header-fixo');
    //     };
    // });


</script>