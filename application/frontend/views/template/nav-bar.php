<div class="header">
    <div class="header-content">
        <div class="left-menu">
            <div class="bloco-menu">
                <a href="<?= base_url('curso/'); ?>">
                    <img src="<?= base_url('assets/img/logo3.png'); ?>" style='max-width: 78px;position: absolute;top: -15px;' alt="Legal Bot">
                </a>
            </div>    
        </div>
        <div class="right-menu">
            <div class="bloco-menu dropdown">
                <a class="dropdown-link remove-padding" href="">
                    <div class="pos-r">
                        <div class="user-avatar">
                            <div class="user-avatar-inner fx-c">
                                <span class="user-avatar-initials"><?= $userLogged[0]->txtNome[0]; ?></span>
                            </div>
                        </div>
                        <b class="badge" aria-label="Mensagens não lidas">&nbsp;</b>
                    </div>
                </a>
                <div class="dropdown__menu" role="dialog">
                    <ul class="dropdown__menu-list">
                        <li class="menu__link">
                            <a href="<?= base_url('usuario/perfil_usuario/'. encode($this->session->userdata['user']['id'])); ?>" aria-label="Edite seu perfil">
                                <div class="user-avatar">
                                    <div class="user-avatar-inner fx-c">
                                        <span class="user-avatar-initials"><?= $userLogged[0]->txtNome[0]; ?></span>
                                    </div>
                                </div>
                                <span class="user-info">
                                    <span class="user-name">
                                        <?= $userLogged[0]->txtNome ?>
                                    </span>
                                    <span class="user-email">
                                        <?= $userLogged[0]->txtEmail ?>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <!-- <li class="menu__link">
                            <a href="<?= base_url('dashboard/'); ?>">
                                <span class="udi udi-bell-line"></span>
                                <span>Caixa de Entrada</span>
                            </a>
                        </li> -->
                        <li class="menu__link">
                            <a href="<?= base_url('curso/'); ?>">
                                <span class="udi udi-bell-line"></span>
                                <span>Meus Cursos</span>
                                <b class="badge"><?= $qtdSecoes ?></b>
                            </a>
                        </li>
                        <li role="presentation" class="link_bordered"></li>
                        <li class="menu__link">
                            <a href="<?= base_url('logout'); ?>">
                                <span>Sair</span>
                            </a>
                        </li>
                    </ul>
                   <!--  <ul class="dropdown__menu-list dropdown__menu_bottom">
                        <li class="menu__link">
                            <a href="#">
                                <span class="menu__title">Sair</span>
                            </a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>