<html lang="PT-br">
<head>
    <meta charset="utf-8">
    <title><?= (isset($page_title)) ? $page_title : TITLE; ?></title>
    <meta name="description" content="<?= (isset($page_description)) ? $page_description : DESCRIPTION; ?>">
    <meta name="keywords" content="<?= (isset($page_keywords)) ? $page_keywords : KEYWORDS; ?>">
    <meta name="author" content="<?= (isset($page_author)) ? $page_author : AUTHOR; ?>">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">
    <meta http-equiv="Name" content="LegalBot" />
    <meta http-equiv="City" content="São Paulo" />
    <meta http-equiv="State" content="SP" />
    <meta http-equiv="Country" content="Brasil" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="article">
    <meta property="og:title" content="<?= (isset($page_description)) ? $page_description : TITLE; ?>">
    <meta property="og:description" content="<?= (isset($page_description)) ? $page_description : DESCRIPTION; ?>">
    <meta property="og:url" content="<?= (isset($page_description)) ? $page_description : URL_CLIENT; ?>">
    <meta property="og:site_name" content="<?= (isset($page_description)) ? $page_description : DESCRIPTION; ?>">
    <meta property="article:publisher" content="">
    <meta property="fb:admins" content="">
    <meta property="og:image" content="<?php if (isset($og_image)) { echo base_url($og_image); } ?>">
    <link rel='index' title='<?= NAME_CLIENT?>' href="<?= (isset($page_description)) ? $page_description : URL_CLIENT; ?>"/>
    <link rel="canonical" href="<?= (isset($page_description)) ? $page_description : URL_CLIENT; ?>">
    
    <!-- CSS LINKS -->
    
    <!-- Client CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/video-js.css'); ?>" />    
    <link rel="resource" type="application/l10n" href="<?= base_url('assets/js/plugins/pdfjs-1.9.426-dist/web/locale/pt-BR/viewer.properties'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/js/plugins/pdfjs-1.9.426-dist/web/viewer.css'); ?>">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/js/plugins/dhtmlxScheduler_v4.4.0/codebase/dhtmlxscheduler.css'); ?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/fonts.css'); ?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/styles.css'); ?>" />

    
    <!-- JQUERY -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/core/jquery.min.js'); ?>"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-101205731-1', 'auto');
      ga('send', 'pageview');

    </script>
    
    <!-- Hotjar Tracking Code for http://sample-env.pvnhwahhmq.sa-east-1.elasticbeanstalk.com/ -->
    <script>
        // (function(h,o,t,j,a,r){
        //     h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        //     h._hjSettings={hjid:570167,hjsv:5};
        //     a=o.getElementsByTagName('head')[0];
        //     r=o.createElement('script');r.async=1;
        //     r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        //     a.appendChild(r);
        // })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
