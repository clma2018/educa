<header>
<?php  
    if ($this->agent->is_mobile()):
?>
<nav id="mobile">
    <div class="menu-hb">
        <div class="menuIcon">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
        </div>
    </div>
    <div class="nav-menu menu-mobile">
        <?php 
            $this->template->showTemplate('template/nav-bar');
        ?>
    </div>
</nav>
<?php else: ?>
<?php 
    $this->template->showTemplate('template/nav-bar');
?>
<?php endif; ?>
</header>
<script type="text/javascript">
    // ANIMAÇÃO MENU MOBILE
    $(".menuIcon").click(function () {
        $(".menu-mobile").toggle(500);
        $(this).toggleClass('showMenu');
    });
</script>