<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/styles2.css'); ?>" />
<body class="bg-main">  
<div class="sidebar-l sidebar-mini sidebar-o side-scroll">    
    <?php $this->template->showTemplate('template/menu'); ?>
    <main class="dashboard">
        <div class="container-fluid push-20-t">
            <div class="col-xs-12 col-sm-7">
                <!-- NORMATIVOS RECENTES --> 
                <div class="box">
                    <div class="box-title">
                        <h5>Normativos Recentes</h5> 
                        <div class="box-tools">
                            <a href="#" id="tour-focus" class="btn-focus">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-content">
                        <ul class="nav nav-tabs nav-filtros-normativos">
                            <li class="active"><a href="#diario" role="tab" data-toggle="tab">Diário</a></li>
                            <li><a href="#semanal" role="tab" data-toggle="tab" class='filtro-semanal'>Semanal</a></li>
                            <li><a href="#mensal" role="tab" data-toggle="tab" class='filtro-mensal'>Mensal</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="diario">
                                <div class="panel-group bloco-normativos" id="listagemNormativos">


                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab">
                                            <a role="button" data-toggle="collapse" data-parent="#listagemNormativos" href="#3463d">
                                                20/11/2017                                                                                                    <span class="badge">2</span>
                                            </a>
                                        </div>
                                        <div id="3463d" class="panel-collapse collapse" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="list-group">
                                                    <a class="list-group-item push-15" href="<?= base_url('curso/detalhe/1'); ?>" style="border: 2px solid #992337;:">
                                                        <h4>Curabitur a laoreet arcu</h4>
                                                        <h5>1:30</h5>
                                                        <p>
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet tincidunt tellus.
                                                        </p>
                                                    </a>
                                                    <a class="list-group-item push-15" href="#" style="background:rgba(255,181,1,0.5)">
                                                        <h5 style="color:rgba(255,181,1,0.8)">CRSFN</h5>
                                                        Recurso 13.696                                                        <h4>CRSFN Recurso</h4>
                                                        <ul class="footerNormativo push-10">
                                                            <li>Relevância: <strong>Relevante</strong></li>
                                                            <li>
                                                                Avaliação: <strong>
                                                                <i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i>                                                                </strong>
                                                            </li>
                                                        </ul>
                                                    </a>
                                                    <a class="list-group-item push-15" href="#" style="background:rgba(255,181,1,0.5)">
                                                        <h5 style="color:rgba(255,181,1,0.8)">CRSFN</h5>
                                                        Recurso 14.016                                                        <h4>CRSFN Recurso</h4>
                                                        <ul class="footerNormativo push-10">
                                                            <li>Relevância: <strong>Relevante</strong></li>
                                                            <li>
                                                                Avaliação: <strong>
                                                                <i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i><i class="fa fa-fw fa-star push-5-r"></i>                                                                </strong>
                                                            </li>
                                                        </ul>
                                                    </a>
                                                    
                                                    <a class="list-group-item push-15" href="<?= base_url('curso/detalhe/1'); ?>" style="border: 2px solid #992337;:">
                                                        <h4>Curabitur a laoreet arcu2</h4>
                                                        <h5>3:30</h5>
                                                        <p>
                                                             Mauris sit amet tincidunt tincidunt tellus tincidunt tellus tellus.
                                                        </p>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <?php foreach ($datanormativos as $key => $datanormativo): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab">
                                            <a role="button" data-toggle="collapse" data-parent="#listagemNormativos" href="#<?= $datanormativo->id; ?>">
                                                <?= $datanormativo->dateCadastro; ?>
                                                <?php 
                                                    foreach ($dataNormativo as $key => $dataN): 
                                                        if ($datanormativo->dateCadastro == $key):
                                                ?>
                                                    <span class="badge"><?= $dataN; ?></span>
                                                <?php
                                                        endif;
                                                    endforeach;
                                                ?> 
                                            </a>
                                        </div>
                                        <div id="<?= $datanormativo->id; ?>" class="panel-collapse collapse" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="list-group">
                                                <?php 
                                                    foreach ($normativos as $key => $normativo):
                                                        if ($datanormativo->dateCadastro == $normativo->dateCadastro):            
                                                ?>
                                                    <a class="list-group-item push-15" href="<?= base_url('normativo/detalhe/'.encode($normativo->id)); ?>" style="background:<?= $normativo->txtCorPrimaria;?>">
                                                        <h5 style="color:<?= $normativo->txtCorSecundaria;?>"><?= $normativo->txtOrigem; ?></h5>
                                                        <?= $normativo->txtTitulo; ?>
                                                        <h4><?= $normativo->txtClasse; ?></h4>
                                                        <?php if ($normativo->bitNormativoHistorico == 1): ?>
                                                        <h4 style='font-family: "GothamBook";color:#990000;'>Normativo Histórico <i class="fa fa-history"></i></h4>
                                                        <?php endif; ?>
                                                        <ul class="footerNormativo push-10">
                                                            <li>Relevância: <strong><?= $normativo->txtRelevancia; ?></strong></li>
                                                            <li>
                                                                Avaliação: <strong>
                                                                <?php 
                                                                    $var ='';
                                                                    if ($normativo->txtNota > 0) {
                                                                        for ($i=0; $i < $normativo->txtNota; $i++){
                                                                            $var .= '<i class="fa fa-fw fa-star push-5-r"></i>';
                                                                        }
                                                                        echo $var;
                                                                    }else{
                                                                        echo $normativo->txtNota;
                                                                    }
                                                                ?>
                                                                </strong>
                                                            </li>
                                                            <?php if ($normativo->bitNormativoHistorico == 1): ?>
                                                            <li>
                                                                Data de Publicação: <?= $normativo->txtDataVigencia; ?>
                                                            </li>
                                                            <?php endif; ?>
                                                        </ul>
                                                    </a>
                                                <?php 
                                                        endif;
                                                    endforeach;
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="semanal">
                                <div class="panel-group bloco-normativos" id="listagemNormativosSemana"></div>
                                <div class="loader">
                                    <div class="block-content"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="mensal">
                                <div class="panel-group bloco-normativos" id="listagemNormativosMes"></div>
                                <div class="loader">
                                    <div class="block-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END NORMATIVOS RECENTES --> 
            </div>
            <div class="col-xs-12 col-sm-5">
                <!-- HISTORICO DE NORMATIVOS --> 
                <div class="box">
                    <div class="box-title">
                        <h5>Histórico de Normativos</h5> 
                        <div class="box-tools">
                            <a href="#" id="tour-focus" class="btn-focus">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-content">
                        <canvas id="js-chartjs-donut" class="js-chartjs-donut" width="400" height="350"></canvas>
                    </div>
                </div>
                <!-- END HISTORICO DE NORMATIVOS --> 

                
            </div>
        </div>
    </main>
</div>

<script type="text/javascript" src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>  
<script type="text/javascript" src="<?= base_url('assets/js/core/jquery.slimscroll.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/core/jquery.scrollLock.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/plugins/chartjs/Chart.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/legalbot.js'); ?>"></script>
<script type="text/javascript">

    var ctx = document.getElementById("js-chartjs-donut").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
                responsive: false,
maintainAspectRatio: false,

        data: {
            labels: ["Não lidos", "Não Relevantes", "Relevantes"],
            datasets: [{
                backgroundColor: [
                    "rgba(205, 91, 91, 1)",
                    "rgba(226, 178, 64, 1)",
                    "rgba(46, 155, 189, 1)"
                ],
                data: [<?= $relevante0; ?>, <?= $relevante2; ?> , <?= $relevante1; ?>]
            }]
        }
    });


    $(".js-chartjs-donut").click( 
        function(evt){
            var activePoints = myChart.getElementAtEvent(evt);
            var parametroGroup = '';
            if (activePoints[0]['_view']['label'] == "Não Relevantes") {
               parametroGroup = 0;
            }else if (activePoints[0]['_view']['label'] == "Relevantes") {
                parametroGroup = 1;

            }else{
                parametroGroup = 2;
            }        
        
            var url = "/normativo/list-normativos/txtRelevancia_" + parametroGroup;
            window.location.href = url; 
        }
    );     



    //MENU LATERAL
    $lSidebar           = $('#sidebar');
    $lSidebarScroll     = $('#sidebar-scroll');     
    $($lSidebar).scrollLock('off');
    if ($lSidebarScroll.length && (!$lSidebarScroll.parent('.slimScrollDiv').length)) {
        $lSidebarScroll.slimScroll({
            height: $lSidebar.outerHeight(),
            color: '#fff',
            size: '5px',
            opacity : .35,
            wheelStep : 15,
            distance : '2px',
            railVisible: false,
            railOpacity: 1
        });
    }
    else { 
        $lSidebarScroll
            .add($lSidebarScroll.parent())
            .css('height', $lSidebar.outerHeight());
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    <?php if ($this->session->userdata['user']['txtModelo'] == 1): ?>
        $(document).ready(function() {            
            $.ajax({
                dataType : "json",
                data: {}, 
                type : 'post',
                url : '/normativo/new_user_normativo',
                success : function(json) {
                    console.log(json.valido);
                    if (json.valido == 1) {
                        location.reload();
                    }else{
                    };
                },
                error : function(e) {
                    
                }
            });
        }); 
    <?php endif; ?>

    $('body').on('click', '.filtro-semanal', function(e) {       
        $('.loader').addClass('block-opt-refresh');
        $('#listagemNormativosSemana').html('');
        $.ajax({
            dataType : "json",
            data: {}, 
            type : 'post',
            url : '/dashboard/filtrar_semana',
            success : function(json) {
                $('#listagemNormativosSemana').append(json.html);
                $('.loader').removeClass('block-opt-refresh');
            },
            error : function(e) {
                console.log('erro');
                $('.loader').removeClass('block-opt-refresh');
            }
        });
    });

    $('body').on('click', '.filtro-mensal', function(e) {       
        $('.loader').addClass('block-opt-refresh');
        $('#listagemNormativosMes').html('');
        $.ajax({
            dataType : "json",
            data: {}, 
            type : 'post',
            url : '/dashboard/filtrar_mes',
            success : function(json) {
                $('#listagemNormativosMes').append(json.html);
                $('.loader').removeClass('block-opt-refresh');
            },
            error : function(e) {
                console.log('erro');
                $('.loader').removeClass('block-opt-refresh');
            }
        });
    });

</script>
</body>
