<body>        
    <?php $this->template->showTemplate('template/menu'); ?>
    <main class='bg-cinza'>
        <div class="section-courses">
            <div class="container">
                <div class="section-header">
                    <div>
                        <h1>Meus Cursos</h1>
                    </div>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active">
                        <a href="#secoes" role="tab" data-toggle="tab">Módulos</a>
                    </li>
                    <li role="presentation">
                        <a href="#licoes" role="tab" data-toggle="tab">Lições</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="tab-content">
                <div class="list-cards tab-pane fade in active" id='secoes'>    
                <?php foreach ($secoes as $secao): ?>      
                    <div class="card"> 
                        <a href="<?= base_url('curso/detalhe/'.encode($secao->idLicao)); ?>"> 
                            <div class="card-image"> 
                                <img src="<?= base_url('assets/img/'.$secao->txtImagemCapa); ?>">
                                <div class="play-button button-icone"></div> 
                            </div> 
                        </a> 
                        <a href="<?= base_url('curso/detalhe/'.encode($secao->idLicao)); ?>"> 
                            <div class="card-details"> 
                                <strong class="details-name"><?= $secao->txtTitulo; ?></strong> 
                                <div class="details-course push-10-t"><?= $secao->txtResumo; ?></div>
                                <?php if ($secao->porcentagem > 0): ?>
                                <!-- <div class="details-bottom">
                                    <span class="details-progress"> 
                                        <span class="progress-bar" style="width: <?= $secao->porcentagem; ?>%;"> </span> 
                                    </span>
                                    <span class="progress-text">
                                        <div class="ellipsis">
                                            <span><?= $secao->porcentagem; ?>% concluído</span>
                                        </div> 
                                    </span>
                                </div>  -->
                                <?php else: ?>
                                <div class="details-bottom">
                                    <div class="ellipsis">
                                        <button type="button" name="btnLogin" class="btn btn-primary ">Começar Seção</button>
                                    </div>
                                </div>
                                <?php endif ?>
                            </div> 
                        </a> 
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="list-cards tab-pane fade" id='licoes'>
                    <?php foreach ($licoes as $licao): ?>
                    <div class="card"> 
                        <a href="<?= base_url('curso/detalhe/'.encode($licao->id)); ?>"> 
                            <div class="card-image"> 
                                <img src="<?= base_url('assets/img/'.$licao->txtImagemCapa); ?>">
                                <?php if ($licao->intTipoLicao == 1): ?>
                                <div class="play-button button-icone"></div> 
                                <?php elseif ($licao->intTipoLicao == 2): ?>
                                <div class="pdf-button button-icone"></div> 
                                <?php else: ?>
                                <div class="book-button button-icone"></div>
                                <?php endif ?>   
                            </div> 
                        </a> 
                        <a href="<?= base_url('curso/detalhe/'.encode($licao->id)); ?>"> 
                            <div class="card-details"> 
                                <strong class="details-name"><?= $licao->txtTitulo; ?><span class='secaoLicao'><?= $licao->txtTituloSecao; ?></span> </strong> 
                                <div class="details-course push-10-t"><?= $licao->txtResumo; ?> </div>
                                <?php if ($licao->intTipoLicao == 1): ?>
                                    <div class="details-bottom">
                                        <span class="details-progress"> 
                                            <span class="progress-bar" style="width: <?= $licao->porcentagem; ?>0%;"> </span> 
                                        </span>
                                        <span class="progress-text">
                                            <div class="ellipsis">
                                                <span><?= $licao->porcentagem; ?>0% concluído</span>
                                            </div> 
                                        </span>
                                    </div> 
                                <?php endif; ?>
                            </div> 
                        </a> 
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript" src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
</body>