<body>   
    <?php $this->template->showTemplate('template/menu'); ?>
    <main class="bg-cinza">
        <div class="container push-70-t">
             <form class="form-login form-log js-validation-login" action="" method="post" id="formCadastro" style='margin:0 auto;width:450px;'>
                <h3 style='margin-top: 0px;margin-bottom: 25px;'>Cadastre as informações do usuário</h3>
                <div class="block-content">
                    <div class="form-group form-name">
                        <input type="text" name="txtNome" class="form-control" value="" required="required" autocomplete="off" placeholder="Nome">
                    </div>
                    <div class="form-group form-email">
                        <input type="email" name="txtEmail" class="form-control" value="" required="required" autocomplete="off" placeholder="E-mail">
                    </div>
                    <div class="form-group text-center remove-margin-b">
                        <button type="submit" name="btnLogin" class="btn btn-primary btn-full">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </main>

    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/base_page_validation.js'); ?>"></script>
    <script type="text/javascript">
          //AJAX SUBMIT
        $('#formCadastro').submit(function() {
            var $form = $(this);
            if(! $form.valid()) return false;

            $('#formCadastro').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: $("#formCadastro").serialize(),
                type : 'post',
                url : '/cadastro/new_user',
                success : function(json) {
                    $('.form-login').removeClass('block-opt-refresh');
                    $.notify({
                        icon: 'fa fa-close',
                        message: 'Informações inseridas com sucesso',
                    },
                    {
                        element: 'body',
                        type: 'success',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 99999,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                    setTimeout(function () {
                           location.reload();
                    }, 3500);

                },
                error : function(e) {
                    $('.form-login').removeClass('block-opt-refresh');
                }
            });
            return false;
        });
    </script>
</body>