<body class="bg-padrao">
    <main>
        <div class="box-login" id="boxLogin">            
            <div class="box-imagem"> 
                <img src="<?= base_url('assets/img/logo2.png'); ?>" alt="Legal Bot">
            </div>
            <h4 class="header-box">Entrar</h4>
            <form class="form-login form-log js-validation-login" action="" method="post" id="formCadastro">
                <div class="block-content">
                    <div class="form-group form-email">
                        <input type="text" name="txtEmail" class="form-control" value="" required="required" autocomplete="off" placeholder="Usuário ou E-mail">
                    </div>
                    <div class="form-group form-senha">
                        <input type="password" name="txtSenha" id="txtSenha" class="form-control" value="" required="required" autocomplete="off" placeholder="Senha">
                    </div>
                    <div class="form-group text-center remove-margin-b">
                        <button type="submit" name="btnLogin" class="btn btn-primary btn-full">Entrar</button>
                    </div>
                </div>
            </form>
        </div>
    </main>

    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/base_page_validation.js'); ?>"></script>
    <script type="text/javascript">

        //AJAX SUBMIT
        $('#formCadastro').submit(function() {
            var $form = $(this);
            if(! $form.valid()) return false;

            $('#formCadastro').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: $("#formCadastro").serialize(),
                type : 'post',
                url : '/login/validar_login',
                success : function(json) {
                    if(json.valid == true){
                        window.location.assign('/curso');
                    }
                    else{
                        $('.form-login').removeClass('block-opt-refresh');
                        $.notify({
                            icon: 'fa fa-close',
                            message: json.mensagem,
                        },
                        {
                            element: 'body',
                            type: 'danger',
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                align: 'center'
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                            delay: 5000,
                            timer: 1000,
                            animate: {
                                enter: 'animated fadeIn',
                                exit: 'animated fadeOutDown'
                            }
                        });
                        $('[name="txtEmail"]').val('').focus();
                        $('[name="txtSenha"]').val('');
                    }
                },
                error : function(e) {
                    $('.form-login').removeClass('block-opt-refresh');
                }
            });
            return false;
        });

       
         $('#formSenha').submit(function() {
            $('#formSenha').addClass('block-opt-refresh');
            $.ajax({
                dataType : "json",
                data: $("#formSenha").serialize(),
                type : 'post',
                url : '/login/esqueci_senha',
                success : function(json) {
                    $('#formSenha').removeClass('block-opt-refresh');
                    $.notify({
                        icon: 'fa fa-close',
                        message: json.mensagem,
                    },
                    {
                        element: 'body',
                        type: json.styleMensagem,
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                    $('#txtEmailSenha').val('').focus();
                },
                error : function(json) {
                    $('#formSenha').removeClass('block-opt-refresh');
                    $.notify({
                        icon: 'fa fa-close',
                        message: json.mensagem,
                    },
                    {
                        element: 'body',
                        type: 'danger',
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            align: 'center'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                }
            });
            return false;
        });

    </script>
</body>