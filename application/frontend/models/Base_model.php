<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

     function get_bases($idBase = 0){
        $this->db->select( 'BAS.id, BAS.txtNome, BAS.txtCorPrimaria, BAS.txtCorSecundaria');
                
        $this->db->from('tabbase AS BAS');
        
        if ($idBase != 0)
            $this->db->where('BAS.id', $idBase);

        $this->db->order_by('BAS.txtNome');
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

     function get_temas_base($idBase = 0){

        $this->db->select('SUBT.id AS idTema, SUBT.txtTitulo');

        $this->db->distinct();
        
        $this->db->from('tabtemasbase AS TEMB');

        $this->db->join('tabsubtema AS SUBT', 'TEMB.idTema = SUBT.id', 'left');
        
        if ($idBase != 0)
            $this->db->where('TEMB.idBase', $idBase);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

    function get_bases_area($idArea = 0, $idInstituicao = 0){
        $this->db->select( 'BAS.id, BAS.txtNome, BAS.txtCorPrimaria, BAS.txtCorSecundaria');
        
        $this->db->distinct();

        $this->db->select( 'ART.idArea');             
                    
        $this->db->from('tabbase AS BAS');

        $this->db->join('tabtemasbase AS TEMB', 'BAS.id = TEMB.idBase', 'left');

        $this->db->join('tabareatemas AS ART', 'TEMB.idTema = ART.idTema', 'left');

        if ($idArea != 0)
            $this->db->where('ART.idArea', $idArea);
        
        if ($idInstituicao != 0)
            $this->db->where('ART.idInstituicao', $idInstituicao);
        
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    


    function get_grupo_base($idBase = 0){
        // $this->db->select('TEMB.id');

        $this->db->select('TEM.id AS idTema, TEM.txtTitulo');
        
        $this->db->distinct();
        
        $this->db->from('tabtemasbase AS TEMB');

        $this->db->join('tabsubtema AS SUBTEM', 'TEMB.idTema = SUBTEM.id', 'left');
        
        $this->db->join('tabtema AS TEM', 'SUBTEM.idTema = TEM.id', 'left');

        if ($idBase != 0)
            $this->db->where('TEMB.idBase', $idBase);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    


    function get_subtema_base($idTema = 0){
        $this->db->select('TEM.id, TEM.idBase, TEM.idTema');
        
        $this->db->from('tabtemasbase  AS TEM');

        $this->db->group_by('TEM.id');
        
        $this->db->where('TEM.idTema', $idTema);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

}



