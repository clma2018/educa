<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

    function validate_user($txtLogin = '', $txtSenha = '', $txtNickname = ''){
		$this->db->select('	USER.id, USER.txtNome, USER.txtEmail, USER.bitCadastro, USER.txtFuncao, USER.bitExclusaoTemas, USER.txtAreaAtuacao, USER.idAdministrador, USER.idInstituicao, USER.txtNickname, USER.txtBPO');
        
        $this->db->from('tabusuario AS USER');

        $this->db->select('INST.txtModelo');

        $this->db->join('tabinstituicao AS INST', 'USER.idInstituicao = INST.id', 'left');

        if ($txtLogin != '')
		  $this->db->where('USER.txtEmail', $txtLogin);

        if ($txtNickname != '')
          $this->db->where('USER.txtNickname', $txtNickname);

        $this->db->where('USER.txtSenha', $txtSenha);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
        	return $get->result();
		
		return array();
	}

    function validate_user_admin($txtLogin = '', $txtSenha = ''){
        $this->db->select('ADM.id, ADM.txtNome, ADM.txtEmail');
        
        $this->db->from('tabadministrador AS ADM');
                    
        $this->db->where('ADM.txtEmail', $txtLogin);
        $this->db->where('ADM.txtSenha', $txtSenha);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

	function get_users($idUsuario = 0, $idAdministrador = 0, $idInstituicao = 0){
		$this->db->select( 'USER.id, USER.txtNome, USER.txtEmail, USER.bitCadastro, USER.idAdministrador, USER.txtFuncao, USER.bitExclusaoTemas, USER.txtAreaAtuacao, USER.idInstituicao, USER.txtNickname, USER.txtBPO');

		$this->db->select('INST.txtNomeInstituicao, INST.txtModelo');

        $this->db->select('ARE.txtArea');

        $this->db->from('tabusuario AS USER');
		
		if ($idUsuario != 0)
			$this->db->where('USER.id', $idUsuario);

        if ($idInstituicao != 0)
            $this->db->where('USER.idInstituicao', $idInstituicao);
        
        if ($idAdministrador != 0)
            $this->db->where('USER.idAdministrador', $idAdministrador);

        $this->db->join('tabinstituicao AS INST', 'USER.idInstituicao = INST.id', 'left');

        $this->db->join('tabarea AS ARE', 'USER.txtAreaAtuacao = ARE.id', 'left');

		$this->db->order_by('USER.txtNome', 'ASC');

		$get = $this->db->get();

		if($get->num_rows() > 0) 
			return $get->result();
		
		return array();
    }

     function get_users_temaexclusao($idUsuario = 0){
        $this->db->select('TEMU.id, TEMU.idUsuario, TEMU.idTemaExclusao');
        
        $this->db->select('TE.txtTitulo');
        
        $this->db->from('tabtemaexclusaousuario AS TEMU');

        $this->db->join('tabtemaexclusao AS TE', 'TEMU.idTemaExclusao = TE.id', 'left');

        $this->db->where('TEMU.idUsuario', $idUsuario);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_users_tema($idUsuario = 0){
        $this->db->select('TEMU.id, TEMU.idUsuario, TEMU.idTema');

        $this->db->select('SUBT.txtTitulo');
        
        $this->db->from('tabtemausuario  AS TEMU');

        $this->db->join('tabsubtema AS SUBT', 'TEMU.idTema = SUBT.id', 'left');
        
        $this->db->where('TEMU.idUsuario', $idUsuario);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function valida_email($txtEmail = ''){
        $this->db->select( 'USER.id, USER.txtNome, USER.txtEmail');
        
        $this->db->from('tabusuario AS USER');
        
        $this->db->where('USER.txtEmail', $txtEmail);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function validate_token($txtToken = ''){
        $this->db->select( 'TOK.id, TOK.idUsuario, TOK.txtToken, TOK.datCreate, TOK.bitStatus');
        
        $this->db->from('tabtoken AS TOK');
        
        $this->db->where('TOK.txtToken', $txtToken);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function valida_duplicidade_email($txtEmail = ''){
        $this->db->from('tabusuario AS USER');
        $this->db->where('USER.txtEmail', $txtEmail);
        $total = $this->db->count_all_results();

        return ($total > 0) ? TRUE : FALSE;
    }


     function valida_duplicidade_nickname($txtNickname = ''){
        $this->db->from('tabusuario AS USER');
        $this->db->where('USER.txtNickname', $txtNickname);
        $total = $this->db->count_all_results();

        return ($total > 0) ? TRUE : FALSE;
    }

    


    function get_temas_user($idUsuario = 0){

        $this->db->select('TASUB.id, TASUB.txtTitulo, TEMU.idUsuario');

        $this->db->from('tabtemausuario AS TEMU');

        $this->db->join('tabsubtema AS TASUB', 'TEMU.idTema = TASUB.id', 'left');

        if ($idUsuario != 0)
            $this->db->where('TEMU.idUsuario', $idUsuario);

        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

    function get_user_id($idUsuario = ''){
        $this->db->select(' USER.id, USER.txtNome, USER.txtEmail, USER.bitCadastro, USER.txtFuncao, USER.bitExclusaoTemas, USER.txtAreaAtuacao, USER.idAdministrador, USER.idInstituicao, USER.txtNickname, USER.txtBPO');
        
        $this->db->from('tabusuario AS USER');

        $this->db->select('INST.txtModelo');

        $this->db->join('tabinstituicao AS INST', 'USER.idInstituicao = INST.id', 'left');

        if ($idUsuario != '')
          $this->db->where('USER.id', $idUsuario);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }



}



