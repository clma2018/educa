<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

     function get_logins($idInstituicao = 0){
        $this->db->select('LOG.id, LOG.idUsuario, LOG.dateCreate');
        
        $this->db->select('USER.txtNome, USER.txtEmail');

        $this->db->select('INS.txtNomeInstituicao');
        
        $this->db->from('tabloglogin AS LOG');

        $this->db->join('tabusuario AS USER', 'LOG.idUsuario = USER.id', 'left');

        $this->db->join('tabinstituicao AS INS', 'USER.idInstituicao = INS.id', 'left');
        
        $this->db->order_by('LOG.dateCreate' , 'desc');

        if ($idInstituicao != 0)
            $this->db->where('LOG.idInstituicao', $idInstituicao);

       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

    function get_disparos_email($idInstituicao = 0){
        $this->db->select('LOG.id, LOG.idNormativo, LOG.txtEmail, LOG.dateCreate');

        $this->db->select('NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtOrigem, NORM.txtLink');
        
        $this->db->select('ARE.txtArea');
        
        $this->db->from('tablogemailnormativo AS LOG');

        $this->db->join('tabnormativoarea AS NORM', 'LOG.idNormativo = NORM.id', 'left');

        $this->db->join('tabarea AS ARE', 'NORM.idArea = ARE.id', 'left');

        $this->db->where('NORM.bitCiente', '4');

        $this->db->order_by('LOG.dateCreate' , 'desc');
        
        if ($idInstituicao != 0)
            $this->db->where('LOG.idInstituicao', $idInstituicao);

       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    } 

    function get_acessos_normativos($idInstituicao = 0){
        $this->db->select('LOG.id, LOG.idUsuario, LOG.dateCreate');
        
        $this->db->select('USER.txtNome, USER.txtEmail');

        $this->db->select('INS.txtNomeInstituicao');
        
        $this->db->from('tablogacessolistnormativo AS LOG');

        $this->db->join('tabusuario AS USER', 'LOG.idUsuario = USER.id', 'left');

        $this->db->join('tabinstituicao AS INS', 'USER.idInstituicao = INS.id', 'left');
        
        $this->db->order_by('LOG.dateCreate' , 'desc');

        if ($idInstituicao != 0)
            $this->db->where('LOG.idInstituicao', $idInstituicao);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


     function get_logs_filtro($txtDataInicio= '', $txtDataFim = ''){
        $this->db->select('LOG.id, LOG.idUsuario, LOG.dateCreate');
        
        $this->db->select('USER.txtNome, USER.txtEmail');

        $this->db->select('INS.txtNomeInstituicao');
        
        $this->db->from('tabloglogin AS LOG');

        $this->db->join('tabusuario AS USER', 'LOG.idUsuario = USER.id', 'left');

        $this->db->join('tabinstituicao AS INS', 'USER.idInstituicao = INS.id', 'left');

        if ($txtDataInicio != '')
            $this->db->where('DATE(LOG.dateCreate) BETWEEN "'.$txtDataInicio.'" AND "'.$txtDataFim.'"');
       
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    } 


}



