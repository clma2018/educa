<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anotacao_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

    function get_anotacoes_bpo($idNorma = '', $idInstituicao = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.Revogado');

        $this->db->select('ANOT.id, ANOT.idNorma, ANOT.idInstituicao, ANOT.txtClasse, ANOT.txtGrupo, ANOT.txtArea, ANOT.txtProcesso, ANOT.txtSistema, ANOT.txtComentario');

        $this->db->select('USER.txtNome');

        $this->db->from('LegalbotN1_cont AS LEG');

        if ($idInstituicao != '')
            $this->db->where('ANOT.idInstituicao', $idInstituicao);

        if ($idNorma != '')
            $this->db->where('ANOT.idNorma', $idNorma);

        $this->db->join('tabAnotacoesBPO AS ANOT', 'ANOT.idNorma = LEG.idNorma', 'left');

        $this->db->join('tabusuario AS USER', 'ANOT.idUsuario = USER.id', 'left');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_data_anotacao($idAnotacao = ''){

        $this->db->select('DATV.idAnotacao, DATV.txtDataInicio, DATV.txtDataFim');

        $this->db->from('tabdataVigenciaBPO AS DATV');
        
        if ($idAnotacao != '')
            $this->db->where('DATV.idAnotacao', $idAnotacao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_anotacao($idAnotacao = ''){

        $this->db->select('ANOT.id, ANOT.idNorma, ANOT.idInstituicao, ANOT.txtClasse, ANOT.txtGrupo, ANOT.txtArea, ANOT.txtProcesso, ANOT.txtSistema, ANOT.txtComentario');

        $this->db->from('tabAnotacoesBPO AS ANOT');
        
        if ($idAnotacao != '')
            $this->db->where('ANOT.id', $idAnotacao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_anotacoes_bpo_enviadas($idInstituicao = ''){
        $this->db->select('ANOTE.idAnotacao, ANOTE.idUsuario');

        $this->db->from('tabanotacoesenviadasBPO AS ANOTE');

        if ($idInstituicao != '')
            $this->db->where('ANOT.idInstituicao', $idInstituicao);

        $this->db->join('tabAnotacoesBPO AS ANOT', 'ANOT.id = ANOTE.idAnotacao', 'left');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_comentario_feeder($idNormativo = ''){

        $this->db->select('ANOT.id, ANOT.idNormativo, ANOT.txtComentario');

        $this->db->from('tabcomentarionormativofeeder AS ANOT');

        if ($idNormativo != '')
            $this->db->where('ANOT.idNormativo', $idNormativo);


        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

   
}