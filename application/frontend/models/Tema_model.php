<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tema_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

    function get_all_temas($idTema = 0){
        $this->db->select('TEM.id, TEM.txtTitulo');
        
        
        $this->db->from('tabtema AS TEM');

        if ($idTema != 0)
            $this->db->where('TEM.id', $idTema);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

  	function get_all_temas_exclusao(){
        $this->db->select('TEM.id, TEM.txtTitulo, TEM.txtCategoriaTema');
        
        
        $this->db->from('tabtemaexclusao AS TEM');
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_all_cat_exclusao(){
        $this->db->select('TEM.id, TEM.txtCategoriaTema');
        
        $this->db->distinct();
        
        $this->db->from('tabtemaexclusao AS TEM');

        $this->db->group_by('TEM.txtCategoriaTema');
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_all_subtemas($idTema = 0){
        $this->db->select('TEM.id, TEM.idTema, TEM.txtTitulo');
        
        $this->db->distinct();
        
        $this->db->from('tabsubtema  AS TEM');

        $this->db->group_by('TEM.id');

        
        if ($idTema != 0)
            $this->db->where('TEM.idTema', $idTema);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_all_subtemas_titulo($txtTitulo = ''){
        $this->db->select('TEM.id, TEM.idTema, TEM.txtTitulo');
                
        $this->db->from('tabsubtema  AS TEM');
    
        $this->db->where('TEM.txtTitulo', $txtTitulo);
                    
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    // function get_bases($idTema = 0){
    //     $this->db->select('TEM.txtBase');
        
    //     $this->db->distinct();
        
    //     $this->db->from('tabsubtema AS TEM');

    //     $this->db->where('TEM.id', $idTema);

                    
    //     $get = $this->db->get();

    //     if($get->num_rows() > 0)
    //         return $get->result();
        
    //     return array();
    // }
    
}



