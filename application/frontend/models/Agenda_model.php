<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agenda_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }


    function get_anotacoes_user($idUsuario = 0){
        $this->db->select( 'ANOT.id, ANOT.txtDescricao, ANOT.DateCreate, ANOT.txtDataVencimento, ANOT.idAdminComentario');
        
        $this->db->from('tabanotacaousuario AS ANOT');
        
        if ($idUsuario != 0)
            $this->db->where('ANOT.idUsuario', $idUsuario);

       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


      function get_normativos_prazo($idInstituicao = 0){
        $this->db->select( 'ANOT.id, ANOT.txtDescricao, ANOT.txtObservacao, ANOT.txtDataInicio, ANOT.txtDataFim');
        
        $this->db->from('tabnormativosprazos AS ANOT');
        
        if ($idInstituicao != 0)
            $this->db->where('ANOT.idInstituicao', $idInstituicao);

       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

}



