<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Normativo_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

    function get_normativo_user_data($idUsuario = 0){
        $this->db->select('NORM.id, NORM.bitCiente, NORM.dateCadastro, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
        

        // $this->db->distinct();
        
        $this->db->from('tabnormativousuario AS NORM');

        $this->db->where('NORM.idUsuario', $idUsuario);

        $this->db->where_in('NORM.bitCiente', array('1','2'));

        $this->db->group_by('concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2))' , 'desc');

        $this->db->order_by('data2' , 'desc');
       
        $this->db->limit('32');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_user($idUsuario = 0){
		 $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtDataVigencia, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.bitNormativoHistorico, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
       
         $this->db->select('TAV.txtNota');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabavaliacao AS TAV', 'NORM.id = TAV.idNormativoUsuario', 'left');

        $this->db->where('NORM.idUsuario', $idUsuario);

        $this->db->where_in('NORM.bitCiente', array('1','2'));
        
        // $this->db->order_by('data2' , 'asc');

        $this->db->order_by('data2' , 'desc');

        $this->db->limit('175');        
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
        	return $get->result();
		
		return array();
	}

     function get_normativo($idNormativo = 0){
        $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtTipoAcao, NORM.txtDataVigencia, NORM.txtRevogado, NORM.bitNormativoHistorico');
        
        $this->db->select('USER.txtNome AS txtNomeUsuario');

        $this->db->from('tabnormativousuario AS NORM');
                    
        $this->db->join('tabusuario AS USER', 'NORM.idUsuario = USER.id', 'left');

        $this->db->where('NORM.id', $idNormativo);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_id($idNormativo = 0, $idUsuario = 0){
        $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtDataVigencia');
        
        $this->db->from('tabnormativousuario AS NORM');
                    
        $this->db->where('NORM.idNormativo', $idNormativo);

        if ($idUsuario != 0)
            $this->db->where('NORM.idUsuario', $idUsuario);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_avalicao($idNormativo = 0){
        $this->db->select('AVA.id, AVA.idUsuario, AVA.txtNota, AVA.idNormativoUsuario');
        
        $this->db->from('tabavaliacao AS AVA');
                    
        $this->db->where('AVA.idNormativoUsuario', $idNormativo);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_normativo_avaliacao($idUsuario = 0, $txtNota = 90, $bitCiente = 3, $limit = 0, $txtOrigem = '', $txtDataInicio = '', $txtDataFim = ''){
        $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
       
         $this->db->select('TAV.txtNota');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabavaliacao AS TAV', 'NORM.id = TAV.idNormativoUsuario', 'left');

        $this->db->where('NORM.idUsuario', $idUsuario);

        if (($txtNota != 90))
            if ($txtNota != 0)   
                $this->db->where('TAV.txtNota', $txtNota);
            else
                $this->db->where('TAV.txtNota IS NULL');
        
        if ($bitCiente != 3)
            $this->db->where('NORM.bitCiente', $bitCiente);

        if ($txtOrigem != '')
            $this->db->where('NORM.txtOrigem', $txtOrigem);

        if ($txtDataInicio != '')
            $this->db->where('concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) BETWEEN "'.$txtDataInicio.'" AND "'.$txtDataFim.'"');

        if ($limit != 0)
            $this->db->limit($limit);

        $this->db->order_by('data2' , 'desc');


        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_user_encaminhado($idNormativo = 0, $idUsuario = 0){
        $this->db->select('ENC.id, ENC.idUsuario, ENC.txtEmail, ENC.idNormativoUsuario');
        
        $this->db->distinct();
        
        $this->db->select('NORMU.txtTitulo');

        $this->db->select('USER.txtNome AS txtNomeResponsavel, USER.txtEmail AS txtEmailResponsavel');

        $this->db->from('tabencaminhar AS ENC');
        
        $this->db->join('tabnormativousuario AS NORMU', 'ENC.idNormativoUsuario = NORMU.idNormativo', 'left');

        $this->db->join('tabusuario AS USER', 'ENC.idUsuario = USER.id', 'left');

        $this->db->where('ENC.idNormativoUsuario', $idNormativo);

        $this->db->where('ENC.idUsuario', $idUsuario);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_planoAcao($idNormativo = 0, $idInstituicao = 0){
        $this->db->select('PLA.id, PLA.idUsuario, PLA.txtDataVencimento, PLA.txtDescricao, PLA.txtStatus');

        $this->db->select('USER.txtNome AS txtNomeUsuario');
        
        $this->db->from('tabplanoacao AS PLA');
                    
        $this->db->where('PLA.idNormativo', $idNormativo);

        if ($idInstituicao != 0)
            $this->db->where('PLA.idInstituicao', $idInstituicao);

        $this->db->join('tabusuario AS USER', 'PLA.idUsuario = USER.id', 'left');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_emails_normativo($idNormativo = 0){
        $this->db->select('ENC.id, ENC.idUsuario, ENC.txtEmail, ENC.idNormativoUsuario');
        
        $this->db->from('tabencaminhar AS ENC');
                    
        $this->db->where('ENC.idNormativoUsuario', $idNormativo);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_anotacaoNormativo($idNormativo = 0){
        $this->db->select('ANT.id, ANT.txtAnotacao');
        
        $this->db->from('tabanotacaonormativo AS ANT');
                    
        $this->db->where('ANT.idNormativo', $idNormativo);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_planoAcao_user($idUsuario = 0){
        $this->db->select('PLA.id, PLA.idUsuario, PLA.txtDataVencimento, PLA.txtDescricao, PLA.txtStatus');

        $this->db->select('NORMU.id AS idNormativo, NORMU.txtTitulo');
        
        $this->db->from('tabplanoacao AS PLA');
                    
        $this->db->join('tabnormativousuario AS NORMU', 'PLA.idNormativo = NORMU.idNormativo', 'left');

        $this->db->where('PLA.idUsuario', $idUsuario);

        $this->db->where('NORMU.idUsuario', $idUsuario);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_normativos_tema($txtClasse = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.Revogado');

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        $this->db->where('LEG.Classe', $txtClasse);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    //   function get_normativos_tema2($txtClasse = '', $idInstituicao = ''){
    //     $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.Revogado');

    //     // $this->db->from('LegalbotN1_cont AS LEG');
    //     $this->db->from('tabnormativos AS LEG');

    //     $this->db->where('LEG.Classe', $txtClasse);
        
    //     $this->db->where('LEG.idInstituicao', $idInstituicao);

    //     $get = $this->db->get();

    //     if($get->num_rows() > 0)
    //         return $get->result();
        
    //     return array();
    // }

    
    //  function get_normativo_area($idArea = 0){
    //     $this->db->select('NORM.id, NORM.idArea, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante');
        
    //     $this->db->from('tabnormativoarea AS NORM');
        
    //     if ($idArea != 0)
    //         $this->db->where('NORM.idArea', $idArea);
        
    //     $get = $this->db->get();

    //     if($get->num_rows() > 0)
    //         return $get->result();
        
    //     return array();
    // }

    function get_normativo_idArea($idNormativo = '', $idArea = 0, $bitStatus = array('1','2'), $idInstituicao = 0){
        $this->db->select('NORM.id, NORM.idArea, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtTipoNorma, NORM.txtNumero, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
        
        $this->db->select( 'TA.txtArea, TA.txtDescricaoArea');

        $this->db->select( 'COMEN.txtComentario');

        $this->db->from('tabnormativoarea AS NORM');
        
        if ($idNormativo != '')            
            $this->db->where('NORM.idNormativo', $idNormativo);

        if ($idArea != 0)
            $this->db->where('NORM.idArea', $idArea);

        $this->db->join('tabarea AS TA', 'NORM.idArea = TA.id', 'left');

         $this->db->join('tabcomentarionormativofeeder AS COMEN', 'NORM.id = COMEN.idNormativo', 'left');

        if ($idInstituicao != 0)
            $this->db->where('NORM.idInstituicao', $idInstituicao);

        $this->db->where_in('NORM.bitCiente', $bitStatus);

        $this->db->order_by('data2' , 'desc');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }
    
     function get_normativo_export($idArea = 0, $dataInicio = '', $dataFim = '', $txtBases = array(), $idInstituicao = 0){

        $this->db->select('NORM.id, NORM.idArea, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtTipoNorma, NORM.txtNumero, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
      
        $this->db->select('AR.txtArea');

        $this->db->from('tabnormativoarea AS NORM');

        $this->db->join('tabarea AS AR', 'NORM.idArea = AR.id', 'left');
        
        if ($idArea != 0)
            $this->db->where('NORM.idArea', $idArea);


        $this->db->where('concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"');

        if($txtBases[0])
            $this->db->where_in('NORM.txtOrigem', $txtBases);

        if ($idInstituicao != 0)
            $this->db->where('NORM.idInstituicao', $idInstituicao);


        $this->db->order_by('data2' , 'asc');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_bases(){
        $this->db->select('LEG.txtOrigem');

        $this->db->distinct();

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        $this->db->order_by('LEG.txtOrigem');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_bases_temas($txtBase = ''){
        $this->db->select('LEG.Classe');

        $this->db->distinct();

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        if ($txtBase != '')
            $this->db->where('LEG.txtOrigem', $txtBase);
        
        $this->db->where('LEG.Classe !=', '|');

        $this->db->order_by('LEG.Classe');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_bases_grupos($txtBase = ''){
        $this->db->select('LEG.Grupo');

        $this->db->distinct();

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        if ($txtBase != '')
            $this->db->where('LEG.txtOrigem', $txtBase);
        
        $this->db->where('LEG.Grupo !=', '|');

        $this->db->order_by('LEG.Grupo');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_base($txtBase= '', $dataInicio = '', $dataFim = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.Revogado, LEG.DataEmissao, LEG.Responsavel, LEG.Grupo, LEG.Exclusao, concat(substring(LEG.DataPubli,7,4),substring(LEG.DataPubli,4,2),substring(LEG.DataPubli,1,2)) as data2');

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        if ($txtBase != '')
            $this->db->where('LEG.txtOrigem', $txtBase);

        if ($dataInicio != '')
        $this->db->where('concat(substring(LEG.DataPubli,7,4),substring(LEG.DataPubli,4,2),substring(LEG.DataPubli,1,2)) BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"');

        $this->db->order_by('data2' , 'desc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_normativo_idNorma($idNormativo = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero');

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        $this->db->where('LEG.idNorma', $idNormativo);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_idNorma2($idNormativo = '', $idInstituicao = 0){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.txtDataVigencia');

        $this->db->from('tabnormativos AS LEG');

        $this->db->where('LEG.idNorma', $idNormativo);

        if ($idInstituicao != 0)
            $this->db->where('LEG.idInstituicao', $idInstituicao);


        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_base_area($idArea= '', $dataInicio = '', $dataFim = '', $txtBase= '', $idInstituicao = 0){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero');

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        if ($idArea != '')

            $this->db->join('tabnormativoarea AS NORM', 'NORM.idNormativo = LEG.idNorma', 'left');

            $this->db->where('NORM.idArea', $idArea);

        if ($txtBase != '')
            $this->db->where('LEG.txtOrigem', $txtBase);

        if ($idInstituicao != 0)
            $this->db->where('NORM.idInstituicao', $idInstituicao);


        $this->db->where('concat(substring(LEG.DataPubli,7,4),substring(LEG.DataPubli,4,2),substring(LEG.DataPubli,1,2)) BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"');

        $this->db->group_by('LEG.idNorma' , 'desc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_normativo_comentario($idNormativo = '', $idInstituicao = 0){
        $this->db->select('COM.id, COM.idNorma, COM.txtComentario');

        $this->db->from('tabcomentarionormativo AS COM');

        $this->db->where('COM.idNorma', $idNormativo);

        if ($idInstituicao != 0)
            $this->db->where('COM.idInstituicao', $idInstituicao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_planoAcao_atrasado($dataInicio = '', $dataFim = '', $idInstituicao = 0){
        $this->db->select('PLA.id, PLA.idUsuario, PLA.idNormativo, PLA.txtDataVencimento, PLA.txtDescricao, PLA.txtStatus');
        
        $this->db->select('LEG.idNormativo, LEG.txtOrigem, LEG.dateCadastro, LEG.txtTitulo, LEG.txtLink, LEG.txtClasse, LEG.txtAssunto, LEG.txtDataVigencia');

        $this->db->select('USER.txtNome, USER.txtEmail, USER.idAdministrador');

        $this->db->from('tabplanoacao AS PLA');

        if ($dataInicio != '')
        $this->db->where('concat(substring(PLA.txtDataVencimento,7,4),substring(PLA.txtDataVencimento,4,2),substring(PLA.txtDataVencimento,1,2)) BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"');

        if ($idInstituicao != 0)
            $this->db->where('PLA.idInstituicao', $idInstituicao);
    
        $this->db->where('PLA.txtStatus', 'Ativo');

        $this->db->where('USER.id = LEG.idUsuario');

        $this->db->join('tabusuario AS USER', 'PLA.idUsuario = USER.id', 'left');

        $this->db->join('tabnormativousuario AS LEG', 'PLA.idNormativo = LEG.idNormativo', 'left');

        $this->db->group_by('LEG.idNormativo' , 'desc');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

     function get_comentariosNormativo($idNormativo = 0, $idInstituicao = 0){
        $this->db->select('COM.id, COM.idUsuario, COM.idComentarioResposta, COM.idNormativo,
         COM.dateCreate, COM.txtComentario');

        $this->db->select('USER.txtNome AS txtNomeUsuario');
        
        $this->db->from('tabcomentarios AS COM');
                    
        $this->db->where('COM.idNormativo', $idNormativo);

        $this->db->where('COM.idInstituicao', $idInstituicao);

        $this->db->join('tabusuario AS USER', 'COM.idUsuario = USER.id', 'left');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_novos_normativos(){
        $this->db->select('NORM.id, NORM.idNorma, NORM.txtOrigem, NORM.DataPubli, NORM.txtTitulo, NORM.txtLink, NORM.txtGrupo, NORM.txtAssunto, NORM.txtTipoNorma, NORM.txtNumero, NORM.txtRevogado, NORM.DataEmissao, NORM.txtClasse, NORM.txtExclusao, NORM.txtResponsavel');

        $this->db->from('tabnovosnormativos AS NORM');
        
        // $this->db->where('LEG.Classe', $txtClasse);


        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_novos_normativos_id($idNorma = 0){
        $this->db->select('NORM.id, NORM.idNorma, NORM.txtOrigem, NORM.DataPubli, NORM.txtTitulo, NORM.txtLink, NORM.txtGrupo, NORM.txtAssunto, NORM.txtTipoNorma, NORM.txtNumero, NORM.txtRevogado, NORM.DataEmissao, NORM.txtClasse, NORM.txtExclusao, NORM.txtResponsavel');

        $this->db->from('tabnovosnormativos AS NORM');
        
        
        $this->db->where('NORM.idNorma', $idNorma);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

     function get_normativos_dataVigencia($idUsuario = 0){
        $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtTipoAcao, NORM.txtDataVigencia, NORM.txtRevogado, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
        
        
        $this->db->from('tabnormativousuario AS NORM');

        $this->db->where('NORM.idUsuario', $idUsuario);

        $this->db->where('NORM.dateCadastro != NORM.txtDataVigencia');

        $this->db->order_by('data2' , 'desc');
       
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_normativos_N1($idNorma= '', $txtBase = '', $txtClasse = '', $txtDataInicio= '', $txtDataFim = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero, LEG.Exclusao, LEG.Grupo, LEG.Responsavel, LEG.DataEmissao, LEG.DataPubli, LEG.Revogado');

        $this->db->from('LegalbotN1_cont AS LEG');

        if ($idNorma != '')
            $this->db->where('LEG.idNorma', $idNorma);

        if ($txtBase != '')
            $this->db->where('LEG.txtOrigem', $txtBase);

        if ($txtClasse != '')
            $this->db->where('LEG.Classe', $txtClasse);

         if ($txtDataInicio != '')
        $this->db->where('concat(substring(LEG.DataPubli,7,4),substring(LEG.DataPubli,4,2),substring(LEG.DataPubli,1,2)) BETWEEN "'.$txtDataInicio.'" AND "'.$txtDataFim.'"');

        $this->db->group_by('LEG.idNorma' , 'desc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_link($txtLink = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero');

        $this->db->from('LegalbotN1_cont AS LEG');
        // $this->db->from('tabnormativos AS LEG');

        $this->db->where('LEG.link', $txtLink);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


     function get_normativos_semana($dataInicio = '', $dataFim = ''){

         $this->db->select('LEG.idNormativo, LEG.txtOrigem, LEG.dateCadastro, LEG.txtTitulo, LEG.txtLink, LEG.txtClasse, LEG.txtAssunto, LEG.txtDataVigencia');

        $this->db->select('USER.txtNome, USER.txtEmail, USER.idAdministrador');

        $this->db->from('tabnormativousuario AS LEG');

        if ($dataInicio != '')
        $this->db->where('concat(substring(LEG.dateCadastro,7,4),substring(LEG.dateCadastro,4,2),substring(LEG.dateCadastro,1,2)) BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"');

        $this->db->join('tabusuario AS USER', 'LEG.idUsuario = USER.id', 'left');
        
        $this->db->where('LEG.bitCiente', '2');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_anotacoes_normativo_encaminhado($idNormativo = 0, $idUsuario = 0){
        $this->db->select('ENC.id, ENC.idUsuario, ENC.txtEmail, ENC.idNormativoUsuario, ENC.txtAnotacao');
        
        $this->db->from('tabencaminhar AS ENC');

        $this->db->where('ENC.idNormativoUsuario', $idNormativo);

        $this->db->where('ENC.idUsuario', $idUsuario);
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


     function get_normativo_titulo($txtTitulo = ''){
        $this->db->select('LEG.idNorma, LEG.txtOrigem, LEG.DataPubli, LEG.Titulo, LEG.link, LEG.Classe, LEG.Assunto, LEG.TipoNorma, LEG.Numero');

        $this->db->from('LegalbotN1_cont AS LEG');

        $this->db->where('LEG.Titulo', $txtTitulo);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_user_semana($idUsuario = 0){
         $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtDataVigencia, NORM.bitNormativoHistorico, concat(substring(NORM.dateCadastro,7,4),"-",substring(NORM.dateCadastro,4,2),"-",substring(NORM.dateCadastro,1,2)) as data2');
       
         $this->db->select('TAV.txtNota');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabavaliacao AS TAV', 'NORM.id = TAV.idNormativoUsuario', 'left');

        $this->db->where('NORM.idUsuario', $idUsuario);

        $this->db->where_in('NORM.bitCiente', array('1','2'));
        
        $this->db->order_by('data2' , 'desc');

        $this->db->limit('200');
        
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_user_mes($idUsuario = 0){
         $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtDataVigencia, NORM.bitNormativoHistorico, concat(substring(NORM.dateCadastro,7,4),"-",substring(NORM.dateCadastro,4,2),"-",substring(NORM.dateCadastro,1,2)) as data2');
       
         $this->db->select('TAV.txtNota');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabavaliacao AS TAV', 'NORM.id = TAV.idNormativoUsuario', 'left');

        $this->db->where('NORM.idUsuario', $idUsuario);

        $this->db->where_in('NORM.bitCiente', array('1','2'));
        
        $this->db->order_by('data2' , 'desc');

        $this->db->limit('350');
        
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativos_instituicao($idInstituicao = ''){
        $this->db->select('LEG.idNorma, LEG.idUsuario, LEG.idInstituicao, LEG.txtOrigem, LEG.txtTitulo, LEG.txtTipoNorma, LEG.txtNumero, LEG.txtDataPubli, LEG.txtRevogado, LEG.txtLink, LEG.txtDataEmissao, LEG.txtAssunto, LEG.txtResponsavel, LEG.txtGrupo, LEG.txtClasse, LEG.txtExclusao, LEG.dateCreate');

        $this->db->from('tabnormativosinstituicao AS LEG');
        
        $this->db->where('LEG.idInstituicao', $idInstituicao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

     function get_normativos_instituicao_idNorma($idNorma = ''){
        $this->db->select('LEG.idNorma, LEG.idUsuario, LEG.idInstituicao, LEG.txtOrigem, LEG.txtTitulo, LEG.txtTipoNorma, LEG.txtNumero, LEG.txtDataPubli, LEG.txtRevogado, LEG.txtLink, LEG.txtDataEmissao, LEG.txtAssunto, LEG.txtResponsavel, LEG.txtGrupo, LEG.txtClasse, LEG.txtExclusao, LEG.dateCreate');

        $this->db->from('tabnormativosinstituicao AS LEG');
        
        $this->db->where('LEG.idNorma', $idNorma);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normas_instituicao($idInstitucao = 0, $txtOrigem = '', $txtArea = '', $txtStatus = ''){
        $this->db->select('USER.txtNome, USER.txtEmail, USER.txtAreaAtuacao, USER.idInstituicao');

        $this->db->select('TA.txtArea');

        $this->db->select('NORM.id, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.txtDataVigencia');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->select('COUNT(PLA.id) AS qtdPlanosAcao, PLA.txtStatus');
        
        $this->db->join('tabusuario AS USER', 'USER.id = NORM.idUsuario', 'left');

        $this->db->join('tabplanoacao AS PLA', 'NORM.idNormativo = PLA.idNormativo', 'left');

        $this->db->join('tabarea AS TA', 'TA.id = USER.txtAreaAtuacao', 'left');

        if ($idInstitucao != 0)
            $this->db->where('USER.idInstituicao', $idInstitucao);

        if ($txtOrigem != '')
            $this->db->where_not_in('NORM.txtOrigem', $txtOrigem);

        if ($txtArea != '')
            $this->db->where_not_in('TA.txtArea', $txtArea);

        if ($txtStatus != '')
            $this->db->where_not_in('NORM.bitCiente', $txtStatus);
        
        $this->db->group_by('NORM.id' , 'desc');

       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }   

    function get_normativo_user_data_instituicao($idInstituicao = 0){
        $this->db->select('NORM.id, NORM.bitCiente, NORM.dateCadastro, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');
        
        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabusuario AS USER', 'NORM.idUsuario = USER.id', 'left');

        $this->db->where('USER.idInstituicao', $idInstituicao);

        $this->db->where_in('NORM.bitCiente', array('1','2'));

        $this->db->group_by('concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2))' , 'desc');

        $this->db->order_by('data2' , 'desc');
       
        $this->db->limit('32');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_normativo_user_instituicao($idInstituicao = 0){
        $this->db->select('NORM.id, NORM.idUsuario, NORM.idNormativo, NORM.bitCiente, NORM.txtOrigem, NORM.txtTitulo, NORM.txtClasse, NORM.dateCadastro, NORM.txtDataVigencia, NORM.txtAssunto, NORM.txtLink, NORM.txtRelevante, NORM.bitNormativoHistorico, concat(substring(NORM.dateCadastro,7,4),substring(NORM.dateCadastro,4,2),substring(NORM.dateCadastro,1,2)) as data2');

        $this->db->select('TA.txtArea');

        $this->db->select('COUNT(COM.txtComentario) AS qtdComentarios');

        $this->db->select('USER.txtNome, USER.txtEmail, USER.idInstituicao');

        $this->db->select('PLA.txtStatus');

        $this->db->from('tabnormativousuario AS NORM');

        $this->db->join('tabusuario AS USER', 'NORM.idUsuario = USER.id', 'left');

        $this->db->join('tabplanoacao AS PLA', 'NORM.idNormativo = PLA.idNormativo AND USER.id = PLA.idUsuario', 'left');

        $this->db->join('tabcomentarios AS COM', 'NORM.idNormativo = COM.idNormativo AND USER.idInstituicao = COM.idInstituicao', 'left');

        $this->db->join('tabarea AS TA', 'TA.id = USER.txtAreaAtuacao', 'left');
       
        $this->db->where('USER.idInstituicao', $idInstituicao);

        $this->db->where('TA.bitStatus', '1');

        $this->db->where_in('NORM.bitCiente', array('1','2'));
        
        // $this->db->order_by('data2' , 'asc');

        $this->db->group_by('NORM.id' , 'desc');

        $this->db->order_by('data2' , 'desc');

        $this->db->limit('375');        
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_planoAcao_id($idNormativo = 0){
        $this->db->select('PLA.id, PLA.idUsuario, PLA.txtDataVencimento, PLA.txtDescricao, PLA.txtStatus');

        $this->db->select('USER.txtNome AS txtNomeUsuario');
        
        $this->db->from('tabplanoacao AS PLA');
                    
        $this->db->where('PLA.idNormativo', $idNormativo);

        $this->db->join('tabusuario AS USER', 'PLA.idUsuario = USER.id', 'left');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


}


