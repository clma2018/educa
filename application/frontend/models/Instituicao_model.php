<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instituicao_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }


    function get_instituicao($idInstituicao = 0){
        $this->db->select( 'TI.id, TI.txtNumeroFuncionarios, TI.txtNomeInstituicao, TI.txtModelo');
        
        $this->db->from('tabinstituicao AS TI');
        
        if ($idInstituicao != 0)
            $this->db->where('TI.id', $idInstituicao);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    } 
}



