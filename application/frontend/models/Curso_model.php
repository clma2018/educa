<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curso_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

     function get_licoes_simple($idSecao = 0, $idLicao = 0){
        $this->db->select('LIC.id, LIC.intTipoLicao, LIC.intOrdem, LIC.txtTitulo, LIC.txtResumo');
        
        $this->db->from('tabLicao AS LIC');

        if ($idSecao != 0)
            $this->db->where('LIC.idSecao', $idSecao);

        if ($idLicao != 0)
            $this->db->where('LIC.id', $idLicao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_licoes($idSecao = 0, $idLicao = 0){
        $this->db->select('LIC.id, LIC.intTipoLicao, LIC.intOrdem, LIC.txtTitulo, LIC.txtResumo');

        $this->db->select('SEC.txtTitulo AS txtTituloSecao, SEC.txtImagemCapa');

        // $this->db->select('TEMP.txtTempo, , TEMP.idUsuario');

        $this->db->select('VID.txtTempoVideo');

         // $this->db->select('round(( TEMP.txtTempo/VID.txtTempoVideo * 100 ),1) AS porcentagem');
        
        $this->db->from('tabLicao AS LIC');

        $this->db->join('tabSecao AS SEC', 'LIC.idSecao = SEC.id', 'left');

        // $this->db->join('tabTempoVideo AS TEMP', 'LIC.id = TEMP.idLicao', 'left');

        $this->db->join('tabVideo AS VID', 'LIC.id = VID.idLicao', 'left');
        
        if ($idSecao != 0)
            $this->db->where('LIC.idSecao', $idSecao);

        if ($idLicao != 0)
            $this->db->where('LIC.id', $idLicao);

        $this->db->order_by('LIC.idSecao' , 'asc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_secoes($idSecao = 0){

        $this->db->select('SEC.id, SEC.txtTitulo, SEC.txtResumo, SEC.txtImagemCapa');

        $this->db->from('tabSecao AS SEC');

        if ($idSecao != 0)
            $this->db->where('SEC.id', $idSecao);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_status_licoes($idSecao = 0, $idUsuario = 0){

        $this->db->select('COUNT(LIC.id) AS qtdLicoes, LIC.id AS idLicao');

        $this->db->select('round(( COUNT(STA.id)/COUNT(LIC.id)  * 100 ),0) AS porcentagem');

        $this->db->from('tabSecao AS SEC');

        $this->db->join('tabLicao AS LIC', 'SEC.id = LIC.idSecao', 'left');

        $this->db->join('tabStatusLicao AS STA', 'LIC.id = STA.idLicao', 'left');

        $this->db->where('SEC.id', $idSecao);

        $this->db->where('STA.idUsuario', $idUsuario);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_detalhe_licao($idLicao = 0, $idUsuario = 0){
        $this->db->select('LIC.id, LIC.idSecao, LIC.intTipoLicao, LIC.intOrdem, LIC.txtTitulo, LIC.txtResumo');

        $this->db->select('SEC.txtTitulo AS txtTituloSecao, SEC.txtImagemCapa');

        $this->db->select('TEMP.txtTempo AS txtTempoVideoAssistido, TEMP.idUsuario');

        $this->db->select('VID.txtUrl');

        $this->db->select('PDF.txtArquivo');

        $this->db->select('TEXT.txtNome, TEXT.txtData, TEXT.txtTexto');

        $this->db->select('STA.txtStatus');
        
        $this->db->from('tabLicao AS LIC');

        $this->db->join('tabSecao AS SEC', 'LIC.idSecao = SEC.id', 'left');

        $this->db->join('tabTempoVideo AS TEMP', 'LIC.id = TEMP.idLicao', 'left');

        $this->db->join('tabVideo AS VID', 'LIC.id = VID.idLicao', 'left');

        $this->db->join('tabPDF AS PDF', 'LIC.id = `PDF.idLicao', 'left');

        $this->db->join('tabTextos AS TEXT', 'LIC.id = TEXT.idLicao', 'left');

        $this->db->join('tabStatusLicao AS STA', 'LIC.id = STA.idLicao', 'left');
        
        if ($idLicao != 0)
            $this->db->where('LIC.id', $idLicao);

        if ($idUsuario != 0)
            $this->db->where('TEMP.idUsuario', $idUsuario);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_licao_ordem($idSecao = 0, $intOrdem = 0){

        $this->db->select('LIC.id');

        $this->db->from('tabLicao AS LIC');

        if ($idSecao != 0)
            $this->db->where('LIC.idSecao', $idSecao);

        if ($intOrdem != 0)
            $this->db->where('LIC.intOrdem', $intOrdem);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

    function get_licoes_secao($idSecao = 0, $idUsuario = 0){

        $this->db->select('LIC.id, LIC.txtTitulo, LIC.txtResumo, LIC.intOrdem, LIC.idSecao, LIC.intTipoLicao');

        $this->db->select('STA.txtStatus');

        $this->db->select('VID.txtTempoVideo');

        $this->db->from('tabLicao AS LIC');

        if ($idSecao != 0)
            $this->db->where('LIC.idSecao', $idSecao);

        if ($idUsuario != 0)
            $this->db->where('STA.idUsuario', $idUsuario);

        $this->db->join('tabStatusLicao AS STA', 'LIC.id = STA.idLicao', 'left');

         $this->db->join('tabVideo AS VID', 'LIC.id = VID.idLicao', 'left');

        $this->db->order_by('LIC.idSecao' , 'asc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_qtd_licoes($idSecao = 0, $idUsuario = 0, $txtStatus = ''){

        $this->db->select('COUNT(LIC.id) as  qtdLicoes');

        $this->db->from('tabLicao AS LIC');

        if ($idSecao != 0)
            $this->db->where('LIC.idSecao', $idSecao);

        if ($idUsuario != 0)
            $this->db->where('STA.idUsuario', $idUsuario);

        if ($txtStatus != '')
            $this->db->where('STA.txtStatus', $txtStatus);

        $this->db->join('tabStatusLicao AS STA', 'LIC.id = STA.idLicao', 'left');


        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_tempos_video($idLicao = 0, $idUsuario = 0){

        $this->db->select('TEMP.id, TEMP.idUsuario, TEMP.txtTempo');

        $this->db->select('round((TEMP.txtTempo/VID.txtTempoVideo * 100 ),1) AS porcentagem');

        $this->db->from('tabTempoVideo AS TEMP');

        $this->db->join('tabVideo AS VID', 'TEMP.idLicao = VID.idLicao', 'left');

        if ($idLicao != 0)
            $this->db->where('TEMP.idLicao', $idLicao);

        if ($idUsuario != 0)
            $this->db->where('TEMP.idUsuario', $idUsuario);

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    } 


}



