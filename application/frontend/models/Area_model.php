<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_model extends CI_Model {
	public function __construct(){
		parent::__construct();
    }

     function get_temas_area($idArea = 0, $idInstituicao = 0){
        $this->db->select( 'TAT.id, TAT.idArea, TAT.idTema, TAT.idInstituicao');
        
        $this->db->select('SUBT.txtTitulo');
        
        $this->db->from('tabareatemas AS TAT');

        $this->db->join('tabsubtema AS SUBT', 'TAT.idTema = SUBT.id', 'left');
        
        if ($idArea != 0)
            $this->db->where('TAT.idArea', $idArea);

        if ($idInstituicao != 0)
            $this->db->where('TAT.idInstituicao', $idInstituicao);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

    function get_area($idArea = 0, $idInstituicao = 0, $bitStatus = ''){
        $this->db->select( 'TA.id, TA.txtArea, TA.txtDescricaoArea, TA.idInstituicao, TA.bitStatus');

        $this->db->select('COUNT(USER.id) AS qtdUser');
        
        $this->db->from('tabarea AS TA');

        $this->db->join('tabusuario AS USER', 'TA.id = USER.txtAreaAtuacao', 'left');
        
        if ($idArea != 0)
            $this->db->where('TA.id', $idArea);
        
        if ($idInstituicao != 0)
            $this->db->where('TA.idInstituicao', $idInstituicao);

        if ($bitStatus != '')
            $this->db->where('TA.bitStatus', $bitStatus);

        $this->db->group_by('TA.id' , 'desc');
        
        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    } 

    function get_emails_area($idArea = 0,$idInstituicao = 0){
        $this->db->select( 'TA.txtEmail, TA.idArea');
        
        $this->db->from('tabareaemails AS TA');
        
        if ($idArea != 0)
            $this->db->where('TA.idArea', $idArea);

        
        if ($idInstituicao != 0)
            $this->db->where('TA.idInstituicao', $idInstituicao);
        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_base_tema($idTema = 0, $idInstituicao = 0){
        $this->db->select('TEMB.id');

        $this->db->select('BAS.txtNome, BAS.txtCorPrimaria, BAS.txtCorSecundaria');
        
        $this->db->from('tabtemasbase AS TEMB');

        $this->db->join('tabbase AS BAS', 'TEMB.idBase = BAS.id', 'left');
        
        if ($idTema != 0)
            $this->db->where('TEMB.idTema', $idTema);
        
        if ($idInstituicao != 0)
            $this->db->where('TEMB.idInstituicao', $idInstituicao);

        
       $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }    

    function get_normativos_email($txtEmail = ''){

        $this->db->select( 'NORM.id, NORM.txtTitulo');
        
        $this->db->from('tablogemailnormativo AS TA');
        
        if ($txtEmail != '')
            $this->db->like('TA.txtEmail', $txtEmail, 'both');

        $this->db->join('tabnormativoarea AS NORM', 'TA.idNormativo = NORM.id', 'left');
        
        $this->db->group_by('NORM.id' , 'desc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }


    function get_respostas_area($idArea = 0, $idInstituicao = 0){

        $this->db->select( 'RES.id, RES.txtUsuario, RES.txtNormativo, RES.txtResposta, RES.datCreate');

         $this->db->select( 'ARE.txtArea');
        
        $this->db->from('tabrespostanormativoarea AS RES');
        
        if ($idArea != 0)
            $this->db->where('ARE.id', $idArea);

            $this->db->join('tabarea AS ARE', 'RES.txtArea = ARE.id', 'left');

        if ($idInstituicao != 0)
            $this->db->where('ARE.idInstituicao', $idInstituicao);

        $this->db->group_by('RES.id' , 'desc');

        $get = $this->db->get();

        if($get->num_rows() > 0)
            return $get->result();
        
        return array();
    }

}



