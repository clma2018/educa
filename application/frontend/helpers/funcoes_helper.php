<?php
/**
 * Helper function for Diversos
 *
 * @author Jorge Ribeiro Junior
 * @version 1.0
 */
// function assets_url($uri = ''){
// 	$url = get_instance()->config->base_url($uri);
// 	return str_replace('/admin/', '/assets/', $url);
// }

// function assets_url($uri = ''){
// 	$url = get_instance()->config->base_url($uri);
// 	return str_replace('/admin/', '/assets/', $url);
// }

function dvd($data){
	echo '<pre>';
	die(var_dump($data));
}

function formataZeroEsquerda($valor, $zeroEsquerda){
	return str_pad($valor, $zeroEsquerda, "0", STR_PAD_LEFT);
}

function formataData($data, $db = false, $time = false) {
	if($db === false && $time == false) {
		return date('d/m/Y', strtotime($data));
	} elseif($db == true && $time == false) {
		return implode('-', array_reverse(explode('/', $data)));
	} elseif($time == true){
		return date('d/m/Y H:m:s', strtotime($data));
	}
}
function formataHora($time){
	return strftime("%H:%M", strtotime($time));
	// return substr($time, 0, 5);
}

function formataValorMonetario($valor, $casasDecimais = 2, $banco = FALSE){
	if($banco === FALSE)
		return number_format($valor, $casasDecimais, ',', '.');
	else
		return doubleval(str_replace(array('.', ','), array('', '.'), $valor));
}

function MesPorExtenso($mes){
	$meses = array (
		'01' => "Janeiro", 
		'02' => "Fevereiro", 
		'03' => "Março", 
		'04' => "Abril", 
		'05' => "Maio", 
		'06' => "Junho", 
		'07' => "Julho", 
		'08' => "Agosto", 
		'09' => "Setembro", 
		'10' => "Outubro", 
		'11' => "Novembro", 
		'12' => "Dezembro");

	return $meses[$mes];
}

function diaSemana($data = '', $bitRetorno = false){
	// Traz o dia da semana para qualquer data informada
	$dia = substr($data, 0, 2);
	$mes = substr($data, 3, 2);
	$ano = substr($data, 6, 4);
	
	$intDiaSemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));
	switch($intDiaSemana) {
		case"0":
			$txtDiaSemana = "Domingo";
			break;
		case"1":
			$txtDiaSemana = "Segunda";
			break;
		case"2":
			$txtDiaSemana = "Terça";
			break;
		case"3":
			$txtDiaSemana = "Quarta";
			break;
		case"4":
			$txtDiaSemana = "Quinta";
			break;
		case"5":
			$txtDiaSemana = "Sexta";
			break;
		case"6":
			$txtDiaSemana = "Sábado";
			break;
	}

	if($bitRetorno)
		return $intDiaSemana;
	else
		return $txtDiaSemana;
	;
}

function qtdParcelas($valor){
	if($valor <= 600.99):
		return 1;
	
	elseif($valor >= 601 && $valor <= 899.99):
		return 2;
	
	elseif($valor >= 900):
		return 3;
	
	endif;
}

// Função para poder cortar um texto pela metade
function str_truncate($str, $length, $rep = TRUNC_BEFORE_LENGHT) {
	//adicionada em 27/06/2006 para corrigir um bug
	if(strlen($str)<=$length)
		return $str;

	if($rep==TRUNC_BEFORE_LENGHT)
		$oc = strrpos(substr($str, 0, $length), ' ');
	if($rep==TRUNC_AFTER_LENGHT)
		$oc = strpos(substr($str, $length), ' ') + $length;

	return substr($str, 0, $oc);
}

function convert_seconds($seconds){
	$horas = floor($seconds / 3600);
	$minutos = floor(($seconds - ($horas * 3600)) / 60);
	$segundos = floor($seconds % 60);

	if($horas >= JORNADA_TRABALHO){
		$dias = floor($horas / JORNADA_TRABALHO);
		$horas = floor($horas % JORNADA_TRABALHO);
		return formataZeroEsquerda($dias, 2) . ' dia(s) ' . formataZeroEsquerda($horas, 2) . "h:" . formataZeroEsquerda($minutos, 2) . "m";
	}
	else{
		return formataZeroEsquerda($horas, 2) . "h:" . formataZeroEsquerda($minutos, 2) . "m";
	}
}
// echo convert_seconds(200000)."\n";  

// function session_create_client($objData, $redirect = true){
// 	$ci = & get_instance();


// 	// Salva os dados na sessão do usuário
// 	$ci->data[$objData[0]->txtNameSession] = array(
// 		'id'				=> $objData[0]->id,
// 		'idCriptografado'	=> $ci->encrypt->encode($objData[0]->id),
// 		'txtPrimeiroNome'	=> $objData[0]->txtPrimeiroNome,
// 		'txtSobrenome'		=> $objData[0]->txtSobrenome,
// 		'txtNomeCompleto'	=> $objData[0]->txtNome,
// 		'txtEmail' 			=> $objData[0]->txtEmail,
// 		'txtFoto'			=> $objData[0]->txtFoto,
// 		'logged' 			=> true
// 	);

// 	$ci->session->set_userdata($ci->data);

// 	if($redirect === true){
// 		if(isset($ci->session->userdata['order']))
// 			redirect('order/meu-carrinho','refresh');
// 		else
// 			redirect($objData[0]->txtUrlDestino, 'refresh');
// 	}
// 	else{
// 		return;
// 	}

// }

// function session_create_creditos($objData){
// 	$ci = & get_instance();

// 	if($objData[0]->txtCreditos != ''){
// 		$creditos = explode('||', $objData[0]->txtCreditos);
// 		for ($i=0; $i < count($creditos); $i++) { 
// 			$tipoCreditos = explode(';', $creditos[$i]);
// 			$ci->data[strtolower(str_replace(' ', '_', $tipoCreditos[1]))] = $tipoCreditos[0];
// 		}
// 	}

// 	$data['creditos'] = array(
// 		'creditos_adquiridos'	=> $ci->data['creditos_adquiridos'],
// 		'a_vencer'				=> $ci->data['a_vencer'],
// 		'creditos_vencidos'		=> $ci->data['creditos_vencidos'],
// 		'creditos_utilizados' 	=> $ci->data['creditos_utilizados'],
// 		'creditos_disponiveis'	=> $ci->data['creditos_disponiveis'],
// 	);

// 	$ci->session->set_userdata($data);
// }

function logged($txtTipoUsuario = '') {
	$CI = & get_instance();

	if($txtTipoUsuario = 'avaliado'){
		if(isset($CI->session->userdata['avaliado']['logged']))
			$logged = $CI->session->userdata['avaliado']['logged'];
		
		if (!isset($logged) || $logged != true){
			redirect('avaliacao', 'refresh');
		}
	}
}

function logout($txtTipoUsuario = ''){
	$CI = & get_instance();

	if($txtTipoUsuario = 'avaliado'){
		unset($CI->session->userdata['avaliado']);
		redirect('avaliacao','refresh');
	}
	else{
		$this->session->sess_destroy();
	}
}

/**
 * Função para poder encurtar a URL de criptografia
 * @param  string 	$value 	Dado a ser criptogragado
 * @return string        	Dado criptografado
 */
function encode($value = ''){
	$CI = & get_instance();
	return $CI->encrypt->encode($value);
}

/**
 * Função para poder encurtar a URL de decriptação
 * @param  string 	$value 	Dado a ser decriptado
 * @return string 			Dados decriptado
 */
function decode($value = ''){
	$CI = & get_instance();
	return $CI->encrypt->decode($value);
}

function array_sort($array, $on, $order=SORT_ASC){
	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} 
			else {
				$sortable_array[$k] = $v;
			}
		}
		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}
	return $new_array;
}

/**
 *  [diferente_entre_datas description]
 *  @method  diferente_entre_datas
 *  This is a cool function
 *  @author nickf
 *  @version [version]
 *  @date    2014-09-25
 *  @param   string                $dataInicial [string no formata Y-m-d H:m:s]
 *  @param   string                $dataFinal   [string no formata Y-m-d H:m:s]
 *  @return  int                             	[quantidade de dias entre as duas datas]
 */
function diferente_entre_datas($data_inicial = '', $data_final = ''){
	// Usa a função strtotime() e pega o timestamp das duas datas:
	$time_inicial = strtotime($data_inicial);
	$time_final = strtotime($data_final);

	// Calcula a diferença de segundos entre as duas datas:
	$diferenca = $time_final - $time_inicial; // 19522800 segundos

	// Calcula a diferença de dias
	$dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias

	// Exibe uma mensagem de resultado:
	return $dias;


}

/**
 * [in_array_field - Determinar se um campo de objeto/array corresponde a um determinado dado]
 * @param  string  		$needle       	[Variável a ser pesquisada dentro do Array]
 * @param  string  		$needle_field 	[Campo ou item do array que deverá sre utilizado para a pesquisa]
 * @param  objeto  		$haystack     	[Array com o conteúdo para pesquisa]
 * @param  boolean 		$strict       	[description]
 * @return boolean                		[Retorna true ou false]
 */
function in_array_field($needle, $needle_field, $haystack, $strict = false) { 
    if ($strict) { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field === $needle) 
                return true; 
    } 
    else { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field == $needle) 
                return true; 
    } 
    return false; 
}

// function imprimir_linha_mapa($intLinha, $mapeamento, $creditos, $usuarioPossuiReserva, $arrayMapaBicicletaReservada){
// 	$CI = & get_instance();
// 	//$arrayAlfabeto = array('','A','B','C','D','E','F','G','H','I','J');
// 	$arrayAlfabeto =  unserialize(LINHA_MAPA_BICICLETA);
// 	$impressaoMapa = '';
// 	$impressaoMapa .= '<div class="linha">';
// 	// $impressaoMapa .= '<span>' . $arrayAlfabeto[$intLinha] . '</span>';
// 	foreach($mapeamento as $mapa){
// 		if((int)$mapa->intNumLinha == (int)$intLinha){
// 			if($mapa->txtReservado == NULL){
// 				if($mapa->idTipoPosicao == 1 && (int)$creditos >= (int)$mapa->intQtdCreditos){
// 					$impressaoMapa .= '	<a id="bike_' . $mapa->id . '" class="' . $mapa->txtClass . ' reserva-bike" href="#"
// 									  	onclick="adicionarReserva(' . $mapa->id . ',\'' . $arrayAlfabeto[$intLinha] . $mapa->intNumColuna . '\')"> '.formataZeroEsquerda($mapa->txtPosicao, 2).'</a>';
// 				}else if($mapa->idTipoPosicao == 1){
// 					$impressaoMapa .= '	<span class="' . $mapa->txtClass . '"><a id="bike_' . $mapa->id . '" class="' . $mapa->txtClass . ' reserva-bike" href="#"
// 								  		onclick="alterarReserva(' . $mapa->id . ',\'\', \'bike\', \''.$arrayAlfabeto[$intLinha] . $mapa->intNumColuna.'\')">'.$arrayAlfabeto[$intLinha] . $mapa->intNumColuna.'</a></span>';
// 				}else if($mapa->idTipoPosicao == 2){
// 					$impressaoMapa .= '<span class="instrutor"><img class="imagem-instrutor" src="' . base_url('assets/img/site/'. $mapa->txtImagemInstrutor) . '"/><h6 class="nome-instrutor">' .$mapa->txtPrimeiroNome.'</h6></span>';
//                 }else if($mapa->idTipoPosicao == 6){
//                     $impressaoMapa .= '<span class="' . $mapa->txtClass . '"><i class="si si-wrench"></i></span>';
//                 }else if($mapa->idTipoPosicao == 7){
//                     $impressaoMapa .= '<span class="' . $mapa->txtClass . '"></span>';
// 				}else{
//                     $impressaoMapa .= '<span> </span>';
// 				}
// 			}else{
// 				$usuario = explode('||', $mapa->txtReservado);
// 				if(in_array($mapa->id, $arrayMapaBicicletaReservada)){
// 					$impressaoMapa .= '<span class="minha-reserva">'.$arrayAlfabeto[$intLinha] . $mapa->intNumColuna.'</span>';
// 				}else{
// 					$impressaoMapa .= '<span class="indisponivel font-s12"><a id="bike_'.$mapa->id.'" href="#" class="indisponivel reserva-bike font-s12" onclick="alterarReserva(' . $mapa->id . ',\'' . $usuario[1]. '\', \'user\', \''.$arrayAlfabeto[$intLinha] . $mapa->intNumColuna.'\')" >'.$usuario[1].'</a></span>';
// 				}
// 			}
// 		}
// 	}
// 	$impressaoMapa .= '</div>';

// 	return $impressaoMapa;
// }


/*
* Função para imprimir à arvore de comentários(A função recebe um array com a estrutura já organizada dos comentários(Comentário - comentário resposta) 
* e a quebra a quebra é realizada e os comentários exibidos)
*/
function estrutura_comentarios ($tree) {
    foreach ($tree as $key) {
        echo '<li data-idComentario="'.$key["id"].'">
            <div class="comment-box">
                <div class="comment-head">
                    <h6 class="comment-name">'.$key["txtNomeUsuario"].'</h6>
                    <span>'.date("d/m/Y - H:i:s", strtotime($key["dateCreate"])  - 60 * 30 * 6).'</span>
                    <i class="fa fa-reply"></i>
                </div>
                <div class="comment-content">
                  '.$key["txtComentario"].'
                </div>
            </div>';
            if($key['children'])  {
                echo '<ul class="comments-list reply-list">';
                estrutura_comentarios($key['children']);
                echo '</ul>';
            }
        '</li>';   
    }
}
