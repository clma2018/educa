$(document).ready(function () {

    //Open Content
    $('.collapse-link').on('click', function () {
        var ibox = $(this).closest('div.box');
        var button = $(this).find('i');
        var content = ibox.children('.box-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close box
    $('.close-link').on('click', function () {
        var content = $(this).closest('div.box');
        content.remove();
    });
    
});


function animationHover(element, animation) {
    element = $(element);
    element.hover(
        function () {
            element.addClass('animated ' + animation);
        },
        function () {
            //wait for animation to finish before removing classes
            window.setTimeout(function () {
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

$.fn.clickToggle = function clickToggle( f1, f2 ) {
    return this.each( function() {
        var clicked = false;
        $(this).bind('click', function() {
            if(clicked) {
                clicked = false;
                return f2.apply(this, arguments);
            }

            clicked = true;
            return f1.apply(this, arguments);
        });
    });

}

// widget focus
$('.box .btn-focus').clickToggle(
    function(e) {
        e.preventDefault();
        $(this).find('i.fa-eye').toggleClass('fa-eye-slash');
        $(this).parents('.box').find('.close-link').addClass('link-disabled');
        $(this).parents('.box').addClass('box-focus-enabled');
        $('body').addClass('focus-mode');
        $('<div id="focus-overlay"></div>').hide().appendTo('body').fadeIn(300);

    },
    function(e) {
        e.preventDefault();
        $theWidget = $(this).parents('.box');
        
        $(this).find('i.fa-eye').toggleClass('fa-eye-slash');
        $theWidget.find('.close-link').removeClass('link-disabled');
        $('body').removeClass('focus-mode');
        $('body').find('#focus-overlay').fadeOut(function(){
            $(this).remove();
            $theWidget.removeClass('box-focus-enabled');
        });
    }
);
