$(document).ready(function() {            
    //INSERCAO/REMOCAO DA CLASSE OPEN QUANDO OS CAMPOS SOFREM ALGUMA ACAO
    $('.form-material .form-control').each(function(){
        var $input  = $(this);
        var $parent = $input.parent('.form-material');

        if ($input.val()) {
            $parent.addClass('open');
        }

        $input.on('change', function(){
            if ($input.val()) {
                $parent.addClass('open');
            } else {
                $parent.removeClass('open');
            }
        });
    }); 

    //VALIDACAO DOS CAMPOS DO FORMULARIO
    $('.js-validation-login').validate({
        errorClass: 'help-block animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtEmail: {
                required: true
            },
            txtSenha: {
                required: true,
                minlength: 5
            },
        },
        messages: {
            txtEmail: 'Por favor insira um endereço de e-mail válido',
            txtSenha: {
                required: 'Por favor insira uma senha',
                minlength: 'Sua senha deve ter pelo menos 5 caracteres'
            },
        }
    });

    //VALIDACAO DOS CAMPOS DO FORMULARIO DE EDITAR INFORMACOES PESSOAIS
    $('.js-validation-user').validate({
        errorClass: 'help-block animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtNome: {
                required: true
            },
            txtEmail: {
                required: true,
                email:true
            },
            txtAreaAtuacao: {
                required: true
            },
            txtFuncao: {
                required: true
            },
        },
        messages: {
            txtNome: 'Este é um campo obrigatório',
            txtEmail: 'Por favor insira um endereço de e-mail válido',
            txtAreaAtuacao: 'Este é um campo obrigatório',
            txtFuncao: 'Este é um campo obrigatório'
        }
    });

    //VALIDACAO DOS CAMPOS DO FORMULARIO DE EDITAR INFORMACOES PESSOAIS
    $('.js-validation-user').validate({
        errorClass: 'help-block animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtNome: {
                required: true
            },
            txtEmail: {
                required: true,
                email:true
            },
        },
        messages: {
            txtNome: 'Este é um campo obrigatório',
            txtEmail: 'Por favor insira um endereço de e-mail válido'
        }
    });

    //VALIDACAO DOS CAMPOS DO FORMULARIO DE DUPLICAR USUARIO
    $('.js-validation-user-duplicate').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtNomeUsuario: {
                required: true
            },
            txtEmailUsuario: {
                required: true,
                email:true
            },
        },
        messages: {
            txtNomeUsuario: 'Este é um campo obrigatório',
            txtEmailUsuario: 'Por favor insira um endereço de e-mail válido'
        }
    });

    //VALIDACAO DOS CAMPOS DO FORMULARIO DE DUPLICAR USUARIO
    $('.js-validation-user-repeater').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            // $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
    });

    $.validator.addClassRules("txtNome", { 
        required: true
    });

    $.validator.addClassRules("txtEmail", { 
        required: true,
        email:true
    });


    $('.js-validation-prioridade').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtDataVencimento: {
                required: true
            },
            txtDescricao: {
                required: true
            },
        },
        messages: {
            txtDataVencimento: 'Este é um campo obrigatório',
            txtDescricao: 'Este é um campo obrigatório'
        }
    });


    $('.js-validation-prioridade').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtDataVencimento: {
                required: true
            },
            txtDescricao: {
                required: true
            },
        },
        messages: {
            txtDataVencimento: 'Este é um campo obrigatório',
            txtDescricao: 'Este é um campo obrigatório'
        }
    });

    $('.js-validation-encaminhar').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append(error);
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtEmails: {
                required: true
            }
        },
        messages: {
            txtEmails: 'Este é um campo obrigatório'
        }
    });

    $('.js-validation-exportar').validate({
        errorClass: 'help-block animated fadeInDown text-left',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            $(e).closest('.form-group').append();
        },
        highlight : function(e) {
            $(e).closest('.form-group').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.help-block').remove();
        },
        rules: {
            txtDataInicio: {
                required: true
            },
            txtDataFim: {
                required: true
            }
        },
    });

    
});