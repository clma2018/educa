<head>
      <meta charset="utf-8">
      <title>Home - Login</title>
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="AUTHOR">
      <meta name="robots" content="ROBOTS">
      <link rel="shortcut icon" href="assets/img/favicon.png">
      <meta http-equiv="Name" content="Legal Bot" />
      <meta http-equiv="City" content="São Paulo" />
      <meta http-equiv="State" content="SP" />
      <meta http-equiv="Country" content="Brasil" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
      
      <!-- CSS LINKS -->
      <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="assets/css/fonts.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="assets/css/styles.css" />

      <!-- JQUERY -->
      <script type="text/javascript" src="assets/js/core/jquery.min.js"></script>        
  </head>